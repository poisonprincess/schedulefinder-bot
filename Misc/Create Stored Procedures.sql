USE [schemaName];

	CREATE DEFINER=`root`@`localhost` PROCEDURE `addPoll` (IN inID VARCHAR(40), IN poll_title VARCHAR(100))
	BEGIN
		INSERT INTO polls 
		(ID, pollTitle, dateCreated, status) VALUES 
		(inID, poll_title, NOW(), 1);
	END;

	CREATE DEFINER=`root`@`localhost` PROCEDURE `getActivePolls`()
	BEGIN
		SELECT ID 
		FROM polls 
		WHERE Status = 1;
	END;

	CREATE DEFINER=`root`@`localhost` PROCEDURE `deactivatePoll`(IN inID varchar(40))
	BEGIN
		UPDATE polls 
		SET status = 0
		WHERE ID = inID;
	END;