﻿using Discord;
using System.Collections.Generic;
using System.Linq;

namespace PolyphasicScheduleFinder_Bot
{
    class Command
    {
        #region attributes
        /// <summary> String for alert name </summary>
        internal static GuildEmote _alert;
        /// <summary> Prefix of commands </summary>
        internal static string _prefix = "";
        /// <summary> List of information about commands (Name, Category, Summary, Useage) </summary>
        internal List<(string commandName, string category, string description, string properFormat)> _commands;

        /// <summary> Returns Category of a command and the number of commands in that category </summary>
        internal static List<(string categoryName, int commandCount)> getCategoriesAndCount(Command command)
        {
            List<(string categoryName, int commandCount)> categories = new();

            foreach ((string commandName, string category, string description, string properFormat) in command._commands)
            {
                if (categories.Count == 0 || !categories.Exists(c => c.categoryName == category))
                    categories.Add((category, 1));
                else
                {
                    (string categoryName, int commandCount) temp = (categories.Last().categoryName, categories.Last().commandCount + 1);
                    categories.Remove(categories.Last());
                    categories.Add(temp);
                }
            }

            return categories;
        }

        /// <summary> Returns a string representation of the the information of the passed Command
        /// or of all of the Commands in the same Category as the passed Command </summary>
        internal static string getCategoriesStringFormat(Command command, bool includeAll = false)
        {
            string categoriesString = "";
            int count = 0, maxLength = 0;

            List<(string categoryName, int commandCount)> categories = getCategoriesAndCount(command);

            foreach ((string categoryName, int commandCount) in categories)
            {
                if (categoryName.Length > maxLength) maxLength = categoryName.Length;
                if (includeAll) count += commandCount;
            }

            maxLength += 3;

            foreach ((string categoryName, int commandCount) in categories)
                categoriesString += $"{categoryName}: {new string(' ', maxLength - categoryName.Length - commandCount.ToString().Length)}{commandCount}\n";

            return (includeAll ? $"```All: {new string(' ', maxLength - "All".Length - count.ToString().Length)}{count}\n" : "```") + categoriesString + "```";
        }
        #endregion
    }
}
