﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace PolyphasicScheduleFinder_Bot
{
	internal class RoleManager
	{
		#region attributes
		/// <summary> String for alert name </summary>
		internal static GuildEmote _alert;

		/// <summary> updates any local stored config variables </summary>
		internal static void updateConfigVars() => _alert = Bot.getEmote(Config._emojiNames["Alert"]);
		#endregion

		#region RSE
		internal static void sendRoleMessageModal(SocketSlashCommand sc) => 
			sc.RespondWithModalAsync(new ModalBuilder()
			{
				Title = "Role Selection Embed Builder",
				CustomId = "RSE[Embed]",
				Components = new ModalComponentBuilder()
					.WithTextInput(new TextInputBuilder()
					{
						Label = "Title",
						Placeholder = "[Embed's title]",
						Value = "Role Selection",
						Style = TextInputStyle.Short,
						CustomId = "title",
						MinLength = 0,
						MaxLength = 256,
						Required = false
					}) //Title
					.WithTextInput(new TextInputBuilder()
					{
						Label = "Description",
						Placeholder = "[Embed's description]",
						Style = TextInputStyle.Paragraph,
						CustomId = "description",
						MinLength = 0,
						MaxLength = 2048,
						Required = false
					}) //Description
					.WithTextInput(new TextInputBuilder()
					{
						Label = "Thumbnail URL",
						Placeholder = "[Embed's Thumbnail URL]",
						Style = TextInputStyle.Short,
						CustomId = "thumbnail",
						MinLength = 0,
						MaxLength = 2048,
						Required = false
					}) //Thumbnail URL
					.WithTextInput(new TextInputBuilder()
					{
						Label = "Color",
						Placeholder = "Hex or a basic color (roygbip).",
						Style = TextInputStyle.Short,
						CustomId = "color",
						MinLength = 3,
						MaxLength = 7,
						Required = false
					}) //Color
			}.Build()).Wait();

		internal static void createRoleMessage(IModalInteraction mi)
		{
			string title = "", description = "", thumbnailURL = "";
			Color color = Color.Default;

			foreach (var option in mi.Data.Components)
			{
				string name = option.CustomId, value = option.Value;

				switch (name)
				{
					case "title":		title		 = value; break;
					case "thumbnail":	thumbnailURL = value; break;
					case "description": description  = value; break;
					case "color":		color		 = Bot.getColorByString(value); break;
				}
			}

			description = Bot.replaceTextRoleMentions_ModalSubmit(description, mi.GuildId.Value);

			Bot.getIMessageChannel(mi.ChannelId.Value).SendMessageAsync(embed: new EmbedBuilder()
			{
				Color = color,
				Title = title,
				Description = description,
				ThumbnailUrl = thumbnailURL
			}.Build()).Wait();

			AdminCommand.sendEmbed(Color.Green, "Role Selection Embed Created", interaction: mi);
		}

		internal static void modifyRoleSelectEmbed(SocketSlashCommand sc)
		{
			IUserMessage message = null;

			if (sc.Data.Options.First().Name.ToString() == "addrole")
			{
				IEmote emote = null;
				IRole role = null;
				ButtonStyle color = ButtonStyle.Secondary;
				Color embedColor = Color.Green;

				foreach (SocketSlashCommandDataOption data in sc.Data.Options.First().Options)
				{
					string name = data.Name;

					switch (name)
					{
						case "existingrse":
						{
							if ((message = (IUserMessage)Bot.getMessageFromLink(data.Value.ToString())) == null)
							{
								AdminCommand.sendEmbed(Color.Red, $"{_alert} Role Select Embed {_alert}", $"`[{data.Value}]` is an invalid message link.\n\n" +
									$"__**1.**__`Right click` on the message containing the __Role Selection Embed__ you'd like to modify.\n" +
									$"__**2.**__ select `Copy Message Link`.", interaction: sc);
								return;
							}
							else break;
						}

						case "role":
						{
							role = (IRole)data.Value;
							if (role == null)
							{
								AdminCommand.sendEmbed(Color.Red, $"{_alert} Role Select Embed {_alert}", $"`[{data.Value}]` is an invalid role.", interaction: sc);
								return;
							}

							break;
						}

						case "emoji":
						{
							string emoteName = data.Value.ToString().Trim();

							emote = emoteName.StartsWith("<:") && emoteName.EndsWith(">")
								? Bot.getEmote(emoteName[2..emoteName.IndexOf(':', 2)])
								: new Emoji(emoteName);

							if (emote == null)
							{
								AdminCommand.sendEmbed(Color.Red, $"{_alert} Role Select Embed {_alert}", $"`[{data.Value}]` is an invalid emoji.", interaction: sc);
								return;
							}

							break;
						}

						case "color":
						{
							switch (data.Value.ToString())
							{
								case "Red":
									color = ButtonStyle.Danger;
									embedColor = Bot.getColorByHash("e03c3c"); break;

								case "Green":
									color = ButtonStyle.Success;
									embedColor = Bot.getColorByHash("307c44"); break;

								case "Blue":
									color = ButtonStyle.Primary;
									embedColor = Bot.getColorByHash("6064f4"); break;

								case "Gray":
									color = ButtonStyle.Secondary;
									embedColor = Bot.getColorByHash("50545c"); break;

								default: 
									embedColor = Bot.getColorByHash("50545c"); break;
							}

							break;
						}
					}
				}

				ComponentBuilder comp = new();
				if (message.Components.Count > 0 && (message.Components.First() as ActionRowComponent).Components.Count > 0)
				{
					foreach (ActionRowComponent row in message.Components.ToList().Cast<ActionRowComponent>())
					{
						foreach (ButtonComponent c in row.Components.Cast<ButtonComponent>())
							comp.WithButton(new ButtonBuilder(c));
					}
				}

				message.ModifyAsync(s => s.Components = comp.WithButton(role.Name, $"RSE:ToggleRole:{role.Id}", color, emote).Build());

				AdminCommand.sendEmbed(embedColor, $"{Bot.getEmote(Config._emojiNames["Yes"])} Successfully Added Role!", $"Role {role.Mention} has been added.", interaction: sc);
			}
			else
			{
				_interactionsToDelete.Add(sc);

				string messageLink = sc.Data.Options.First().Options.First().Value.ToString().Trim();
				if ((message = (IUserMessage)Bot.getMessageFromLink(messageLink)) == null)
				{
					AdminCommand.sendEmbed(Color.Red, $"{_alert} Role Select Embed {_alert}", $"`[{messageLink}]` is an invalid message link.\n\n" +
						$"__**1.**__`Right click` on the message containing the __Role Selection Embed__ you'd like to modify.\n" +
						$"__**2.**__ select `Copy Message Link`.", interaction: sc);
					return;
				}

				List<SelectMenuOptionBuilder> roles = new();
				foreach (ActionRowComponent row in message.Components.ToList().Cast<ActionRowComponent>())
				{
					foreach (ButtonComponent c in row.Components.Cast<ButtonComponent>())
					{
						IRole role = null;
						if ((role = Bot.getGuild(sc.Channel.Id).Roles.ToList().Find(r => r.Id == ulong.Parse(c.CustomId.Split(':')[2]))) != null)
						{
							roles.Add(new SelectMenuOptionBuilder()
							{
								Value = role.Id.ToString(),
								Label = $"@{role.Name}",
								Emote = c.Emote ?? null
							});
						}
					}
				}

				sc.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.LightGrey,
					Title = "Select the role you'd like to remove."
				}.Build(),
				components: new ComponentBuilder().WithSelectMenu(new SelectMenuBuilder()
				{
					CustomId = $"RSE:RemoveRole:{message.Channel.Id}:{message.Id}",
					Options = roles
				}).Build(), ephemeral: true);
			}
		}

		internal static void toggleRole(SocketMessageComponent mc)
		{
			IGuildUser user = Bot.getGuildUser(mc.Channel.Id, mc.User.Id);
			IRole role = user.Guild.GetRole(ulong.Parse(mc.Data.CustomId.Split(':')[2]));

			try
			{
				if (user.RoleIds.Contains(role.Id))
				{
					user.RemoveRoleAsync(role.Id).Wait();
					AdminCommand.sendEmbed(Color.Green, description: $"Successfully removed {role.Mention}.", interaction: mc);
				}
				else
				{
					user.AddRoleAsync(role.Id).Wait();
					AdminCommand.sendEmbed(Color.Green, description: $"Successfully Added {role.Mention}.", interaction: mc);
				}
			}
			catch
			{
				AdminCommand.sendEmbed(Color.Green, $"{_alert} Role Select Error {_alert}", $"Sorry, your request could not be completed.\n" +
					$"Please try again, and if the error persists, please contact a moderator for help.", interaction: mc);
			}
		}

		internal static List<IDiscordInteraction> _interactionsToDelete = new();
		internal static void removeRoleOption(SocketMessageComponent mc)
		{
			string[] ids = mc.Data.CustomId.Split(':');
			IUserMessage message = (IUserMessage)Bot.getIMessageChannel(ulong.Parse(ids[2])).GetMessageAsync(ulong.Parse(ids[3])).Result;
			string roleName = "";

			ComponentBuilder comp = new();

			foreach (ActionRowComponent row in message.Components.ToList().Cast<ActionRowComponent>())
			{
				foreach (ButtonComponent c in row.Components.Cast<ButtonComponent>())
				{
					if (!c.CustomId.EndsWith(mc.Data.Values.First())) comp.WithButton(new ButtonBuilder(c));
					else roleName = c.Label;
				}
			}

			SelectMenuComponent selectMenu = mc.Message.Components.First().Components.First() as SelectMenuComponent;
			var interactionsToDelete = _interactionsToDelete.FindAll(s => s.User.Id == mc.User.Id);
			foreach (var sc in interactionsToDelete) sc.DeleteOriginalResponseAsync().Wait();
			_interactionsToDelete.RemoveAll(s => interactionsToDelete.Exists(v => v.Id == s.Id));

			SelectMenuBuilder sm = null;
			if (selectMenu.Options.Count > 1)
			{
				sm = new SelectMenuBuilder() { CustomId = $"RSE:RemoveRole:{message.Channel.Id}:{message.Id}" };
				foreach (SelectMenuOption option in selectMenu.Options)
				{
					if (!option.Label.Contains(roleName)) sm.AddOption(new SelectMenuOptionBuilder()
					{
						Label = option.Label,
						Value = option.Value,
						Emote = option.Emote ?? null
					});
				}

				_interactionsToDelete.Add(mc);
			}

			message.ModifyAsync(s => s.Components = comp.Build()).Wait();

			mc.RespondAsync(embed: new EmbedBuilder()
			{
				Color = Color.Green,
				Title = $"Successfully removed {roleName}",
				Description = sm != null ? "Select other roles from the select menu below to delete them. Otherwise, dismiss this message." : null
			}.Build(), components: sm != null ? new ComponentBuilder().WithSelectMenu(sm).Build() : null, ephemeral: true).Wait();
		}
		#endregion

		#region dividers
		internal static (int, int) updateExistingUsers(IGuild server)
		{
			bool consoleWrite = true;

			List<(ulong ID, int index)> dividerIndexes = getDividerIndexes(server);
			int count = 0, updated = 0;
			IReadOnlyCollection<IGuildUser> users = server.GetUsersAsync().Result;

			foreach (SocketGuildUser user in users.Cast<SocketGuildUser>())
			{
				bool updatedUser = false;
				count++;
				string toPrint = $"[{count}/{users.Count}]: {user.DisplayName}";

				for (int i = 0; i < dividerIndexes.Count; i++)
				{
					(ulong ID, int dividerIndex) = dividerIndexes[i];

					if (user.Roles.Count > 0 && !user.Roles.ToList().Exists(r => r.Id == ID))
					{
						if ((i + 1 == dividerIndexes.Count && user.Roles.ToList().Any(r => r.Position < dividerIndex && !r.IsEveryone)) ||
							user.Roles.ToList().Exists(r => r.Position < dividerIndex && i + 1 < dividerIndexes.Count && r.Position > dividerIndexes[i + 1].index))
						{
							if (!user.Roles.ToList().Exists(r => r.Id == ID))
							{
								user.AddRoleAsync(ID).Wait();
								updatedUser = true;
							}
						}
					}
				}

				if (updatedUser)
				{
					updated++;
					toPrint += " -\t-Updated.";
				}

				if (consoleWrite) Console.WriteLine(toPrint);
			}

			if (consoleWrite) Console.WriteLine("\n----Done----\n\n");
			return (count, updated);
		}

		internal static Task userUpdated(Cacheable<SocketGuildUser, ulong> userBeforeValue, SocketGuildUser userAfter)
		{
			if (userBeforeValue.HasValue && !userBeforeValue.Value.IsWebhook)
			{
				SocketGuildUser userBefore = userBeforeValue.Value;
				if ((userBefore.Guild.Id == Config._napGodServerID || (Bot._testBot && Config._testingServers.Exists(id => id == userBefore.Guild.Id))) && 
						userBefore.Roles.Count < userAfter.Roles.Count)
				{
					IGuild server = userAfter.Guild;
					IRole newRole = userAfter.Roles.ToList().Find(r => !userBefore.Roles.ToList().Contains(r));
					updateUserDividers(userAfter, server, newRole);
				}
			}

			return Task.CompletedTask;
		}

		internal static void updateUserDividers(SocketGuildUser user, IGuild server, IRole newRole)
		{
			if (newRole != null && !Config._roleDividers[server.Id].Contains(newRole.Id))
			{
				List<(ulong ID, int index)> dividerIndexes = getDividerIndexes(server);
				int newRoleIndex = server.Roles.ToList().Find(r => r.Id == newRole.Id).Position;

				for (int i = 0; i < dividerIndexes.Count; i++)
				{
					(ulong ID, int dividerIndex) = dividerIndexes[i];

					if (newRoleIndex < dividerIndex && (i + 1 == dividerIndexes.Count || newRoleIndex > dividerIndexes[i + 1].index))
					{
						if (!user.Roles.ToList().Exists(r => r.Id == ID))
							user.AddRoleAsync(ID).Wait();

						return;
					}
				}
			}
		}

		internal static void setFromModal(IModalInteraction interaction) => Bot._ngc.Set(
			interaction.Data.Components.ToList().First().Value,
			Bot._client.GetChannelAsync(interaction.ChannelId.Value).Result as IMessageChannel,
			Bot.getGuildUser(interaction.ChannelId.Value, interaction.User.Id));

		internal static List<(ulong ID, int index)> getDividerIndexes(IGuild server) => server.Roles.OrderByDescending(r => r.Position).ToList().
			FindAll(r => r.Id == Config._roleDividers[server.Id][0] || r.Id == Config._roleDividers[server.Id][1] || r.Id == Config._roleDividers[server.Id][2]).Select(r => (r.Id, r.Position)).ToList();
		#endregion
	}
}
