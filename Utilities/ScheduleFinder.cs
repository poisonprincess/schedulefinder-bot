﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolyphasicScheduleFinder_Bot
{
	class ScheduleFinder
    {
        #region schedulefinding utilities
        /// <summary> Handler for ScheduleFinding functionality </summary>
        internal Task scheduleFinding(SocketMessage message, SocketMessageComponent componentInteraction)
        {
            if (message != null && message.Content.ToLower().Trim().EndsWith("exit"))
            {
                exit(message.Author, message.Channel);
                return Task.CompletedTask;
            }
            else if (componentInteraction != null && componentInteraction.Data.CustomId == "EXIT")
            {
                exit(componentInteraction.User, componentInteraction.Channel);
                return Task.CompletedTask;
            }

            if (_scheduleFinding == ScheduleFindingStage.NOT_SCHEDULEFINDING)
            {
                _scheduleFinding = ScheduleFindingStage.AGE;
                _initialMessage = message;
                _responder = message.Author;
                _responderName = Bot.getUsersName(message.Author, _parentServer);
                _responderName ??= _responder.Username;
                _responseChannel = message.Channel;
                _embedMsg = Bot.sendEmbed(message.Channel, Color.LightGrey, fields: buildFields(), components: buildButtons(),
                    author: Bot.getGuildUser(message.Channel.Id, _responder.Id), authorName: $"{(_advancedResults ? "Advanced " : "")}Results for {_responderName}");
            }
            else if (_scheduleFinding == ScheduleFindingStage.AGE)
            {
                if (int.TryParse(message.ToString().ToLower().Trim(), out int age) && age >= _minAge && age <= _maxAge) //Valid Response
                {
                    _age = age;
                    updateEmbed(message.Channel);
                }
                else updateEmbedError(message);
            }
            else if (_scheduleFinding == ScheduleFindingStage.POLYEXPERIENCE)
            {
                if (componentInteraction != null)
                {
                    _componentInteraction = componentInteraction;
                    if (Enum.TryParse(componentInteraction.Data.CustomId, out Bot.Level selection))
                        _experience = selection;
                    updateEmbed(_embedMsg.Channel);
                }
                else updateEmbedError(message);
            }
            else if (_scheduleFinding == ScheduleFindingStage.MONOBASELINE)
            {
                string monoLength;

                if (componentInteraction != null)
                {
                    _monoBaseline = _defaultMono;
                    updateEmbed(_embedMsg.Channel);
                }
                else if (message != null)
                {
                    monoLength = message.ToString().ToLower().Trim();
					
                    if (double.TryParse(monoLength, out double m) && m >= _minMono && m <= _maxMono) //Valid Response
                    {
                        _monoBaseline = m;
                        updateEmbed(message.Channel);
                    }
                    else updateEmbedError(message);
                }
            }
            else if (_scheduleFinding == ScheduleFindingStage.ACTIVITYLEVEL)
            {
                if (componentInteraction != null)
                {
                    if (Enum.TryParse(componentInteraction.Data.CustomId, out Bot.Level selection))
                        _activityLevel = selection;
                    updateEmbed(_embedMsg.Channel);
                }
                else updateEmbedError(message);
            }
            else if (_scheduleFinding == ScheduleFindingStage.SLEEPTIMES)
            {
                bool noRestrict = false;
                string[] sleepTimes = new string[100];
                Schedule availability = null;

                if (message == null && componentInteraction == null) updateEmbedError(message);

                if (message != null)
                {
                    if (NapchartInterface.isLinkValidNapchart(message.Content.Trim().Replace("<", "").Replace(">", "")))
                    {
                        List<Schedule> schedulesFromChart = NapchartInterface.getSchedulesFromLink(message.Content.Trim().Replace("<", "").Replace(">", ""), "Availability");

                        if (schedulesFromChart.Count == 1)
                            schedulesFromChart[0].name = "Availability";

                        Schedule a = schedulesFromChart.Find(s => s.name == "Availability");

                        if (a != null) availability = a;
                        else
                        {
                            _napchartSchedules = schedulesFromChart;
                            updateEmbedError(message);
                            return Task.CompletedTask;
                        }
                    }
                    else
                    {
                        _sleepTimes = message.ToString().ToLower().Trim().Replace(" ", "");
                        sleepTimes = _sleepTimes.Split(',');
                    }
                }
                else if (componentInteraction != null)
                {
                    if (componentInteraction.Data.CustomId == "selectNapchart" && componentInteraction.Data.Values.First() != "0")
                        availability = _napchartSchedules[int.Parse(componentInteraction.Data.Values.First()) - 1];
                    else noRestrict = true;
                }

                if (availability != null)
                {
                    _sleepTimes = getSleepTimesFromSchedule(availability);
                    sleepTimes = _sleepTimes.Split(',');
                }

                if (noRestrict || (sleepTimes != null && validateSleepTimes(sleepTimes))) //Valid Response
                {
                    if (_sleepTimeAvailability == null && !noRestrict) _sleepTimeAvailability = createScheduleFromSleepTimes(sleepTimes, true, $"Availability for {_responderName}", $"Generated by ScheduleHelper.", new ColorTag("Availability", Napchart.Color.green), Napchart.Color.green);
                    if (noRestrict) _sleepTimes = "No Restrictions";
                    updateEmbed(_embedMsg.Channel);
                    startSchedulefinding(_initialMessage.Channel, _initialMessage.Author, _age, _experience, _monoBaseline, _activityLevel, sleepTimes, noRestrict);
                }
                else updateEmbedError(message);
            }

            return Task.CompletedTask;
        }

        /// <summary> Handle error counting </summary>
        /// <param name="errorCount">New error count to be updated to</param>
        internal void updateErrorCount(int errorCount)
        {
            _errorCount = errorCount;
            if (_errorCount > 3) _parentServer.clear();
        }

        /// <summary> Check if sleep times are reasonable and valid </summary>
        /// <param name="times">Array of times in the format 00:00-23:59</param>
        internal static bool validateSleepTimes(string[] times, bool twentyFourHoursValid = false, bool zeroTimeValid = false)
        {
            bool pass = true;

            foreach (string s in times)
            {
                if (s.Trim().Length > 11 || s.Trim().Length < 9 || !SFAlgorithm.checkLongSleepTime(s.Trim(), twentyFourHoursValid, zeroTimeValid)) pass = false;
            }

            if (SFAlgorithm.checkSleepOverlaps(times)) pass = false;

            return pass;
        }

        /// <summary> Begin schedulefinding process and send results </summary>
        /// <param name="message">The message being handled</param>
        /// <param name="server">The server that the message is being handled on</param>
        /// <param name="activity">User's activity level</param>
        /// <param name="age">User's age</param>
        /// <param name="experience">User's experience level</param>
        /// <param name="monoBaseline">User's mono baseline length</param>
        /// <param name="noRestrict">Whether user has restrictions on sleep times or not</param>
        /// <param name="sleepTimes">User's sleep times</param>
        internal void startSchedulefinding(IMessageChannel channel, IUser author, int age, Bot.Level experience, double monoBaseline, Bot.Level activityLevel, string[] sleepTimes, bool noRestrict, IModalInteraction interaction = null, string napchartImage = "")
        {
			if (_responder == null || _responderName == null)
			{
				_responder ??= author as SocketUser;
				_responderName ??= Bot.getUsersName(_responder, _parentServer);
			}

			List<SleepBlock> sleepBlocks = new();
			if (!noRestrict)
            {
                foreach (string s in sleepTimes) //Add sleepblocks to list
                {
                    string s2 = s.Trim();
                    sleepBlocks.Add(new SleepBlock(SFAlgorithm.getDifferenceString(s2[..s2.IndexOf("-")], s2.Remove(0, s2.IndexOf("-") + 1)), s2[..s2.IndexOf("-")], s2.Remove(0, s2.IndexOf("-") + 1)));
                }
			}

			//Run schedulefinder algorithm
			List<Schedule> schedules = new SFAlgorithm().start(age, activityLevel == Bot.Level.None ? 0 : activityLevel == Bot.Level.Medium ? 1 : 2, monoBaseline, sleepBlocks, noRestrict, experience == Bot.Level.None ? 0 : experience == Bot.Level.Medium ? 1 : 2);

            //Build Embed
			string data = "";
			if (interaction != null)
			{
				string sleeps = "";
                if (!noRestrict)
                {
                    if (string.IsNullOrWhiteSpace(napchartImage))
                    {
                        Schedule s = createScheduleFromSleepTimes(sleepTimes, false, "Availability");

						napchartImage = NapchartInterface.getChartImg(NapchartInterface.createChart(new Napchart(
					        new List<Objective> { new Objective(s, 0, Napchart.Color.green) },
					        new List<ColorTag> { new ColorTag("Availability", Napchart.Color.green) },
					        $"{s.name} for {_responderName}", $"Designed by ScheduleHelper.")));
					}

                    foreach (string sleepTime in sleepTimes)
                    {
                        sleeps += sleepTime;
                        if (sleepTimes.ToList().IndexOf(sleepTime) < sleepTimes.Length - 1)
                            sleeps += ", ";
                    }
                }
                else sleeps = "No Restrictions";

				data += $"```" +
					$"Activity:      {activityLevel}\n" +
					$"Age:           {age}\n" +
					$"Experience:    {experience}\n" +
					$"Mono Baseline: {monoBaseline}\n" +
					$"Sleep Ranges:  {sleeps}```\n";
			}

			EmbedBuilder embed = new()
			{
				Author = new EmbedAuthorBuilder
				{
					Name = $"{(_advancedResults ? "Advanced " : "")}Results for {_responderName}",
					IconUrl = _responder.GetAvatarUrl()
				},
                ImageUrl = !string.IsNullOrWhiteSpace(napchartImage) ? napchartImage : ""
			};

            //Different values based on whether there are results or not
			if (schedules.Count > 0)
			{
                foreach (Schedule s in schedules) //Create schedule info output
                {
                    if (s.tst == "00:00") SFAlgorithm.updateTST(s);
                    embed.AddField(s.name, $"{s.sleeps.Count} sleep{(s.sleeps.Count > 1 ? "s" : "  ")} \t|\t {s.tst} hours{(s.name == "Monophasic" ? "" : $" \t|\t [More Info]({s.link})")}");
                }

                embed.WithDescription($"{(!_advancedResults ? "*The following schedules are just suggestions.\nConsult an advisor for more info and scheduling help!*" : "*Select a schedule in the drop-down menu to get more information.*")}\n‎")
                    .WithColor(Color.Green);

                if (interaction != null)
                    embed.AddField(Config._blank, Config._blank).AddField("Values Entered:", data);
            }
            else
			{
				embed.WithDescription($"**Sorry! I didn't find any schedules that would work with the inputs you provided 😔**\n" +
					$"Please contact an advisor- there may be advanced or alternative scheduling options that may work with your requirements." +
					$"{(interaction != null ? $"\n\n**Values Entered:**\n{data}" : "")}")
                    .WithColor(Color.LightOrange);
            }

			if (_advancedResults && schedules.Count > 0)
			{
				if (interaction != null) interaction.DeferAsync();

				advancedResults(channel, schedules, embed, _responderName, sleepTimes, noRestrict);
			}
            else
            {
				if (interaction != null) interaction.RespondAsync(embed: embed.Build());
				else channel.SendMessageAsync(embed: embed.Build());
				_parentServer.clear();
			}			

			_parentServer._awaitingThanks = true;
            _parentServer._responder = author as SocketUser;
            _parentServer._responseChannel = channel.Id;
        }

        /// <summary> Send advanced schedulefinding results </summary>
        private void advancedResults(IMessageChannel channel, List<Schedule> schedules, EmbedBuilder embed, string responderName, string[] sleepTimes, bool noRestrict)
        {
            _scheduleFinding = ScheduleFindingStage.VIEWING_RESULTS;

            if (!noRestrict && _embedMsg == null)
            {
                _sleepTimeAvailability ??= createScheduleFromSleepTimes(sleepTimes, true, $"Availability for {_responderName}", $"Generated by ScheduleHelper.", new ColorTag("Availability", Napchart.Color.green), Napchart.Color.green);

                channel.SendMessageAsync(embed: new EmbedBuilder()
                    .WithColor(Color.Green)
                    .WithImageUrl(NapchartInterface.getChartImg(_sleepTimeAvailability.link))
                    .WithTitle(_sleepTimeAvailability.name).Build())
                    .Wait();
            }

            SelectMenuBuilder menu = new SelectMenuBuilder().WithCustomId($"menu for {responderName}");
            menu.AddOption(new SelectMenuOptionBuilder()
            {
                Label = "Select a schedule",
                Value = "0",
                Description = "Select a schedule in the drop-down menu to get more information.",
                IsDefault = true
            });

            foreach (Schedule s in schedules)
            {
                menu.AddOption(new SelectMenuOptionBuilder()
                {
                    Label = s.name,
                    Value = (schedules.IndexOf(s) + 1).ToString(),
                    Description = $"Show details for {s.name}",
                    IsDefault = false
                });

                s.link = "";
            }

            _results = schedules;
            _resultsInitialEmbed = embed.Build();
			_resultsMessage = channel.SendMessageAsync(embed: embed.Build(), components: new ComponentBuilder().WithSelectMenu(menu, 0).Build()).Result;		
        }

        /// <summary> Updates the Results embed with the user's inputs </summary>
        internal Task updateResultsEmbed(string selection)
        {
            if (selection == "0")
            {
                if (_resultsMessage.Embeds.First().Title != _resultsInitialEmbed.Title)
                {
                    _resultsMessage.ModifyAsync(x => x.Embed = _resultsInitialEmbed).Wait();
                }
            }
            else if (selection == "exit")
            {
                _resultsMessage?.ModifyAsync(x => x.Components = new ComponentBuilder().Build()).Wait();

                _parentServer.clear();
            }
            else
            {
                Schedule s = _results[int.Parse(selection) - 1];

                s.link = NapchartInterface.createChart(new Napchart(
                    new List<Objective>
                    {
                        new Objective(s, _sleepTimeAvailability == null ? 0 : 1, Napchart.Color.red),
                        _sleepTimeAvailability == null ? null : new Objective(_sleepTimeAvailability, 0, Napchart.Color.green)
                    },
                    new List<ColorTag>
                    {
                        new ColorTag("Sleep", Napchart.Color.red),
                        _sleepTimeAvailability == null ? null : new ColorTag("Availability", Napchart.Color.green)
                    },
                    $"Custom {s.name} for {_responderName}", $"Designed by ScheduleHelper."));

                EmbedBuilder e = new EmbedBuilder()
                    .WithFields(new EmbedFieldBuilder
                    {
                        Name = s.name,
                        Value = _sleepTimeAvailability == null ? $"[This]({s.link}) is the default {s.name} schedule." : $"[This schedule]({s.link}) fits the availability you entered, and is a potentially viable version of this schedule."
                    });
                e.ImageUrl = NapchartInterface.getChartImg(s.link);
                e.Color = Color.Green;

                _resultsMessage.ModifyAsync(x => x.Embed = e.Build()).Wait();
            }

            return Task.CompletedTask;
        }

        /// <summary> Creates a Schedule object from a string[] of sleeptimes </summary>
        /// <param name="generateNapchartLink"> Adds a napchart link for the schedule to the returned Schedule object </param>
        internal static Schedule createScheduleFromSleepTimes(string[] sleepTimes, bool generateNapchartLink = false, string title = "", string description = "", ColorTag colorTag = null, Napchart.Color color = Napchart.Color.red, string[] sleepBlockText = null)
        {
            List<SleepBlock> availabilitySleepBlocks = new();
            foreach (string sb in sleepTimes)
            {
                if (sb != "")
                {
                    string start = sb.Split('-')[0];
                    string end = sb.Split('-')[1];
                    
                    availabilitySleepBlocks.Add(
                        new SleepBlock(
                            SFAlgorithm.getDifferenceTime(start, end), 
                            start, 
                            end, 
                            inBlockText: sleepBlockText == null ? "" : sleepBlockText[Array.IndexOf(sleepTimes, sb)]));
                }
            }

            Schedule schedule = new(title, availabilitySleepBlocks);

            if (generateNapchartLink)
            {
                schedule.link = NapchartInterface.createChart(new Napchart(
                    new List<Objective> { new Objective(schedule, 0, color) },
                    new List<ColorTag> { colorTag },
                    title, description));
            }

            return schedule;
        }

        /// <summary> Returns a string of sleep times such as 00:00-01:00, 04:10-12:32, 15:59-16:00 </summary>
        internal static string getSleepTimesFromSchedule(Schedule availability)
        {
            string sleepTimes = "";

            foreach (SleepBlock sb in availability.sleeps)
            {
                if (sleepTimes.Length > 0) sleepTimes += ", ";

                sleepTimes += sb.startTime + "-" + sb.endTime;
            }

            return sleepTimes;
        }
        #endregion

        #region attributes
        /// <summary> ScheduleFinder object to reference the server that ScheduleFinding is on, and the NapchartInterface object </summary>
        public ScheduleFinder(Server parentServer, NapchartInterface napchartInterface) 
        { 
            _parentServer = parentServer;
            _napchartInterface = napchartInterface;
        }

        /// <summary> The Server that ScheduleFinding is occurring on </summary>
        internal Server _parentServer;

        /// <summary> Tracks the stage of ScheduleFinding that the user is on </summary>
        internal enum ScheduleFindingStage { NOT_SCHEDULEFINDING, AGE, POLYEXPERIENCE, MONOBASELINE, ACTIVITYLEVEL, SLEEPTIMES, VIEWING_RESULTS };

        /// <summary> The interface to create and get information about napcharts </summary>
        internal NapchartInterface _napchartInterface;
        /// <summary> returns potential schedules' napcharts as well </summary>
        internal bool _advancedResults;
        /// <summary> waiting response in schedulefinder </summary>
        internal ScheduleFindingStage _scheduleFinding;
        /// <summary> The person that is using the schedulefinder </summary>
        internal SocketUser _responder;
        /// <summary>  </summary>
        internal string _responderName;
        /// <summary> The channel that thanks may be occurring in </summary>
        internal ISocketMessageChannel _responseChannel;
        /// <summary> The embed to remove and replace with further instructions </summary>
        internal IUserMessage _embedMsg;
        /// <summary> The message that initiated schedulefinding </summary>
        internal SocketMessage _initialMessage;
        /// <summary>  </summary>
        internal List<Schedule> _napchartSchedules;
        /// <summary> Results of ScheduleFinding </summary>
        internal List<Schedule> _results;
        /// <summary> Times user is availabile to sleep </summary>
        internal Schedule _sleepTimeAvailability = null;
        /// <summary> The embed that contains the results of schedulefinding </summary>
        internal IUserMessage _resultsMessage;
        /// <summary> The initial results embed </summary>
        internal Embed _resultsInitialEmbed;
        /// <summary>  </summary>
        internal SocketMessageComponent _componentInteraction;
        /// <summary> User's age for schedulefinding </summary>
        internal int _age = 0;
        /// <summary> User's polyphasic sleep experience for schedulefinding </summary>
        internal Bot.Level _experience;
        /// <summary> User's monophasic sleep length (hours) for schedulefinding </summary>
        internal double _monoBaseline = -1;
        /// <summary> User's activity level for schedulefinding </summary>
        internal Bot.Level _activityLevel;

        /// <summary> User's availability to sleep for schedulefinding </summary>
        internal string _sleepTimes;
        /// <summary> Count of invalid inputs from user for schedulefinding </summary>
        internal int _errorCount = 0;

        /// <summary> Minimum age for users of ScheduleHelper </summary>
        internal static int _minAge = 13;
        /// <summary> Maximum age for users of ScheduleHelper </summary>
        internal static int _maxAge = 100;
        /// <summary> Minimum monophasic baseline length for users of ScheduleHelper </summary>
        internal static double _minMono = 1.5;
        /// <summary> Maximum monophasic baseline length for users of ScheduleHelper </summary>
        internal static int _maxMono = 14;
        /// <summary> Default monophasic baseline length  for users of ScheduleHelper </summary>
        internal static int _defaultMono = 8;

        /// <summary> Retrieves the variables for ScheduleFinding parameter validation </summary>
        internal static void updateScheduleFinderConfigVars()
        {
            if (int.TryParse(Config._scheduleFinderVars["Minimum Age"], out int minAge)) _minAge = minAge;
			if (int.TryParse(Config._scheduleFinderVars["Maximum Age"], out int maxAge)) _maxAge = maxAge;
			if (double.TryParse(Config._scheduleFinderVars["Minimum Mono"], out double minMono)) _minMono = minMono;
			if (int.TryParse(Config._scheduleFinderVars["Maximum Mono"], out int maxMono)) _maxMono = maxMono;
			if (int.TryParse(Config._scheduleFinderVars["Default Mono"], out int defaultMono)) _defaultMono = defaultMono;
        }

        /// <summary> Instructional messages to display at each step of ScheduleFinding </summary>
        internal List<(string parameter, string description)> messages = new()
		{
            ("Age", $"Please enter a number between `{_minAge}`-`{_maxAge}`."),
            ("Polyphasic Sleep Experience Level", "Please click the button that best corresponds to the amount of Polyphasic Sleeping experience you have."),
            ("Monophasic Sleep Length", $"Please enter the number of hours you need to feel rested when sleeping monophasically (one sleep per day): (`{_minMono}`-`{_maxMono}`)\n\n*Click `Unsure` if you don't know.*"),
            ("Activity Level", "Please click the button that best corresponds to the activity level you intend to maintain while on your polyphasic schedule."),
            ("Times You'd Be Able To Sleep", "Enter time ranges during which you'll be able to sleep every single day in the following format:\n`00:00-07:00, 12:30-14:50, 21:00-21:35`\n\nOr send a [Napchart Link](https://napchart.com/app) of the times you're available to sleep.\n\n*Click `No Restrictions` if you don't have any restrictions.*")
        };
        #endregion

        #region message utilities
        /// <summary> Updates the ScheduleFinding embed </summary>
        internal void updateEmbed(IMessageChannel channel)
        {
            _scheduleFinding++;
            if ((int)_scheduleFinding > 5) _scheduleFinding = ScheduleFindingStage.NOT_SCHEDULEFINDING;
            updateErrorCount(0);
            _embedMsg?.DeleteAsync();

            if (_scheduleFinding != ScheduleFindingStage.NOT_SCHEDULEFINDING || _sleepTimes == "No Restrictions")
            {
                _embedMsg = Bot.sendEmbed(
                    channel as ISocketMessageChannel,
                    _scheduleFinding != ScheduleFindingStage.NOT_SCHEDULEFINDING ? Color.LightGrey : Color.Green,
                    fields: buildFields(), components: buildButtons(), 
                    author: Bot.getGuildUser(channel.Id, _responder.Id), 
                    authorName: $"{(_advancedResults ? "Advanced " : "")}ScheduleFinding for {_responderName}");
            }
            else if (_sleepTimes != "No Restrictions")
            {
                EmbedBuilder e = new()
                {
                    ImageUrl = NapchartInterface.getChartImg(_sleepTimeAvailability.link),
                    Color = Color.Green,
                    Author = new EmbedAuthorBuilder
                    {
                        IconUrl = _responder.GetAvatarUrl(),
                        Name = $"{(_advancedResults ? "Advanced " : "")}ScheduleFinding for {_responderName}"
                    }
                };

                foreach(EmbedFieldBuilder field in buildFields()) e.AddField(field.Name, field.Value, field.IsInline);

                _embedMsg = channel.SendMessageAsync(embed: e.Build(), components: buildButtons().Build()).Result;
            }
        }

        /// <summary> Creates the user response fields for the ScheduleFinding embed </summary>
        internal List<EmbedFieldBuilder> buildFields()
        {
            List<EmbedFieldBuilder> fields = new();

            EmbedFieldBuilder blank = new()
			{
                Name = Config._blank,
                Value = Config._blank,
                IsInline = false
            };
            EmbedFieldBuilder blankInline = new()
			{
                Name = Config._blank,
                Value = Config._blank,
                IsInline = true
            };

            fields.Add(new EmbedFieldBuilder
            {
                Name = "Age:",
                Value = _age != 0 ? _age.ToString() : "-",
                IsInline = true
            });
            fields.Add(blankInline);
            fields.Add(new EmbedFieldBuilder
            {
                Name = "Experience:",
                Value = _experience != Bot.Level.unset ? _experience.ToString() : "-",
                IsInline = true
            });
            fields.Add(blank);
            fields.Add(new EmbedFieldBuilder
            {
                Name = "Mono Req:",
                Value = _monoBaseline != -1 ? _monoBaseline + "h" : "-",
                IsInline = true
            });
            fields.Add(blankInline);
            fields.Add(new EmbedFieldBuilder
            {
                Name = "Activity:",
                Value = _activityLevel != Bot.Level.unset ? _activityLevel.ToString() : "-",
                IsInline = true
            });
            fields.Add(blank);
            fields.Add(new EmbedFieldBuilder
            {
                Name = "Sleep Times:",
                Value = _sleepTimes is not "" and not null ? _sleepTimes : "-",
                IsInline = false
            });

            if (_scheduleFinding != ScheduleFindingStage.NOT_SCHEDULEFINDING)
            {
                fields.Add(blank);
                fields.Add(buildInstructionFields());
            }

            return fields;
        }

        /// <summary> Builds the instruction fields for the ScheduleFinding embed </summary>
        private EmbedFieldBuilder buildInstructionFields()
        {
            EmbedFieldBuilder fields = new()
			{
                Name = $"**{messages[(int)_scheduleFinding - 1].parameter}**:",
                Value = messages[(int)_scheduleFinding - 1].description,
                IsInline = false
            };

            return fields;
        }

        /// <summary> Builds the buttons for the ScheduleFinding embed </summary>
        private ComponentBuilder buildButtons()
        {
            ComponentBuilder buttons = new();

            if (_scheduleFinding == ScheduleFindingStage.NOT_SCHEDULEFINDING) 
                return buttons;

            bool age = _scheduleFinding == ScheduleFindingStage.AGE;
            bool hml = _scheduleFinding is ScheduleFindingStage.POLYEXPERIENCE or ScheduleFindingStage.ACTIVITYLEVEL;
            bool mono = _scheduleFinding == ScheduleFindingStage.MONOBASELINE;
            bool sleepTimes = _scheduleFinding == ScheduleFindingStage.SLEEPTIMES;
            string btn1Text = hml ? " None " : (mono ? "Unsure" : (sleepTimes ? "No Restrictions" : "EXIT"));
            
            buttons.WithButton(btn1Text, customId: hml ? "None" : (age ? "EXIT" : "High"), hml ? ButtonStyle.Danger : (mono ? ButtonStyle.Primary : (age ? ButtonStyle.Secondary : ButtonStyle.Success)), row: 0);

            if (hml)
            {
                buttons
                    .WithButton("Medium", customId: "Medium", ButtonStyle.Primary, row: 0)
                    .WithButton(" High ", customId: "High", ButtonStyle.Success, row: 0);
            }

            if (!age) buttons.WithButton("EXIT", customId: "EXIT", ButtonStyle.Secondary, row: 0);

            return buttons;
        }

        /// <summary> Updates the ScheduleFinding embed with errors in response to invalid inputs </summary>
        private void updateEmbedError(SocketMessage message)
        {
            if (_errorCount + 1 > 3)
            {
				List<EmbedField> fields = new()
				{
					new EmbedFieldBuilder
					{
						Name = "Too many invalid entries.",
						Value = "Please try again."
					}.Build()
				};

				Bot.editEmbed(
                    _embedMsg,
                    _embedMsg.Embeds.First(),
                    fields: fields,
                    description: $"{Bot.getEmote(Config._emojiNames["Alert"])} {messages[(int)_scheduleFinding - 1].parameter} Error");
                Bot.editMessage(_embedMsg, components: new ComponentBuilder());
            }
            else if(_scheduleFinding == ScheduleFindingStage.SLEEPTIMES && _napchartSchedules != null)
            {
                List<EmbedField> fields = new();
                List<EmbedField> currentFields = _embedMsg.Embeds.First().Fields.ToList();

                EmbedField instructions = currentFields.Last();
                string newValue = "Your Napchart contained more than one color and/or lane. Please select the color/lane combination of your availability from the drop-down menu below.";

                foreach (EmbedField f in currentFields)
                    if (currentFields.IndexOf(f) != currentFields.Count - 1) fields.Add(f);
                
                fields.Add(new EmbedFieldBuilder
                {
                    Name = instructions.Name,
                    Value = newValue,
                    IsInline = false

                }.Build());

                Bot.editEmbed(
                    _embedMsg,
                    _embedMsg.Embeds.First(),
                    Color.Red,
                    fields,
                    description: $"{Bot.getEmote(Config._emojiNames["Alert"])} {messages[(int)_scheduleFinding - 1].parameter} " +
                    $"Error{(_errorCount > 0 ? $" [{_errorCount}]" : "")}");

                ComponentBuilder menuComponent = new();
				SelectMenuBuilder menu = new()
				{
					CustomId = "selectNapchart"
				};
				menu.AddOption(new SelectMenuOptionBuilder()
                {
                    IsDefault = false,
                    Label = "Select one of the following options.",
                    Value = "0"
                });

                foreach (Schedule s in _napchartSchedules)
                {
                    menu.AddOption(new SelectMenuOptionBuilder()
                    {
                        IsDefault = false,
                        Label = s.name,
                        Value = (_napchartSchedules.IndexOf(s) + 1).ToString()
                    });
                }

                Bot.editMessage(_embedMsg, components: menuComponent.WithSelectMenu(menu, 0));
            }
            else
            {
                List<EmbedField> fields = new();
                List<EmbedField> currentFields = _embedMsg.Embeds.First().Fields.ToList();

                bool click = currentFields.Last().Value.Contains("click the button");
                double num = -99.99;

                if (click || (double.TryParse(message.ToString().Replace("−", "-"), out num) && (_scheduleFinding == ScheduleFindingStage.AGE || _scheduleFinding == ScheduleFindingStage.MONOBASELINE)))
                {
                    EmbedField instructions = currentFields.Last();
                    string newValue = instructions.Value;

                    if (click)
                    {
                        newValue = instructions.Value.Replace("click the button",
                        (_errorCount > 1 ? "__**CLICK THE BUTTON**__" : (_errorCount > 0 ? "**CLICK THE BUTTON**" : "**click the button**")));
                    }
                    else if (num != -99.99)
                    {
                        if (num is < 0 or >= int.MaxValue)
                            newValue += $"\n\n||*Yes, yes, let's all try to break the bot {Bot.getEmote(Config._emojiNames["Blob Unamused"])}*||";
                        
                        else if (_scheduleFinding == ScheduleFindingStage.AGE)
                        {
                            if (num == 0)
                                newValue += "\n\n||*Wow, you're a talented baby if you're using discord all by yourself! How many months old are you?*||";
                            
                            else if (num < _minAge)
                                newValue += "\n\n||*You're a deviant, defying Discord's TOS' minimum age requirements!*||";
                            
                            else if (num == 420)
                                newValue += "\n\n||*lmao nice*||";
                            
                            else if (num > 200)
                                newValue += "\n\n||*Congratulations on achieving immortality! Please share your wisdom with us younglings.*||";
                            
                            else if (num > _maxAge)
                                newValue += "\n\n||*Hey old-timer! What are you doing on discord trying to find sleep schedules instead of making the most of the time you have left?*||";
                        }
                        else if (_scheduleFinding == ScheduleFindingStage.MONOBASELINE)
                        {
                            if (num == 0)
                            {
                                if(Bot._rng.Next(2) == 1)
                                    newValue += "\n\n||*You're trying to tell me you never sleep? " +
                                        "If true, you're a medical marvel; please teach us your ways! If not, please enter a realistic value.*||";
                                
                                else newValue += "\n\n||*If you never sleep, why are you trying to get sleep schedule recommendations from me? :thinking:*||";                                
                            }
                            else if (num < _minMono)
                                newValue += $"\n\n||*You only sleep {num} hour{(num == 1 ? "" : "s")} a day? {Bot.getEmote(Config._emojiNames["Blob Insomnia"])}*||";
                            
                            else if (num is 69 or 420)
                                newValue += "\n\n||*lmao nice*||";
                            
                            else if (num > 24)
                                newValue += $"\n\n||*Oh, wow, you sleep {num} hours every single day? You're so funny {Bot.getEmote(Config._emojiNames["Blob Eye Roll"])}*||";
                            
                            else if (num > 23)
                                newValue += $"\n\n||*If you're sleeping {num} hours every day you don't need a sleep schedule, you need a doctor {Bot.getEmote(Config._emojiNames["Concerned"])}*||";
                            
                            else if (num > 18)
                                newValue += $"\n\n||*You sleep {num} hours a day?? That's far beyond my advising capabilities :sweat_smile:*||";
                            
                            else if (num > _maxMono)
                                newValue += $"\n\n||*You sleep {num} hours a day?? I can't even begin to imagine the neck pain :weary:*||";
                        }
                    }

                    foreach (EmbedField f in currentFields)
                        if (currentFields.IndexOf(f) != currentFields.Count - 1) fields.Add(f);

                    fields.Add(new EmbedFieldBuilder
                    {
                        Name = instructions.Name,
                        Value = newValue,
                        IsInline = false

                    }.Build());
                }
                else fields = currentFields;

                Bot.editEmbed(
                    _embedMsg,
                    _embedMsg.Embeds.First(),
                    Color.Red,
                    fields,
                    description: $"{Bot.getEmote(Config._emojiNames["Alert"])} {messages[(int)_scheduleFinding - 1].parameter} " +
                    $"Error{(_errorCount > 0 ? $" [{_errorCount}]" : "")}");
            }

            updateErrorCount(_errorCount + 1);
        }

        /// <summary> Handles the exit button selection </summary>
        internal void exit(SocketUser user, ISocketMessageChannel channel)
        {
            if (_responder.Id == user.Id && _responseChannel.Id == channel.Id)
            {
                IEmbed e = _embedMsg.Embeds.First();
                List<EmbedField> fields = e.Fields.ToList();
                fields.RemoveAt(0);
                fields.RemoveAt(0);
                ButtonComponent btn = null;
                ComponentBuilder component = null;

                if (_embedMsg.Components.Count > 0 && (ActionRowComponent)_embedMsg.Components.First() != null && 
                    (btn = ((ActionRowComponent)_embedMsg.Components.First()).Components.OfType<ButtonComponent>().ToList().Find(b => b.CustomId == "EXIT")) != null)
                    component = new ComponentBuilder().WithButton(btn.Label, btn.CustomId, btn.Style, disabled: true);
                        
                Bot.editEmbed(_embedMsg, _embedMsg.Embeds.First(), fields: fields, description: "", authorName: e.Author.Value.Name + " [Exited]", components: component);

                _parentServer.clear();
            }
        }
        #endregion
    }
}