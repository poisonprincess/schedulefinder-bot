﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace PolyphasicScheduleFinder_Bot
{
	class NapchartInterface
    {
		#region attributes
		/// <summary> HTTP Client to connect to the Napchart API </summary>
		private static readonly HttpClient _client = new();
		/// <summary> The link to access the Napchart API </summary>
		internal static string _napchartEndpoint = "https://api.napchart.com/v1/";
        /// <summary> Link postfix for Napchart snapshot creation </summary>
        internal static string _createSnapshot = "createSnapshot";
        /// <summary> Link postfix for Napchart image creation </summary>
        internal static string _getImage = "getImage";
        /// <summary> Link postfix for Napchart information retrieval </summary>
        internal static string _getChart = "getChart";
        /// <summary> Regex to verify validity of potential Napchart links </summary>
        internal static List<Regex> _napchartLinkRegex = new()
		{ 
            new(@"^((\w|\d){5,6})$"),
            new(@"^snapshot\/((\w|\d){9})$"),
            new(@"^\w{6,100}\/.*\-\w{6,9}$")
        };

        /// <summary> Updates the Napchart config parameters </summary>
        internal static void updateNapchartConfigVars()
        {
            _napchartEndpoint = Config._napchartInterfaceVars["Napchart API Endpoint"];
            _createSnapshot = Config._napchartInterfaceVars["Create Snapshot"];
            _getImage = Config._napchartInterfaceVars["Get Image"];
            _getChart = Config._napchartInterfaceVars["Get Chart"];
            List<string> regex = Config._napchartInterfaceVars["Napchart Regex"].Split('&').ToList();
            foreach(string r in regex) _napchartLinkRegex.Add(new(r));            
        }
        #endregion

        #region utilities
        /// <summary> Creates a napchart using a Napchart object </summary>
        internal static string createChart(Napchart napchart)
        {
            bool consoleWrite = true; 

			string d = JsonConvert.SerializeObject(new
            {
                chartData = "{",
                napchart.chartData.elements,
                napchart.chartData.colorTags,
                napchart.chartData.lanes,
                napchart.chartData.shape,
                lanesConfig = new object(),
                napchart.title,
                napchart.description
            });

			int start = d.IndexOf("elements\":[{"),
                end = d.IndexOf("}],\"color") + 1;

			string processed = "";
            if (napchart.chartData.elements.Length > 0)
            {
                foreach (Element e in napchart.chartData.elements)
                {
                    if (processed.Length > 0) processed += ",";

                    if (e != null) e.text ??= "";

                    if ((int)e.color > 7)
                    {
                        processed += JsonConvert.SerializeObject(new
                        {
                            e.end,
                            e.lane,
                            e.text,
                            color = $"custom_{napchart.customColors.IndexOf(napchart.customColors.Find(c => c.colorName == Napchart.getCustomColorName(e)))}",
                            e.start
                        });
                    }
                    else
                    {
                        processed += JsonConvert.SerializeObject(new
                        {
                            e.end,
                            e.lane,
                            e.text,
                            color = e.color.ToString(),
                            e.start
                        });
                    }
                }

                if (start == -1 || end == 0)
                {
                    start = d.IndexOf("elements\":[");
                    end = d.IndexOf("],\"color") + 1;
                }

                d = d.Replace(d[start..end], "elements\":[" + processed)
                    .Replace("\"{\",", "{")
                    .Replace("null", "")
                    .Replace("\"lanesConfig\":{}", "\"lanesConfig\":{}}");
            }
            else d = d.Replace("\"{\",\"elements\"", "{\"elements\"").Replace("},\"title", "}},\"title");

            if (napchart.chartData.colorTags?.Length > 0)
            {
                processed = "";
                foreach (ColorTag ct in napchart.chartData.colorTags)
                {
                    if (processed.Length > 1) processed += ",";

                    if ((int)ct.color > 7)
                    {
                        string customName = Napchart.getCustomColorName(ct);
                        bool custom = customName.Contains("custom");
                        processed += JsonConvert.SerializeObject(new
                        {
                            ct.tag,
                            color = $"custom_{napchart.customColors.IndexOf(napchart.customColors.Find(c => c.colorName == customName))}",
                            colorValue = custom ? 
                            napchart.customColors.Find(c => c.colorName == customName).hex : 
                            Napchart.extraColors.Find(c => c.colorName == customName).hex
                        });
                    }
                    else
                    {
                        processed += JsonConvert.SerializeObject(new
                        {
                            ct.tag,
                            color = ct.color.ToString()
                        });
                    }
                }

                processed += "";

                start = d.IndexOf("colorTags\":[");
                end = d.IndexOf("],\"lanes");
                d = d.Replace(d[start..end], "colorTags\":[" + processed);
            }

			HttpResponseMessage response = _client.PostAsync(_napchartEndpoint + _createSnapshot,
                new StringContent(d, Encoding.UTF8, "application/json")).Result;

            try
            {
                response.EnsureSuccessStatusCode();
                string result = response.Content.ReadAsStringAsync().Result;
                napchart.publicLink = result[(result.IndexOf("publicLink") + "publicLink\":\"".Length)..];
                napchart.publicLink = napchart.publicLink[..napchart.publicLink.IndexOf("\"")];
                napchart.chartID = result[(result.IndexOf("chartid") + "chartid\":\"".Length)..];
                napchart.chartID = napchart.chartID[..napchart.chartID.IndexOf("\"")];
                return napchart.publicLink;
            }
            catch (Exception e) 
            { 
                MM.Print(consoleWrite, $"{e}\nDATA:\n{{\n\t{d}\n}}\n");
                string error = e.ToString()[(e.ToString().IndexOf(":") + 2)..];
                return error.Remove(error.IndexOf("\n"));
            }
        }

        /// <summary> Gets the ID of a napchart from a Napchart link </summary>
        internal static string getChartID(string napchartLink)
        {
            try
            {
				string napchartId = napchartLink.Contains(".com/") ? napchartLink[(napchartLink.IndexOf(".com/") + 5)..] : napchartLink;
				if (napchartId.Contains('/') && napchartId.Contains('-')) napchartId = napchartId.Split('-').Last();
				else if (napchartId.Contains('/')) napchartId = napchartId.Split('/').Last();
				return napchartId;
			}
            catch { }

            return "";
        }

        /// <summary> Gets the image from a Napchart </summary>
        internal static string getChartImg(string napchartLink, bool highRes = true) => $"{_napchartEndpoint}{_getImage}/{getChartID(napchartLink)}?hr={(highRes ? 1 : 0)}";

        /// <summary> Gets the information from an existing Napchart </summary>
        internal static string getChart(string napchartLink)
        {
            bool consoleWrite = true;
			HttpClient client = _client;
            client.DefaultRequestHeaders.Add("User-Agent", "request");

            try { return client.GetStringAsync($"{_napchartEndpoint}{_getChart}/{getChartID(napchartLink)}").Result; }
            catch (Exception e)
            {
                MM.Print(consoleWrite, e.ToString(), useMethod: true);
                return "";
            }
        }

        internal static bool doesNapchartDataContainProfanity(string napchartLink)
        {
            string data = getChart(napchartLink);

            foreach (string searchTerm in Config._SalertKeywords)
			{ if (data.Contains(searchTerm)) return true; }

            return false;
        }

        /// <summary> Retrieves a list of schedules (each lane = one schedule) from an existing Napchart link </summary>
        internal static List<Schedule> getSchedulesFromLink(string napchartLink, string searchKey = null)
		{
			if (napchartLink.StartsWith("napchart.com/")) napchartLink = "https://" + napchartLink;

			string data = getChart(napchartLink);

			if (data.Length == 0) return null;
            
            //if there are no elements, there are no schedules
            if (data.Contains("elements\":[]")) return new List<Schedule>();

            //retrieve all element data
            int eStart = data.IndexOf("elements\":[{") + 11;
            int eEnd = data.IndexOf("}],\"color") + 1;
            string[] elements = data[eStart..eEnd].Replace("\"", "").Replace("},{", "};{").Replace("{", "").Replace("}", "").Split(';');

            //retrieve all colorTag data
            int cStart = data.IndexOf("colorTags\":[{") + 12;
            int cEnd = data.IndexOf("}],\"lanes") + 1;
            List<string> colorInfo = new();
                
            //skips if there's no colorTag data
            if(!data.Contains("colorTags\":[]"))
            {
                colorInfo = data[cStart..cEnd]
                .Replace("\"", "")
                .Replace("},{", "};{")
                .Replace("{", "")
                .Replace("}", "")
                .Split(';').ToList();
            }

            List<(string color, string tag, string hex)> colorTags = new();
            
            //fill the list with the color/tag combos
            foreach(string info in colorInfo)
            {
                List<string> parameters = info.Split(',').ToList();
                string hex = "", tag = parameters.Find(x => x.StartsWith("tag")).Replace("tag:", ""),
			        color = parameters.Find(x => x.StartsWith("color")).Replace("color:", "");

				if (color.StartsWith("custom_"))
				{
					color = color[0..color.IndexOf('_')];
					hex = parameters.Find(x => x.StartsWith("colorValue")).Replace("colorValue:", "");
				}

				colorTags.Add((color, tag, hex));
            }

            //list to return
            List<Schedule> schedules = new();
            
            //fill list with a schedule for every group of elements that share a lane and a color
            foreach (string e in elements)
            {
                List<string> parameters = e.Split(',').ToList();
                string c = parameters.Find(x => x.StartsWith("color")).Replace("color:", "");
				if (c.StartsWith("custom_")) c = c[0..c.IndexOf('_')];
				bool laneFound = int.TryParse(parameters.Find(x => x.StartsWith("lane")).Replace("lane:", ""), out int l);
               
                if (laneFound && c != "")
                {
                    (string color, string tag, string hex) = colorTags.Find(ct => ct.color == c);

                    string scheduleName = $"{c}{(!string.IsNullOrEmpty(hex) ? $"[{hex}]" : "")} ({tag}) on lane {l + 1}";
					Schedule sched = schedules.Find(s => s.name == scheduleName);

                    if (sched == null)
                    {
                        sched = new Schedule(scheduleName, new List<SleepBlock>());
                        schedules.Add(sched);
                    }

                    SleepBlock sb = null;
                    try
                    {
                        sb = new SleepBlock("",
                           SFAlgorithm.convertDoubleToTime(double.Parse(parameters.Find(x => x.StartsWith("start")).Replace("start:", "")) / 60),
                           SFAlgorithm.convertDoubleToTime(double.Parse(parameters.Find(x => x.StartsWith("end")).Replace("end:", "")) / 60));
                    }
                    catch { }

                    if (sb != null)
                    {
                        sched.sleeps.Add(sb);
                        SFAlgorithm.updateTST(sched);
                    }
                }
            }

            if (colorTags.Exists(ct => ct.tag == searchKey))
            {
                //find any lanes that have the search key
                List<Schedule> searchKeyLanes = schedules.FindAll(s => s.name.Contains(colorTags.Find(t => t.tag == searchKey).color));

                //if there's only one schedule with the color of the search key, set that schedule's name to the search key
                if (searchKeyLanes.Count == 1) searchKeyLanes[0].name = searchKey;
            }
            
            return schedules;
        }

        internal static Napchart getNapchartFromSchedules(List<Schedule> schedules)
        {
            List<Objective> objectives = new();
            List<ColorTag> colorTags = new();

            foreach (Schedule schedule in schedules)
            {
                string[] data = schedule.name[..schedule.name.IndexOf(" on")].Replace(" ", "").Replace(")", "").Split('('),
                         colorData = data[0].Trim().Replace("]", "").Split("[");
                string tag = data.Length > 1 ? data[1].Trim() : "",
                       colorName = colorData[0].Trim(),
                       hex = colorName.ToLower() == "custom" ? colorData[1].Trim() : "",
                       laneNum = schedule.name[(schedule.name.IndexOf("lane ") + 5)..];

                if (int.TryParse(laneNum.Trim(), out int lane) && Enum.TryParse(colorName, true, out Napchart.Color color))
                {
					objectives.Add(new (schedule, lane - 1, color, hex));
                    if (!string.IsNullOrEmpty(tag))
                        colorTags.Add(new (tag, color, hex));
				}
            }

            return new (objectives, colorTags, "", "");
        }

        internal static Napchart getNapchartFromLink(string napchartLink) => getNapchartFromSchedules(getSchedulesFromLink(napchartLink));

        /// <summary> Validates Napchart links using the Napchart Regexes </summary>
        internal static bool isLinkValidNapchart(string napchartLink) 
        {
            if (!napchartLink.Contains("napchart.com/")) return false;

            string napchartString = napchartLink[(napchartLink.IndexOf(".com/") + 5)..];

            foreach (Regex r in _napchartLinkRegex)
            { if (r.IsMatch(napchartString)) return true; }

            return false;
        }

		internal static Napchart createNapchartFromSleepArray(string[] sleepTimes, Napchart.Color color, string hex = "", string label = "", string name = "", string description = "") => new(
            new() { new(Schedule.scheduleFromSleepArray(sleepTimes), 0, color, hex) }, 
            string.IsNullOrWhiteSpace(label) ? new() : new() { new(label, color, hex) }, 
            name, description);

        internal static bool isNapchartOnline()
        {
			HttpWebRequest request = (HttpWebRequest) WebRequest.Create("https://napchart.com/app");
			request.Timeout = 15000;
			request.Method = "HEAD";

			try
			{
				using HttpWebResponse response = (HttpWebResponse)request.GetResponse();
				return response.StatusCode == HttpStatusCode.OK;
			}
			catch (WebException)
			{
				return false;
			}
		}
		#endregion
	}
}
