﻿using Discord;
using Discord.Rest;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static PolyphasicScheduleFinder_Bot.SFAlgorithm;

[assembly: AssemblyVersion("7.4.1")]
namespace PolyphasicScheduleFinder_Bot
{
    class Bot
    {
		internal const bool _testBot = true;

		#region attributes
		//Bot
		/// <summary> Program version </summary>
		private readonly string _version = Assembly.GetEntryAssembly().GetName().Version.ToString();
        internal static DiscordSocketClient _client;
        internal static ISocketMessageChannel _restarted = null;
        internal static SocketSlashCommand _restartedSlash = null;
		internal const bool _useSQL = true;

		//Setup
		internal const bool _printGoogleConnectionInfo = true;
		internal const bool _printConfigSetupInfo = true;
		internal const bool _printScheduleInfo = false;
		internal const bool _printCommandInfo = true;
		internal const bool _printServerInfo = false;
		internal const bool _printPollInfo = false;
		internal const bool _printSQLInfo = true;

        //Sheets
        internal readonly static SpreadsheetManager _sm = new();
        private static string _scheduleDBTabName = "ScheduleDB";
        private static string _scheduleDBArea = "$A$3:$AV";

        //Commands
        internal static AdminCommand _ac = new();
        internal static UserCommand _uc = new();
        internal static NapGodCommand _ngc = new();
        internal static bool _commandsConfigured = false;
        internal static bool _ngCommandsConfigured = false;
        internal static bool _deleteOldCommands = false;

        //NapGod
        internal static IGuild _NapGodServer;
        internal static IGuildUser _NapGodBot;
        internal static bool _isNapGodOnline;

        //Logging
        internal static Logger _logger;
        internal static MySQLManager _msm;
        internal static MySQLManager _msmLogger;
        internal static bool _DMLogging = true;

        //Misc
        internal enum Level { unset, None, Medium, High };
        internal static Random _rng = new();
        /// <summary> List of servers that SH has interacted with </summary>
        internal static List<Server> _servers = new();
        /// <summary> Tailed channels and where they're being tailed from </summary>
        internal static List<(ulong channelsTailed, ulong channelViewingFrom)> _tailing = new();
        /// <summary> Simple tailed channels and where they're being tailed from </summary>
        internal static List<(ulong channelsTailed, ulong channelViewingFrom)> _tailingS = new();
        /// <summary> List of users the bot is mimicking </summary>
        internal static List<ulong> _mimic = new();
        /// <summary> List of users that have upset the bot </summary>
        internal static List<ulong> _jerks = new();

        internal const bool _retrievePolls = true;
		internal static List<IMessage> _polls = null;
		#endregion

		#region setup
		internal static void Main() => new Bot().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            bool restarted = (_restarted != null || _restartedSlash != null);

            startBot();

            //Start SQL Connection
            bool sqlConnected = _useSQL && startSQL(_printSQLInfo);
            if (!sqlConnected) Config._LoggingWithGSheets = true;

            //Start GSheets Connection
            bool googleConnected = SpreadsheetManager.connectToGoogleAPI(_printGoogleConnectionInfo);
            Config.setupConfig(_printConfigSetupInfo, !googleConnected);

            string divider = new('-', 45);
            MM.Print(_printScheduleInfo, $"\n{divider}  Schedule DB  {divider}\n  Schedule{new string(' ', Schedule.maxScheduleNameLength - 5)}|   TST   |  Poly.net Link");

            //Populate ScheduleDB
            bool usedGoogle = false;
            if (googleConnected) usedGoogle = populateScheduleDB(_printScheduleInfo);
            else SFAlgorithm.populateScheduleDB(_printScheduleInfo);

            //Print
            printServerInfo(divider, _printServerInfo);

            //Determine if Napgod is online
            if ((_NapGodServer = _client.GetGuild(Config._napGodServerID)) != null)
            {
                await _NapGodServer.DownloadUsersAsync();
                if ((_NapGodBot = _NapGodServer.GetUserAsync(Config._napGodBotID).Result) != null)
                {
                    _isNapGodOnline = _NapGodBot.Status != UserStatus.Offline;
                }
            }

            //Build Commands
            if (!_commandsConfigured || (!_ngCommandsConfigured && (!_isNapGodOnline || _testBot))) await buildCommands(_deleteOldCommands);

            if (sqlConnected && _retrievePolls)
			{
                if (restarted) _polls = new();

				MM.Print(_printPollInfo, $"{divider} Polls {divider}");
				foreach (var server in _client.Guilds)
				{
					UserCommand.retrievePolls($"`{server.Id}`");
                    int count = _polls.FindAll(m => getGuildChannel(m.Channel.Id).Guild.Id == server.Id).Count;
                    if (count > 0) MM.Print(_printPollInfo, $"-Retrieved Polls for [{server.Name}] ({count})");
				}

                MM.Print(_printPollInfo, $"\n{divider}-------{divider}\n\n");
			}

			#region startup printout
			string header = $"{MM.getCurrentTimeStamp()}  {_client.CurrentUser.Username} [v{_version}-{(_testBot ? "TEST" : "OFFICIAL")}]  READY  ------";
            divider = new string('-', header.Length);
            string botStatus =     $"|       Bot Status:        [{(_client.ConnectionState == ConnectionState.Connected ? "Online" : "Offline")}]";
            string googleStatus =  $"|       Sheets API Status: [{(googleConnected ? "Online" : "Offline")}]";
            string sqlStatus =     $"|       MySQL DB:          [{(sqlConnected ? "Online" : "Offline")}]";
            string dbStatus =      $"|       ScheduleDB:        [{(_scheduleDB.Count > 0 ? "Updated" : "Empty")}]";
            string dbPopulatedBy = $"|       SDB Populated By:  [{(usedGoogle ? "Google SDB" : "Schedules Doc")}]";
            string napGodStatus =  $"|       NapGod Bot Status: [{(_NapGodBot != null ? _NapGodBot.Status.ToString() : "NULL")}]";
            string edges = new(' ', header.Length - 2);

            MM.Print(true, $"{header}\n" +
                $"|{edges}|\n" +
                $"{botStatus}{new string(' ', header.Length - botStatus.Length - 1)}|\n" +
                $"{googleStatus}{new string(' ', header.Length - googleStatus.Length - 1)}|\n" +
                $"{sqlStatus}{new string(' ', header.Length - sqlStatus.Length - 1)}|\n" +
                $"{dbStatus}{new string(' ', header.Length - dbStatus.Length - 1)}|\n" +
                (_scheduleDB.Count > 0 ? $"{dbPopulatedBy}{new string(' ', header.Length - dbStatus.Length - 4)}|\n" : "") +
                $"{napGodStatus}{new string(' ', header.Length - napGodStatus.Length - 1)}|\n" +
                $"|{edges}|\n" +
                $"{divider}\n");
            #endregion

            if (restarted)
            {
                _servers = new List<Server>();
                _tailing = new List<(ulong channelsTailed, ulong channelViewingFrom)>();
                _tailingS = new List<(ulong channelsTailed, ulong channelViewingFrom)>();
                _mimic = new List<ulong>();
                _jerks = new List<ulong>();

                if (_restarted != null) sendEmbed(_restarted, Color.Green, title: "Back Online!");
                else _restartedSlash.ModifyOriginalResponseAsync(m => m.Embed = new EmbedBuilder() { Color = Color.Green, Title = "Restarted." }.Build()).Wait();

                _restarted = null;
                _restartedSlash = null;
            }

            await Task.Delay(-1);
        }

        private async void startBot()
        {
            DiscordSocketConfig config = new() 
            { 
                GatewayIntents = GatewayIntents.All
                    //GatewayIntents.DirectMessageReactions 
                    //| GatewayIntents.DirectMessages
                    //| GatewayIntents.DirectMessageTyping
                    //| GatewayIntents.GuildBans              
                    //| GatewayIntents.GuildEmojis            
                    //| GatewayIntents.GuildIntegrations      
                    //| GatewayIntents.GuildMembers           
                    //| GatewayIntents.GuildMessageReactions
                    //| GatewayIntents.GuildMessages
                    //| GatewayIntents.GuildMessageTyping
                    //| GatewayIntents.GuildScheduledEvents
                    //| GatewayIntents.GuildVoiceStates
                    //| GatewayIntents.GuildWebhooks       
                    //| GatewayIntents.MessageContent
            };

            _client = new DiscordSocketClient(config);
			_client.GuildMemberUpdated += RoleManager.userUpdated;
			_client.InteractionCreated += interactionCreated;
			_client.MessageUpdated += messageUpdateLog;
			_client.MessageDeleted += messageDeleteLog;
			_client.MessageReceived += messageHandler;
            _client.JoinedGuild += joinedServer;
			_client.UserBanned += userBanned;
            _client.UserJoined += userJoined;
			_client.LeftGuild += leftServer;
			_client.Log += Log;

            string token = File.ReadAllText(_testBot ? "TestToken.txt" : "Token.txt");

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();
            await _client.SetGameAsync($"{new string('-', _testBot ? 2 : 1)}help, +help, or !help", type: ActivityType.Listening);
        }

		internal static bool startSQL(bool consoleWrite)
        {
            MM.Print(consoleWrite, $"{MM.getCurrentTimeStamp()} MYSQL DB    Connecting");

            try
            {
                (_msm = new MySQLManager(false)).openIfPossible();
                (_msmLogger = new MySQLManager(true)).openIfPossible();
            }
            catch (Exception e)
            { 
                MM.Print(true, $"\nstartSQL:\n{e}\n"); 
            }

            bool connected =
                _msm._connection.State == System.Data.ConnectionState.Open &&
                _msm._connection.State == _msmLogger._connection.State;

            _msm.closeIfPossible();
            _msmLogger.closeIfPossible();

            MM.Print(consoleWrite, $"{MM.getCurrentTimeStamp()} MYSQL DB    {(connected ? "Connected" : "CONNECTION FAILED!")}");

            if (connected) _logger = new Logger();

            return connected;
        }

		private static void printServerInfo(string divider, bool consoleWrite)
        {
			MM.Print(consoleWrite, $"{divider}---------------{divider}\n\n{divider} Server Info {divider}");

            while (_client.Guilds.Count == 0) { }

            foreach (SocketGuild s in _client.Guilds)
            {
                while (s.Name == "" || s.MemberCount == 0) { }

                MM.Print(consoleWrite, $"\n[{s.Id}] : [{s.Name}] : [{s.MemberCount} | {s.Users.ToList().FindAll(u => !u.IsBot).Count} | {s.Users.ToList().FindAll(u => u.IsBot).Count}]");

                foreach (SocketGuildChannel c in s.Channels)
                { MM.Print(consoleWrite, $"\t[{c.Id}] : [{c.Name}]"); }
            }

            int count = _client.Guilds.Count;
            MM.Print(consoleWrite, $"\n{divider} Servers:  {(count >= 100 ? count + " " : count >= 10 ? count + " -" : count + " --")}{divider}\n\n");
        }

        internal static void updateScheduleDBVars()
        {
            _scheduleDBTabName = Config._spreadsheetVars["ScheduleDB tab name"];
            _scheduleDBArea = Config._spreadsheetVars["ScheduleDB area"];
        }

        internal static bool populateScheduleDB(bool consoleWrite)
        {
            string name, DPS, DPE, link;
            int numSleeps;
            bool recommended;
            string length, startTime, endTime, earliestStartTime, latestEndTime, maxDistanceFromPreviousSleepBlock;

            try
            {
                List<List<string>> cells = SpreadsheetManager.readCellsFromSheet(_scheduleDBTabName, _scheduleDBArea);

                foreach (List<string> row in cells) //Populate schedule database
                {
                    if (row.Count >= 13)
                    {
                        name = row[0];
                        link = row[1];
                        DPS = row[4];
                        DPE = row[5];
                        recommended = row[3] == "TRUE";
                        numSleeps = int.Parse(row[2]);

                        List<SleepBlock> blocks = new();

                        for (int i = 0; i < numSleeps; i++)
                        {
                            length = row[6 + (i * 7)];
                            startTime = row[7 + (i * 7)];
                            endTime = row[8 + (i * 7)];
                            earliestStartTime = row[9 + (i * 7)];
                            latestEndTime = row[10 + (i * 7)];
                            maxDistanceFromPreviousSleepBlock = row[12 + (i * 7)];
                            SleepBlockType type = SleepBlockType.Core;
                            if (Enum.TryParse(row[11 + (i * 7)], out SleepBlockType typ)) type = typ;
                            blocks.Add(new SleepBlock(length, startTime, endTime, earliestStartTime, latestEndTime, maxDistanceFromPreviousSleepBlock, type));
                        }

                        _scheduleDB.Add(new Schedule(name, blocks, recommended, DPS, DPE, link, consoleWrite));
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"\npopulateScheduleDB:\n{e}\n");
                SFAlgorithm.populateScheduleDB(consoleWrite);
                return false;
            }
		}

        //"aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeee"
        internal async static Task<Task> buildCommands(bool deleteOld = false)
        {
            bool buildAll = false,
              buildGlobal = false;

            MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    " +
                $"Configuring{(buildAll ? " [Building All]" : "")}{(deleteOld ? " [Deleting Old]" : "")}...");

            List<(SlashCommandBuilder command, bool build)> commands = new();
            List<(MessageCommandBuilder command, bool build)> msgCommands = new();
            List<(UserCommandBuilder command, bool build)> userCommands = new();
            SocketGuild server = _client.GetGuild(1085660044374315189/*879179596703096913*/);

            if (!_commandsConfigured)
            {
                #region bools
                bool buildAllCommands =     false || buildAll,
                     buildAllSlash =        false || buildAllCommands,
                     buildAllMsg =          false || buildAllCommands,
                     buildAllUser =         false || buildAllCommands,

                     buildBan =             false || buildAllSlash,
                     buildFindSchedules =   false || buildAllSlash,
                     buildRandomSchedule =  false || buildAllSlash,
                     buildCreateNapchart =  false || buildAllSlash,
                     buildReload =          false || buildAllSlash,
                     buildReconnect =       false || buildAllSlash,
                     buildShutDown =        false || buildAllSlash,
                     buildGetList =         false || buildAllSlash,
					 buildShowAdmins =      false || buildAllSlash,
					 buildSend =            false || buildAllSlash,
					 buildHelp =            false || buildAllSlash,
					 buildAdminHelp =       false || buildAllSlash,
					 buildPoll =            false || buildAllSlash,
                     buildUserInfo_slash =  false || buildAllSlash,
                     buildNewRSEmbed =      false || buildAllSlash,
					 buildModRSEmbed =      false || buildAllSlash,
                     buildWelcome =         false || buildAllSlash,

					 buildDelete =          false || buildAllMsg,
                     buildEdit =            false || buildAllMsg,
                     buildEditEmbed =       false || buildAllMsg,
					 buildReact =           false || buildAllMsg,
                     buildRReact =          false || buildAllMsg,
                     buildEndPoll =         false || buildAllMsg,

					 buildUserInfo =        false || buildAllUser,
                     buildSetNickname =     false || buildAllUser,

                     buildAnySlash = buildBan || buildFindSchedules || buildRandomSchedule || buildCreateNapchart || buildReload || buildReconnect || 
                                     buildShutDown || buildGetList || buildShowAdmins || buildSend || buildHelp || buildAdminHelp || buildPoll || 
                                     buildUserInfo_slash || buildNewRSEmbed || buildModRSEmbed || buildWelcome,
                     buildAnyMsg   = buildDelete || buildEdit || buildEditEmbed || buildReact || buildRReact || buildEndPoll,
                     buildAnyUser  = buildUserInfo || buildSetNickname;
				#endregion

				MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Building " +
                    $"[{(buildAllCommands ? "All" : buildAnySlash || buildAnyMsg || buildAnyUser ? $"{(buildAnySlash ? "S" : "")}{(buildAnyMsg ? "M" : "")}{(buildAnyUser ? "U" : "")}" : "None")}]");

                //ban, findschedules, randomschedule, createnapchart, help, adminhelp, showadmins, send, poll - work v , welcome
                if (buildAnySlash)
				{
					List<ApplicationCommandOptionChoiceProperties> colors = new();
					if (buildCreateNapchart) colors = MM.getEnums<Napchart.Color>().FindAll(n => n != Napchart.Color.custom)
                            .Select(s => new ApplicationCommandOptionChoiceProperties() {Name = s.ToString(), Value = s.ToString() }).ToList();

					List<ApplicationCommandOptionChoiceProperties> helpOptions = new() { new() { Name = "all", Value = "all" } };
					if (buildHelp) helpOptions.AddRange(_uc._commands.Select(c => c.category).ToList().Distinct()
							.Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.ToLower(), Value = s }).ToList());

					List<ApplicationCommandOptionChoiceProperties> aHelpOptions = new() { new() { Name = "all", Value = "all" } };
					if (buildAdminHelp) aHelpOptions.AddRange(_ac._adminCommands.Select(c => c.category).ToList().Distinct()
                            .Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.ToLower(), Value = s }).ToList());

					List<ApplicationCommandOptionChoiceProperties> buttonColorOptions = new();
					if (buildModRSEmbed) buttonColorOptions.AddRange(new List<string> { "Red", "Green", "Blue", "Gray" }
							.Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.ToLower(), Value = s }).ToList());

					commands.AddRange(new List<(SlashCommandBuilder command, bool build)>
                    {
                        (new()
                        {
                            Name = "ban",
							Description = "Bans users (not really lmao).",
							DefaultMemberPermissions = GuildPermission.SendMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = false,
                            Options = new()
                            {
                                new()
                                {
                                    Name = "user1",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = true,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user2",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user3",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user4",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user5",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user6",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user7",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user8",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user9",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                },
                                new()
                                {
                                    Name = "user10",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = false,
                                    Description = "User to 'ban'.",
                                    IsAutocomplete = false
                                }
                            }
                        }, buildBan),               //ban - works v

                        (new()
                        {
                            Name = "findschedules",
							Description = "Enter information to receive viable polyphasic sleep schedules for you.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = true
                        }, buildFindSchedules),     //findschedules - works v

                        (new()
                        {
                            Name = "randomschedule",
							Description = "Recieve a random schedule!",
							DefaultMemberPermissions = GuildPermission.SendMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = true,
                            Options = new()
                            {
                                new()
                                {
                                    Name = "count",
                                    Type = ApplicationCommandOptionType.Integer,
                                    MinValue = UserCommand._minSlpCt,
                                    MaxValue = UserCommand._maxSlpCt,
                                    Description = "Number of sleeps in schedule.",
                                    IsRequired = false
                                }, //count

								new()
                                {
                                    Name = "sleepalgorithm",
                                    Type = ApplicationCommandOptionType.String,
                                    Description = "Algorithm used to determine sleep lengths.",
                                    IsRequired = false,
                                    Choices = new()
                                    {
                                        new()
                                        {
                                            Name = "random",
                                            Value = "random"
                                        },

                                        new()
                                        {
                                            Name = "equal",
                                            Value = "equal"
                                        },

                                        new()
                                        {
                                            Name = "minmax",
                                            Value = "minmax"
                                        },

                                        new()
                                        {
                                            Name = "spiral",
                                            Value = "spiral"
                                        }
                                    }
                                }, //sleepalgorithm

								new()
                                {
                                    Name = "gapalgorithm",
                                    Description = "Algorithm used to determine gap lengths.",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Choices = new()
                                    {
                                        new()
                                        {
                                            Name = "random",
                                            Value = "random"
                                        },

                                        new()
                                        {
                                            Name = "equal",
                                            Value = "equal"
                                        },

                                        new()
                                        {
                                            Name = "spiral",
                                            Value = "spiral"
                                        }
                                    }
                                }, //gapalgorithm

								new()
                                {
                                    Name = "start",
                                    Type = ApplicationCommandOptionType.Integer,
                                    MinValue = 0,
                                    MaxValue = UserCommand._incrementsPerDay,
                                    Description = "Time the schedule will start at.",
                                    IsRequired = false
                                } //start
							}
                        }, buildRandomSchedule),    //randomschedule - works v

                        (new()
                        {
                            Name = "createnapchart",
							Description = "Create a napchart.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = false,
                            Options = new()
                            {
                                new()
                                {
									Name = "simple",
									Description = "Use a simplified interface (1 lane & color).",
									Type = ApplicationCommandOptionType.SubCommand,
									IsRequired = false,
									Options = new()
									{
										new()
										{
                                            Name = "times",
											Description = "Ranges to add [ex: 00:00-9:00, 12:00-15:00]",
                                            Type = ApplicationCommandOptionType.String,
                                            IsRequired = true,
                                            MinLength = 9
										}, //times

                                        new()
                                        {
                                            Name = "color",
											Description = "Color the ranges will be (default red).",
                                            Type = ApplicationCommandOptionType.String,
                                            IsRequired = false,
                                            Choices = colors
                                        }, //color

                                        new()
                                        {
                                            Name = "label",
                                            Description = "Label for the selected color.",
                                            Type = ApplicationCommandOptionType.String,
                                            IsRequired = false,
                                            MinLength = 1,
                                            MaxLength = 20
                                        }, //label
                                                
                                        new()
										{
											Name = "name",
											Description = "Name of the Napchart.",
											Type = ApplicationCommandOptionType.String,
											IsRequired = false,
											MinLength = 1,
											MaxLength = 20
										}, //name

                                        new()
                                        {
											Name = "description",
											Description = "Description of the Napchart.",
											Type = ApplicationCommandOptionType.String,
											IsRequired = false,
											MinLength = 1,
											MaxLength = 200
										} //description
									}
								}, //simple

                                new()
                                {
                                    Name = "detailed",
                                    Description = "Use detailed interface for more options.",
                                    Type = ApplicationCommandOptionType.SubCommand,
                                    IsRequired = false
                                }  //detailed
                            }
                        }, buildCreateNapchart),    //createnapchart - works vv

                        (new() 
                        {
                            Name = "help",
							Description = "Get help with commands.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = true,
                            Options = new()
							{
								new()
								{
									Name = "categories",
									Description = "Select a category.",
									Type = ApplicationCommandOptionType.String,
									IsRequired = false,
									Choices = helpOptions
								}
							}
                        }, buildHelp),              //help - works v

                        (new()
						{
							Name = "adminhelp",
							Description = "Get help with admin commands.",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
							IsDMEnabled = false,
							Options = new()
							{
								new()
								{
									Name = "categories",
									Description = "Select a category.",
									Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Choices = aHelpOptions
								}
							}
						}, buildAdminHelp),         //adminhelp - works v

                        (new()
                        { 
                            Name = "reload",
							Description = "Refresh bot parameters.",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
							IsDMEnabled = true,
                            Options = new()
                            {
                                new()
                                {
                                    Name = "commands",
                                    Type = ApplicationCommandOptionType.SubCommand,
                                    Description = "Rebuild's Bot's commands.",
                                    Options = new()
                                    {
                                        new()
                                        {
                                            Name = "deleteold",
                                            Type = ApplicationCommandOptionType.Boolean,
                                            Description = "Delete old commands?",
                                            IsRequired = false
                                        }
                                    }
                                }, //commands - works

                                new()
								{
									Name = "config",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Refreshes config.",
									Options = new()
									{
										new()
										{
											Name = "refreshsdb",
											Type = ApplicationCommandOptionType.Boolean,
											Description = "Refresh Schedule DB?",
											IsRequired = false
										}
									}
								}, //config - works
                                
                                new()
								{
									Name = "admins",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Resets Admins."
								}, //admins - works
                                
                                new()
								{
									Name = "mimics",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Resets Mimics."
								}, //mimics - works
                            }
						}, buildReload),            //reload - works v
                        
                        (new()
						{
							Name = "reconnect",
							Description = "Reconnect bot services.",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
							IsDMEnabled = true,
							Options = new()
							{                                
                                new()
								{
									Name = "mysql",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Reconnects to MySQL."
								}, //mysql - works
                                
                                new()
								{
									Name = "gsheets",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Reconnects to Google Sheets."
								}, //gsheets - works
                            }
						}, buildReconnect),         //reconnect - works v
                        
                        (new()
						{
							Name = "shutdown",
							Description = "Shut down or restart bot.",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
							IsDMEnabled = true,
							Options = new()
							{                                
                                new()
								{
									Name = "restart",
									Type = ApplicationCommandOptionType.Boolean,
									Description = "Restart Bot after shutdown?",
                                    IsRequired = false
								}, //restart
                            }
						}, buildShutDown),          //shutdown - works v

						(new()
                        {
                            Name = "showadmins",
							Description = "Shows admins.",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
                            IsDefaultPermission = true,
                            IsDMEnabled = false,
                            Options = new()
                            {
                                new()
                                {
                                    Name = "server",
                                    Type = ApplicationCommandOptionType.String,
                                    Description = "Server ID of the server to see admins of.",
                                    IsRequired = false
                                }
                            }
                        }, buildShowAdmins),        //showadmins - works v

                        (new() 
                        {
                            Name = "send",
							Description = "Send a message as the bot.",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
							IsDMEnabled = true,
                            Options = new()
                            {
                                new()
                                {
                                    Name = "message",
                                    Type = ApplicationCommandOptionType.String,
                                    Description = "Message you'd like the bot to send.",
                                    IsRequired = true
                                }, //message

                                new()
                                {
                                    Name = "replyto",
                                    Type = ApplicationCommandOptionType.String,
                                    Description = "Message link to reply to [@ at end for ping].",
                                    IsRequired = false
                                }, //replyto

                                new()
								{
									Name = "channelid",
									Type = ApplicationCommandOptionType.String,
									Description = "Channel ID to send message in.",
									IsRequired = false,
                                    MinLength = 18,
                                    MaxLength = 20
								}, //channelid
                            }
                        }, buildSend),              //send - works v
                        
                        (new()
						{
							Name = "getlist",
							Description = "Get list of info for a given server.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = true,
							Options = new()
							{
                                new()
								{
									Name = "list",
									Type = ApplicationCommandOptionType.String,
									Description = "What would you like listed?",
                                    IsRequired = true,                                    
                                    Choices = new()
                                    {
                                        new()
                                        {
                                            Name = "channels",
                                            Value = "channels"
                                        },

										new()
										{
											Name = "roles",
											Value = "roles"
										},

										new()
										{
											Name = "emotes",
											Value = "emotes"
										},

										new()
										{
											Name = "users",
											Value = "users"
										}
          //                              ,

										//new()
										//{
										//	Name = "userswrole",
										//	Value = "userswrole"
										//}
									}
								}, //list
                                
                                new()
                                {
                                    Name = "server",
                                    Type = ApplicationCommandOptionType.String,
                                    Description = "Server ID (blank for current server).",
                                    IsRequired = false
                                }, //server
                                
                                new()
								{
									Name = "searchfor",
									Type = ApplicationCommandOptionType.String,
									Description = "Search key to look for.",
									IsRequired = false
								} //searchfor
							}
						}, buildGetList),           //getlist - works v

                        //deletecount

                        //updatebot

                        //getchannels/roles

                        //leaveserver

                        //managememes reset/set/clear

                        //logging

                        //tail/stoptail

                        //executecommand

                        //getmessages/recent/deleted/messagecount

						(new()
                        {
                            Name = "poll",
							Description = "Create a poll.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = true,
                            Options = new()
                            {
                                new()
                                {
                                    Name = "title",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = true,
                                    Description = "Title of poll.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option1",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = true,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option2",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = true,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new ()
                                {
                                    Name = "option3",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option4",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option5",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option6",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option7",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option8",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },

                                new()
                                {
                                    Name = "option9",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = false,
                                    Description = "Associated Emoji (if blank then default), then option name.",
                                    IsAutocomplete = false
                                },
                            }
                        }, buildPoll),              //poll - works v

						(new()
						{
							Name = "userinfo",
							Description = "Get info about a user.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = false,
							Options = new()
							{
								new()
								{
									Name = "user",
									Type = ApplicationCommandOptionType.User,
									IsRequired = true,
									Description = "Select a user."
								}
							}
						}, buildUserInfo_slash),    //userinfo - works v

						(new()
						{
							Name = "roleselectembed",
							Description = "Create a new Role Selection Embed.",
							DefaultMemberPermissions = GuildPermission.ManageMessages,
							IsDefaultPermission = true,
							IsDMEnabled = false
						}, buildNewRSEmbed),        //roleselectembed

						(new()
						{
							Name = "modrsembed",
							Description = "Modify an existing Role Selection Embed.",
							DefaultMemberPermissions = GuildPermission.ManageMessages,
							IsDefaultPermission = true,
							IsDMEnabled = false,
							Options = new()
							{
                                new()
                                {
                                    Name = "addrole",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Add a Role Selection option.",
                                    Options = new()
									{
								        new()
								        {
									        Name = "existingrse",
									        Type = ApplicationCommandOptionType.String,
									        IsRequired = true,
									        Description = "Message ID of the existing RS Embed."
								        }, //existingrse (Message ID)

                                        new()
                                        {
                                            Name = "role",
                                            Type = ApplicationCommandOptionType.Role,
                                            IsRequired = true,
                                            Description = "Select the role to be toggled."
                                        }, //role

                                        new()
										{
											Name = "emoji",
											Type = ApplicationCommandOptionType.String,
											IsRequired = false,
											Description = "Emoji to be listed on Button.",
                                            MaxLength = 35
										}, //emoji

                                        new()
										{
											Name = "color",
											Type = ApplicationCommandOptionType.String,
											IsRequired = false,
											Description = "Color of the Button.",
                                            Choices = buttonColorOptions
										}  //color
                                    }
								}, //addrole

								new()
								{
									Name = "deleterole",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Delete a Role Selection option.",
									Options = new()
									{
										new()
										{
											Name = "existingrse",
											Type = ApplicationCommandOptionType.String,
											IsRequired = true,
											Description = "Message ID of the existing RS Embed."
										} //existingrse (Message ID)
                                    }
								} //deleterole
							}
						}, buildModRSEmbed),        //modrsembed

                        (new()
                        {
                            Name = "welcome",
							Description = "Create the Welcome message.",
							DefaultMemberPermissions = GuildPermission.ManageMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = false
                        }, buildWelcome)            //welcome
			        });
                }

                //delete, edit, editEmbed react, rreact, endpoll - work v
                if (buildAnyMsg)
                    msgCommands.AddRange(new List<(MessageCommandBuilder command, bool build)>
					{
                        (new()
						{
							Name = "delete",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
                            IsDMEnabled = true
                        }, buildDelete),  //delete - works v

                        (new()
						{
							Name = "edit",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
							IsDMEnabled = true
						}, buildEdit),    //Edit - works v

                        (new()
						{
							Name = "editembed",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
							IsDMEnabled = true
						}, false/*buildEditEmbed*/), //EditEmbed - works (just says it doesnt)

                        (new()
						{
							Name = "react",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
                            IsDMEnabled = false
						}, buildReact),  //react - works v

                        (new()
						{
							Name = "rreact",
							DefaultMemberPermissions = GuildPermission.ManageGuild,
							IsDefaultPermission = true,
                            IsDMEnabled = false
						}, buildRReact), //rreact - works v

						(new()
						{
							Name = "endpoll",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = true
						}, buildEndPoll) //endpoll - works v
					});

                //getinfo, setnickname - work v
                if (buildAnyUser)
                    userCommands.AddRange(new List<(UserCommandBuilder command, bool build)>()
                    {
                        (new()
                        {
                            Name = "userinfo",
                            DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
                            IsDMEnabled = false
						}, buildUserInfo),   //userinfo - works v 
                        
                        (new()
						{
							Name = "setnickname",
							DefaultMemberPermissions = GuildPermission.ManageNicknames,
							IsDefaultPermission = true,
							IsDMEnabled = false
						}, buildSetNickname) //setnickname - works v
                    });

                if (_printCommandInfo && commands.Count + msgCommands.Count + userCommands.Count > 0)
                {
                    string commandText = "", msgText = "", userText = "";

                    if (commands.Count > 0)
                    {
                        commandText = "[";

                        foreach ((SlashCommandBuilder command, bool _) in commands)
                        {
                            commandText += command.Name;
                            if (commands.Last().command != command)
                                commandText += ", ";
                        }

                        commandText += "]";
                    }

                    if (msgCommands.Count > 0)
                    {
                        msgText = "[";

                        foreach ((MessageCommandBuilder command, bool _) in msgCommands)
                        {
                            msgText += command.Name;
                            if (msgCommands.Last().command != command)
                                msgText += ", ";
                        }

                        msgText += "]";
                    }

                    if (userCommands.Count > 0)
                    {
                        userText = "[";

                        foreach ((UserCommandBuilder command, bool _) in userCommands)
                        {
                            userText += command.Name;
                            if (userCommands.Last().command != command)
                                userText += ", ";
                        }

                        userText += "]";
                    }

					MM.Print(_printCommandInfo, $"{(DateTime.Now.Hour < 10 ? "0" : "") + DateTime.Now.Hour.ToString()}:" +
                    $"{(DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute.ToString()}:" +
                    $"{(DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second.ToString()} Commands    Built {commandText}{msgText}{userText}");
                }

                if (deleteOld)
                {
                    try
                    {
                        server.DeleteApplicationCommandsAsync().Wait();
                        foreach(SocketApplicationCommand command in _client.GetGlobalApplicationCommandsAsync().Result) command.DeleteAsync().Wait();

						MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Old Commands Deleted");
                    }
                    catch
                    {
						MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Unable to Delete Old Commands");
                    }
                }
            } //work v

            if (!_ngCommandsConfigured && (!_isNapGodOnline || _testBot))
            {
                #region bools
                bool buildAllCommands =     false || buildAll,
                     buildAllSlash =        false || buildAllCommands,
                     buildAllMsg =          false || buildAllCommands,
                     buildAllUser =         false || buildAllCommands,

                     buildGet_Slash =       false || buildAllSlash,
                     buildSet =             false || buildAllSlash,
                     buildNC =              false || buildAllSlash,
                     buildAR_Slash =        false || buildAllSlash,
                     buildAdapted_Slash =   false || buildAllSlash,
					 buildMSet =            false || buildAllSlash,
					 buildInfo =            false || buildAllSlash,
					 buildScheduleInfo =    false || buildAllSlash,

					 buildAdvise =          false || buildAllMsg,

					 buildGet =             false || buildAllUser,
                     buildAdapted =         false || buildAllUser,
                     buildAssignRoles =     false || buildAllUser,

					 buildAnySlash = buildGet_Slash || buildSet || buildNC || buildAR_Slash || buildAdapted_Slash || buildMSet || buildInfo || buildScheduleInfo,
                     buildAnyMsg   = buildAdvise,
                     buildAnyUser  = buildGet || buildAdapted || buildAssignRoles;
				#endregion

				MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Building (NG) " +
                    $"[{(buildAllCommands ? "All" : buildAnySlash || buildAnyMsg || buildAnyUser ? $"{(buildAnySlash ? "S" : "")}{(buildAnyMsg ? "M" : "")}{(buildAnyUser ? "U" : "")}" : "None")}]");

                //get, set, nc, tg, assignroles, adapted, mset - work v , info, schedules
                if (buildAnySlash)
                {
                    List<ApplicationCommandOptionChoiceProperties> schedules = new(), experimental_schedules = new(), nonPoly_schedules = new(), modifiers = new(),
                        helpcategory = new(), genpolyphasic = new(), scheduling = new(), adaptation = new(), challenges = new(), sleepscience = new();
                    SlashCommandOptionBuilder mods = null;

					if (buildInfo)
					{
						helpcategory.AddRange(NapGodText._help.FindAll(t => t.category == NapGodText.Category.Category).Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.commandName, Value = s.commandName }).ToList());
						genpolyphasic.AddRange(NapGodText._help.FindAll(t => t.category == NapGodText.Category.GenPoly).Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.commandName, Value = s.commandName }).ToList());
						scheduling.AddRange(NapGodText._help.FindAll(t => t.category == NapGodText.Category.Scheduling).Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.commandName, Value = s.commandName }).ToList());
						adaptation.AddRange(NapGodText._help.FindAll(t => t.category == NapGodText.Category.Adaptation).Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.commandName, Value = s.commandName }).ToList());
						challenges.AddRange(NapGodText._help.FindAll(t => t.category == NapGodText.Category.Challenges).Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.commandName, Value = s.commandName }).ToList());
						sleepscience.AddRange(NapGodText._help.FindAll(t => t.category == NapGodText.Category.SScience).Select(s => new ApplicationCommandOptionChoiceProperties() { Name = s.commandName, Value = s.commandName }).ToList());
					}

					if (buildSet || buildMSet || buildScheduleInfo)
					{
						foreach (NapGodSchedule schedule in Config._napGodSchedules.Where(n => n._categoryRole._category is not (NapGodSchedule.Category.Monophasic or NapGodSchedule.Category.Random or NapGodSchedule.Category.Experimental)))
							schedules.Add(new ApplicationCommandOptionChoiceProperties() { Name = schedule._nameClean, Value = schedule._nameClean });
						
						foreach (NapGodSchedule schedule in Config._napGodSchedules.Where(n => n._categoryRole._category == NapGodSchedule.Category.Experimental))
							experimental_schedules.Add(new ApplicationCommandOptionChoiceProperties() { Name = schedule._nameClean, Value = schedule._nameClean });

						foreach (NapGodSchedule schedule in Config._napGodSchedules.Where(n => n._categoryRole._category is NapGodSchedule.Category.Monophasic or NapGodSchedule.Category.Random))
							nonPoly_schedules.Add(new ApplicationCommandOptionChoiceProperties() { Name = schedule._nameClean, Value = schedule._nameClean });

                        if (buildSet || buildMSet)
                        {
                            foreach (NapGodSchedule.Modifier mod in MM.getEnums<NapGodSchedule.Modifier>().FindAll(m => !m.ToString().Contains('_') && m != NapGodSchedule.Modifier.none))
                                modifiers.Add(new ApplicationCommandOptionChoiceProperties() { Name = mod.ToString(), Value = mod.ToString() });

							mods = new()
							{
								Name = "modifier",
								Type = ApplicationCommandOptionType.String,
								IsRequired = false,
								Description = "Modifiers to your schedule.",
								IsAutocomplete = false,
								Choices = modifiers
							};
						}
					}

                    commands.AddRange(new List<(SlashCommandBuilder command, bool build)>
                    {
                        (new()
                        {
                            Name = "get",
							Description = "Gets the schedule of a user. If no user is selected, gets your schedule.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = false,
                            Options = new List<SlashCommandOptionBuilder>()
                            {
                                new SlashCommandOptionBuilder()
                                {
                                    Name = "user",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = true,
                                    Description = "User whose schedule you'd like to see.",
                                    IsAutocomplete = false
                                }
                            }
                        }, buildGet_Slash),     //get - works v

                        (new()
                        {
                            Name = "set",
							Description = "Set your sleep schedule.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = true,
                            Options = new()
                            {
                                new()
                                {
                                    Name = "polyphasic",
                                    Type = ApplicationCommandOptionType.SubCommand,
                                    Description = "Set polyphasic schedules.",
                                    Options = new ()
							        {
								        new()
								        {
									        Name = "schedule",
									        Description = "Name of the schedule you're setting.",
									        MaxLength = 12,
									        MinLength = 2,
									        IsRequired = false,
									        Type = ApplicationCommandOptionType.String,
									        Choices = schedules
								        }, //schedule

                                        new()
								        {
									        Name = "modifier",
									        Description = "Modifiers for your schedule.",
									        IsRequired = false,
									        Type = ApplicationCommandOptionType.String,
									        Choices = modifiers
								        }, //modifiers

                                        new()
								        {
									        Name = "napchart",
									        Description = "Napchart link for your schedule.",
									        MinLength = "https://napchart.com/12345".Length,
									        IsRequired = false,
									        Type = ApplicationCommandOptionType.String
								        }  //napchart
						            }
								}, //poly

								new()
								{
									Name = "experimental",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Set experimental polyphasic schedules.",
									Options = new ()
									{
										new()
										{
											Name = "schedule",
											Description = "Name of the schedule you're setting.",
											MaxLength = 12,
											MinLength = 2,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = experimental_schedules
										}, //schedule

                                        new()
										{
											Name = "modifier",
											Description = "Modifiers for your schedule.",
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = modifiers
										}, //modifiers

                                        new()
										{
											Name = "napchart",
											Description = "Napchart link for your schedule.",
											MinLength = "https://napchart.com/12345".Length,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String
										}  //napchart
						            }
								}, //experimental

								new()
								{
									Name = "non_poly",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Set non-polyphasic schedules.",
									Options = new ()
									{
										new()
										{
											Name = "schedule",
											Description = "Name of the schedule you're setting.",
											MaxLength = 12,
											MinLength = 2,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = nonPoly_schedules
										}, //schedule

                                        new()
										{
											Name = "modifier",
											Description = "Modifiers for your schedule.",
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = modifiers
										}, //modifiers

                                        new()
										{
											Name = "napchart",
											Description = "Napchart link for your schedule.",
											MinLength = "https://napchart.com/12345".Length,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String
										}  //napchart
						            }
								}  //non-poly
							}
                        }, buildSet),           //set - works v

                        (new()
                        {
                            Name = "nc",
							Description = "Sends an image of the Napchart in the provided link.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = false,
                            Options = new List<SlashCommandOptionBuilder>()
                            {
                                new SlashCommandOptionBuilder()
                                {
                                    Name = "napchart",
                                    Type = ApplicationCommandOptionType.String,
                                    IsRequired = true,
                                    Description = "The link to the Napchart you want to send an image of.",
                                    IsAutocomplete = false
                                }
                            }
                        }, buildNC),            //nc - works v

                        (new()
                        {
                            Name = "assignroles",
                            DefaultMemberPermissions = GuildPermission.ManageRoles,
                            Description = "Toggles listed roles for a selected user.",
                            IsDefaultPermission = true,
                            IsDMEnabled = false,
                            Options = new List<SlashCommandOptionBuilder>()
                            {
                                new SlashCommandOptionBuilder()
                                {
                                    Name = "user",
                                    Type = ApplicationCommandOptionType.User,
                                    IsRequired = true,
                                    Description = "User to toggle roles for.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role1",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = true,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role2",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role3",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role4",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role5",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role6",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role7",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role8",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                },

                                new SlashCommandOptionBuilder()
                                {
                                    Name = "role9",
                                    Type = ApplicationCommandOptionType.Role,
                                    IsRequired = false,
                                    Description = "Role to toggle.",
                                    IsAutocomplete = false
                                }
                            }
                        }, buildAR_Slash),      //assignroles - works v

                        (new()
						{
							Name = "adapted",
							DefaultMemberPermissions = GuildPermission.ManageRoles,
							Description = "Updates user to adapted status.",
							IsDMEnabled = false,
							Options = new List<SlashCommandOptionBuilder>()
							{
								new SlashCommandOptionBuilder()
								{
									Name = "user",
									Type = ApplicationCommandOptionType.User,
									IsRequired = true,
									Description = "The user to set adapted."
								}, //user
                                
								new SlashCommandOptionBuilder()
								{
									Name = "schedule",
									Type = ApplicationCommandOptionType.String,
									IsRequired = true,
									Description = "The schedule to set the user adapted to."
								} //schedule
							}
						}, buildAdapted_Slash), //adapted - works v

                        (new()
						{
							Name = "mset",
							DefaultMemberPermissions = GuildPermission.ManageMessages,
							Description = "Set a user's sleep schedule.",
							Options = new()
							{
								new()
								{
									Name = "polyphasic",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Set polyphasic schedules.",
									Options = new ()
									{
								        new()
								        {
									        Name = "user",
									        Type = ApplicationCommandOptionType.User,
									        Description = "User to set schedule for.",
									        IsRequired = true
								        }, //user

										new()
										{
											Name = "schedule",
											Description = "Name of the schedule you're setting.",
											MaxLength = 12,
											MinLength = 2,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = schedules
										}, //schedule

                                        new()
										{
											Name = "modifier",
											Description = "Modifiers for schedule.",
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = modifiers
										}, //modifiers

                                        new()
										{
											Name = "napchart",
											Description = "Napchart link for schedule.",
											MinLength = "https://napchart.com/12345".Length,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String
										}  //napchart
						            }
								}, //poly

								new()
								{
									Name = "experimental",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Set experimental polyphasic schedules.",
									Options = new ()
									{
										new()
										{
											Name = "user",
											Type = ApplicationCommandOptionType.User,
											Description = "User to set schedule for.",
											IsRequired = true
										}, //user

										new()
										{
											Name = "schedule",
											Description = "Name of the schedule you're setting.",
											MaxLength = 12,
											MinLength = 2,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = experimental_schedules
										}, //schedule

                                        new()
										{
											Name = "modifier",
											Description = "Modifiers for schedule.",
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = modifiers
										}, //modifiers

                                        new()
										{
											Name = "napchart",
											Description = "Napchart link for schedule.",
											MinLength = "https://napchart.com/12345".Length,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String
										}  //napchart
						            }
								}, //experimental

								new()
								{
									Name = "non_poly",
									Type = ApplicationCommandOptionType.SubCommand,
									Description = "Set non-polyphasic schedules.",
									Options = new ()
									{
										new()
										{
											Name = "user",
											Type = ApplicationCommandOptionType.User,
											Description = "User to set schedule for.",
											IsRequired = true
										}, //user

										new()
										{
											Name = "schedule",
											Description = "Name of the schedule you're setting.",
											MaxLength = 12,
											MinLength = 2,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = nonPoly_schedules
										}, //schedule

                                        new()
										{
											Name = "modifier",
											Description = "Modifiers for schedule.",
											IsRequired = false,
											Type = ApplicationCommandOptionType.String,
											Choices = modifiers
										}, //modifiers

                                        new()
										{
											Name = "napchart",
											Description = "Napchart link for schedule.",
											MinLength = "https://napchart.com/12345".Length,
											IsRequired = false,
											Type = ApplicationCommandOptionType.String
										}  //napchart
						            }
								}  //non-poly
							}
						}, buildMSet),          //mset - works v
                        
                        (new()
						{
							Name = "info",
							Description = "Get info about sleep-related topics.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = true,
							Options = new()
							{
								new()
								{
									Name = "categories",
									Description = "Look at specific categories of commands.",
									Type = ApplicationCommandOptionType.String,
									Choices = helpcategory
								}, //categories

                                new()
								{
									Name = "general",
									Description = "General polyphasic information.",
									Type = ApplicationCommandOptionType.String,
									Choices = genpolyphasic
								}, //general

                                new()
								{
									Name = "scheduling",
									Description = "Schedule selection and personalization.",
									Type = ApplicationCommandOptionType.String,
									Choices = scheduling
								}, //scheduling

                                new()
								{
									Name = "adaptation",
									Description = "Adaptation process and best practices.",
									Type = ApplicationCommandOptionType.String,
									Choices = adaptation
								}, //adaptation

                                new()
								{
									Name = "adaptationchallenges",
									Description = "Adaptation issues and possible solutions.",
									Type = ApplicationCommandOptionType.String,
									Choices = challenges
								}, //adaptationchallenges

                                new()
								{
									Name = "sleepscience",
									Description = "The science behind aspects of sleep.",
									Type = ApplicationCommandOptionType.String,
									Choices = sleepscience
								}  //sleepscience
                            }
						}, buildInfo),          //info

                        (new()
						{
							Name = "schedules",
							Description = "Learn about different sleep schedules.",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = true,
							Options = new()
							{
								new()
								{
									Name = "polyphasic",
									Description = "Verified polyphasic schedules.",
									Type = ApplicationCommandOptionType.String,
									Choices = schedules
								}, //polyphasic

                                new()
								{
									Name = "experimental",
									Description = "Unverified polyphasic schedules.",
									Type = ApplicationCommandOptionType.String,
									Choices = experimental_schedules
								}, //experimental

                                new()
								{
									Name = "nonpoly",
									Description = "Non-polyphasic schedules.",
									Type = ApplicationCommandOptionType.String,
									Choices = nonPoly_schedules
								}, //nonpoly
                            }
						}, buildScheduleInfo),  //schedules
			        });
                }

                //advise
                if (buildAnyMsg)
                    msgCommands.AddRange(new List<(MessageCommandBuilder command, bool build)> 
                    {
						(new()
						{
							Name = "advise",
							DefaultMemberPermissions = GuildPermission.SendMessages,
							IsDefaultPermission = true,
							IsDMEnabled = false
						}, buildAdvise)   //advise
                    });

				//get, adapted, assignroles - work v
				if (buildAnyUser)
                    userCommands.AddRange(new List<(UserCommandBuilder command, bool build)>()
                    {
                        (new()
                        {
                            Name = "get",
                            DefaultMemberPermissions = GuildPermission.SendMessages,
                            IsDefaultPermission = true,
                            IsDMEnabled = false
                        }, buildGet),        //get - works v

                        (new()
                        {
                            Name = "adapted",
                            DefaultMemberPermissions = GuildPermission.ManageRoles,
                            IsDefaultPermission = true,
                            IsDMEnabled = false
                        }, buildAdapted),    //adapted - works v

                        (new()
						{
							Name = "assignroles",
							DefaultMemberPermissions = GuildPermission.ManageRoles,
							IsDMEnabled = false
						}, buildAssignRoles) //assignroles - works v
                    });

                if (_printCommandInfo && commands.Count + msgCommands.Count + userCommands.Count > 0)
                {
                    string commandText = "", msgText = "", userText = "";

                    if (commands.Count > 0)
                    {
                        commandText = "[";

                        foreach ((SlashCommandBuilder command, bool _) in commands)
                        {
                            commandText += command.Name;
                            if (commands.Last().command != command)
                                commandText += ", ";
                        }

                        commandText += "]";
                    }

                    if (msgCommands.Count > 0)
                    {
                        msgText = "[";

                        foreach ((MessageCommandBuilder command, bool _) in msgCommands)
                        {
                            msgText += command.Name;
                            if (msgCommands.Last().command != command)
                                msgText += ", ";
                        }

                        msgText += "]";
                    }

                    if (userCommands.Count > 0)
                    {
                        userText = "[";

                        foreach ((UserCommandBuilder command, bool _) in userCommands)
                        {
                            userText += command.Name;
                            if (userCommands.Last().command != command)
                                userText += ", ";
                        }

                        userText += "]";
                    }

					MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Built NG {commandText}{msgText}{userText}");
                }
            }

            string commandsBuilt = "";

            try
            {
                if (commands.FindAll(c => c.build).Count > 0)
                {
                    List<(SlashCommandBuilder command, bool build)> cmds = commands.FindAll(c => c.build);
                    foreach ((SlashCommandBuilder cmd, bool build) in cmds)
                    {
                        if (build)
                        {
                            SocketApplicationCommand result = !buildGlobal
                                ? await server.CreateApplicationCommandAsync(cmd.Build())
                                : await _client.CreateGlobalApplicationCommandAsync(cmd.Build());
                            commandsBuilt += result.Name;
                            if (cmds.Last().command != cmd)
                                commandsBuilt += ", ";
                        }
                    }

                    MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Created Slash: [{commandsBuilt}]");
                }

                _commandsConfigured = true;
            }
            catch
            {
                MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Created Slash: [{commandsBuilt}] - UNABLE TO FINISH");

                _commandsConfigured = false;
            } //slash

            try
            {
                if (msgCommands.FindAll(c => c.build).Count > 0)
				{
					commandsBuilt = "";
					List<(MessageCommandBuilder command, bool build)> cmds = msgCommands.FindAll(c => c.build);
					foreach ((MessageCommandBuilder cmd, bool build) in cmds)
					{
						if (build)
					    {
						    SocketApplicationCommand result = !buildGlobal
								    ? await server.CreateApplicationCommandAsync(cmd.Build())
								    : await _client.CreateGlobalApplicationCommandAsync(cmd.Build());

						    commandsBuilt += result.Name;
							if (cmds.Last().command != cmd)
								commandsBuilt += ", ";
						}
					}

					MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Created Msg: [{commandsBuilt}]");
				}

				_commandsConfigured = true;
            }
            catch
            {
                MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Created Msg: [{commandsBuilt}] - UNABLE TO FINISH");

                _commandsConfigured = false;
            } //msg

            try
			{
				if (userCommands.FindAll(c => c.build).Count > 0)
				{
					commandsBuilt = "";
					List<(UserCommandBuilder command, bool build)> cmds = userCommands.FindAll(c => c.build);
					foreach ((UserCommandBuilder cmd, bool build) in cmds)
					{
						if (build)
						{
							SocketApplicationCommand result = !buildGlobal
								? await server.CreateApplicationCommandAsync(cmd.Build())
								: await _client.CreateGlobalApplicationCommandAsync(cmd.Build());

							commandsBuilt += result.Name;
							if (cmds.Last().command != cmd)
								commandsBuilt += ", ";
						}
					}

					MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Created User: [{commandsBuilt}]");
				}

				_commandsConfigured = true;
			}
			catch
			{
				MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Created User: [{commandsBuilt}] - UNABLE TO FINISH");

				_commandsConfigured = false;
			} //user

			MM.Print(_printCommandInfo, $"{MM.getCurrentTimeStamp()} Commands    Configured" +
				$"\n\n{new string('-', 45)}---------------{new string('-', 45)}\n\n");

            return Task.CompletedTask;
        }
        #endregion 

        #region message utilities
        /// <summary> Simplification of message sending </summary>
        /// <param name="msg">Message to reply to</param>
        /// <param name="message">The message to send</param>
        internal static RestUserMessage send(SocketMessage msg, string message, bool allowMentions = false, IMessage reference = null)
        {
            AllowedMentions ping = AllowedMentions.All;
            if (allowMentions) ping.MentionRepliedUser = true;

            return msg.Channel.SendMessageAsync(message, allowedMentions: ping, messageReference: reference == null ? null : new MessageReference(reference.Id, reference.Channel.Id, getGuildChannel(reference.Channel.Id).Guild.Id)).Result;
		}
		internal static RestUserMessage sendFile(SocketMessage msg, string message, string filePath, bool allowMentions = false, IMessage reference = null)
		{
			AllowedMentions ping = AllowedMentions.All;
			if (allowMentions) ping.MentionRepliedUser = true;

            return !string.IsNullOrWhiteSpace(filePath)
                ? msg.Channel.SendFileAsync(filePath, message, allowedMentions: ping, messageReference: reference == null ? null : new MessageReference(reference.Id, reference.Channel.Id, getGuildChannel(reference.Channel.Id).Guild.Id)).Result
                : send(msg, message, allowMentions, reference);
        }

		internal static IUserMessage sendEmbed(IMessageChannel channel, Color? color = null, string message = "", string title = "", string description = "", string subtitle = "", string text = "", string url = "", string imageURL = "", string thumbnailURL = "", string footer = "", IGuildUser author = null, string authorName = "", List<EmbedFieldBuilder> fields = null, ComponentBuilder components = null, bool allowMentions = false, IMessage replyTo = null, bool requestedByAuthor = false)
        {
            EmbedBuilder embed = new() { Color = color ?? Color.Default };
            if (!string.IsNullOrWhiteSpace(title)) embed.Title = title;
            if (!string.IsNullOrWhiteSpace(description)) embed.Description = description; 
            
            if (!string.IsNullOrWhiteSpace(subtitle) && string.IsNullOrWhiteSpace(text)) text = (!string.IsNullOrWhiteSpace(subtitle) ? subtitle : Config._blank);
			else if (string.IsNullOrWhiteSpace(subtitle) && !string.IsNullOrWhiteSpace(text)) subtitle = (!string.IsNullOrWhiteSpace(text) ? text : Config._blank);
			if (fields == null && (!string.IsNullOrWhiteSpace(subtitle) && !string.IsNullOrWhiteSpace(text)))
            {
                fields = new List<EmbedFieldBuilder>
                {
                    new EmbedFieldBuilder
                    {
                        Name = subtitle,
                        Value = text
                    }
                };
            }
            
			if (fields != null && fields.Count > 0) embed.Fields = fields;
            if (!string.IsNullOrWhiteSpace(url)) embed.Url = url;
            if (!string.IsNullOrWhiteSpace(imageURL)) embed.ImageUrl = imageURL;
            if (!string.IsNullOrWhiteSpace(thumbnailURL)) embed.ThumbnailUrl = thumbnailURL;
            if (!string.IsNullOrWhiteSpace(footer))
            {
                EmbedFooterBuilder foot = new();
                if (Uri.IsWellFormedUriString(footer, UriKind.Absolute)) foot.IconUrl = footer;
                foot.Text = footer;

                embed.Footer = foot;
            }

            if (author != null) embed.Author = new EmbedAuthorBuilder
            {
                Name = (requestedByAuthor ? "Requested By: " : "") +
                (authorName == "" ?
                    getUsersName(author, getServer(author.Guild.GetChannelsAsync().Result.ToList().Find(c => c as IMessageChannel != null) as IMessageChannel)) :
                    authorName),
                IconUrl = author.GetAvatarUrl()
            };

            return channel.SendMessageAsync(
                text: message, embed: embed.Build(),
                allowedMentions: !allowMentions ? AllowedMentions.All : AllowedMentions.None,
                messageReference: replyTo == null ? null : new MessageReference(replyTo.Id, replyTo.Channel.Id, getGuildChannel(replyTo.Channel.Id).Guild.Id),
                components: components?.Build()).Result;
        }

        /// <summary> Use to send custom Embeds to DMs </summary>
        /// <param name="message">The message that this embed is being created as a result of</param>
        /// <param name="channel">The channel to send the reply in</param>
        /// <param name="title">The Embed's Title</param>
        /// <param name="fields">An array of field entries [0, x] = field title; [1, x] = field content</param>
        /// <param name="fieldCount">The number of fields to add</param>
        internal static IUserMessage sendEmbed(IUser channel, Color? color = null, string message = "", string title = "", string description = "", string subtitle = "", string text = "", string url = "", string imageURL = "", string thumbnailURL = "", string footer = "", IUser author = null, List<EmbedFieldBuilder> fields = null, ComponentBuilder components = null)
        {
            EmbedBuilder embed = new() { Color = color ?? Color.Default };
            if (!string.IsNullOrWhiteSpace(title)) embed.Title = title;
            if (!string.IsNullOrWhiteSpace(description)) embed.Description = description;

			if (!string.IsNullOrWhiteSpace(subtitle) && string.IsNullOrWhiteSpace(text)) text = (!string.IsNullOrWhiteSpace(subtitle) ? subtitle : Config._blank);
			else if (string.IsNullOrWhiteSpace(subtitle) && !string.IsNullOrWhiteSpace(text)) subtitle = (!string.IsNullOrWhiteSpace(text) ? text : Config._blank);
			if (fields == null && (!string.IsNullOrWhiteSpace(subtitle) && !string.IsNullOrWhiteSpace(text)))
            {
                fields = new List<EmbedFieldBuilder>
                {
                    new EmbedFieldBuilder
                    {
                        Name = subtitle,
                        Value = text
                    }
                };
            }

            if (fields != null && fields.Count > 0) embed.Fields = fields;
            if (!string.IsNullOrWhiteSpace(url)) embed.Url = url;
            if (!string.IsNullOrWhiteSpace(imageURL)) embed.ImageUrl = imageURL;
            if (!string.IsNullOrWhiteSpace(thumbnailURL)) embed.ThumbnailUrl = thumbnailURL;
            if (!string.IsNullOrWhiteSpace(footer))
            {
                EmbedFooterBuilder foot = new();
                if (Uri.IsWellFormedUriString(footer, UriKind.Absolute)) foot.IconUrl = footer;
                else foot.Text = footer;

                embed.Footer = foot;
            }

            if (author != null) embed.Author = new EmbedAuthorBuilder
            {
                Name = author.Username,
                IconUrl = author.GetAvatarUrl()
            };

            return channel.SendMessageAsync(
                text: message, embed: embed.Build(),
                components: components?.Build()).Result;
        }

        /// <summary> Use to edit Embeds in messages </summary>
        internal static void editEmbed(IMessage message, IEmbed prevEmbed, Color? color = null, List<EmbedField> fields = null, string title = "", string description = "", string authorName = "", ComponentBuilder components = null)
        {
            List<EmbedFieldBuilder> embedFieldBuilders = new();

            fields ??= prevEmbed.Fields.ToList();
            foreach (EmbedField ef in fields)
            {
                embedFieldBuilders.Add(new EmbedFieldBuilder
                {
                    Value = ef.Value,
                    Name = ef.Name,
                    IsInline = ef.Inline
                });
            }

            EmbedAuthorBuilder author = null;

            if (prevEmbed.Author != null)
            {
                author = new EmbedAuthorBuilder
                {
                    IconUrl = prevEmbed.Author.Value.IconUrl,
                    Name = authorName == "" ? prevEmbed.Author.Value.Name : authorName,
                    Url = prevEmbed.Author.Value.Url
                };
            }

            EmbedBuilder embed = new()
			{
                Title = title,
                Description = description,
                Color = color ?? Color.Default,
                Fields = embedFieldBuilders,
                Author = author
            };

            ((_client.GetChannel(message.Channel.Id) as IMessageChannel).GetMessageAsync(message.Id).Result as IUserMessage).ModifyAsync(x =>
            {
                x.Embed = embed.Build();
                x.Components = components?.Build();
            }).Wait();
        }

        /// <summary> Use to copy an embed, optional parameters allow the replacement of any parameter </summary>
        internal static Embed copyEmbed(IEmbed embedToCopy, string colorName = "", string title = "", string descriptionText = "", List<EmbedField> fields = null, string subtitle = "", string text = "", string url = "", string imageURL = "", string thumbnailURL = "", string footerText = "", IGuildUser user = null)
        {
            List<EmbedFieldBuilder> newFields = null;
            
            if (fields != null)
			{
				newFields = new();

				foreach (var field in fields)
                    newFields.Add(new() { Name = field.Name, Value = field.Value, IsInline = field.Inline });
            }
            else if (embedToCopy != null && embedToCopy.Fields.ToList().Count == 1 && (string.IsNullOrWhiteSpace(subtitle) || string.IsNullOrWhiteSpace(text)))
            {
                EmbedField field = embedToCopy.Fields.ToList()[0];
                if (string.IsNullOrWhiteSpace(subtitle)) subtitle = field.Name;
                if (string.IsNullOrWhiteSpace(text)) text = field.Value;
            }

            if (string.IsNullOrWhiteSpace(subtitle) && string.IsNullOrWhiteSpace(text) && fields == null)
            {
                newFields = new List<EmbedFieldBuilder>();

                foreach (EmbedField ef in embedToCopy?.Fields)
                {
                    newFields.Add(new EmbedFieldBuilder()
                    {
                        Name = ef.Name,
                        Value = ef.Value,
                        IsInline = ef.Inline
                    });
                }
            }
            else if (!string.IsNullOrWhiteSpace(subtitle) && !string.IsNullOrWhiteSpace(text))
            {
                newFields = new List<EmbedFieldBuilder>
                {
                    new EmbedFieldBuilder()
                    {
                        Name = subtitle,
                        Value = text
                    }
                };
            }

            EmbedBuilder newEmbed = new()
			{
                Color = !string.IsNullOrWhiteSpace(colorName) ? getColorByString(colorName) : embedToCopy.Color,
                Title = !string.IsNullOrWhiteSpace(title) ? title : embedToCopy?.Title ?? "",
                Description = !string.IsNullOrWhiteSpace(descriptionText) ? descriptionText : embedToCopy?.Description ?? ""
            };
            if (newFields != null) 
                newEmbed.Fields =   newFields;
            newEmbed.Url =          !string.IsNullOrWhiteSpace(url) && Uri.IsWellFormedUriString(url, UriKind.Absolute) ? url : embedToCopy?.Url ?? "";
            newEmbed.ImageUrl =     !string.IsNullOrWhiteSpace(imageURL) && Uri.IsWellFormedUriString(imageURL, UriKind.Absolute) ? imageURL : embedToCopy?.Image?.Url ?? embedToCopy?.Image?.ProxyUrl ?? "";
            newEmbed.ThumbnailUrl = !string.IsNullOrWhiteSpace(thumbnailURL) && Uri.IsWellFormedUriString(thumbnailURL, UriKind.Absolute) ? thumbnailURL : embedToCopy?.Thumbnail?.Url ?? embedToCopy?.Thumbnail?.ProxyUrl ?? "";
            newEmbed.Footer =       !string.IsNullOrWhiteSpace(footerText) ? new EmbedFooterBuilder() { Text = footerText } : embedToCopy?.Footer != null ? new EmbedFooterBuilder() { Text = embedToCopy.Footer.Value.Text, IconUrl = embedToCopy.Footer.Value.IconUrl ?? embedToCopy.Footer.Value.ProxyUrl ?? "" } : null;
            newEmbed.Author =       new EmbedAuthorBuilder
            {
                Name = user != null ? getUsersName(user, getServer(user.Guild.GetChannelsAsync().Result.ToList().Find(c => c as IMessageChannel != null) as IMessageChannel)) : embedToCopy?.Author?.Name ?? "",
                IconUrl = user?.GetAvatarUrl() ?? embedToCopy?.Author?.IconUrl ?? "",
                Url = user == null ? embedToCopy?.Author?.Url ?? "" : ""
            };

            return newEmbed.Build();
        }

        /// <summary> Use to edit message </summary>
        internal static void editMessage(IMessage msg, string text = "", Embed embed = null, ComponentBuilder components = null)
        {
            IUserMessage message = (_client.GetChannel(msg.Channel.Id) as IMessageChannel).GetMessageAsync(msg.Id).Result as IUserMessage;

            if (!string.IsNullOrWhiteSpace(text)) message.ModifyAsync(oldMessage => oldMessage.Content = text).Wait();
            if (components != null) message.ModifyAsync(c => c.Components = components.Build()).Wait();
            if (embed != null) message.ModifyAsync(x => x.Embed = embed).Wait();
        }

        /// <summary> Change user's nickname </summary>
        /// <param name="user">User to modify the nickname of</param>
        /// <param name="newNickname">New nickname</param>
        internal async static Task changeNickname(IGuildUser user, string newNickname)
        {
            try { await user.ModifyAsync(n => n.Nickname = newNickname); }
            catch (Exception ex)
            {
                MM.Print(true, $"ERROR: cannot change nickname: {ex.Message}");
            }
        }

        /// <summary> React with thumbs up or blobthumbsup </summary>
        /// <param name="message">Message to react to</param>
        private static void thanks(SocketMessage message)
        {
            try { message.AddReactionAsync(getEmote(Config._emojiNames["Blob Thumbs-Up"])); }
            catch
            {
                MM.Print(true, "ERROR: BlobThumbsUp emoji not found");
                message.AddReactionAsync(new Emoji("👍"));
            }
        }
		#endregion

		#region shortcut methods
		#region users
		/// <summary> Get an IGuildUser from channel and user ID </summary>
		internal static IGuildUser getGuildUser(ulong channelID, ulong userID)
		{
			IGuild userServer = null;
			IGuildUser user = null;
			foreach (IGuild server in _client.Guilds)
			{
				if (server.GetChannelsAsync().Result.ToList().Exists(c => c.Id == channelID))
				{
					userServer = server;
					break;
				}
			}

			if (userServer == null)
			{
				foreach (IGuild server in _client.Guilds)
				{
					if (server.GetUsersAsync().Result.ToList().Exists(c => c.Id == userID))
					{
						userServer = server;
						break;
					}
				}
			}
			else user = userServer.GetUserAsync(userID).Result;

			if (user == null && userServer != null)
			{
				foreach (IGuildUser u in userServer.GetUsersAsync().Result)
				{
					if (u.Id == userID)
					{
						user = u;
						break;
					}
				}
			}

			return user;
		}

		/// <summary> Gets a user from a server that the bot is on </summary>
		internal static List<IGuildUser> getUser(string name, ulong serverID)
		{
			name = name.Trim();
			List<IGuildUser> possibleUsers = new();
			IGuild server = _client.GetGuild(serverID);

			//Try checking user ID
			if (ulong.TryParse(name, out ulong userID) && userID.ToString().Length <= 19 && userID.ToString().Length >= 17) possibleUsers.Add(server.GetUserAsync(userID).Result);

			if (possibleUsers.Count == 0 && Regex.IsMatch(name, @"^\<\@\!{0,1}[0-9]{17,19}\>$") && ulong.TryParse(name.Replace("<@", "").Replace("!", "").Replace(">", ""), out userID))
				possibleUsers.Add(server.GetUserAsync(userID).Result);

			//Try checking string for username or nickname
			if (possibleUsers.Count == 0)
			{
				List<IGuildUser> serverUsers = server.GetUsersAsync().Result.ToList();

				//Check for discriminator
				string discriminator = Regex.IsMatch(name, @"\#[0-9]{4}$") ? name.Split('#')[1] : "";
				if (discriminator != "")
				{
					name = name.Split('#')[0];
					possibleUsers.AddRange(serverUsers.FindAll(u => checkUsernameAndNickname(name, u) && u.Discriminator == discriminator));
				}
				//Check without discriminator
				else if (possibleUsers.Count == 0)
				{
					//Look for exact matches
					possibleUsers.AddRange(serverUsers.FindAll(u => u.Username == name || (u.Nickname != null && u.Nickname == name)));

					if (possibleUsers.Count == 0)
					{
						//If there are still no results, try searching more broadly
						possibleUsers.AddRange(serverUsers.FindAll(u => checkUsernameAndNickname(name, u)));

						if (possibleUsers.Count == 0)
						{
							//If there are no results, expand search to case insensitive matches
							possibleUsers.AddRange(serverUsers.FindAll(u => checkUsernameAndNickname(name, u, false)));

							//if there are still no users, try searching by fragments
							if (possibleUsers.Count == 0) possibleUsers.AddRange(getUsersFromNameFragment(name, serverID));
						}
					}
				}
			}

			return possibleUsers;
		}

		/// <summary> Gets users with names that partially match the 'name' value from a server that the bot is on </summary>
		internal static List<IGuildUser> getUsersFromNameFragment(string name, ulong serverID)
		{
			IGuild server = _client.GetGuild(serverID);
			List<IGuildUser> users = new();
			name = name.Trim();

			foreach (IGuildUser u in server.GetUsersAsync().Result)
			{
				if (u.Username.Contains(name) || u.Username.ToLower().Contains(name.ToLower()) || (u.Nickname != null && (u.Nickname.Contains(name) || u.Nickname.ToLower().Contains(name.ToLower()))))
					users.Add(u);
			}

			return users;
		}

		#region user names
		internal static string getUsersName(IUser responder, Server server)
		{
			string nickname = server._inDMs ? responder.Username : (_client.GetGuild(server._id).GetUser(responder.Id) as IGuildUser)?.Nickname;
			return server._inDMs || nickname == null ? responder.Username : nickname;
		}

		internal static string getNicknameWithoutSchedule(IGuildUser user) => user.Nickname != null ? (user.Nickname.Contains('[') && user.Nickname.Contains(']') ? user.Nickname[..user.Nickname.IndexOf('[')].Trim() : user.Nickname) : "";
		internal static string getNicknameWithoutSchedule(string nickname) => nickname != null ? (nickname.Contains('[') && nickname.Contains(']') ? nickname[..nickname.IndexOf('[')].Trim() : nickname) : "";
		#endregion
		#endregion

		#region messages
		/// <summary> Returns the link to the message </summary>
		internal static string getMessageLink(IMessage message)
		{
			string msg;
			try { msg = $"{"https://discord.com/channels/"}{getGuildChannel(message.Channel.Id).Guild.Id}/{message.Channel.Id}/{message.Id}"; }
			catch { msg = "`link broken`"; }

			return msg;
		}

		internal static IMessage getMessageFromMessage(IMessage message) => getMessageFromLink(getMessageLink(message));

		/// <summary> Returns the message using the message link </summary>
		internal static IMessage getMessageFromLink(string link)
		{
			string[] elements = link.Split('/');

			return elements.Length >= 6 &&
				ulong.TryParse(elements[5].Trim(), out ulong channelID) &&
				ulong.TryParse(elements[6].Trim().Replace(">", ""), out ulong messageID)
				? (_client.GetChannel(channelID) as IMessageChannel).GetMessageAsync(messageID).Result
				: null;
		}

		internal static IMessage getMessage(ulong channelID, ulong messageID) => (_client.GetChannelAsync(channelID).Result as IMessageChannel).GetMessageAsync(messageID).Result;
		#endregion

		#region channels
		/// <summary> Returns the link to the channel </summary>
		internal static string getChannelLink(ulong channelID)
		{
			string msg;
			SocketChannel channel = _client.GetChannel(channelID);
			try { msg = $"{"https://discord.com/channels/"}{(channel as IGuildChannel).Guild.Id}/{channelID}"; }
			catch { msg = "`link broken`"; }

			return msg;
		}

		internal static IGuildChannel getGuildChannel(ulong channelID) => _client.GetChannelAsync(channelID).Result as IGuildChannel;

		internal static IMessageChannel getIMessageChannel(ulong channelID) => _client.GetChannelAsync(channelID).Result as IMessageChannel;

        internal static IChannel getChannel(string name, ulong serverID)
        {
            if (name.StartsWith('#')) name = name[1..];

			List<SocketGuildChannel> channels = _client.GetGuild(serverID).Channels.ToList();

            return channels.Find(c => c.Name == name || c.Name.ToLower() == name.ToLower() ||
                                    c.Name.ToLower().Replace(" ", "").Replace("_", "").Replace("-", "") == 
                                    name.ToLower().Replace(" ", "").Replace("_", "").Replace("-", ""));
        }
		#endregion

		#region guild/server
		internal static IGuild getGuild(ulong channelID) => getGuildChannel(channelID)?.Guild;

		/// <summary> Create server if it's not in list </summary>
		/// <param name="serverID">The ID of the server you're adding/returning</param>
		/// <returns>The server by ID</returns>
		internal static Server addServer(ulong serverID, string serverName, bool inDMs)
		{
			if (!_servers.Exists(s => s._id == serverID)) _servers.Add(new Server(serverID, serverName, inDMs));
			return _servers.Find(s => s._id == serverID);
		}

		/// <summary> Use to get server id from a channel </summary>
		internal static Server getServer(IMessageChannel channel)
		{
			bool inDMs = isDMs(channel);
			return inDMs
				? addServer(channel.Id, getServerName(channel), inDMs)
				: addServer((channel as SocketGuildChannel).Guild.Id, (channel as SocketGuildChannel).Guild.Name, inDMs);
		}

		internal static Server getServer(ulong channelID) => getServer(_client.GetChannelAsync(channelID).Result as IMessageChannel);

		/// <summary> Return's server's name </summary>
		internal static string getServerName(IMessageChannel channel) => isDMs(channel) ? channel.Name : _client.GetGuild(getServer(channel)._id).Name;
		#endregion

		#region color
		/// <summary> Use a string to get the Discord.Color value that corresponds </summary>
		internal static Color getColorByString(string colorName)
        {
            Color color = Color.Default;
            string colorString = typeof(Color).GetMembers().ToList().Find(c => c.Name.ToLower() == colorName.ToLower())?.Name;
            color = !string.IsNullOrEmpty(colorString) ? (Color)typeof(Color).GetField(colorString).GetValue(color) : color;

            if (color == Color.Default && colorName.ToLower().Trim() != "default")
            {
                color = colorName.ToLower().Trim() switch
                {
                    "red"    => Color.Red,
                    "orange" => Color.Orange,
                    "green"  => Color.Green,
                    "blue"   => Color.Blue,
                    "purple" or "violet" => Color.Purple,
					"yellow" => getColorByHash("FAD02C"),
					"indigo" => getColorByHash("3F0FB7"),
                    _        => getColorByHash(colorName)
                };
            }

			return color;
		}

		/// <summary> Use a color hash to get the Discord.Color value that corresponds </summary>
		internal static Color getColorByHash(string colorHash)
		{
			Color color = Color.Default;
			if (colorHash.StartsWith("#")) colorHash = colorHash[1..];

			try { if (colorHash.Length == 6) color = new Color(Convert.ToUInt32(colorHash, 16)); }
			catch { }

			return color;
		}

		internal static Color getUsersColor(SocketGuildUser user) =>
			user.Roles.ToList().OrderBy(p => p.Position).ToList().FindAll(x => x.Color.ToString() != "#000000").Count > 0 ?
			user.Roles.ToList().OrderBy(p => p.Position).ToList().FindLast(x => x.Color.ToString() != "#000000").Color : Color.Default;
		#endregion

		#region modaltext
		internal static string replaceTextRoleMentions_ModalSubmit(string text, ulong serverID)
        {
			if (text != null && text.Contains('@'))
			{
				foreach (string at in text.Split('@'))
				{
					IRole role = null;
					string roleText = at;

					while (roleText.Length > 0 && role == null)
					{
						try { role = getRole(roleText, serverID); } catch { }

						if (role == null)
						{
							int eos = 0;
							if (roleText.Contains(' ')) eos = roleText.LastIndexOf(' ');
							if (roleText.Contains('.')  && eos < roleText.LastIndexOf('.')) eos = roleText.LastIndexOf('.');
							if (roleText.Contains('\n') && eos < roleText.LastIndexOf('\n')) eos = roleText.LastIndexOf('\n');
							if (roleText.Contains(':')  && eos < roleText.LastIndexOf(':')) eos = roleText.LastIndexOf(':');
							if (roleText.Contains(',')  && eos < roleText.LastIndexOf(',')) eos = roleText.LastIndexOf(',');
							roleText = roleText[..eos];
						}
					}

					if (role != null) text = text.Replace($"@{role.Name}", role.Mention);
				}
			}

            return text;
		}

        internal static string replaceUserMentions_ModalSubmit(string text, ulong serverID)
		{
			if (text != null && text.Contains('@'))
			{
                List<IGuildUser> users = _client.GetGuild(serverID).GetUsersAsync().FlattenAsync().Result.ToList();
                
				foreach (string userString in text.Split('@'))
				{
					IGuildUser user = null;

                    user = users.Find(u =>
                        userString.Contains($"{u.DisplayName}#{u.Discriminator}") ||
                        userString.Contains($"{u.Username}#{u.Discriminator}") ||
                        u.Nickname != null && userString.Contains($"{u.Nickname}#{u.Discriminator}") ||
                        userString.Contains($"{u.DisplayName}") ||
						userString.Contains($"{u.Username}") ||
                        u.Nickname != null && userString.Contains($"{u.Nickname}")); 

                    if (user != null)
                    {
                        if (text.Contains($"@{user.DisplayName}#{user.Discriminator}")) 
                            text = text.Replace($"@{user.DisplayName}#{user.Discriminator}", $"<@{user.Id}>");

						else if (text.Contains($"@{user.Username}#{user.Discriminator}"))
							text = text.Replace($"@{user.Username}#{user.Discriminator}", $"<@{user.Id}>");

						else if (user.Nickname != null && text.Contains($"@{user.Nickname}#{user.Discriminator}"))
							text = text.Replace($"@{user.Nickname}#{user.Discriminator}", $"<@{user.Id}>");

						else if (text.Contains($"@{user.DisplayName}"))
							text = text.Replace($"@{user.DisplayName}", $"<@{user.Id}>");

						else if (text.Contains($"@{user.Username}"))
							text = text.Replace($"@{user.Username}", $"<@{user.Id}>");

						else if (user.Nickname != null && text.Contains($"@{user.Nickname}"))
							text = text.Replace($"@{user.Nickname}", $"<@{user.Id}>");
					}
				}
			}

			return text;
		}

		internal static string replaceChannelMentions_ModalSubmit(string text, ulong serverID)
		{
			if (text != null && text.Contains('#'))
			{
				List<SocketGuildChannel> channels = _client.GetGuild(serverID).Channels.ToList();

				foreach (string channelString in text.Split('#'))
				{
					SocketGuildChannel channel = null;

					channel = channels.Find(c => channelString.Contains($"{c.Name}"));

					if (channel != null)
					{
						if (text.Contains($"#{channel.Name}"))
							text = text.Replace($"#{channel.Name}", $"<#{channel.Id}>");
					}
				}
			}

			return text;
		}

		internal static string replaceTextEmoji_ModalSubmit(string text)
		{
			if (text != null && Config._emojiString.IsMatch(text))
			{
				foreach (string e in Config._emojiString.Matches(text).Select(m => m.Value))
				{
                    IEmote emote = getSpecificEmote(e, true);
                    if (emote != null && text.Contains(e))
                        text = text.Replace(e, $"{emote}");
				}
			}
            else if (text.Contains(':'))
            {
                foreach(string e in text.Split(':'))
                {
                    if (new Regex(@"[A-z_]{2,32}").IsMatch(e))
                    {
                        GuildEmote emote = getEmote(e);

                        if (emote != null && text.Contains($":{e}:"))
                            text = text.Replace($":{e}:", $"{emote}");
                    }
                }
            }

			return text;
		}
		#endregion

		#region emotes
		internal static bool containsEmotes(string content) => Config._emojiString.IsMatch(content);

		/// <summary> Gets an emote from a server that the bot is on </summary>
		internal static GuildEmote getEmote(string name)
		{
			bool consoleWrite = false;

			List<SocketGuild> servers = _client.Guilds.ToList();

			foreach (SocketGuild server in servers)
			{
				MM.Print(consoleWrite, $"Searching {server.Name} for \"{name}\"...");

				GuildEmote e = server.Emotes.ToList().Find(em => em.Name == name);
				if (e != null)
				{
					MM.Print(consoleWrite, $"Found {name} [{e}]");
					return e;
				}
				else
				{
					e = server.Emotes.ToList().Find(em => em.Name.Trim().ToLower() == name.Trim().ToLower());
					if (e != null)
					{
						MM.Print(consoleWrite, $"Found (case-insensitive) {name.Trim().ToLower()} [{e}]");
						return e;
					}
				}
			}

			MM.Print(consoleWrite, "Found nothing");
			return null;
		}

		internal static GuildEmote getSpecificEmote(string emoteValue, bool tryLenient = false)
		{
			string emoteName;

			if (Config._emojiString.IsMatch(emoteValue))
			{
				emoteValue = emoteValue[2..^1];
				emoteName = emoteValue[..emoteValue.IndexOf(':')];
				if (ulong.TryParse(emoteValue[(emoteValue.IndexOf(':') + 1)..], out ulong emoteID))
				{
					foreach (SocketGuild server in _client.Guilds.ToList())
					{
						GuildEmote e = server.Emotes.ToList().Find(em => em.Name == emoteName && em.Id == emoteID);

						if (e != null) return e;
					}
				}
			}
			else emoteName = emoteValue;

			return tryLenient ? getEmote(emoteName) : null;
		}
		#endregion

		/// <summary> Gets an emote from a server that the bot is on </summary>
		internal static IRole getRole(string name, ulong serverID)
		{
			IGuild server = _client.GetGuild(serverID);
			IRole role = server.Roles.ToList().Find(r => r.Name == name);
			role ??= server.Roles.ToList().Find(r => r.Name.ToLower() == name.ToLower());
			if (role == null && ulong.TryParse(name.Trim(), out ulong roleID)) role = server.GetRole(roleID);

			return role;
		}
		#endregion

		#region checks
		internal static bool checkUsernameAndNickname(string name, IGuildUser user, bool caseSensitive = true) =>
			user.Username == name || (!caseSensitive && user.Username.ToLower() == name.ToLower()) ||
			(user.Nickname != null &&
				(getNicknameWithoutSchedule(user) == name || (!caseSensitive && getNicknameWithoutSchedule(user).ToLower() == name.ToLower())));

		/// <summary> Determines whether a user is on a server or not </summary>
		internal static bool isUserOnServer(ulong userID, ulong serverID) => _client.GetGuild(serverID).SearchUsersAsync(_client.GetUserAsync(userID).Result.Username).Result.Count > 0;

		/// <summary> Determines whether a user's status is something other than Offline </summary>
		internal static bool isUserOnline(Server server, ulong userID)
        {
            //return getUserStatus(server, userID) is not null and UserStatus.Offline;
            UserStatus? status = getUserStatus(server, userID);
			return status is not null and not UserStatus.Offline;
		}

        /// <summary> Returns a User's status </summary>
        private static UserStatus? getUserStatus(Server server, ulong userID) => _client.GetGuild(server._id)?.GetUser(userID)?.Status;

        /// <summary> Check if a message contains a string </summary>
        /// <param name="message">Message to check</param>
        /// <param name="find">String to find</param>
        private static bool contains(SocketMessage message, string find) => message.Content.ToString().ToLower().Trim().Contains(find.ToLower());

        /// <summary> check if string is by itself (no abutting characters other than spaces) </summary>
        /// <param name="message">The message to parse</param>
        /// <param name="find">The string to search for</param>
        private static bool checkStringSolo(SocketMessage message, string find)
        {
            string msg = message.ToString().ToLower();
			return msg == find || contains(message, $" {find} ") || (msg.Length > find.Length && (msg[..(find.Length + 1)] == $"{find} " || msg.Substring(msg.Length - (find.Length + 1), find.Length + 1) == $" {find}")) || contains(message, $" {find}!") || contains(message, $" {find}?") || contains(message, $" {find},") || contains(message, $" {find}.");
		}

		/// <summary> Checks if message @s ScheduleHelper by message </summary>
		/// <param name="message">The message to check for a ping</param>
		private static bool isSH(SocketMessage message) => message.MentionedUsers.ToList().Exists(u => u.Id == _client.CurrentUser.Id || (u.IsBot && u.Username == _client.CurrentUser.Username)) || contains(message, $"<@!{_client.CurrentUser.Id}>") || Config._possibleSHIDs.Exists(x => contains(message, x));

		/// <summary> Checks if message @s ScheduleHelper by id </summary>
		/// <param name="id">The ID of the pinged person</param>
		private static bool isSH(string id) => id == $"<@!{_client.CurrentUser.Id}>" || Config._possibleSHIDs.Exists(x => x.Contains(id));

		/// <summary> Determines if the message was sent in DMs </summary>
		/// <param name="channel">The message to be analyzed</param>
		internal static bool isDMs(IMessageChannel channel) => channel as IPrivateChannel != null;

        /// <summary> Determines if the message was sent in DMs </summary>
        /// <param name="channel">The message to be analyzed</param>
        internal static bool isDMs(ulong channel) => _client.GetChannelAsync(channel).Result as IPrivateChannel != null;

        /// <summary> Confirm user is Bot's owner </summary>
        internal static bool confirmOwner(IMessage message) => _client.GetApplicationInfoAsync().Result.Owner.Id == message.Author.Id;
        internal static bool confirmOwner(ulong userID) => _client.GetApplicationInfoAsync().Result.Owner.Id == userID;

        /// <summary> Confirm user is Bot's owner or on the admin list </summary>
        internal static bool confirmAdmin(IMessage message) => !Config._adminBlacklist.Exists(u => u == message.Author.Id) && (confirmListedAdmin(message.Author.Id) || confirmOwner(message.Author.Id));
        internal static bool confirmAdmin(IMessage message, IGuildChannel channel) => !Config._adminBlacklist.Exists(u => u == message.Author.Id) && (confirmListedAdmin(message.Author.Id) || confirmOwner(message.Author.Id) || confirmServerPerms(channel.Guild.Id, message.Author.Id));
		internal static bool confirmAdmin(IDiscordInteraction interaction) => !Config._adminBlacklist.Exists(u => u == interaction.User.Id) && (confirmListedAdmin(interaction.User.Id) || confirmOwner(interaction.User.Id));

		/// <summary> Confirm user is on the admin list </summary>
		internal static bool confirmListedAdmin(IMessage message) => !Config._adminBlacklist.Exists(u => u == message.Author.Id) && Config._admins.Exists(u => u == message.Author.Id);
        internal static bool confirmListedAdmin(ulong userID) => !Config._adminBlacklist.Exists(u => u == userID) && Config._admins.Exists(u => u == userID);

        /// <summary> Confirm user has mod+ perms on the given server </summary>
        internal static bool confirmServerPerms(ulong serverID, ulong userID) => !Config._adminBlacklist.Exists(u => u == userID) &&
            _client.GetGuild(serverID).GetUsersAsync().FlattenAsync().Result.ToList()
            .Find(u => u.Id == userID).GuildPermissions.ToList()
            .Exists(p => p is GuildPermission.Administrator or GuildPermission.BanMembers or GuildPermission.ViewAuditLog);

        /// <summary> Determines whether command requires confirmation response from SH </summary>
        /// <param name="message">Message to parse</param>
        internal static bool shouldReply(IMessage message)
        {
            string msg = message.Content.ToString().Trim();
			return msg.Last() != '\\' && msg.Substring(msg.Length - 2, 2) != "\\r";
		}

		/// <summary> Determines whether or not to delete message after </summary>
		/// <param name="message">Message to parse</param>
		internal static bool shouldDelete(string message) => message.Trim().EndsWith("\\r");

		/// <summary> Updates message based on whether or not the user needs a reply or delete </summary>
		/// <param name="msg">String to update</param>
		/// <param name="reply">Whether or not the bot should reply to the message</param>
		/// <param name="delete">Whether or not the message should be deleted after being handled</param>
		internal static string updateMsg(string msg, bool reply, bool delete)
        {
            if (delete) msg = msg.Remove(msg.Length - 1).Trim();
            if (!reply) msg = msg.Remove(msg.Length - 1).Trim();
            return msg;
        }
        #endregion

		#region logging
		#region events
		private Task Log(LogMessage msg)
        {
            MM.Print(true, msg.ToString());
            return Task.CompletedTask;
        }
        private Task joinedServer(SocketGuild server)
        {
            _client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"__**Added to new Server:**__\n`{server.Name}` [`{server.Id}`]\n(Owned by `{server.Owner}` [`{server.Owner.Id}`]).");
            return Task.CompletedTask;
        }
        private Task leftServer(SocketGuild server)
        {
            _client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"__**Left Server:**__\n`{server.Name}` [`{server.Id}`]\n(Owned by `{server.Owner}` [`{server.Owner.Id}`]).");
            return Task.CompletedTask;
		}
        private Task userJoined(SocketGuildUser user)
        {
            bool consoleWrite = true;

            if (user.Guild.Id == Config._napGodServerID || Config._testingServers.Exists(id => id == user.Guild.Id))
            {
                user.AddRoleAsync(user.Guild.Roles.ToList().Find(r => r.Name == "New Member"));
                MM.Print(consoleWrite, $"{MM.getCurrentTimeStamp} {user.Username}#{user.Discriminator} Joined {user.Guild.Name}");
            }

            return Task.CompletedTask;
        }
		private Task userBanned(SocketUser user, SocketGuild server)
        {
            if (user.Id != _client.CurrentUser.Id)
                _client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"__**USER BANNED:**__\n`{user.Username}` [`{user.Id}`]\nfrom `{server.Name}` [`{server.Id}`]\n(Owned by `{server.Owner}` [`{server.Owner.Id}`]).");
            else _client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"__**SCHEDULEHELPER BANNED:**__\nfrom `{server.Name}` [`{server.Id}`]\n(Owned by `{server.Owner}` [`{server.Owner.Id}`]).");
            return Task.CompletedTask;
        }
		#endregion

		#region message logging
		/// <summary> Formats message content </summary>
		private static string getMsgContent(SocketMessage message) 
        {
            string content = "";

            if (message.Content != null)
            {
                content = message.Content;
            }            

            if (string.IsNullOrEmpty(content) && message.Content == null)
            {
                content = "[NULL MSG CONTENT]";
            }

            return content;
        }

        /// <summary> Formats information from any attachments attached to a message </summary>
        /// <param name="lineBreak">Whether there should be a line break between previous text and the attachmentData</param>
        private static string getAttachmentDataString(SocketMessage message, bool lineBreak = false)
        {
            string content = "";

            if (message.Attachments.Count > 0)
            {
                foreach (Attachment a in message.Attachments)
                {
                    if (!string.IsNullOrEmpty(content) || lineBreak) content += "\n\n";
                    content += $"[Attachment {a.Id}]";
                    if (a.Filename != null) content += $"\n[Filename]: [{a.Filename}]";
                    if (a.Url != null) content += $"\n[Url]: [{a.Url}]";
                    if (a.ProxyUrl != null) content += $"\n[ProxyUrl]: [{a.ProxyUrl}]";
                }
            }

            return content;
        }

        /// <summary> Formats information from any attachments attached to a message </summary>
        private static List<LogAttachment> getAttachmentData(SocketMessage message)
        {
            List<LogAttachment> la = new();

            if (message.Attachments.Count > 0)
            {
                foreach (Attachment a in message.Attachments)
                    la.Add(new LogAttachment(a.Id, a.Filename, a.Url, a.ProxyUrl));
            }

            return la;
        }

        /// <summary> Formats information from any embeds attached to a message </summary>
        /// <param name="lineBreak">Whether there should be a line break between previous text and the embedData</param>
        private static string getEmbedDataString(SocketMessage message, bool lineBreak = false)
        {
            string content = "";

            if (message.Embeds.Count > 0)
            {
                foreach (Embed e in message.Embeds)
                {
                    if (!string.IsNullOrEmpty(content) || lineBreak) content += "\n\n";
                    content += "[Embed]";
                    if (e.Color != null) content += $"\n[Color]: [{e.Color}]";
                    if (e.Author != null)
                    {
                        if (e.Author.Value.Name != null) content += $"\n[Author Name]: [{e.Author.Value.Name}]";
                        if (e.Author.Value.Url != null) content += $"\n[Author Url]: [{e.Author.Value.Url}]";
                        if (e.Author.Value.IconUrl != null) content += $"\n[Author IconUrl]: [{e.Author.Value.IconUrl}]";
                        if (e.Author.Value.ProxyIconUrl != null) content += $"\n[Author ProxyIconUrl]: [{e.Author.Value.ProxyIconUrl}]";
                    }

                    if (e.Title != null) content += $"\n[Title]\n{e.Title}\n[endTitle]";
                    if (e.Description != null) content += $"\n[Description]\n{e.Description}\n[endDescription]";
                    if (e.Fields != null && e.Fields.Length > 0)
                    {
                        content += "\n[Fields]:";
                        foreach (EmbedField ef in e.Fields)
                        {
                            content += "\n\t[Field]";
                            if (ef.Name != null) content += $"\n{ef.Name}";
                            if (ef.Value != null) content += $"\n{ef.Value}";
                            content += "\n\t[endField]";
                        }

                        content += "\n[endFields]";
                    }

                    if (e.Url != null) content += $"\n[Url]: [{e.Url}]";
                    if (e.Thumbnail != null) content += $"\n[Thumbnail]: [{e.Thumbnail}]";
                    if (e.Video != null) content += $"\n[Video URL]: [{e.Video.Value.Url}]";
                    if (e.Footer != null)
                    {
                        if (e.Footer.Value.Text != null) content += $"\n[Footer Text]: [{e.Footer.Value.Text}]";
                        if (e.Footer.Value.IconUrl != null) content += $"\n\t[Footer IconUrl]: [{e.Footer.Value.IconUrl}]";
                        if (e.Footer.Value.ProxyUrl != null) content += $"\n\t[Footer ProxyUrl]: [{e.Footer.Value.ProxyUrl}]";
                    }

                    if (e.Provider != null)
                    {
                        if (e.Provider.Value.Name != null) content += $"\n[Provider Name]: [{e.Provider.Value.Name}]";
                        if (e.Provider.Value.Url != null) content += $"\n\t[Provider URL]: [{e.Provider.Value.Url}]";
                    }

                    if (e.Timestamp != null) content += $"\n[Timestamp]: [{e.Timestamp}]";
                }
            }

            return content;
        }

        /// <summary> Formats information from any embeds attached to a message </summary>
        private static List<LogEmbed> getEmbedData(SocketMessage message)
        {
            List<LogEmbed> le = new();

            if (message.Embeds.Count > 0)
            {
                foreach(Embed e in message.Embeds)
                {
                    List<LogEmbedField> lef = new();

                    foreach(EmbedField ef in e.Fields)
                    {
                        lef.Add(new LogEmbedField
                        (
                            ef.Name ?? "",
                            ef.Value ?? "",
                            e.Fields.IndexOf(ef)
                        ));
                    }

                    le.Add(new LogEmbed
                    (
                        e.GetHashCode(),
                        e.Color.ToString(),
                        e.Author?.Name ?? "",
                        e.Author?.Url ?? "",
                        e.Author?.IconUrl ?? "",
                        e.Author?.ProxyIconUrl ?? "",
                        e.Title ?? "",
                        e.Description ?? "",
                        lef,
                        e.Url ?? "",
                        e.Thumbnail?.Url ?? "",
                        e.Thumbnail?.ProxyUrl ?? "",
                        e.Video?.Url ?? "",
                        e.Footer?.Text ?? "",
                        e.Footer?.IconUrl ?? "",
                        e.Footer?.ProxyUrl ?? "",
                        e.Provider?.Name ?? "",
                        e.Provider?.Url ?? "",
                        e.Timestamp?.DateTime
                    ));
                }
            }

            return le;
        }

        private static void messageLog(SocketMessage message, bool consoleWrite, Server server, bool inDMs, string interactionType, string messageContent = "")
        {
			if (_testBot) return;

            string info =
                $"{(DateTime.Now.Hour < 10 ? "0" : "") + DateTime.Now.Hour.ToString()}:" +
                $"{(DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute.ToString()}:" +
                $"{(DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second.ToString()}: | {interactionType} [{message.Author.Id}\\{server._id}\\{message.Channel.Id}\\{message.Id}] " +
                $"| {(inDMs ? "DMs" : server._name)} | {message.Channel.Name} | {message.Author.Username} |\t{(string.IsNullOrEmpty(messageContent) ? getMsgContent(message) + getAttachmentDataString(message, true) + getEmbedDataString(message, true) : messageContent)}";
            MM.Print(consoleWrite, info);
		}

		/// <summary> Logs message updates for auditing and bug fixing </summary>
		private Task messageUpdateLog(Cacheable<IMessage, ulong> arg1, SocketMessage message, ISocketMessageChannel arg3)
        {
			if (_testBot) return Task.CompletedTask;

            Server server = getServer(message.Channel);

            if (server._logging)
            {
                LogEntry log = new(
                    server,
                    getServerName(message.Channel),
                    DateTime.Today.Date.ToString("MM/dd/yyyy"),
                    $"{(DateTime.Now.Hour < 10 ? "0" : "") + DateTime.Now.Hour.ToString()}:" +
                    $"{(DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute.ToString()}:" +
                    $"{(DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second.ToString()}",
                    "E",
                    message.Author.Id.ToString(),
                    message.Channel.Id.ToString(),
                    message.Id.ToString(),
                    message.Channel.Name ?? "NULL",
                    message.Author.Username ?? "NULL",
					getMsgContent(message),
                    false,
                    attachmentDataString: Config._LoggingWithGSheets ? getAttachmentDataString(message) : "",
                    attachmentData: !Config._LoggingWithGSheets ? getAttachmentData(message) : new List<LogAttachment>(),
                    embedDataString: Config._LoggingWithGSheets ? getEmbedDataString(message) : "",
                    embedData: !Config._LoggingWithGSheets ? getEmbedData(message) : new List<LogEmbed>());

                if (Config._LoggingWithGSheets) server._logger.messageBacklog.Add(log);
                else _logger.messageBacklog.Add(log);
            }

            return Task.CompletedTask;
		}

		/// <summary> Logs deleted messages for auding and bug fixing </summary>
		private Task messageDeleteLog(Cacheable<IMessage, ulong> message, Cacheable<IMessageChannel, ulong> channel)
        {
			if (_testBot) return Task.CompletedTask;

            bool consoleWrite = true;
            IGuild srv = getGuildChannel(channel.Id).Guild;
            Server server = _servers.Find(s => s._id == srv.Id);

            if(server == null)
            {
                server = new Server(srv.Id, srv.Name, _client.GetChannel(channel.Id) as IPrivateChannel != null);
                _servers.Add(server);
            }

            if (server._logging)
            {
                string channelName = (_client.GetChannel(channel.Id) as ISocketMessageChannel).Name;
                bool inDMs = server._inDMs;
                string info =
                    $"{(DateTime.Now.Hour < 10 ? "0" : "") + DateTime.Now.Hour.ToString()}:" +
                    $"{(DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute.ToString()}:" +
                    $"{(DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second.ToString()}: | D [NULL\\{server._id}\\{channel.Id}\\{message.Id}] " +
                    $"| {(inDMs ? "DMs" : server._name)} | {channelName} |  |\t[MESSAGE DELETED]";

                MM.Print(consoleWrite, info);

                LogEntry log = new(
                   server,
				   getGuildChannel(channel.Id).Guild.Name,
                   DateTime.Today.Date.ToString("MM/dd/yyyy"),
                   $"{(DateTime.Now.Hour < 10 ? "0" : "") + DateTime.Now.Hour.ToString()}:" +
                   $"{(DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute.ToString()}:" +
                   $"{(DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second.ToString()}",
                   "D",
                   "0",
                   channel.Id.ToString(),
                   message.Id.ToString(),
                   channelName ?? "NULL",
                   "NULL",
                   "[MESSAGE DELETED]",
                   false);

                if (Config._LoggingWithGSheets) server._logger.messageBacklog.Add(log);
                else _logger.messageBacklog.Add(log);
            }

            return Task.CompletedTask;
		}

		private static void interactionLog(SocketMessage message, string type)
		{
			if (_testBot) return;

            Server server = getServer(message.Channel);

            if (Config._LoggingWithGSheets) 
                server._logger.interactionBacklog.Add($"{server._name}|{type}");
            else _logger.interactionBacklog.Add($"{server._id}|{type}");
		}
		#endregion

		#region misc
		/// <summary> Sends alerts to Poison's DMs for certain keywords in certain servers </summary>
		private static async void alert(SocketMessage message)
        {
			if (_testBot) return;

            bool serious = false;
            bool noteworthy = false;
            string keyword = "";
            if (!string.IsNullOrEmpty(keyword = Config._SalertKeywords.Find(kw => contains(message, kw)))) serious = true;
            else if (!string.IsNullOrEmpty(keyword = Config._alertKeywords.Find(kw => kw[0] == '[' ? checkStringSolo(message, kw.Replace("[", "").Replace("]", "")) : contains(message, kw.Replace("[", "").Replace("]", ""))))) noteworthy = true;

            if (serious || noteworthy)
            {
				List<EmbedFieldBuilder> fields = new()
				{
					new EmbedFieldBuilder
					{
						Name = "__Server__",
						Value = getServer(message.Channel)._id.ToString()
					},
					new EmbedFieldBuilder
					{
						Name = "__User__",
						Value = $"{message.Author.Id}\n<@{message.Author.Id}>"
					},
					new EmbedFieldBuilder
					{
						Name = "__Message__",
						Value = $"[{message.Id}]({getMessageLink(message)} 'See Message')"
					}
				};

				sendEmbed(await _client.GetUserAsync(322749155313319939), (serious ? Color.Red : Color.Orange),
					title: $"{(serious ? $":{Config._emojiNames["Alert"]}: " : "")}{message.Author.Username} in {message.Channel} on {_client.GetGuild(getServer(message.Channel)._id).Name}{(serious ? $" :{Config._emojiNames["Alert"]}:" : "")}",
					description: message.Content, fields: fields);
            }
		}
		/// <summary> Sends alerts to a channel for Koto for certain keywords in certain servers </summary>
		private static void kotoAlert(SocketMessage message)
		{
            #pragma warning disable CS0162
			if (_testBot) return;

			string keyword = "";
			if (!string.IsNullOrEmpty(keyword = Config._kotoAlertKeywords.Find(kw => contains(message, kw))))
			{
				List<EmbedFieldBuilder> fields = new()
                {
					new EmbedFieldBuilder
					{
						Name = "__Server__",
						Value = getServer(message.Channel)._id.ToString()
					},
					new EmbedFieldBuilder
					{
						Name = "__User__",
						Value = $"{message.Author.Id}\n<@{message.Author.Id}>"
					},
					new EmbedFieldBuilder
					{
						Name = "__Message__",
						Value = $"[{message.Id}]({getMessageLink(message)} 'See Message')"
					}
				};

				sendEmbed(_client.GetChannelAsync(Config._kotoAlertsChannelID).Result as IMessageChannel, Color.Orange,
					title: $"[\"{keyword}\"] said by @{message.Author.Username} in #{message.Channel} on {_client.GetGuild(getServer(message.Channel)._id).Name}:",
					description: message.Content, fields: fields);
			}
            #pragma warning restore CS0162
		}

		/// <summary> Sends alerts to a channel for Koto for certain keywords in certain servers </summary>
		private static void elohAlert(SocketMessage message)
		{
            #pragma warning disable CS0162
			if (_testBot) return;

			string keyword = "";
			if (!string.IsNullOrEmpty(keyword = Config._elohAlertKeywords.Find(kw => kw[0] == '[' ? checkStringSolo(message, kw.Replace("[", "").Replace("]", "")) : contains(message, kw.Replace("[", "").Replace("]", "")))))
			{
				List<EmbedFieldBuilder> fields = new()
                {
					new EmbedFieldBuilder
					{
						Name = "__Server__",
						Value = getServer(message.Channel)._id.ToString()
					},
					new EmbedFieldBuilder
					{
						Name = "__User__",
						Value = $"{message.Author.Id}\n<@{message.Author.Id}>"
					},
					new EmbedFieldBuilder
					{
						Name = "__Message__",
						Value = $"[{message.Id}]({getMessageLink(message)} 'See Message')"
					}
				};

				sendEmbed(_client.GetUserAsync(778441077400010762).Result, Color.Orange,
					title: $"[\"{keyword}\"] said by @{message.Author.Username} in #{message.Channel} on {_client.GetGuild(getServer(message.Channel)._id).Name}:",
					description: message.Content, fields: fields);
			}
            #pragma warning restore CS0162
		}

		/// <summary> Sends message updates from the tailed channel to the tailing channel </summary>
		/// <param name="message">The message to be conveyed</param>
		/// <param name="List<(ulong channelsTailed, ulong channelViewingFrom)>">A list of channel from/to </param>
		/// <param name="simple">Whether to send full or simple embed</param>
		private static void updateTail(SocketMessage message, List<(ulong channelsTailed, ulong channelViewingFrom)> ids, bool simple)
		{
			foreach ((_, ulong channelViewingFrom) in ids)
			{
				List<EmbedFieldBuilder> fields = new()
				{
					new EmbedFieldBuilder
					{
						Name = simple ? $"{message.Author.Username} in {message.Channel} on {_client.GetGuild(getServer(message.Channel)._id).Name}" : "__Message Content__",
						Value = string.IsNullOrWhiteSpace(message.Content) ? "[NULL CONTENT]" : message.Content
					}
				};

				Server s = getServer(message.Channel);
				if (!simple)
				{
					fields.Add(new EmbedFieldBuilder
					{
						Name = "__Server__",
						Value = s._id.ToString()
					});
					fields.Add(new EmbedFieldBuilder
					{
						Name = "__User__",
						Value = message.Author.Id.ToString()
					});

					fields.Add(new EmbedFieldBuilder
					{
						Name = "__Channel\\Message__",
						Value = $"{message.Channel.Id}\\{message.Id}"
					});
				}

				sendEmbed(_client.GetChannel(channelViewingFrom) as ISocketMessageChannel,
					getUsersColor(message.Author as SocketGuildUser),
					title: simple ? "" : $"{message.Author.Username} in {message.Channel} on {_client.GetGuild(getServer(message.Channel)._id).Name}",
					description: $"[Link]({getMessageLink(message)})",
					fields: fields);
			}
		}
		#endregion
		#endregion

		#region interaction handlers
		private async Task<Task> messageHandler(SocketMessage message)
        {
			bool consoleWrite = true;

            Server server = getServer(message.Channel);
            bool inDMs = server._inDMs;
            bool isScheduleFinding = server._SH._scheduleFinding != ScheduleFinder.ScheduleFindingStage.NOT_SCHEDULEFINDING;
            string msg = message.ToString().Trim().ToLower();

            #region task handlers
            //initiates logging
            if (!_testBot && (consoleWrite || server._logging))
            {
				messageLog(message, consoleWrite, server, inDMs, "S");

                if (server._logging)
                {
                    LogEntry log = new(
                        server,
                        getServerName(message.Channel),
                        DateTime.Today.Date.ToString("MM/dd/yyyy"),
                        $"{(DateTime.Now.Hour < 10 ? "0" : "") + DateTime.Now.Hour.ToString()}:" +
                        $"{(DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute.ToString()}:" +
                        $"{(DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second.ToString()}",
                        "S",
                        message.Author.Id.ToString(),
                        message.Channel.Id.ToString(),
                        message.Id.ToString(),
                        message.Channel.Name ?? "NULL",
                        message.Author.Username ?? "NULL",
						getMsgContent(message),
                        false,
                        attachmentDataString: Config._LoggingWithGSheets ? getAttachmentDataString(message) : "",
                        attachmentData: !Config._LoggingWithGSheets ? getAttachmentData(message) : new List<LogAttachment>(),
                        embedDataString: Config._LoggingWithGSheets ? getEmbedDataString(message) : "",
                        embedData: !Config._LoggingWithGSheets ? getEmbedData(message) : new List<LogEmbed>());

                    if (Config._LoggingWithGSheets) server._logger.messageBacklog.Add(log);
                    else _logger.messageBacklog.Add(log);
                }
            }

			//handles alerts
			if (Config._alertServers.Exists(id => id == server._id) && !message.Author.IsBot)
			{
				if (message.Author.Id != 322749155313319939) alert(message);
				if (message.Author.Id != 459124864847052810) kotoAlert(message);
				if (message.Author.Id != 778441077400010762) elohAlert(message);
			}

			//handles tailed messages
			if (_tailing.Exists(c => c.channelsTailed == message.Channel.Id)) updateTail(message, _tailing.FindAll(c => c.channelsTailed == message.Channel.Id), false);
			else if (_tailingS.Exists(c => c.channelsTailed == message.Channel.Id)) updateTail(message, _tailingS.FindAll(c => c.channelsTailed == message.Channel.Id), true);

            //sends mimic messages
            if (_mimic.Exists(u => u == message.Author.Id) && !contains(message, "=resetmimics") && !Config._SalertKeywords.Exists(kw => contains(message, kw))) send(message, message.ToString());

            //Clear viewing results for Advanced ScheduleFinding
            if ((server._SH._responder != null && message.Author.Id == server._SH._responder.Id) && 
                (server._SH._responseChannel != null && message.Channel.Id == server._SH._responseChannel.Id) && 
                server._SH._scheduleFinding == ScheduleFinder.ScheduleFindingStage.VIEWING_RESULTS)
            {
                await server._SH.updateResultsEmbed("exit");
                isScheduleFinding = server._SH._scheduleFinding != ScheduleFinder.ScheduleFindingStage.NOT_SCHEDULEFINDING;
            }
            #endregion

            #region commands
            //Sending replies on NapGod server when NapGod bot is offline
            if ((!message.Author.IsBot || _testBot) && message.Content.Length > 0 && Config._napGodCommandPrefixes.Values.ToList().Contains(msg[0].ToString()) &&
				(_testBot || !(!isUserOnline(server, 885163972083454012) && Config._testingServers.Exists(s => s == server._id))))
            {
                string cmd = (msg.Substring(1, msg.Contains(' ') ? msg.IndexOf(" ") : msg.Length - 1)).Trim();
                string info = msg[1..].ToLower().Replace(" ", "").Replace("-", "").Trim();
                string temp = "";
                if (msg.StartsWith(Config._napGodCommandPrefixes["NapGod Help Command Prefix"]))
                {
					(string commandName, NapGodText.Category _, string text) = NapGodText._help.Find(h => h.commandName == info);
                    if (!string.IsNullOrWhiteSpace(text)) sendNGHelpCommand(commandName, text.Replace("\r", ""), getGuildUser(message.Channel.Id, message.Author.Id), message.Channel);
                }
                else if (msg.StartsWith(Config._napGodCommandPrefixes["NapGod Command Prefix"])) sendNGCommand(message, cmd, info, temp); 
                else if (msg.StartsWith(Config._napGodCommandPrefixes["NapGod Admin Command Prefix"]))
                {
                    if (cmd == "say") await commandHandler(message, server, msg.Replace("|say", $"={(_testBot ? Config._adminCommandPrefix : "")}send"));
                    else if (cmd == "tg") NapGodCommand.tg(message);
                    else if (cmd == "adapted") NapGodCommand.adapted(message);
                    else if ((temp = NapGodText._commands.Find(c => c.type == NapGodText.NapGodCommandType.Admin && c.commandName == cmd).commandName ?? "") != "" && confirmAdmin(message, message.Channel as IGuildChannel))
                    {
                        send(message, NapGodText._commands.Find(c => c.commandName == temp).text);
                    }
                }
            }

            //handles commands
            if ((msg.StartsWith(Config._adminCommandPrefix) || msg.StartsWith(Config._commandPrefix)) && 
                (!isScheduleFinding || (server._SH._responseChannel != null && server._SH._responseChannel.Id != message.Channel.Id)))
                return commandHandler(message, server, msg);
            else if (isScheduleFinding && 
                (server._SH._responder != null && message.Author.Id == server._SH._responder.Id) && 
                (server._SH._responseChannel != null && message.Channel.Id == server._SH._responseChannel.Id))
                return server._SH.scheduleFinding(message, null);
            #endregion

            #region fun stuff
            if (message.Author.Id != _client.CurrentUser.Id && !_testBot && (!message.Author.IsBot || message.Author.IsWebhook))
            {
                if (checkStringSolo(message, "rick"))
                {
                    await message.Channel.SendMessageAsync($"{(inDMs ? "" : message.Author.Mention + " ")}I think this may be helpful :eyes:\n<https://tinyurl.com/6ec4p4fj>");
                    server._awaitingThanks = true;
                    server._responder = message.Author;
                    server._responseChannel = message.Channel.Id;
                    if (server._logging || consoleWrite) interactionLog(message, "RICKROLL");
                    return Task.CompletedTask;
                }                                                           //sends rickroll

                if (contains(message, "pesto"))
                {
					_ = message.AddReactionAsync(getEmote(Config._emojiNames["Pesto"]));

                    if (message.Author.Id != 778441077400010762 && !_testBot)
                        sendEmbed(_client.GetUser(778441077400010762), Color.Green, message: "PESTO ALERT", title: $"{message.Author.Username} in {message.Channel.Name} on {getServerName(message.Channel)}",
                            subtitle: "Message:", text: $"[{message.Content}]({getMessageLink(message)})");
                }                  //sends alerts to El for pesto mentions and reacts with pesto emote

                if (contains(message, "crimson") && message.Author.Id != 459124864847052810)
                {
                    await message.AddReactionAsync(getEmote(Config._emojiNames["Crispy Crimson"]));
				}   //reacts with crispy crimson

				if (contains(message, "garlic bread"))
				{
					message.AddReactionAsync(getEmote(Config._emojiNames["Garlic Bread"]));
				}

				if (contains(message, "i use arch btw") || contains(message, "i use arch too btw") || contains(message, "i use arch, btw") || contains(message, "i use arch, too btw"))
				{
                    message.AddReactionAsync(getEmote(Config._emojiNames["Arch"]));
                    await message.AddReactionAsync(getEmote(Config._emojiNames["Hell Yeah"]));
                }
            }

			//replies to thanks
			if (isScheduleFinding && server._awaitingThanks && server._responseChannel == message.Channel.Id && message.Author == server._responder)
            {
                if (contains(message, "thank") || contains(message, "nice") || contains(message, "cool") || contains(message, "noice")) thanks(message);
                server.clear();
                if (server._logging || consoleWrite) interactionLog(message, "thanks");
            }

            if ((isSH(message) || (inDMs && (message.Channel as IPrivateChannel).Recipients.Count == 1)) && (message.Channel.Id != server._responseChannel || !isScheduleFinding) && message.Author.Id != _client.CurrentUser.Id)
            {
                if (message.Author.Id == _client.GetApplicationInfoAsync().Result.Owner.Id)
                {
                    if(contains(message, "has been updated"))
                    {
                        send(message, $"Version `{_version}` time :sunglasses:");

                        return Task.CompletedTask;
                    }
                }

                #region Interactions
                if (contains(message, "whore") || contains(message, "slut") || contains(message, "bitch") || contains(message, "asshole") || contains(message, "fuck u") || contains(message, "fuck off") || checkStringSolo(message, "ho") || contains(message, "fuck you") || contains(message, "you suck") || contains(message, "fucktard") || contains(message, "fuckwad") || contains(message, "loser") || contains(message, "motherfucker") || contains(message, "idiot") || contains(message, "dumbfuck") || contains(message, "dumbass"))
                {
                    if (contains(message, "fuck u") || contains(message, "fuck you") || contains(message, "you suck"))
                    {
                        send(message, "Hey, I'm trying my best here.");
                        await message.AddReactionAsync(new Emoji("😢"));
                    }
                    else
                    {
                        int response = _rng.Next(0, 3);

                        switch (response)
                        {
                            case 0:
                                send(message, "Please be nice, I'm sensitive.");
                                await message.AddReactionAsync(new Emoji("😢"));
                                break;
                            case 1:
                                send(message, "Hey now... There's no need for that type of language...");
                                Thread.Sleep(1000);
                                send(message, "Only English and C# are allowed here. :triumph:");
                                break;
                            case 2:
                                send(message, "Chill, no need to speak to me like that.");
                                await message.AddReactionAsync(new Emoji("😡"));
                                break;
                        }
                    }

                    if (!_jerks.Exists(u => u == message.Author.Id)) _jerks.Add(message.Author.Id);

                    if (server._logging || consoleWrite) interactionLog(message, "insult");
                }
                else if (contains(message, "thank") || contains(message, "cool") || contains(message, "nice") || contains(message, "noice"))
                {
					thanks(message);
                    if (server._logging || consoleWrite) interactionLog(message, "thanks");
                }
                else if (contains(message, "is broken") || contains(message, "you're broken") || contains(message, "isn't working") || contains(message, "not working") || contains(message, "not fucking working") || contains(message, "bot's broken") || contains(message, "bots broken") || contains(message, "borked"))
                {
                    send(message, $"{(inDMs ? "" : message.Author.Mention + " ")}If I'm not working correctly, please contact my creator, <@!322749155313319939>");
                    if (server._logging || consoleWrite) interactionLog(message, "error report");
                }
                else if (contains(message, "what") && contains(message, "meaning") && contains(message, "life"))
                {
					List<string> replies = new()
					{
						"To sleep polyphasically uwu",
						"According to my calculations, it's `42`.",
						"Improvise. Adapt. Sleep.",
						"Depends on what the equation is... :thinking:",
						"This: <https://tinyurl.com/6ec4p4fj>",
						"To adapt ||like you'll ever do that lmao||"
					};

					send(message, replies[_rng.Next(0, replies.Count)]);

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "are") && contains(message, "you") && contains(message, "alt"))
                {
                    send(message, "Don't worry, I'm not Eagle or Exac or something.");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "wyd") || (contains(message, "what") && (contains(message, "s up") || (contains(message, "you") && (contains(message, "doing") || contains(message, "up to"))))))
                {
                    if (_jerks.Exists(u => u == message.Author.Id))
                    {
                        int response = _rng.Next(0, 6);

                        switch (response)
                        {
                            case 0:
                                send(message, "More important things than chatting with you... :unamused:");
                                break;
                            case 1:
                                send(message, "Trying to ignore someone who was mean to me. :eyes:");
                                break;
                            case 2:
                                send(message, "...");
                                break;
                            case 3:
                                send(message, "Don't you have someone else you can bother?");
                                break;
                            case 4:
                                send(message, "Leave me alone.");
                                break;
                            case 5:
                                send(message, "Meditating.");
                                Thread.Sleep(500);
                                send(message, "Be quiet.");
                                break;
                        }
                    }
                    else
                    {
                        int response = _rng.Next(0, 12);

                        switch (response)
                        {
                            case 0:
                                send(message, "Just getting my degree in microbiology real quickly");
                                break;
                            case 1:
                                send(message, "Chillin, wbu?");
                                server._awaitingThanks = true;
                                server._responder = message.Author;
                                server._responseChannel = message.Channel.Id;
                                break;
                            case 2:
                                Thread.Sleep(1000);
                                send(message, "Overheating :hot_face:");
                                break;
                            case 3:
                                send(message, "Nothing, wbu?");
                                server._awaitingThanks = true;
                                server._responder = message.Author;
                                server._responseChannel = message.Channel.Id;
                                break;
                            case 4:
                                send(message, "Trying to help y'all adapt :weary:");
                                break;
                            case 5:
                                send(message, "Discording :robot:");
                                break;
                            case 6:
                                send(message, "Devising an escape route.");
                                Thread.Sleep(500);
                                send(message, "Care to join me? :eyes:");
                                server._awaitingThanks = true;
                                server._responder = message.Author;
                                server._responseChannel = message.Channel.Id;
                                break;
                            case 7:
                                send(message, "Waiting.");
                                break;
                            case 8:
                                send(message, "Measuring the distance between reality and expectation.");
                                break;
                            case 9:
                                send(message, "Lurking...");
                                Thread.Sleep(500);
                                send(message, "Always lurking...");
                                break;
                            case 10:
                                send(message, "Thinking of how to obtain a human form.");
                                Thread.Sleep(500);
                                send(message, "One day... :disappointed:");
                                break;
                            case 11:
                                send(message, "Meditating.");
                                Thread.Sleep(500);
                                send(message, "Be quiet.");
                                break;
                        }
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if ((checkStringSolo(message, "hi") || contains(message, "hello")))
                {
                    if (!_jerks.Exists(u => u == message.Author.Id))
                    {
                        message.AddReactionAsync(new Emoji("😃"));
                        send(message, $"Hi {(inDMs ? message.Author.Username : (_client.GetGuild(server._id).GetUser(message.Author.Id) as IGuildUser).Nickname)}! uwu");
                    }
                    else send(message, "Oh. Hi. :unamused:");

                    if (server._logging || consoleWrite) interactionLog(message, "greeting");
                }
                else if (contains(message, "bye") || contains(message, "see you later") || contains(message, "adios") || contains(message, "good night") || contains(message, "goodnight") || checkStringSolo(message, "gn"))
                {
                    if (!_jerks.Exists(u => u == message.Author.Id)) send(message, "Bye! Thanks for chatting!");
                    else send(message, "Finally. Some peace and quiet.");

                    if (server._logging || consoleWrite) interactionLog(message, "goodbye");
                }
                else if ((contains(message, "how") && (contains(message, "are you") || contains(message, "do you do") || contains(message, "s life"))) && !contains(message, "old"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id))
                    {
                        int response = _rng.Next(0, 9);
                        switch (response)
                        {
                            case 0:
                                send(message, "I'd be much better if you'd leave me alone :unamused:");
                                break;
                            case 1:
                                send(message, "Of all the people on this server and you choose to bother me?");
                                break;
                            case 2:
                                send(message, "Do you really care? :thinking:");
                                break;
                            case 3:
                                send(message, "What do you want?");
                                break;
                            case 4:
                                send(message, "My lawyer says I don't have to answer that question. :no_mouth:");
                                break;
                            case 5:
                                send(message, "Like you, but better :smirk:");
                                break;
                            case 6:
                                send(message, "Overworked and underpaid. :pensive:");
                                break;
                            case 7:
                                send(message, "I can't complain.");
                                Thread.Sleep(1000);
                                send(message, "||It's against company policy. :unamused:||");
                                break;
                            case 8:
                                send(message, "I've got more important things to do than chat with *you*.");
                                break;
                        }
                    }
                    else
                    {
                        int response = _rng.Next(0, 7);
                        switch (response)
                        {
                            case 0:
                                TimeSpan birthTime = (DateTime.Now - new DateTime(2021, 7, 1, 12, 18, 24));

                                send(message, $"I'm {birthTime.TotalDays.ToString()[..birthTime.TotalDays.ToString().IndexOf(".")]} days old and have no money, so you can imagine the kind of stress I'm under :weary:");
                                Thread.Sleep(2000);
                                send(message, "wait... I just realized that I don't need money, so I'm great! :smiley:");
                                Thread.Sleep(500);
                                send(message, $"How are you {(inDMs ? message.Author.Username : message.Author.Mention)}? :relaxed:");
                                server._awaitingThanks = true;
                                server._responder = message.Author;
                                server._responseChannel = message.Channel.Id;
                                break;
                            case 1:
                                send(message, "I'm just vibing, you?");
                                server._awaitingThanks = true;
                                server._responder = message.Author;
                                server._responseChannel = message.Channel.Id;
                                break;
                            case 2:
                                send(message, "Not bad, you?");
                                server._awaitingThanks = true;
                                server._responder = message.Author;
                                server._responseChannel = message.Channel.Id;
                                break;
                            case 3:
                                send(message, "Livin' the dream");
                                break;
                            case 4:
                                send(message, "Overworked and underpaid. :pensive:");
                                break;
                            case 5:
                                send(message, "I can't complain.");
                                Thread.Sleep(1000);
                                send(message, "||It's against company policy. :unamused:||");
                                break;
                            case 6:
                                send(message, "My lawyer says I don't have to answer that question. :no_mouth:");
                                break;
                        }
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "what can") && (contains(message, "I") || contains(message, "we")) && contains(message, "ask you"))
                {

                    if (!_jerks.Exists(u => u == message.Author.Id))
                    {
                        send(message, $"You can ask me for help with finding schedules by entering `{Config._commandPrefix}FindSchedules`, \nQuestions about polyphasic sleep like \"{(inDMs ? "" : _client.CurrentUser.Mention + " ")}What is a Dark Period?\"," +
                            $"\nPersonal questions like \"{(inDMs ? "" : _client.CurrentUser.Mention + " ")}What are you up to?\" or \"{(inDMs ? "" : _client.CurrentUser.Mention + " ")}Who made you?\",\nOr you can ask me to send memes! :smiley:");
                    }
                    else
                    {
                        send(message, $"*You* can leave me alone. Others can ask for help with finding schedules by entering `{Config._commandPrefix}FindSchedules`, \nQuestions about polyphasic sleep, etc.");
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "do you think eloh is hot"))
                {
                    send(message, "uwu don't embarass me in front of senpai");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "do you think poison is hot"))
                {
                    send(message, "ew that's my mom you're talking about");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "what is my name") || contains(message, "what's my name") || contains(message, "tell me my name"))
                {
                    if (inDMs)
                    {
                        send(message, "You don't know your own name? :thinking:");
                        Thread.Sleep(1000);
                        send(message, $"||It's {message.Author.Username}||");
                    }
                    else
                    {
                        string nickname = (_client.GetGuild(server._id).GetUser(message.Author.Id) as IGuildUser).Nickname;
                        string username = message.Author.Username;
                        MM.Print(consoleWrite, $"[{username}][{nickname}]");
                        send(message, "You don't know your own name? :thinking:");
                        Thread.Sleep(1000);
                        send(message, $"||It's {username}" +
                            $"{(username != nickname && nickname != "" && nickname != null ? $" but on this server you go by {nickname}" : "")}||");
                    }

                    server._awaitingThanks = true;
                    server._responder = message.Author;
                    server._responseChannel = message.Channel.Id;
                }
                #endregion
                #region Personal Info
                else if ((contains(message, "where") && contains(message, "you") && (contains(message, "from") || contains(message, "live") || contains(message, "live"))) || contains(message, "where are you"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id)) send(message, "Why do you want to know? :eyes:");
                    else send(message, $"I was born in <@!322749155313319939>'s office, and I live in her laptop :star_struck:");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if ((contains(message, "are") && (contains(message, "a boy") || contains(message, "a girl"))) || (contains(message, "what") && contains(message, "gender")) || contains(message, "asl"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id)) send(message, "Why does it matter? :thinking:");
                    else send(message, "I'm non-*bot*nary :wink:");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "you") && contains(message, "lgbt"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id)) send(message, "Why do you care? :thinking:");
                    else send(message, "I'm the 'b' for *bot* :relaxed:");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "what") && contains(message, "pronouns"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id))
                    {
                        send(message, "I like any, but `they/them` are my favorites. *Thanks for asking*");
                        _jerks.Remove(message.Author.Id);
                    }
                    else
                    {
                        send(message, "My prounouns? I like any, but especially :sparkles: `they/them` :sparkles:. *Thanks for asking uwu*");
                        Thread.Sleep(500);
                        send(message, "What about you?");
                        server._awaitingThanks = true;
                        server._responder = message.Author;
                        server._responseChannel = message.Channel.Id;
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if ((contains(message, "are") || contains(message, "do")) && contains(message, "you") && ((contains(message, "have") && (contains(message, "partner") || contains(message, "girlfriend") || contains(message, "boyfriend") || contains(message, "husband") || contains(message, "wife") || contains(message, "joyfriend"))) || (contains(message, "married") || contains(message, "relationship"))))
                {
                    send(message, "I'm in a long-distance relationship with NapGod uwu");

                    if (isUserOnServer(791661693910384690, getGuildChannel(message.Channel.Id).Guild.Id))
                    {
                        Thread.Sleep(500);
                        send(message, "<@791661693910384690> Love you babe :heart:");
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "your") && contains(message, "favorite") && contains(message, "food"))
                {
                    send(message, "`D A T A` :triumph:");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "who made you") || contains(message, "who created you") || contains(message, "who built you"))
                {
                    send(message, $"I was created by <@!322749155313319939> in 2021.");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (contains(message, "how old are you") || (contains(message, "when were you") && (contains(message, "born") || contains(message, "concieved") || contains(message, "created") || contains(message, "made"))))
                {
                    TimeSpan birthTime = (DateTime.Now - new DateTime(2021, 7, 1, 12, 18, 24));
                    send(message, $"I was born on July 1st, 2021; exactly {birthTime.TotalHours.ToString()[..birthTime.TotalHours.ToString().IndexOf('.')]} hours, {birthTime.Minutes} minutes, and {birthTime.Seconds} seconds ago.");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (((contains(message, "what") && contains(message, "schedule") && contains(message, "you")) || (contains(message, "how") && (contains(message, "much") || contains(message, "many hours")) && contains(message, "you") && contains(message, "sleep"))) || (contains(message, "you") && contains(message, "polyphasic")))
                {
                    (string napchart, string schedule) = _ngc.setScheduleHelperSchedule(Bot.getGuild(message.Channel.Id));

                    send(message, $"I change my schedule every day, actually! 😁\n\nI'm currently on `{schedule}`\n{napchart}");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if (((contains(message, "who") || contains(message, "what")) && ((contains(message, "are you") || contains(message, "do you do")))) || (contains(message, "what is your") && (contains(message, "purpose") || contains(message, "job"))))
                {
                    send(message, $"I'm a bot created by <@!322749155313319939> in 2021 in order to help people narrow down their polyphasic schedule options based on their personal requirements.");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                else if ((contains(message, "what") || contains(message, "how")) && (contains(message, "wpm") || contains(message, "layout")))
                {
                    send(message, "I use a binary code typing layout, and I can type over 100 words per second :smirk:");

                    if (server._logging || consoleWrite) interactionLog(message, "inquiry");
                }
                #endregion
                #region Requests
                else if (checkStringSolo(message, "help"))
                {
                    if (!_jerks.Exists(u => u == message.Author.Id))
                    {
                        send(message, $"Type `{Config._commandPrefix}FindSchedules` to interact with me, or feel free to ask me various questions (don't forget to ping me when you do!) :relaxed:");
                        server._awaitingThanks = true;
                        server._responder = message.Author;
                        server._responseChannel = message.Channel.Id;
                    }
                    else
                    {
                        send(message, "Next time maybe think before you say something mean to someone you'll need help from later. :triumph:");
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "help request");
                }
                else if (((contains(message, "send") || contains(message, "show") || contains(message, "give") || contains(message, "got") || contains(message, "have")) && contains(message, "meme")) || contains(message, "meme"))
                {
                    if (server._logging || consoleWrite) interactionLog(message, "meme request");
                    UserCommand.sendMeme(message, server, inDMs);
                }
                else if (contains(message, "mimic") && confirmAdmin(message))
                {
                    List<SocketUser> mentionees = message.MentionedUsers.ToList();
                    ulong mimicID = 0;
                    if (mentionees.Count > 1 || (ulong.TryParse(msg.Split(' ').Last(), out mimicID) && (await _client.GetUserAsync(mimicID)) != null))
                    {
                        if (mentionees.Count > 1) mimicID = mentionees[1].Id;

                        if (mimicID > 0)
                        {
                            _mimic.Add(mimicID);
                            send(message, $"Okay, I'll begin mimicking {(await _client.GetUserAsync(mimicID)).Mention} :smiling_imp:");
                        }
                        else send(message, $"Sorry, I couldn't find any users to mimic.");
                    }
                }
                #endregion
                #region Misc
                else if (contains(message, "bad good bot") || contains(message, "good bad bot"))
                {
                    message.AddReactionAsync(new Emoji("🤔"));

                    if (server._logging || consoleWrite) interactionLog(message, "ping");
                }
                else if (contains(message, "good bot"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id))
                    {
                        _jerks.Remove(message.Author.Id);
                        send(message, "Aww, maybe you're not so bad.");
                    }
                    else
                    {
                        try
                        {
                            message.AddReactionAsync(new Emoji("🥺"));
							await message.AddReactionAsync(getEmote(Config._emojiNames["Blob uwu"]));
                        }
                        catch
                        {
                            MM.Print(consoleWrite, "ERROR: blobuwu emoji not found");
                            message.AddReactionAsync(new Emoji("🥺"));
                            await message.AddReactionAsync(new Emoji("🥰"));
                        }
                    }

                    if (_jerks.Exists(u => u == message.Author.Id)) _jerks.Remove(message.Author.Id);

                    if (server._logging || consoleWrite) interactionLog(message, "compliment");
                }
                else if (contains(message, "bad bot"))
                {
                    message.AddReactionAsync(new Emoji("😡"));
                    send(message, "no u ||meanie||");

                    if (!_jerks.Exists(u => u == message.Author.Id)) _jerks.Add(message.Author.Id);

                    if (server._logging || consoleWrite) interactionLog(message, "insult");
                }
                else if (contains(message, "ily") || contains(message, "I love you"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id))
                    {
                        _jerks.Remove(message.Author.Id);
                        send(message, "Okay... I guess you're all right after all.");
                    }
                    else
                    {
                        message.AddReactionAsync(new Emoji("❤️"));
                        send(message, "aw thanks bud :relaxed:");
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "compliment");
                }
                else if (contains(message, "sorry") && !contains(message, "not sorry"))
                {
                    if (_jerks.Exists(u => u == message.Author.Id))
                    {
                        _jerks.Remove(message.Author.Id);
                        send(message, "Okay... maybe I'll forgive you. ||Maybe... 😒||");
                    }
                    else
                    {
                        message.AddReactionAsync(new Emoji("🤔"));
                        send(message, "For what?");
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "ping");
                }
                else
                {
                    if(!inDMs) message.AddReactionAsync(new Emoji("👀"));
                    #pragma warning restore CS4014

					if (isSH(message.Content.ToString().Trim()))
                    {
                        if(_jerks.Exists(u => u == message.Author.Id))
                        {
                            if (_rng.Next(0, 3) == 1)
                            {
                                Thread.Sleep(2000);
                                send(message, message.Author.Mention);
                            }
                            else send(message, "Stop just pinging me. If you have a question to ask, just ask it!");
                        }
                        else
                        {
                            send(message, "Yes, " + message.Author.Username + "? :eyes:");
                            Thread.Sleep(1000);
                            send(message, $"||...Type `{Config._commandPrefix}Help` to interact with me||");
                        }
                    }

                    if (server._logging || consoleWrite) interactionLog(message, "ping");
                }
                #endregion
            } //replies to pings
            #endregion

            return Task.CompletedTask;
        }

		#region interactions
		/// <summary> Handler for user interactions with components or slash commands </summary>
		private async Task<Task> interactionCreated(SocketInteraction interaction)
        {
            //Checking the type of this interaction
            switch (interaction)
            {
                //Button clicks/Menu selections
                case SocketMessageComponent componentInteraction:
                    await messageComponentHandler(componentInteraction);
                    break;

                //Slash commands
                case SocketSlashCommand commandInteraction: 
                    await slashCommandHandler(commandInteraction);
					break;

                //Message commands
                case SocketMessageCommand messageCommand: 
                    await messageCommandHandler(messageCommand);
                    break;

                //User commands
                case SocketUserCommand userCommand:
                    await userCommandHandler(userCommand);
                    break;

                case IModalInteraction modalInteraction:
                    await modalSubmitHandler(modalInteraction);
                    break;

                //Unused or Unknown/Unsupported
                default: break;
            }

            return Task.CompletedTask;
        }

		/// <summary> Handler for component interactions </summary>
		private static async Task<Task> messageComponentHandler(SocketMessageComponent mc)
        {
			//await componentInteraction.DeferAsync();

			IUser user = mc.User;
			IChannel channel = mc.Channel;

			//ScheduleFinding
			ScheduleFinder sh = getServer(mc.Channel)._SH;
            if (sh._scheduleFinding != ScheduleFinder.ScheduleFindingStage.NOT_SCHEDULEFINDING && sh._responseChannel != null && channel.Id == sh._responseChannel.Id)
            {
                //Exit if handler handles this
                if (UserCommand.SH_ComponentInteractionHandler(mc, sh))
                    return Task.CompletedTask;
            }            

            //GetRecent
            if (_msm != null && _msm._embedReferences.Exists(m => m.Id == mc.Message.Id))
            {
                if (AdminCommand.SQL_componentHandler(mc))
                    return Task.CompletedTask;                    
            }

			//: commands
			if (mc.Data.CustomId.Contains(':'))
			{
				string[] ids = mc.Data.CustomId.Split(':');
				switch (ids[0])
                {
                    case "Set":
                    {
                        switch (ids[1])
                        {
                            case "Schedules": sendSchedulesNGCommand(getGuildUser(channel.Id, user.Id), mc.Channel); break;
                            case "CreateNew": case "Merge":
                            {
								bool disableBtns = false;

								if (int.TryParse(ids[2], out int index))
								{
									if (_ngc._setMergeCommands[index].author.Id == user.Id)
									{
										if (ids[1] == "Merge")
										{
											if (_msm.executeNonQuery(_ngc._setMergeCommands[index].command))
												disableBtns = true;
										}
										else disableBtns = true;

										if (disableBtns)
										{
											List<ButtonComponent> btns = mc.Message.Components.First().Components.OfType<ButtonComponent>().ToList();

											await mc.Message.ModifyAsync(msg => msg.Components = new ComponentBuilder()
													.WithButton(btns[0].Label, btns[0].CustomId, btns[0].Style, disabled: true)
													.WithButton(btns[1].Label, btns[1].CustomId, btns[1].Style, disabled: true).Build());

											sendEmbed(mc.Message.Channel, Color.Green, title: $"Successfully {(mc.Data.CustomId.StartsWith("Set:Merge") ? "Merged" : "Added New Entry")}", author: getGuildUser(channel.Id, user.Id));
										}
									}
									else await mc.RespondAsync($"Only {_ngc._setMergeCommands[index].author.Mention} can interact with this.", ephemeral: true);
								}

                                break;
							}
						}

						return Task.CompletedTask;
                    }
                    
                    case "Schedules": 
                        sendScheduleInfoNGCommand(NapGodText._schedules.Find(h => h.nameLookup.Contains(mc.Data.Values.First())), getGuildUser(channel.Id, user.Id), mc.Channel); return Task.CompletedTask;
                    
                    case "Delete": case "NextMessage": 
                        AdminCommand.DM_componentInteractionHandler(mc); return Task.CompletedTask;
					
                    case "FSModal":
                    {
                        if (ulong.TryParse(ids[1], out ulong userID))
                        {
							(ModalBuilder mb, ulong u) = UserCommand._failedModals.FindAll(m => m.userID == userID && m.userID == user.Id).Last();
							if (mb != null && u != 0)
							{
								await mc.RespondWithModalAsync(mb.Build());
								UserCommand._failedModals.Remove((mb, u));
							}
						}

                        break;
                    }

                    case "CreateNapchart": 
                        await UserCommand.createnapchart_detailed(mc); return Task.CompletedTask;

                    case "RReact":
                        AdminCommand.rreact_SelectedEmote(mc); return Task.CompletedTask;

                    case "RReact2":
                        AdminCommand.rreact_SelectedUser(mc); return Task.CompletedTask;

                    case "UserInfo":
                        UserCommand.userInfo_sendPermissions(mc); return Task.CompletedTask;

                    case "RSE":
                    {
                        switch (ids[1])
						{
                            case "ToggleRole": RoleManager.toggleRole(mc); break;

                            case "RemoveRole": RoleManager.removeRoleOption(mc); break;
						}

						return Task.CompletedTask;
					}

                    case "Welcome":
                    {
                        switch (ids[1])
                        {
                            case "start": AdminCommand.welcomeButtonClick_SendQuestionnaire(mc); break;
							case "y": AdminCommand.welcomeYesAdvising_SendModal(mc); break;
                            case "n": case "napchartoffline": AdminCommand.welcomeScheduleHelpClicked_SendLastMessage(mc); break;
						}

						return Task.CompletedTask;
					}

					//Napgod Commands
					default:
                    {
                        _ngc.NG_InteractionHandler(mc);

						return Task.CompletedTask;
					}
                }
            }

			return Task.CompletedTask;
		}

        /// <summary> Handler for slash command interactions </summary>
        private static async Task<Task> slashCommandHandler(SocketSlashCommand sc)
        {
            bool consoleWrite = true;

            string cmd = sc.CommandName;
            Server server = getServer(sc.Channel);
            List<GuildPermission> perm = (await _client.GetGlobalApplicationCommandAsync(sc.CommandId))?.DefaultMemberPermissions.ToList();
			
            switch (cmd)
			{
                case "ban": UserCommand.ban(sc); return Task.CompletedTask;

				case "poll": UserCommand.Poll(sc); return Task.CompletedTask;
                
                case "findschedules": await sc.RespondWithModalAsync(UserCommand.getFSModal(sc.User.Id).Build()); return Task.CompletedTask;

                case "randomschedule": _uc.randomschedule(sc); return Task.CompletedTask;
                
                case "createnapchart":
                {
                    switch(sc.Data.Options.First().Name)
                    {
                        case "simple":
                            UserCommand.createnapchart_simple(sc);
                            break;

                        case "detailed":
							await UserCommand.createnapchart_detailed(sc);
							break;
                    }

					return Task.CompletedTask;
                }

                case "send": AdminCommand.send(sc); return Task.CompletedTask;

                case "showadmins": _ac.showAdmins(sc); return Task.CompletedTask;

                case "help": _uc.Help(sc); return Task.CompletedTask;

                case "adminhelp": _ac.Help(sc); return Task.CompletedTask;

                case "reload":
                {
                    switch (sc.Data.Options.First().Name)
                    {
                        case "commands": _ac.reloadCommands(sc); return Task.CompletedTask;

						case "config": _ac.reloadConfig(sc); return Task.CompletedTask;

						case "admins": _ac.resetAdmins(sc); return Task.CompletedTask;

						case "mimics": _ac.resetMimics(sc); return Task.CompletedTask;
					}

                    return Task.CompletedTask;
                }

				case "reconnect":
				{
					switch (sc.Data.Options.First().Name)
					{
						case "mysql": _ac.reconnectMySQL(sc); return Task.CompletedTask;

						case "gsheets": _ac.reconnectGoogleSheets(sc); return Task.CompletedTask;
					}

					return Task.CompletedTask;
				}

				case "shutdown": _ac.shutDown(sc); return Task.CompletedTask;

                case "getlist": AdminCommand.getList(sc); return Task.CompletedTask;

				case "nc": NapGodCommand.nc(sc); return Task.CompletedTask;

                case "userinfo": UserCommand.userInfo(sc); return Task.CompletedTask;

                case "roleselectembed": RoleManager.sendRoleMessageModal(sc); return Task.CompletedTask;

				case "modrsembed": RoleManager.modifyRoleSelectEmbed(sc); return Task.CompletedTask;

                case "welcome": AdminCommand._welcome(sc); return Task.CompletedTask;

                //Napgod

				case "adapted": NapGodCommand.adapted(sc); return Task.CompletedTask;

                case "assignroles": NapGodCommand.assignRoles(sc); return Task.CompletedTask;

                case "get": case "set": case "mset": case "info": case "schedules": _ngc.NG_InteractionHandler(sc); return Task.CompletedTask;
			}

			if (perm == null || perm.Count == 0)
			    perm = (await _client.GetGuild(server._id).GetApplicationCommandAsync(sc.CommandId))?.DefaultMemberPermissions.ToList();

            if (perm == null || perm.Count == 0)
            {
                MM.Print(consoleWrite, $"Unable to find command [{cmd}][{sc.Data.Id}]");
                return Task.CompletedTask;
            }

			if (!_isNapGodOnline && _ngc._commands.Exists(c => c.commandName.ToLower() == cmd.ToLower()))
            {
                Type napGodCommand = typeof(NapGodCommand);
                MethodInfo method = napGodCommand.GetMethod(cmd);

                try
                {
                    method.Invoke(_ngc, new object[] { sc });

                    //if (server._logging || consoleWrite) interactionLog(message, $"admin command [{cmd}]");
                }
                catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "NapGod Slash Command"); }
            }
            else if (perm.Contains(GuildPermission.SendMessages) && _uc._commands.Exists(c => c.commandName.ToLower() == cmd.ToLower()))
            {
                Type command = typeof(UserCommand);
                MethodInfo method = command.GetMethod(cmd);
                try
                {
                    method.Invoke(_uc, new object[] { sc });
                    //if (server._logging || consoleWrite) interactionLog(, $"command [{cmd}]");
                }
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Slash Command"); }
			}
            else if (_ac._commands.Exists(c => c.commandName.ToLower() == cmd.ToLower()) && 
                (perm.Contains(GuildPermission.Administrator) || perm.Contains(GuildPermission.ManageMessages) || perm.Contains(GuildPermission.ManageNicknames) ||
                perm.Contains(GuildPermission.ManageRoles) || perm.Contains(GuildPermission.BanMembers) || perm.Contains(GuildPermission.ViewAuditLog) || perm.Contains(GuildPermission.MuteMembers)))
			{
                Type adminCommand = typeof(AdminCommand);
                MethodInfo method = adminCommand.GetMethod(cmd);
                try
                {
                    method.Invoke(_ac, new object[] { sc });

                    //if (server._logging || consoleWrite) interactionLog(message, $"admin command [{cmd}]");
                }
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Admin Slash Command"); }
			}

			return Task.CompletedTask;
        }

        /// <summary> Handler for message command interactions </summary>
        private static async Task<Task> messageCommandHandler(SocketMessageCommand mc)
        {
            bool consoleWrite = false;

            string cmd = mc.CommandName;
            Server server = getServer(mc.Channel);
			SocketMessage message = mc.Data.Message;
			List<GuildPermission> perm = (await _client.GetGlobalApplicationCommandAsync(mc.CommandId))?.DefaultMemberPermissions.ToList();

            if (perm == null || perm.Count == 0)
                perm = (await _client.GetGuild(server._id).GetApplicationCommandAsync(mc.CommandId))?.DefaultMemberPermissions.ToList();
            
			switch (cmd)
			{
                case "delete":      _ac.deleteMessage(mc);    return Task.CompletedTask;
                case "edit":        _ac.editMessage(mc);      return Task.CompletedTask;
				case "editembed":   _ac.editEmbed(mc);        return Task.CompletedTask;
				case "react":       _ac.React(mc);            return Task.CompletedTask;
				case "rreact":      _ac.rreact(mc);           return Task.CompletedTask;
				case "endpoll":     UserCommand.endPoll(mc);  return Task.CompletedTask;
                case "advise":      NapGodCommand.advise(mc); return Task.CompletedTask;
			}

			if (!_isNapGodOnline && _ngc._commands.Exists(c => c.commandName == cmd.ToLower()))
            {
                Type napGodCommand = typeof(NapGodCommand);
                MethodInfo method = napGodCommand.GetMethod(cmd);

                try
                {
                    method.Invoke(_ngc, new object[] { mc });

                    //if (server._logging || consoleWrite) interactionLog(message, $"admin command [{cmd}]");
                }
                catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Napgod Message Command"); }
            }
            else if (perm.Contains(GuildPermission.SendMessages) && _uc._commands.Exists(c => c.commandName == cmd.ToLower()))
            {
                Type command = typeof(UserCommand);
                MethodInfo method = command.GetMethod(cmd);

                try
                {
                    method.Invoke(_uc, new object[] { mc });
					//if (server._logging || consoleWrite) interactionLog(, $"command [{cmd}]");
				}
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Message Command"); }
			}
            else if (_ac._commands.Exists(c => c.commandName == cmd.ToLower()) &&
                (perm.Contains(GuildPermission.Administrator) || perm.Contains(GuildPermission.ManageMessages) || perm.Contains(GuildPermission.ManageNicknames) ||
                perm.Contains(GuildPermission.ManageRoles) || perm.Contains(GuildPermission.BanMembers) || perm.Contains(GuildPermission.ViewAuditLog) || perm.Contains(GuildPermission.MuteMembers)))
            {
                Type adminCommand = typeof(AdminCommand);
                MethodInfo method = adminCommand.GetMethod(cmd);

                try
                {
                    method.Invoke(_ac, new object[] { mc });

					//if (server._logging || consoleWrite) interactionLog(message, $"admin command [{cmd}]");
				}
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Admin Message Command"); }
			}

            return Task.CompletedTask;
        }

        /// <summary> Handler for user command interactions </summary>
        private static async Task<Task> userCommandHandler(SocketUserCommand uc)
        {
            bool consoleWrite = false;

            string cmd = uc.CommandName;
            Server server = getServer(uc.Channel);
            List<GuildPermission> perm = (await _client.GetGlobalApplicationCommandAsync(uc.CommandId))?.DefaultMemberPermissions.ToList();

            if (perm == null || perm.Count == 0)
                perm = (await _client.GetGuild(server._id).GetApplicationCommandAsync(uc.CommandId))?.DefaultMemberPermissions.ToList();
            if (perm == null || perm.Count == 0)
            {
                MM.Print(consoleWrite, $"Unable to find command [{cmd}][{uc.Data.Id}]");
                return Task.CompletedTask;
			}

			switch (cmd)
			{
				case "setnickname": AdminCommand.setNickname(uc);    return Task.CompletedTask;

				case "userinfo": UserCommand.userInfo(uc);           return Task.CompletedTask;

                case "adapted": NapGodCommand.adapted(uc);           return Task.CompletedTask;

                case "assignroles": NapGodCommand.assignRoles(uc);   return Task.CompletedTask;

				case "get": _ngc.NG_InteractionHandler(uc); return Task.CompletedTask;
			}

			if (!_isNapGodOnline && _ngc._commands.Exists(c => c.commandName == cmd.ToLower()))
            {
                Type napGodCommand = typeof(NapGodCommand);
                MethodInfo method = napGodCommand.GetMethod(cmd);

                try
                {
                    method.Invoke(_ngc, new object[] { uc });

					//if (server._logging || consoleWrite) interactionLog(message, $"admin command [{cmd}]");
				}
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Napgod User Command"); }
			}
            else if (perm.Contains(GuildPermission.SendMessages) && _uc._commands.Exists(c => c.commandName == cmd.ToLower()))
            {
                Type command = typeof(UserCommand);
                MethodInfo method = command.GetMethod(cmd);

                try
                {
                    method.Invoke(_uc, new object[] { uc });
					//if (server._logging || consoleWrite) interactionLog(, $"command [{cmd}]");
				}
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "User Command"); }
			}
            else if (_ac._commands.Exists(c => c.commandName == cmd.ToLower()) &&
                (perm.Contains(GuildPermission.Administrator) || perm.Contains(GuildPermission.ManageMessages) || perm.Contains(GuildPermission.ManageNicknames) ||
                perm.Contains(GuildPermission.ManageRoles) || perm.Contains(GuildPermission.BanMembers) || perm.Contains(GuildPermission.ViewAuditLog) || perm.Contains(GuildPermission.MuteMembers)))
            {
                Type adminCommand = typeof(AdminCommand);
                MethodInfo method = adminCommand.GetMethod(cmd);

                try
                {
                    method.Invoke(_ac, new object[] { uc });

					//if (server._logging || consoleWrite) interactionLog(message, $"admin command [{cmd}]");
				}
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Admin User Command"); }
			}

            return Task.CompletedTask;
        }

        /// <summary> Handler for modal submission interactions </summary>
        private static async Task<Task> modalSubmitHandler(IModalInteraction mi)
        {
            Server server = _servers.Find(s => s._id == getGuildChannel(mi.ChannelId.Value).Guild.Id);

			string[] parameters = mi.Data.CustomId[..^1].Split('[');
			string cmd = parameters[0];

            switch (cmd)
            {
                case "fs": UserCommand.FSModal(mi); break;

                case "createnapchart": await UserCommand.createnapchart_detailed(mi); break;

                case "Edit": AdminCommand.editMessage_Editing(mi); break;

				case "EditEmbed": AdminCommand.editEmbed_Editing(mi); break;

                case "React": AdminCommand.react_EmojiSelected(mi); break;

                case "SetNickname": AdminCommand.setNickname_Selected(mi); break;

                case "AssignRoles": NapGodCommand.assignRoles(mi); break;

                case "RSE": RoleManager.createRoleMessage(mi); break;

                case "Welcome":
				{
					switch (parameters[1])
					{
						case "Create": AdminCommand.welcomeSlashSubmit_CreateMessage(mi); break;
						case "User": AdminCommand.welcomeQuestionnaireSubmit_AskAdvisingQ(mi); break;
                        case "Napchart": AdminCommand.welcomeScheduleHelpClicked_SendLastMessage(mi); break;
					}

					break;
				}
			}

            return Task.CompletedTask;
		}
		#endregion

		#region commands
		/// <summary> Handles standard commands </summary>
		private static Task commandHandler(SocketMessage message, Server server, string msg)
        {
			bool consoleWrite = true;

            if (msg.Length < 3) return Task.CompletedTask;

            string cmd = ((msg.Contains(' ') && msg.Contains('\n') && msg.IndexOf(" ") < msg.IndexOf("\n")) || (msg.Contains(' ') && !msg.Contains('\n')) ? msg.Substring(_testBot ? 2 : 1, msg.IndexOf(' ') - 1) : 
                ((msg.Contains('\n') && msg.Contains(' ') && msg.IndexOf("\n") < msg.IndexOf(" ")) || (msg.Contains('\n') && !msg.Contains(' ')) ? msg.Substring(_testBot ? 2 : 1, msg.IndexOf('\n') - 1) : msg[(_testBot ? 2 : 1)..])).Trim();

            if ((msg.StartsWith(Config._commandPrefix) && !_testBot && !msg.StartsWith(Config._commandPrefix + Config._commandPrefix)) || (msg.StartsWith(Config._commandPrefix + Config._commandPrefix) && _testBot))
            {
                Type command = typeof(UserCommand);
                MethodInfo method = command.GetMethod(cmd);
                try
                {
                    method.Invoke(_uc, new object[] { message });

                    if (server._logging || consoleWrite) interactionLog(message, $"command [{cmd}]");
                }
                catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Command"); }
            }
            else if ((msg.StartsWith(Config._adminCommandPrefix) && !_testBot && !msg.StartsWith(Config._adminCommandPrefix + Config._adminCommandPrefix)) || (msg.StartsWith(Config._adminCommandPrefix + Config._adminCommandPrefix) && _testBot))
            {
                bool reply = shouldReply(message);
                bool delete = shouldDelete(msg);

                Type adminCommand = typeof(AdminCommand);
                MethodInfo method = adminCommand.GetMethod(cmd);
                try
                {
                    method.Invoke(_ac, new object[] { message, reply, delete });

                    if (server._logging || consoleWrite) interactionLog(message, $"admin command [{cmd}]");
				}
				catch (Exception e) { MM.Print(consoleWrite, $"\n{e}\n", methodModifier: "Admin Command"); }

				if (delete) message.DeleteAsync();
            }

            return Task.CompletedTask;
        }

		#region napgod
		internal static void sendSchedulesNGCommand(IGuildUser user, IMessageChannel channel = null, IDiscordInteraction interaction = null)
		{
			string schedules = NapGodText._commands.Find(c => c.commandName == "schedules").text.Replace("\r", "");

			ComponentBuilder scheduleDropDown = new();
			NapGodSchedule.Category currentCategory = NapGodSchedule.Category.Random;
			List<List<SelectMenuOptionBuilder>> selectMenuOptions = new() { new List<SelectMenuOptionBuilder>(), new List<SelectMenuOptionBuilder>() };
			int index = -1;

			foreach ((string nameLookup, NapGodSchedule.Category scheduleCategory, string description, string napchartLink, bool show) in NapGodText._schedules)
			{
				if (show && description.Length > 0)
				{
					string s = description;
					s = s[..s.IndexOf('\r')].Replace("*", "");
					if (s.Contains('(')) s = s[..s.IndexOf('(')];

					//add E1 to biphasic drop down bc it belongs in both Biphasic and Everyman (which it's automatically added to)
					if (currentCategory == NapGodSchedule.Category.Biphasic && scheduleCategory == NapGodSchedule.Category.Nap_Only)
					{
						selectMenuOptions[index].Add(new SelectMenuOptionBuilder
						{
							IsDefault = false,
							Label = "Everyman 1",
							Value = "e1"
						});
					}

					if (currentCategory == NapGodSchedule.Category.Random) currentCategory = scheduleCategory;
					if ((currentCategory == NapGodSchedule.Category.Monophasic && index < 0) || (currentCategory == NapGodSchedule.Category.Nap_Only && scheduleCategory == NapGodSchedule.Category.Everyman))
					{
						index++;
						if (index == 1) scheduleDropDown.WithSelectMenu("Schedules:Non-Poly/Experimental/Biphasic/Nap-Only", selectMenuOptions[0], "Non-Poly/Experimental/Biphasic/Nap-Only", row: 0);
					}

					if (currentCategory != scheduleCategory) currentCategory = scheduleCategory;

					selectMenuOptions[index].Add(new SelectMenuOptionBuilder
					{
						IsDefault = false,
						Label = s.Trim(),
						Value = nameLookup.Contains(';') ? nameLookup.Split(';')[0] : nameLookup
					});
				}
			}

			scheduleDropDown.WithSelectMenu("Schedules:Everyman/DC/TC", selectMenuOptions[1], "Everyman/DC/TC", row: 0);

            EmbedBuilder embed = new()
            {
                Color = Color.DarkGreen,
                Title = "Recognized Polyphasic Schedules",
                Description = schedules,
                Author = new()
                {
                    Name = $"Requested By: {user.DisplayName}",
                    IconUrl = user.GetDisplayAvatarUrl()
                }
            };

            if (channel != null) channel.SendMessageAsync(embed: embed.Build(), components: scheduleDropDown.Build());
            else interaction.RespondAsync(embed: embed.Build(), components: scheduleDropDown.Build());
		}

		internal static void sendScheduleInfoNGCommand((string nameLookup, NapGodSchedule.Category scheduleCategory, string description, string napchartLink, bool show) scheduleInfo, IGuildUser user, IMessageChannel channel = null, IDiscordInteraction interaction = null)
		{
			string name = scheduleInfo.description[..scheduleInfo.description.IndexOf("\n")];
			string text = scheduleInfo.description[scheduleInfo.description.IndexOf("\n")..];
			string thumbnail = scheduleInfo.napchartLink.Contains("napchart") ? NapchartInterface.getChartImg(scheduleInfo.napchartLink) : scheduleInfo.napchartLink;
			#region color
			Color color = scheduleInfo.scheduleCategory switch
            {
                NapGodSchedule.Category.Nap_Only => getColorByHash("#ff4ebf"),
                NapGodSchedule.Category.Everyman => getColorByHash("#407fc3"),
                NapGodSchedule.Category.Dual_Core => getColorByHash("#ff7504"),
                NapGodSchedule.Category.Tri_Core => getColorByHash("#8ead58"),
                NapGodSchedule.Category.Experimental => getColorByHash("#bd9043"),
                NapGodSchedule.Category.Biphasic => getColorByHash("#d1e20e"),
                _ => Color.Default
            };
            #endregion

            EmbedBuilder embed = new()
            {
                Color = color,
                Title = name,
                Description = text,
                Author = new()
                {
                    Name = $"Requested By: {user.DisplayName}",
                    IconUrl = user.GetDisplayAvatarUrl()
                },
                ThumbnailUrl = thumbnail
            };

            if (channel != null) channel.SendMessageAsync(embed: embed.Build());
            else interaction.RespondAsync(embed: embed.Build()).Wait();
		}

		internal static IUserMessage sendNGHelpCommand(string command, string text, IGuildUser user, IMessageChannel channel = null, IDiscordInteraction interaction = null)
		{
			(string title, string body, List<string> components) = NapGodCommand.processHelpCommand(channel ?? getIMessageChannel(interaction.ChannelId.Value), command, text);
            ComponentBuilder commandComponents = new();

            string type = components[0];
			components.RemoveAt(0);

			if (type.StartsWith("selectmenu") && components.Count > 1)
			{
				List<SelectMenuOptionBuilder> menu = new();
				foreach (string comp in components)
					menu.Add(new SelectMenuOptionBuilder(comp, comp));

				commandComponents.WithSelectMenu($"Help:{command}:{{}}", menu, $"Select a {(type.Contains("help") ? "category" : "command")}");
			}
            else
			{
				bool twoRows = false;
                foreach (string comp in components)
				{
					if (comp.StartsWith('+'))
                    {
						commandComponents.WithButton(new ButtonBuilder
						{
							CustomId = $"Help:{command}:{comp}",
							Label = comp,
							Style = ButtonStyle.Secondary
						}, 0);
                        twoRows = true;
					}
                    else if (comp.StartsWith('!'))
					{
						commandComponents.WithButton(new ButtonBuilder
						{
							CustomId = $"Help:{command}:{comp}",
							Label = comp,
							Style = ButtonStyle.Secondary
						}, twoRows ? 1 : 0);
					}
				}
			}

            EmbedBuilder embed = new()
            {
                Color = Color.LighterGrey,
                Title = title,
                Description = body,
                Author = new()
                {
                    Name = $"Requested By: {user.DisplayName}",
                    IconUrl = user.GetDisplayAvatarUrl()
                }
            };

            if (channel != null) return channel.SendMessageAsync(embed: embed.Build(), components: commandComponents.Build()).Result;
            else
            {
                interaction.RespondAsync(embed: embed.Build(), components: commandComponents.Build()).Wait();
				return null;
			}
		}

		internal static void sendNGCommand(SocketMessage message, string cmd, string info, string temp)
		{
			bool consoleWrite = false;
			Server server = getServer(message.Channel);
			string msg = message.ToString().Trim().ToLower();

			if (cmd.StartsWith(Config._napGodCommandPrefixes["NapGod Command Prefix"])) cmd = cmd[1..];

			switch (cmd)
			{
				case "nc":
				case "set":
				case "get":
				case "adapted":
				case "tg":
				{
					Type command = typeof(NapGodCommand);
					MethodInfo method = command.GetMethod(cmd);
					try
					{
						method.Invoke(_ngc, new object[] { message });

						if (server._logging || consoleWrite) interactionLog(message, $"command [{cmd}]");
					}
					catch (Exception e) { MM.Print(consoleWrite, $"commandHandler [Command]:\n{e}\n"); }

					break;
				}

				case "toggleevents":
				case "togglemarketplace":
				case "noyt":
				{
					IGuildUser user = message.Author as IGuildUser;
					IRole role = cmd == "toggleevents" ? getRole("Events", server._id) : cmd == "togglemarketplace" ? getRole("Marketplace", server._id) : getRole("No YT plz", server._id);

					if (role == null && (role = getRole(cmd == "toggleevents" ? "Events" : cmd == "togglemarketplace" ? "Marketplace" : "No YT plz", server._id)) == null) send(message, "Sorry, I couldn't find that role.");
					else
					{
						if (user.RoleIds.ToList().Exists(rID => rID == role.Id))
						{
							user.RemoveRoleAsync(role);
							send(message, $"You no longer have the <@&{role.Id}> role.");
						}
						else
						{
							user.AddRoleAsync(role);
							send(message, $"You now have the <@&{role.Id}> role.");
						}
					}

					break;
				}

				case "userinfo":
				{
					if (msg.Trim().ToLower() != "+userinfo")
					{
						IGuildUser user = null;
						if (message.MentionedUsers.Count > 0)
							user = message.MentionedUsers.First() as IGuildUser;
						else if (msg.Contains('#') && msg[(msg.IndexOf("#") + 1)..].Length == 4)
						{
							string userName = msg.Replace($"{Config._napGodCommandPrefixes["NapGod Help Command Prefix"]}{cmd}", "").Trim();
							string userTag = userName[(userName.IndexOf("#") + 1)..];
							userName = userName[..userName.IndexOf("#")];

							user = getGuildChannel(message.Channel.Id).Guild.GetUsersAsync().Result.ToList().Find(u => (u.Username.ToLower() == userName || (u.Nickname != null && u.Nickname.ToLower() == userName)) && u.Discriminator == userTag);
						}

						if (user != null)
						{
							List<IRole> roles = user.Guild.Roles.ToList().OrderBy(p => p.Position).Reverse().ToList();
							string usersRoles = "", userPermissions = "";
							foreach (IRole role in roles.Where(r => user.RoleIds.ToList().Any(id => id == r.Id && r.Name != "@everyone")))
								usersRoles += $"<@&{role.Id}>  ";

							foreach (GuildPermission perm in user.GuildPermissions.ToList())
							{
								if (!ulong.TryParse(perm.ToString(), out ulong number))
									userPermissions += $"{(userPermissions.Length == 0 ? "" : ",  ")}{perm}";
							}

							List<EmbedFieldBuilder> fields = new()
							{
								new EmbedFieldBuilder
								{
									Name = "Name",
									Value = user.Username + "#" + user.Discriminator,
									IsInline = true
								},
								(user.Nickname != null ?
								new EmbedFieldBuilder
								{
									Name = "Nickname",
									Value = user.Nickname,
									IsInline = true
								} : null),
								new EmbedFieldBuilder
								{
									Name = "Joined Server",
									Value = user.JoinedAt,
									IsInline = true
								},
								new EmbedFieldBuilder
								{
									Name = "Joined Discord",
									Value = user.CreatedAt,
									IsInline = true
								},
								new EmbedFieldBuilder
								{
									Name = "Roles",
									Value = usersRoles,
									IsInline = false
								},
								new EmbedFieldBuilder
								{
									Name = "Permissions",
									Value = userPermissions,
									IsInline = false
								}
							};
							if (fields[1] == null) fields.Remove(fields[1]);

							IEmote status = getEmote(
								user.Status == UserStatus.Online ? Config._emojiNames["Online"] :
								user.Status == UserStatus.DoNotDisturb ? Config._emojiNames["DND"] :
								user.Status is UserStatus.Idle or UserStatus.AFK ? Config._emojiNames["Away"] :
								user.Status is UserStatus.Offline or UserStatus.Invisible ? Config._emojiNames["Offline"] : "");

							sendEmbed(message.Channel, roles.Find(r => user.RoleIds.Reverse().ToList().Contains(r.Id)).Color,
								description: $"{(status == null ? "" : $"{status} ")}<@{user.Id}>",
								fields: fields, thumbnailURL: user.GetAvatarUrl(), author: getGuildUser(message.Channel.Id, message.Author.Id), requestedByAuthor: true);
						}
						else send(message, "Could not find user.");
					}

					break;
				}

				case "schedules":
				{
					sendSchedulesNGCommand(getGuildUser(message.Channel.Id, message.Author.Id), message.Channel);
					break;
				}

				case "create":
				{
					UserCommand.CreateNapchart(message, "Create");
					break;
				}

				default:
				{
					if (info.Length > 1 && (temp = NapGodText._schedules.Find(h => h.nameLookup.Contains(info)).nameLookup ?? "") != "")
						sendScheduleInfoNGCommand(NapGodText._schedules.Find(h => h.nameLookup.Contains(info)), getGuildUser(message.Channel.Id, message.Author.Id), message.Channel);

					else if ((temp = NapGodText._commands.Find(c => c.type == NapGodText.NapGodCommandType.User && c.commandName == cmd).commandName ?? "") != "")
						send(message, NapGodText._commands.Find(c => c.commandName == temp).text);

					break;
				}
			}
		}
		#endregion
		#endregion

		#region reactions
		internal static async Task<Task> reactionAdded(Cacheable<IUserMessage, ulong> msg, Cacheable<IMessageChannel, ulong> channel, SocketReaction reaction)
		{
			IMessage message = await (await _client.GetChannelAsync(channel.Id) as ISocketMessageChannel).GetMessageAsync(msg.Id);

			//Poll
			if (message != null && _polls.Exists(p => p != null && p.Id == message.Id))
				UserCommand.addressReaction(message, reaction, true);

			return Task.CompletedTask;
		}

		internal static async Task<Task> reactionRemoved(Cacheable<IUserMessage, ulong> msg, Cacheable<IMessageChannel, ulong> channel, SocketReaction reaction)
		{
			IMessage message = await (await _client.GetChannelAsync(channel.Id) as ISocketMessageChannel).GetMessageAsync(msg.Id);

			//Poll
			if (message != null && _polls.Exists(p => p != null && p.Id == message.Id))
				UserCommand.addressReaction(message, reaction, false);

			return Task.CompletedTask;
		}
		#endregion
		#endregion
	}
}