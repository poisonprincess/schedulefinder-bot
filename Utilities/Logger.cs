﻿using Discord;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PolyphasicScheduleFinder_Bot
{
	class Logger
    {
        #region attributes
        /// <summary> Logger object to retrieve config vars, and create a thread for Logging </summary>
        public Logger()
        {
            _maxDateCell = Config._spreadsheetVars["Max Date cell"];
            _dateFormulaCells = Config._spreadsheetVars["Date Formula cells"];
            _interactionArea = Config._spreadsheetVars["Interaction area"];
            _messageArea = Config._spreadsheetVars["Message area"];

            _t = new Thread(logger);
            messageBacklog.CollectionChanged += startThread;
            interactionBacklog.CollectionChanged += startThread;
            _t.Start();
        }

        /// <summary> Backlog of messages to log </summary>
        public ObservableCollection<LogEntry> messageBacklog = new();
        /// <summary> Backlog of interactions to log </summary>
        public ObservableCollection<string> interactionBacklog = new();
        /// <summary> logging thread </summary>
        private readonly Thread _t;
        /// <summary> wait handler for pausing threads </summary>
        private readonly EventWaitHandle _wh = new AutoResetEvent(true);
        /// <summary> semaphore for locking logger </summary>
        private bool _semaphore = false;

        /// <summary> The most recent date in the google sheet </summary>
        private readonly string _maxDateCell = "";
        /// <summary> The cell references to input the date formulas </summary>
        private readonly string _dateFormulaCells = "";
        /// <summary> The cell references for interaction logging </summary>
        private readonly string _interactionArea = "";
        /// <summary> The cell references for message logging </summary>
        private readonly string _messageArea = "";

        /// <summary> A list of logging tasks </summary>
        private readonly List<Task> tasks = new();
        #endregion

        #region handlers
        /// <summary> Loops until logger is free whenever there's a message that has to be logged, and then unlocks logger </summary>
        private void startThread(object sender, NotifyCollectionChangedEventArgs e)
        {
            bool consoleWrite = false;
            int count = tasks.FindAll(x => !x.IsCompleted).Count;

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                tasks.Add(Task.Factory.StartNew(() => //New thread to prevent waiting in main bot thread
                {
                    if (count > 10) MM.Print(consoleWrite, $"START: There are currently {count} tasks running for {messageBacklog[0]._server._name}.");

                    while (true)
                    {
                        if (!_semaphore)
                        {
                            _wh.Set(); //unlock thread
                            return;
                        }                     
                    } 
                }));
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (count > 10) MM.Print(consoleWrite, $"FINISH: There are now {count} tasks running for {messageBacklog[0]._server._name}.");
                tasks.RemoveAll(x => x.IsCompleted);
            }
        }

        /// <summary> Handles all logging dispatching on the dedicated logging thread </summary>
        public void logger()
        {
            while (true) //loops indefinitely
            {
                _semaphore = false; //unlock
                if(messageBacklog.Count == 0 && interactionBacklog.Count == 0) _wh.WaitOne(); //if backlogs are empty, pause thread
                _semaphore = true; //lock once thread has resumed

                if (messageBacklog.Count > 0)
                {
                    if (messageBacklog[0] != null)
                    {
                        logMessage(new List<LogEntry> { messageBacklog[0] });
                        messageBacklog.RemoveAt(0);
                    }
                }
                else if (interactionBacklog.Count > 0)
                {
                    if (interactionBacklog[0] != null)
                    {
                        string[] parameters = interactionBacklog[0].Split('|');
                        Server srv = Bot._servers.Find(s => s._id.ToString() == parameters[0]);
                        logInteraction(srv?? Bot._servers.Find(s => s._name == parameters[0]), parameters[1]);
                        interactionBacklog.RemoveAt(0);
                    }
                }
            }
        }

        /// <summary> Ensures that the tab for this server exists, and creates it if not </summary>
        private static bool validateDB(string serverName, ulong serverID)
        {
            if (Config._LoggingWithGSheets)
            {
                //if tab doesn't exist, create it
                try
                {
					SpreadsheetManager.readCellFromSheet(serverName, "A1"); //try reading
                    return true;
                }
                catch //throws exception if tab doesn't exist
                {
					SpreadsheetManager.duplicateTab("Template", serverName);
                }

                string B1 = SpreadsheetManager.readCellFromSheet(serverName, "B1"); //checks if server id is on tab, if not, adds it
                if (B1 is "" or null) SpreadsheetManager.writeToNewCell(serverName, "B1", serverID.ToString(), true);
                return true;
            }
            else
            {
                Server server = Bot._servers.Find(s => s._id == serverID);
                if (server == null) Bot._servers.Add(new Server(serverID, serverName, Bot._client.GetDMChannelAsync(serverID).Result != null));
                IUser owner;
                
                if ((!server._inDMs || (owner = Bot._client.GetApplicationInfoAsync().Result.Owner).Id != serverID) && !server._validated)
                {
                    DataSet result;
                    bool update = false, serverSchemaFailed = false, serverSchemaExists = false, anyCreatesFailed = false;

                    #region serverids
                    try
                    {
                        result = Bot._msmLogger.executeQuery($"SELECT `serverID` FROM `servers`.`serverids` WHERE `serverID` = '{serverID}';");
                        if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                            update = true;
                    }
                    catch { update = true; }

                    if (update)
                    {
                        try { Bot._msmLogger.executeNonQuery($"INSERT INTO `servers`.`serverids` (`serverID`) VALUES ('{serverID}');"); }
                        catch { Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to add `{serverID}` to `servers`.`serverids`."); }

                        update = false;
                    }
                    #endregion

                    #region servernames
                    try
                    {
                        result = Bot._msmLogger.executeQuery($"SELECT `serverID` FROM `servers`.`servernames` WHERE `serverID` = '{serverID}' AND `serverName` = \"{serverName}\";");
                        if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                            update = true;
                    }
                    catch { update = true; }

                    if (update)
                    {
                        try { Bot._msmLogger.executeNonQuery($"INSERT INTO `servers`.`servernames` (`serverID`, `serverName`, `dateSet`) VALUES (\"{serverID}\", \"{serverName}\", \"{DateTime.Now:yyyy-MM-dd HH:mm:ss}\");"); }
                        catch { Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to add `{serverName}` to `servers`.`servernames`."); }
                    }

                    update = false;
                    #endregion

                    #region server schema
                    try
                    {//if schema doesn't exist, create it
                        result = Bot._msmLogger.executeQuery($"SHOW DATABASES LIKE '{serverID}';");
                        if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                            update = true;
                        else serverSchemaExists = true;
                    }
                    catch { update = true; }

                    if (update)
                    {
                        try 
                        { 
                            Bot._msmLogger.createSchema(serverID.ToString(), true);
                            serverSchemaExists = true;
                        }
                        catch
                        {
                            serverSchemaFailed = true;
                            Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to create `{serverID}`.");
                        }
                    }

                    update = false;
                    #endregion

                    if (!serverSchemaFailed)
					{
                        string commands = "";

						try { commands = File.ReadAllText("./../../Misc/Create Server Tables.sql"); }
                        catch { commands = File.ReadAllText("./../../../Misc/Create Server Tables.sql"); }

                        int start;

                        #region messages
                        string table = "messages";
                        try
                        {//if table doesn't exist, create it
                            result = Bot._msmLogger.executeQuery($"USE `{serverID}`; SHOW TABLES LIKE '{table}';");
                            if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                                update = true;
                        }
                        catch { update = true; }

                        if (update)
                        {
                            try
                            {
                                start = commands.IndexOf($"CREATE TABLE `{table}`");
                                Bot._msmLogger.executeNonQuery(commands[start..commands.IndexOf(";", start)]);
                            }
                            catch
                            {
                                Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to create `{serverID}`.`{table}`.");
                                anyCreatesFailed = true;
                            }
                        }

                        update = false;
                        #endregion

                        #region attachments
                        table = "attachments";
                        try
                        {//if table doesn't exist, create it
                            result = Bot._msmLogger.executeQuery($"USE `{serverID}`; SHOW TABLES LIKE '{table}';");
                            if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                                update = true;
                        }
                        catch { update = true; }

                        if (update)
                        {
                            try
                            {
                                start = commands.IndexOf($"CREATE TABLE `{table}`");
                                Bot._msmLogger.executeNonQuery(commands[start..commands.IndexOf(";", start)]);
                            }
                            catch
                            {
                                Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to create `{serverID}`.`{table}`.");
                                anyCreatesFailed = true;
                            }
                        }
                        #endregion

                        #region embeds and embed fields
                        #region embeds
                        table = "embeds";
                        try
                        {//if table doesn't exist, create it
                            result = Bot._msmLogger.executeQuery($"USE `{serverID}`; SHOW TABLES LIKE '{table}';");
                            if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                                update = true;
                        }
                        catch { update = true; }

                        if (update)
                        {
                            try
                            {
                                start = commands.IndexOf($"CREATE TABLE `{table}`");
                                Bot._msmLogger.executeNonQuery(commands[start..commands.IndexOf(";", start)]);
                            }
                            catch
                            {
                                Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to create `{serverID}`.`{table}`.");
                                anyCreatesFailed = true;
                            }
                        }
                        #endregion
                        #region embed fields
                        table = "embed_fields";
                        if (!update)
                        {
                            try
                            {//if table doesn't exist, create it
                                result = Bot._msmLogger.executeQuery($"USE `{serverID}`; SHOW TABLES LIKE '{table}';");
                                if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                                    update = true;
                            }
                            catch { update = true; }
                        }

                        if (update)
                        {
                            try
                            {
                                start = commands.IndexOf($"CREATE TABLE `{table}`");
                                Bot._msmLogger.executeNonQuery(commands[start..commands.IndexOf(";", start)]);
                            }
                            catch
                            {
                                Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to create `{serverID}`.`{table}`.");
                                anyCreatesFailed = true;
                            }
                        }

                        update = false;
                        #endregion
                        #endregion

                        #region interactions
                        table = "interactions";
                        try
                        {//if table doesn't exist, create it
                            result = Bot._msmLogger.executeQuery($"USE `{serverID}`; SHOW TABLES LIKE '{table}';");
                            if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                                update = true;
                        }
                        catch { update = true; }

                        if (update)
                        {
                            try
                            {
                                start = commands.IndexOf($"CREATE TABLE `{table}`");
                                Bot._msmLogger.executeNonQuery(commands[start..]);
                            }
                            catch
                            {
                                Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"Unable to create `{serverID}`.`{table}`.");
                                anyCreatesFailed = true;
                            }
                        }
                        #endregion
                    }
                    
                    if (serverSchemaExists && !anyCreatesFailed) server._validated = true;
                }

                return true;
            }
        }
        #endregion

        #region loggers
        /// <summary> Sends logging info to the spreadsheet manager to log </summary>
        private void logMessage(List<LogEntry> logEntries)
        {
            validateDB(logEntries[0]._server._name, logEntries[0]._server._id);

            if (Config._LoggingWithGSheets)
            {
                updateDate(logEntries[0]._server._name, logEntries.FindLast(x => x != null)._date);

                List<IList<object>> ranges = new();
                
                foreach (LogEntry e in logEntries)
                {
                    ranges.Add(new List<object>
                    {
                        e._date,
                        e._time,
                        e._inputType,
                        e._authorId,
                        e._channelId,
                        e._messageId,
                        "'" + e._channelName,
                        "'" + e._authorName,
                        "'" + e._content,
                        e._attachmentDataString,
                        e._embedDataString
                    });
                }

				SpreadsheetManager.writeToNewRange(logEntries[0]._server._name, _messageArea, ranges);
            }
            else
            {
                foreach(LogEntry e in logEntries)
                {
                    Bot._msmLogger.logMessage(e);
                }
            }
        }

        /// <summary> Sends interaction info to the spreadsheet manager to log </summary>
        private void logInteraction(Server server, string interactionType)
        {
            validateDB(server._name, server._id);

            if (Config._LoggingWithGSheets)
            {
                char columnLetter = (char)(_interactionArea.Replace("$", "")[0] + 1);

                List<List<string>> cells = SpreadsheetManager.readCellsFromSheet(server._name, _interactionArea);

                int rowNum = 4;
                foreach (List<string> row in cells)
                {
                    if (row.Count > 0 && row[0].Trim() == interactionType.Trim())
                    {
                        int currentVal = -1;
                        if (int.TryParse(SpreadsheetManager.readCellFromSheet(server._name, $"{columnLetter}{rowNum}"), out int cVal)) currentVal = cVal;
                        if (currentVal > -1) SpreadsheetManager.overwriteCell(server._name, $"{columnLetter}{rowNum}", (currentVal + 1).ToString(), false);
                        return;
                    }
                    else rowNum++;
                }
            }
            else
            {
                Bot._msmLogger.logInteraction(server._id, interactionType);
            }            
        }

        /// <summary> Updates date list if it's a new day </summary>
        private void updateDate(string serverName, string date)
        {
            int row = 0;
            if (int.TryParse(_messageArea.Replace("$", "").Remove(0, 1).Split(':')[0], out int r)) row = r;
            char dateCol = _messageArea.Replace("$", "")[0];
            char SECol = (char)(dateCol + 2);
            char authIDCol = (char)(SECol + 1);

            if (DateTime.Parse(SpreadsheetManager.readCellFromSheet(serverName, _maxDateCell)).Date < DateTime.Parse(date).Date)
            {
                List<IList<object>> entries = new()
				{
                    new List<object>
                    {
                        date,
                        $"=if(ISNA(FILTER(${dateCol}${row}:${dateCol}, ${dateCol}${row}:${dateCol} = INDIRECT(ADDRESS(Row(), Column() - 1)), {SECol}{row}:{SECol} = \"S\")), 0, COUNTA(FILTER(${dateCol}${row}:${dateCol}, ${dateCol}${row}:${dateCol} = INDIRECT(ADDRESS(Row(), Column() - 1)), {SECol}{row}:{SECol} = \"S\")))",
                        $"=if(ISNA(FILTER(${authIDCol}${row}:${authIDCol}, ${dateCol}${row}:${dateCol} = INDIRECT(ADDRESS(Row(), Column() - 2)))), 0, COUNTA(UNIQUE(FILTER(${authIDCol}${row}:${authIDCol}, ${dateCol}${row}:${dateCol} = INDIRECT(ADDRESS(Row(), Column() - 2))))))"
                    }
                };
				SpreadsheetManager.writeToNewRange(serverName, _dateFormulaCells, entries);
            }
        }
        #endregion
    }
}
