﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace PolyphasicScheduleFinder_Bot
{
	class SpreadsheetManager
    {
        #region attributes
        static readonly string[] _Scopes = { SheetsService.Scope.Spreadsheets };
        static readonly string _ApplicationName = "PolyphasicScheduleFinder";
        static SheetsService _service;
        static readonly string _SpreadsheetId = "1bJXaviimqxFXDmBTLGXO7_8vBUp7uvJnK4S2JVsrqyc";

        /// <summary> Connect to google sheets API </summary>
        internal static bool connectToGoogleAPI(bool consoleWrite)
        {
            MM.Print(consoleWrite, $"{MM.getCurrentTimeStamp()} Sheets API  Connecting");
            try
            {
                GoogleCredential credential;
                using (FileStream stream = new("client_secret.json", FileMode.Open, FileAccess.Read))
                {
                    credential = GoogleCredential.FromStream(stream)
                        .CreateScoped(_Scopes);
                }

                // Create Google Sheets API service.
                _service = new SheetsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = _ApplicationName,
                });

                MM.Print(consoleWrite, $"{MM.getCurrentTimeStamp()} Sheets API  Connected");

                return true;
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, e.ToString());
                return false;
            }
        }
        #endregion

        #region read
        /// <summary> read a range of cells from a sheet </summary>
        internal static List<List<string>> readCellsFromSheet(string tabName, string range)
        {
            List<List<string>> cells = new();

            List<IList<object>> list = new();

            bool retry = true;

            while (retry) //retry if ratelimit has been reached
            {
                try
                {
                    list = (_service.Spreadsheets.Values.Get(_SpreadsheetId, $"'{tabName}'!{range}").Execute()).Values.ToList();
                    retry = false;
                }
                catch
                {
                    Thread.Sleep(1000);
                }
            }

            foreach (List<object> row in list.Cast<List<object>>()) //convert object list to string list
            {
                List<string> newRow = new();
                foreach (object cell in row)
                {
                    newRow.Add((string)cell);
                }

                cells.Add(newRow);
            }

            return cells;
        }

        /// <summary> read a single cell from a sheet </summary>
        internal static string readCellFromSheet(string tab, string cell) 
        {
            ValueRange result;
                
            while (true)  //retry if ratelimit has been reached
            {
                try
                {
                    result = _service.Spreadsheets.Values.Get(_SpreadsheetId, $"{tab}!{cell}").Execute();
                    return result.Values == null ? "" : result.Values[0].First().ToString();
                }
                catch (Exception e)
                {
                    //continue if tab doesn't exist- that gets handled elsewhere
                    if (e.GetBaseException().ToString().Contains($"Unable to parse range: {tab}!{cell}")) _service.Spreadsheets.Values.Get(_SpreadsheetId, $"{tab}!{cell}").Execute();
                    Thread.Sleep(1000);
                }
            }
        }
        #endregion

        #region write
        /// <summary> writes to the next empty cell below the passed cell </summary>
        /// <param name="nullifyFormula">adds "'" to the front to nullify any potential formulas</param>
        /// <returns></returns>
        internal static string writeToNewCell(string tabName, string cell, string content, bool nullifyFormula)
        {
			ValueRange valueRange = new()
			{
				Values = new List<IList<object>> { new List<object>() { ((nullifyFormula ? "'" : "") + content) } }
			};

			SpreadsheetsResource.ValuesResource.AppendRequest appendRequest = _service.Spreadsheets.Values.Append(valueRange, _SpreadsheetId, $"{tabName}!{cell}");
            appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

            while (true) //retry if ratelimit has been reached
            {
                try
                {
                    return appendRequest.Execute().Updates.UpdatedRange.ToString();
                }
                catch
                {
                    Thread.Sleep(1000);
                }
            }
        }

        /// <summary> writes to the next empty range below the passed range </summary>
        internal static void writeToNewRange(string tabName, string range, List<IList<object>> content)
        {
			ValueRange valueRange = new()
			{
				Values = content
			};

			SpreadsheetsResource.ValuesResource.AppendRequest appendRequest = _service.Spreadsheets.Values.Append(valueRange, _SpreadsheetId, $"{tabName}!{range}");
            appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

            while (true) //retry if ratelimit has been reached
            {
                try
                {
                    appendRequest.Execute();
                    return;
                }
                catch (Exception )
                {
                    Thread.Sleep(1000);
                }
            }
        }

        /// <summary> overwrites the content in the passed cell </summary>
        /// <param name="nullifyFormula">adds "'" to the front to nullify any potential formulas</param>
        internal static void overwriteCell(string tabName, string cell, string content, bool nullifyFormula)
        {
			ValueRange valueRange = new()
			{
				Values = new List<IList<object>> { new List<object>() { ((nullifyFormula ? "'" : "") + content) } }
			};

			SpreadsheetsResource.ValuesResource.UpdateRequest updateRequest = _service.Spreadsheets.Values.Update(valueRange, _SpreadsheetId, $"{tabName}!{cell}");
            updateRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
            while (true) //retry if ratelimit has been reached
            {
                try
                {
                    updateRequest.Execute();
                    return;
                }
                catch
                {
                    Thread.Sleep(1000);
                }
            }
        }
        #endregion

        #region delete
        /// <summary> deletes the content of a range of cells </summary>
        internal static void deleteCells(string tabName, string range) => _service.Spreadsheets.Values.Clear(new ClearValuesRequest(), _SpreadsheetId, $"{tabName}!{range}").Execute();
        #endregion

        #region tab management
        /// <summary> duplicates a tab with the given name </summary>
        internal static void duplicateTab(string tabName, string newTabName)
        {
            int? tabId = getTabId(tabName);
            bool consoleWrite = true;

            if (tabId == null)
            {
                MM.Print(consoleWrite, $"ERROR: Couldn't find tab with name [{tabName}].");
                return;
            }

            Request request = new()
            {
                DuplicateSheet = new DuplicateSheetRequest()
                {
                    NewSheetName = newTabName,
                    SourceSheetId = tabId
                }
            };

			BatchUpdateSpreadsheetRequest r = new() { Requests = new List<Request> { request } };

			try { _service.Spreadsheets.BatchUpdate(r, _SpreadsheetId).Execute(); }
            catch { MM.Print(consoleWrite, $"ERROR: Couldn't create sheet {tabName}"); }
        }

        /// <summary> gets the id of a tab </summary>
        internal static int? getTabId(string tabName)
        {
            Sheet tab = getTabs().Find(s => s.Properties.Title == tabName);
           return tab.Properties.SheetId;
        }

        /// <summary> changes the name of the passed tab </summary>
        internal static void renameTab(int tabId, string newTabName)
        {
			Request request = new()
            {
                UpdateSheetProperties = new UpdateSheetPropertiesRequest
                {
                    Properties = new SheetProperties()
                    {
                        Title = newTabName,
                        SheetId = tabId

                    },
                    Fields = "Title"
                }
            };

			BatchUpdateSpreadsheetRequest r = new()
			{
				Requests = new List<Request> { request }
			};

			_service.Spreadsheets.BatchUpdate(r, _SpreadsheetId).Execute();
        }

        /// <summary> gets a list of the tabs in the spreadsheet </summary>
        internal static List<Sheet> getTabs()
        {
            while (true)
            {
                try
                {
                    Spreadsheet s = _service.Spreadsheets.Get(_SpreadsheetId).Execute();

                    List<Sheet> tabs = new();
                    foreach (Sheet sheet in s.Sheets)
                    {
                        tabs.Add(sheet);
                    }

                    return tabs;
                }
                catch
                {
                        Thread.Sleep(1000);
                }
            }
        }
        #endregion
    }
}
