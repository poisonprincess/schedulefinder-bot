CREATE SCHEMA `scheduledata`;
USE `scheduledata`;

CREATE TABLE `users` (
	`userID` BIGINT NOT NULL,
	`dateAdded` DATETIME NOT NULL,
	PRIMARY KEY (`userID`));
    
CREATE TABLE `schedules` (
	`entryID` INT NOT NULL AUTO_INCREMENT,
	`userID` BIGINT NOT NULL,
	`userNickname` VARCHAR(32) NULL,
	`scheduleName` VARCHAR(22) NOT NULL,
    `napchartLink` VARCHAR(200) NULL,
	`setDate` DATETIME NOT NULL,
	`napchartSetDate` DATETIME NULL,
	`adaptedDate` DATETIME NULL,
	PRIMARY KEY (`entryID`),
	INDEX `userID_idx` (`userID` ASC) VISIBLE,
	CONSTRAINT `userID`
		FOREIGN KEY (`userID`)
		REFERENCES `scheduledata`.`users` (`userID`)
    ON DELETE NO ACTION ON UPDATE NO ACTION);