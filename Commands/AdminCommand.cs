﻿using Discord;
using Discord.WebSocket;
using Svg;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;

namespace PolyphasicScheduleFinder_Bot
{
    class AdminCommand : Command
    {
        #region attributes
        internal enum Perms { ALERT, Owner, Listed, Server }; //Each encompasses the next
        internal List<(string commandName, string category, string description, string properFormat, Perms perms)> _adminCommands = new();
        internal static bool _commandsConfigured_initialValue = false;
		internal static bool _ngCommandsConfigured_initialValue = false;

		/// <summary> updates any local stored config variables </summary>
		internal void updateConfigVars()
        {
            _alert = Bot.getEmote(Config._emojiNames["Alert"]);
            _prefix = new string(Config._adminCommandPrefix[0], Bot._testBot ? 2 : 1);

			_adminCommands = new List<(string commandName, string category, string description, string properFormat, Perms perms)>
			{
                #region Bot Management
                ("UpdateBot", "Bot Management",
                    "*Set's Bot's Parameters.*\n",
                    $"```{_prefix}UpdateBot [conditions: separated by new lines]\n" +
                    $"username: (string)\navatar: (image hyperlink or path)\nplaying: ([Playing, Listening, Competing, Streaming] = string)\nstatus: (Online, Idle/AFK, DoNotDisturb, Invisible)```",
                    Perms.Listed),
                ("RefreshCommands", "Bot Management",
                    "*Refreshes the bot's commands.*\n",
                    $"```{_prefix}RefreshCommands [Optional: true/false to delete all]```",
                    Perms.Listed),
                ("RefreshConfig", "Bot Management",
                    "*Refreshes the bot's config.*\n",
                    $"```{_prefix}RefreshConfig [Optional: true/false to refresh Schedule Database]```",
                    Perms.Listed),
                ("ReconnectMySQL", "Bot Management",
                    "*Reconnects to MySQL database.*\n",
                    $"```{_prefix}ReconnectMySQL [Optional: true/false to refresh ScheduleDB]```",
                    Perms.Listed),
                ("ReconnectGoogleSheets", "Bot Management",
                    "*Reconnect to Google Sheets database.*\n",
                    $"```{_prefix}ReconnectGoogleSheets [Optional: true/false to refresh Schedule Database]```",
                    Perms.Listed),
                ("SwapSetDB", "Bot Management",
                    "*Change the DB that +Set writes to for the current server.*\n",
                    $"```{_prefix}SwapSetDB [DB Name]```",
                    Perms.Listed),
                ("ShutDown", "Bot Management",
                    "*Shuts bot down.*\n",
                    $"```{_prefix}ShutDown```",
                    Perms.ALERT),
                ("Restart", "Bot Management",
                    "*Restarts bot.*\n",
                    $"```{_prefix}Restart```",
                    Perms.ALERT),
                ("ShowAdmins", "Bot Management",
                    "*Prints admins.*\n",
                    $"```{_prefix}ShowAdmins [Optional: server ID (if blank, current server)]```",
                    Perms.Server),
                ("ResetAdmins", "Bot Management",
                    "*Resets static admin list.*\n",
                    $"```{_prefix}ResetAdmins```",
                    Perms.Owner),
                ("ResetMimics", "Bot Management",
                    "*Resets mimic list.*\n",
                    $"```{_prefix}ResetMimics```",
                    Perms.Server),
                #endregion
                #region Elevated User Commands
                ("Help", "Elevated User Commands",
					"*Shows admin commands.*\n",
					$"```{_prefix}Help [Optional: Command category (show categories if blank)]```",
					Perms.Server),
				("FindSchedules", "Elevated User Commands",
					$"*Sets _advancedResults to true, then calls {Config._commandPrefix + (Bot._testBot ? Config._commandPrefix : "")}FindSchedules.*\n",
					$"```{_prefix}FindSchedules```",
					Perms.Server),
                #endregion
                #region Server Management
                ("SetNickname", "Server Management",
					"*Set the nickname of a user (does not work in DMs).*\n",
					$"```{_prefix}SetNickname serverID\\userID\\nickname```",
					Perms.Server),
				("GetChannels", "Server Management",
					"*Get channels from a specific server.*\n",
					$"```{_prefix}GetChannels [Optional: server ID (if blank, current server)]\\[Optional: searchFor]```",
					Perms.Server),
				("GetRoles", "Server Management",
					"*Get roles from a specific server.*\n",
					$"```{_prefix}GetRoles [Optional: server ID (if blank, current server)]\\[Optional: searchFor]```",
					Perms.Server),
				("LeaveServer", "Server Management",
					$"*Make {Bot._client.CurrentUser?.Username} leave a server.*\n",
					$"```{_prefix}LeaveServer [Optional: server ID (if blank, current server)]```",
					Perms.ALERT),
				("UpdateDividerRoles", "Server Management",
					$"*Update Role Dividers for all users on a server.*\n",
					$"```{_prefix}UpdateDividerRoles [Optional: server ID (if blank, current server)]```",
					Perms.Owner),
				("CreateTestingServer", "Server Management",
					$"*Create testing server.*\n",
					$"```{_prefix}CreateTestingServer```",
					Perms.Owner),
				("Welcome", "Server Management",
					$"*Create and send the Welcome message.*\n",
					$"```{_prefix}Welcome```",
					Perms.Server),
                #endregion
                #region Meme Management
                ("ResetMemes", "Meme Management",
					"*Resets meme list and local variables for current or specified server.*\n",
					$"```{_prefix}ResetMemes [Optional: server ID (if blank, current server)]```",
					Perms.Listed),
				("SetMe", "Meme Management",
					"*Set the Meme Exhaustion threshold.*\n",
					$"```{_prefix}SetMe #\\[Optional: server ID (if blank, current server)]```",
					Perms.Listed),
				("Clear", "Meme Management",
					"*Clears local variables for current server or specified server.*\n",
					$"```{_prefix}Clear [Optional: server ID (if blank, current server)]```",
					Perms.Listed),
                #endregion
                #region Logging
                ("Logging", "Logging",
					"*Toggle logging.*\n",
					$"```{_prefix}Logging bool\\[Optional: server ID (if blank, current server)]```",
					Perms.Owner),
				("Tail", "Logging",
					"*Prints messages sent in specified channel to current channel.*\n",
					$"```{_prefix}Tail channel ID``` for detailed tailing, or\n```{_prefix}Tail channel ID\\s``` for simple tailing.",
					Perms.Listed),
				("StopTail", "Logging",
					"*Stops tailing specified channel or all channels.*\n",
					$"```{_prefix}StopTail [Optional: channel ID (if blank, all channels)]```",
					Perms.Listed),
                #endregion
                #region Message Management
                ("Send", "Message Management",
					"*Send a message to a specific channel.*\n",
					$"```{_prefix}Send [Optional: replyTo link]\\[Optional: channel ID/user ID for DMs (if blank, current channel)]\\message (\"^bs \" for \"\\\")```",
					Perms.Server),
				("SendEmbed", "Message Management",
					"*Send an embed.*\n",
					$"```{_prefix}SendEmbed [channel ID or 'this' for current channel]\\[conditions: separated by new lines]\ncolor: (Red, DarkRed, LightOrange, Orange, DarkOrange, Gold, Green, DarkGreen, Teal, DarkTeal, Blue, DarkBlue, Magenta, DarkMagenta, Purple, DarkPurple, LighterGrey, LightGrey, DarkGrey, DarkerGrey, Default, or hex)\nmessage: (text)\ntitle: (text)\ndescription: (text)\nfields: (<(name)><[value]><&> etc.)\nurl: (url)\nimage: (url)\nthumbnail: (url)\nfooter: (text)\nauthor: (user ID)\nreply: (messageLink; does not work in DMs)\n\n[for text conditions use; \"^v \" for new line; \"^bs \" for \"\\\"]```",
					Perms.Server),
				("CPEmbed", "Message Management",
					"*Copy (and edit) and paste (resend) an embed.*\n",
					$"```{_prefix}CPEmbed [channel ID or 'this' for current channel]\\messageLink\\embed index\\[conditions: separated by new lines]\ncolor: (Red, DarkRed, LightOrange, Orange, DarkOrange, Gold, Green, DarkGreen, Teal, DarkTeal, Blue, DarkBlue, Magenta, DarkMagenta, Purple, DarkPurple, LighterGrey, LightGrey, DarkGrey, DarkerGrey, Default, or hex)\nmessage: (text)\ntitle: (text)\ndescription: (text)\nfields: (<(name)><[value]><&> etc.)\nurl: (url)\nimage: (url)\nthumbnail: (url)\nfooter: (text)\nauthor: (user ID)\nreply: (messageLink; does not work in DMs)\n\n[for text conditions use; \"^v \" for new line; \"^bs \" for \"\\\"]```",
					Perms.Server),
				("Edit", "Message Management",
					"*Edit a message.*\n",
					$"```{_prefix}Edit messageLink\\new text (\"^bs \" for \"\\\")```",
					Perms.Server),
				("Delete", "Message Management",
					"*Delete a specific message.*\n",
					$"```{_prefix}Delete messageLink```",
					Perms.Server),
				("DeleteCount", "Message Management",
					"*Delete a certain number of messages sent by a certain user.*\n",
					$"```{_prefix}DeleteCount [Optional: channel ID (if blank, current channel)]\\#\\[Optional: user ID]```",
					Perms.Listed),
				("DeleteMessages", "Message Management",
					"*Search the message history for specific messages (max 1000 at once) and have the ability to delete them.*\n",
					$"```{_prefix}DeleteMessages [channel name or channel id]\\[conditions: separated by new lines]\nstart: (message link)\nend: (message link)\nuser: (ID or name)\nmax results: (#; defaults to 20)\nshow details: (true or false; defaults to false)\ncontent: (string)\ncase sensitive: (true or false; defaults to false) [only relevant if using a content search]\nshow final: (true or false; defaults to false)```\n\n" +
					$"Show Final puts the message link of the last message that the bot looked at during this search.",
					Perms.Listed),
				("DM", "Message Management",
					"*DeleteMessages but with simplified parameters.*\n",
					$"```{_prefix}DM [content (case insensitive)]\\[max results]\\[start (message link)]```",
					Perms.Listed),
				("FindMessages", "Message Management",
					"*Search the message history for specific messages (max 1000 at once).*\n",
					$"```{_prefix}FindMessages [channel name or channel id]\\[conditions: separated by new lines]\nstart: (message link)\nend: (message link)\nuser: (ID or name)\nmax results: (#; defaults to 20)\nshow details: (true or false; defaults to false)\ncontent: (string)\ncase sensitive: (true or false; defaults to false) [only relevant if using a content search]\nshow final: (true or false; defaults to false)```\n\n" +
					$"Show Final puts the message link of the last message that the bot looked at during this search.",
					Perms.Listed),
                #endregion
                #region Reactions
                ("React", "Reactions",
					"*Add a specific reaction to a specific message.*\n",
					$"```{_prefix}React messageLink\\d\\:emoji_name:``` for default emojis, or \n```=React messageLink\\c\\emoji_name``` for custom emojis.",
					Perms.Server),
				("RReact", "Reactions",
					"*Remove all reactions from a message (cannot remove other users' reactions in DMs).*\n",
					$"```{_prefix}RReact messageLink```",
					Perms.Server),
				("RUReact", "Reactions",
					"*Remove specific reaction by specific user (cannot remove other users' reactions in DMs).*\n",
					$"```{_prefix}RUReact messageLink\\d\\:emoji_name:\\user ID``` for default emojis, or \n```{_prefix}RUReact messageLink\\c\\emoji_name\\user ID``` for custom emojis.",
					Perms.Server),
                #endregion
                #region SQL
                ("ExecuteCommand", "SQL",
					"*Use a specific command.*\n",
					$"```{_prefix}ExecuteCommand [query results (true or false)]\\query\\[Optional: max results]```",
					Perms.ALERT),
				("GetData", "SQL",
					"*Query the SQL database.*\n",
					$"```{_prefix}GetData schema\\table\\columns, separated by commas\\[Optional: max results]```",
					Perms.Listed),
				("GetMessages", "SQL",
					"*Query the SQL database for specific messages.*\n",
					$"```{_prefix}GetMessages [server name or server id]\\[conditions: separated by new lines]\nstart: (datetime)\nend: (datetime)\nuser: (ID or name)\nchannel: (ID or name)\nmax results: (#)\nshow details: (true or false)\nmessage types: (S, E, D)\ncontent: (string)```",
					Perms.Listed),
				("GetMsg", "SQL",
					"*Get all references to a specific message from the SQL database.*\n",
					$"```{_prefix}GetMsg [server name or server id]\\[message link or id]```",
					Perms.Listed),
				("GetRecent", "SQL",
					"*Query the SQL database to get x most recent messages from a certain channel and/or user.*\n",
					$"```{_prefix}GetRecent # of messages\\show details (true or false)\\server name or id\\[Optional: Channel: channel name or id]\\[Optional: User: user name or id]```",
					Perms.Listed),
				("GetDeleted", "SQL",
					"*Query the SQL database to get x most recent deleted messages from a certain channel and/or user.*\n",
					$"```{_prefix}GetDeleted # of messages\\show details (true or false)\\server name or id\\[Optional: Channel: channel name or id]\\[Optional: User: user name or id]```",
					Perms.Listed),
				("GetMessageCount", "SQL",
					"*Query the SQL database to get the number of messages sent with certain parameters.*\n",
					$"```{_prefix}GetMessageCount server name or id\\[Optional: Channel: channel name or id]\\[Optional: User: user name or id]```",
					Perms.Listed),
				("GoogleToSQL", "SQL",
					"*Transfer Google Sheets data to MySQL.*\n",
					$"```{_prefix}GoogleToSQL Server ID\\Tab Name```",
					Perms.Owner)
                #endregion
            };

            _commandsConfigured_initialValue = Bot._commandsConfigured;
            _ngCommandsConfigured_initialValue = Bot._ngCommandsConfigured;
        }

        /// <summary> Returns Category of a command and the number of commands in that category </summary>
        internal static List<(string categoryName, int commandCount)> getAdminCategoriesAndCount(AdminCommand command, IMessage message = null, IDiscordInteraction interaction = null)
        {
            List<(string categoryName, int commandCount)> categories = new();

            foreach ((string commandName, string category, string description, string properFormat, Perms perms) in command._adminCommands)
            {
                if ((message != null && confirmPerms(perms, message, commandName, runningHelpCommand: true)) || (interaction != null && confirmPerms(perms, interaction, commandName, runningHelpCommand: true)))
                {
                    if (categories.Count == 0 || !categories.Exists(c => c.categoryName == category))
                        categories.Add((category, 1));
                    else
                    {
                        (string categoryName, int commandCount) temp = (categories.Last().categoryName, categories.Last().commandCount + 1);
                        categories.Remove(categories.Last());
                        categories.Add(temp);
                    }
                }
            }

            foreach ((string categoryName, int commandCount) category in new List<(string categoryName, int commandCount)>(categories))
            { if (category.commandCount == 0) categories.Remove(category); }

            return categories;
        }

        /// <summary> Returns a string representation of the the information of the passed Command
        /// or of all of the Commands in the same Category as the passed Command </summary>
        internal static string getAdminCategoriesStringFormat(AdminCommand command, IMessage message = null, IDiscordInteraction interaction = null, bool includeAll = false)
        {
            string categoriesString = "";
            int count = 0, maxLength = 0;

            List<(string categoryName, int commandCount)> categories = getAdminCategoriesAndCount(command, message, interaction);

			foreach ((string categoryName, int commandCount) in categories)
			{
				if (categoryName.Length > maxLength) maxLength = categoryName.Length;
				if (includeAll) count += commandCount;
			}

			maxLength += 3;

			foreach ((string categoryName, int commandCount) in categories)
				categoriesString += $"{categoryName}: {new string(' ', maxLength - categoryName.Length - commandCount.ToString().Length)}{commandCount}\n";

            return (includeAll ? $"```All: {new string(' ', maxLength - "All".Length - count.ToString().Length)}{count}\n" : "```") + categoriesString + "```";
        }

        private static bool confirmPerms(Perms perms, IMessage message, string commandName, ulong serverID = 0, bool runningHelpCommand = false)
        {
            switch (perms)
            {
                case Perms.ALERT:
                    if (Bot.confirmOwner(message)) return true;
                    else
                    {
                        if (!runningHelpCommand)
                            Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"__**{commandName} ALERT!**__\n`{message.Author.Username}` attempted to use `{commandName}` on server `{Bot.getGuild(message.Channel.Id).Name}` (channel `{message.Channel.Name}`)\n" +
                                $"Author: `{message.Author.Id}` | {Bot.getMessageLink(message)}\n" +
                                $"Command attempted: \n\n{message.Content}");

                        return false;
                    }

				case Perms.Owner: return Bot.confirmOwner(message);

				case Perms.Listed: return Bot.confirmOwner(message) || Bot.confirmListedAdmin(message);

                case Perms.Server:
                    if (serverID == 0)
                    {
                        Server server = Bot.getServer(message.Channel);
                        if (!server._inDMs) serverID = server._id;
                    }

                    return Bot.confirmOwner(message) || Bot.confirmListedAdmin(message) || (serverID > 0 && Bot.confirmServerPerms(serverID, message.Author.Id));

                default: return false;
            }
        }
        private static bool confirmPerms(Perms perms, IDiscordInteraction interaction, string commandName, ulong serverID = 0, bool runningHelpCommand = false)
        {
            IUser user = interaction.User;
            IChannel channel = Bot._client.GetChannelAsync(interaction.ChannelId.Value).Result;
            string commandAttempted = "";
            IMessage message;

            switch (interaction)
            {
                case SocketUserCommand uInt:
                    commandAttempted = uInt.CommandName;
                    message = null;
                    break;

                case SocketSlashCommand sInt:
                    commandAttempted = sInt.CommandName;
                    message = null;
                    break;

                case SocketMessageCommand gInt:
                    commandAttempted = gInt.CommandName;
                    message = gInt.Data.Message;
                    break;

                case SocketMessageComponent cInt:
                    commandAttempted = cInt.Data.CustomId;
                    message = cInt.Message;
                    break;

                case IModalInteraction mInt:
                    commandAttempted = mInt.Data.CustomId;
                    message = null;
                    break;

                default:
                    message = null;
                    break;
            }

            switch (perms)
            {
                case Perms.ALERT:
                    if (Bot.confirmOwner(user.Id)) return true;
                    else
                    {
                        if (!runningHelpCommand)
                            Bot._client.GetApplicationInfoAsync().Result.Owner.SendMessageAsync($"__**{commandName} ALERT!**__\n`{Bot.getGuildUser(channel.Id, user.Id).Username}` attempted to use `{commandName}` on server `{Bot.getGuild(channel.Id).Name}` (channel `{channel.Name}`)\n" +
                                $"Author: `{user.Id}` {(message != null ? $"| {Bot.getMessageLink(message)}" : "")}\n" +
                                $"Command attempted: \n\n{commandAttempted}");

                        return false;
                    }

                case Perms.Owner: return Bot.confirmOwner(user.Id);

                case Perms.Listed: return Bot.confirmOwner(user.Id) || Bot.confirmListedAdmin(user.Id);

                case Perms.Server:
                    if (serverID == 0)
                    {
                        Server server = Bot.getServer(channel.Id);
                        if (!server._inDMs) serverID = server._id;
                    }

                    return Bot.confirmOwner(user.Id) || Bot.confirmListedAdmin(user.Id) || (serverID > 0 && Bot.confirmServerPerms(serverID, user.Id));

                default: return false;
            }
        }

        private static Perms? getUsersPerms(IMessage message, ulong serverID)
        {
            return Bot.confirmOwner(message)
                ? Perms.ALERT
                : Bot.confirmListedAdmin(message)
                ? Perms.Listed
                : Bot.confirmServerPerms(serverID, message.Author.Id) ? Perms.Server : (Perms?)null;
        }

        private static Perms? getUsersPerms(IDiscordInteraction interaction, ulong serverID)
        {
            return Bot.confirmOwner(interaction.User.Id)
                ? Perms.ALERT
                : Bot.confirmListedAdmin(interaction.User.Id)
                ? Perms.Listed
                : Bot.confirmServerPerms(serverID, interaction.User.Id) ? Perms.Server : (Perms?)null;
        }

        internal static void sendEmbed(Color color, string title = null, string description = null, string subtitle = "", string text = "", List<EmbedFieldBuilder> fields = null, IMessage message = null, IDiscordInteraction interaction = null, bool ephemeral = true)
        {
            fields ??= new();

            if (message != null)
                Bot.sendEmbed(message.Channel, color, title: title, description: description, fields: fields, subtitle: subtitle, text: text);

            else
            {
                if (!string.IsNullOrWhiteSpace(subtitle) || !string.IsNullOrWhiteSpace(text))
                    fields.Add(new EmbedFieldBuilder()
                    {
                        Name = !string.IsNullOrWhiteSpace(subtitle) ? subtitle : Config._blank,
                        Value = !string.IsNullOrWhiteSpace(text) ? text : Config._blank
					});

                interaction.RespondAsync(embed: new EmbedBuilder()
                {
                    Color = color,
                    Title = title,
                    Description = description,
                    Fields = fields
                }.Build(), ephemeral: ephemeral).Wait();
            }
        }
        #endregion

        #region bot management
        /// <summary> Set's Bot's Parameters | =setStatus [conditions: separated by new lines]
        /// username: (string)
        /// avatar: (image link)
        /// playing: ([Playing, Listening, Competing, Streaming] = string)
        /// status: (Online, Idle/AFK, DoNotDisturb, Invisible)
        /// </summary>
        public void updatebot(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = true;
            string commandName = "UpdateBot";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				List<string> rawConditions = msg.Split('\n').ToList();

                if (msg.Trim().Length > 0 && rawConditions.Count > 0 && msg.Trim().ToLower() != "status: offline")
                {
                    List<string> errorText = new();
                    List<string> successText = new();
                    string username = "", avatarLink = "", playing = "";
                    UserStatus? status = null;

                    foreach (string condition in rawConditions)
                    {
                        if (condition.Length > 0)
                        {
                            string conditionName = condition[..condition.IndexOf(":")].Trim();
                            string conditionValue = condition[(condition.IndexOf(":") + 1)..].Trim();
                            switch (conditionName)
                            {
                                case "username":
                                    if (!string.IsNullOrWhiteSpace(conditionValue))
                                        username = conditionValue;
                                    break;

								case "avatar":
									if (!string.IsNullOrWhiteSpace(conditionValue))
										avatarLink = conditionValue;
									break;

								case "playing":
									if (!string.IsNullOrWhiteSpace(conditionValue))
										playing = conditionValue;
									break;

                                case "status":
                                    if (Enum.TryParse(conditionValue[0].ToString().ToUpper() + conditionValue[1..], out UserStatus s) && s != UserStatus.Offline)
                                        status = s;
                                    else if (conditionValue.ToLower().Contains("offline"))
                                        errorText.Add("Cannot set status to Offline.");
                                    break;
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(username))
                    {
                        try
                        {
                            Bot._client.CurrentUser.ModifyAsync(bot => bot.Username = username).Wait();
                            successText.Add($"Updated username to `{username}`.");
                        }
                        catch (Exception e)
                        {
                            errorText.Add($"Error updating username to `{username}`.\n[`{e.InnerException.Message.Split('\n')[0]}`]");
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(avatarLink))
                    {
                        if (avatarLink.Contains("https://") || avatarLink.Contains("http://"))
                        {
                            MemoryStream stream = new();
                            bool saved = false;

                            if (avatarLink.Contains(".svg"))
                            {
                                try
                                {
                                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(avatarLink);
                                    request.Method = "GET";
                                    request.KeepAlive = true;
                                    request.ContentType = "image/png";
                                    request.Accept = "image/webp,image/apng";
                                    SvgDocument.Open<SvgDocument>(((HttpWebResponse)request.GetResponse()).GetResponseStream())
                                        .Draw(rasterWidth: 24, rasterHeight: 24).Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                                    stream.Position = 0;
                                    saved = true;
                                }
                                catch { errorText.Add($"Error updating avatar. Couldn't convert `{avatarLink}` to image."); }
                            }
                            else if (avatarLink.Contains(".png") || avatarLink.Contains(".jpg") || avatarLink.Contains(".webp"))
                            {
                                try
                                {
                                    stream = new MemoryStream(new WebClient().DownloadData(avatarLink))
                                    {
                                        Position = 0
                                    };
                                    saved = true;
                                }
                                catch { errorText.Add($"Error updating avatar. Couldn't convert `{avatarLink}` to image."); }
                            }
                            else
                            {
                                try
                                {
                                    System.Drawing.Image.FromStream(new MemoryStream(new WebClient().DownloadData(avatarLink)))
                                        .Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                                    stream.Position = 0;
                                    saved = true;
                                }
                                catch { errorText.Add($"Error updating avatar. Couldn't convert `{avatarLink}` to image."); }
                            }

                            if (saved)
                            {
                                try
                                {
                                    Bot._client.CurrentUser.ModifyAsync(bot => bot.Avatar = new Discord.Image(stream)).Wait();
                                    successText.Add("Updated Avatar.");
                                }
                                catch (Exception e) { errorText.Add($"Error updating avatar.\n[`{e.InnerException.Message.Split('\n').Last()}`]"); }
                            }
                        }
                        else if (avatarLink[1..].Contains(":\\"))
                        {
                            try
                            {
                                Bot._client.CurrentUser.ModifyAsync(bot => bot.Avatar = new Discord.Image(avatarLink)).Wait();
                                successText.Add("Updated Avatar.");
                            }
                            catch (Exception e) { errorText.Add($"Error updating avatar.\n[`{e.InnerException.Message.Split('\n').Last()}`]"); }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(playing))
                    {
                        try
                        {
                            string[] args = playing.Split('=');
                            string activity = args[1].Trim(),
                                type = args[0].Trim()[0].ToString().ToUpper() + args[0].Trim()[1..];
                            ActivityType activityType = ActivityType.Streaming;

                            if (Enum.TryParse(type, out activityType))
                            {
                                Bot._client.SetGameAsync(activity, type: activityType).Wait();
                                successText.Add($"Updated custom status to `{type} | {activity}`.");
                            }
                            else errorText.Add($"Invalid custom statys activity type [`{type} | {activity}`].");
                        }
                        catch (Exception e)
                        {
                            errorText.Add($"Error updating custom status.\n[`{e.InnerException.Message.Split('\n')[0]}`]");
                        }
                    }

                    if (status != null)
                    {
                        try
                        {
                            Bot._client.SetStatusAsync((UserStatus)status).Wait();
                            successText.Add($"Updated Status to {status}.");
                        }
                        catch (Exception e)
                        {
                            errorText.Add($"Error updating status to `{status}`.\n[`{e.InnerException.Message.Split('\n')[0]}`]");
                        }
                    }

                    List<EmbedFieldBuilder> fields = new();

					if (successText.Count > 0)
					{
						string successes = "";

                        foreach (string success in successText)
                            successes += $"{(successes.Length > 0 ? "\n" : "")}{success}";

                        if (errorText.Count == 0)
                        {
                            Bot.sendEmbed(message.Channel, Color.Green, title: $"**{commandName}**", description: successes);
                            return;
                        }
                        else
                        {
                            fields.Add(new EmbedFieldBuilder
                            {
                                Name = "✅ **Successes**",
                                Value = successes
                            });
                        }
                    }

                    if (errorText.Count > 0)
                    {
                        string errors = "";

                        foreach (string error in errorText)
                            errors += $"{(errors.Length > 0 ? "\n" : "")}{error}";

                        if (successText.Count == 0)
                        {
                            Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: errors);
                            return;
                        }
                        else
                        {
                            fields.Add(new EmbedFieldBuilder
                            {
                                Name = $"{_alert} **Errors**",
                                Value = errors
                            });
                        }
                    }

                    if (successText.Count > 0 && errorText.Count > 0) Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"**{commandName}**", fields: fields);
                    else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description, "Incorrect parameters:", errorMsg, message: message);
                }
                else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description, "Incorrect parameters:", errorMsg, message: message);
			}
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description + errorMsg, message: message);
			}
        }

        /// <summary> rebuilds bot's commands | =refreshCommands [optional: true/false to delete all] </summary>
        public void refreshcommands(SocketMessage message, bool reply, bool delete)
        {
            string commandName = "RefreshCommands";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{Config._adminCommandPrefix}{(Bot._testBot ? Config._adminCommandPrefix : "")}{commandName}".Length), reply, delete);

            if (!confirmPerms(command.perms, message, commandName)) return;



			Bot._commandsConfigured = _commandsConfigured_initialValue;
			Bot._ngCommandsConfigured = _ngCommandsConfigured_initialValue;
			bool deleteOldCommands = false;

            if (bool.TryParse(msg.Trim(), out bool deleteOld)) deleteOldCommands = deleteOld;

            try
            {
                Bot.buildCommands(deleteOldCommands).Wait();
                if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Refreshed Commands{(deleteOldCommands ? " and deleted old commands." : ".")}");
            }
            catch { sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", $"Error with refreshing commands{(deleteOldCommands ? " or deleting old commands." : ".")}", message: message); }
		}
		public void reloadCommands(SocketSlashCommand si)
		{
			string commandName = "RefreshCommands";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            commandName = "ReloadCommands";

			if (!confirmPerms(command.perms, si, commandName)) return;



            Bot._commandsConfigured = _commandsConfigured_initialValue;
            Bot._ngCommandsConfigured = _ngCommandsConfigured_initialValue;

			bool deleteOldCommands = false;
			if (si.Data.Options.First().Options?.Count > 0) deleteOldCommands = (bool)si.Data.Options.First().Options.First().Value;

			try
			{
				Bot.buildCommands(deleteOldCommands).Wait();
                sendEmbed(Color.Green, $"Refreshed Commands{(deleteOldCommands ? " and deleted old commands." : ".")}", interaction: si);
			}
			catch { sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", $"Error with refreshing commands{(deleteOldCommands ? " or deleting old commands." : ".")}", interaction: si); }
		}

		/// <summary> refreshes the bot's config | =refreshConfig [optional: true/false to refresh ScheduleDB] </summary>
		public void refreshconfig(SocketMessage message, bool reply, bool delete)
        {
            string commandName = "RefreshConfig";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{Config._adminCommandPrefix}{(Bot._testBot ? Config._adminCommandPrefix : "")}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;


            bool refreshScheduleDB = false;
            if (bool.TryParse(msg.Trim(), out bool refresh)) refreshScheduleDB = refresh;

            try
            {
                Config.resetConfig(refreshScheduleDB);
                Config.setupConfig(Bot._printConfigSetupInfo);
                if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Refreshed the config{(refreshScheduleDB ? " and schedule database." : ".")}");
			}
			catch { sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", $"Error with refreshing config{(refreshScheduleDB ? " or schedule database." : ".")}", message: message); }
		}
		public void reloadConfig(SocketSlashCommand si)
		{
			string commandName = "RefreshConfig";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			commandName = "ReloadConfig";

			if (!confirmPerms(command.perms, si, commandName)) return;


			bool refreshScheduleDB = false;
			if (si.Data.Options.First().Options?.Count > 0) refreshScheduleDB = (bool)si.Data.Options.First().Options.First().Value;

			try
			{
				Config.resetConfig(refreshScheduleDB);
				Config.setupConfig(Bot._printConfigSetupInfo);
				sendEmbed(Color.Green, $"Refreshed the config{(refreshScheduleDB ? " and schedule database." : ".")}", interaction: si);
			}
			catch { sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", $"Error with refreshing config{(refreshScheduleDB ? " or schedule database." : ".")}", interaction: si); }
		}

		/// <summary> reconnects to MySQL db | =reconnectMySQL </summary>
		public void reconnectmysql(SocketMessage message, bool _0, bool _1)
        {
            string commandName = "ReconnectMySQL";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, message, commandName)) return;



            try
            {
                bool connected = Bot.startSQL(Bot._printSQLInfo);

                Bot.sendEmbed(message.Channel, connected ? Color.Green : Color.LightOrange,
                    title: connected ? $"Successfully reconnected to MySQL Database." : $"{_alert} **ReconnectMySQL Error** {_alert}",
                    description: connected ? "" : "Error reconnecting to MySQL Database.");
            }
            catch (Exception e) { sendEmbed(Color.Red, $"{_alert} **{commandName} Error** {_alert}", $"Error reconnecting to MySQL Database.\n\n`{e}`", message: message); }
		}
		public void reconnectMySQL(SocketSlashCommand si)
		{
			string commandName = "ReconnectMySQL";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, si, commandName)) return;



			try
			{
				bool connected = Bot.startSQL(Bot._printSQLInfo);
				sendEmbed(connected ? Color.Green : Color.LightOrange, connected ? $"Successfully reconnected to MySQL Database." : $"{_alert} **ReconnectMySQL Error** {_alert}", connected ? "" : "Error reconnecting to MySQL Database.", interaction: si);
			}
			catch (Exception e) { sendEmbed(Color.Red, $"{_alert} **{commandName} Error** {_alert}", $"Error reconnecting to MySQL Database.\n\n`{e}`", interaction: si); }
		}

		/// <summary> reconnect to google sheets db | =reconnectGoogleSheets </summary>
		public void reconnectgooglesheets(SocketMessage message, bool _0, bool _1)
        {
            string commandName = "ReconnectGoogleSheets";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, message, commandName)) return;



            try
            {
                bool connected = SpreadsheetManager.connectToGoogleAPI(true);

                Bot.sendEmbed(message.Channel, connected ? Color.Green : Color.LightOrange,
                    title: connected ? $"Successfully reconnected to Google sheets API." : $"{_alert} **ReconnectGoogleSheets Error** {_alert}",
					description: connected ? "" : "Error reconnecting to Google sheets API.");
            }
			catch (Exception e) { sendEmbed(Color.Red, $"{_alert} **{commandName} Error** {_alert}", $"Error reconnecting to Google sheets API.\n\n`{e}`", message: message); }
		}
		public void reconnectGoogleSheets(SocketSlashCommand si)
		{
			string commandName = "ReconnectGoogleSheets";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, si, commandName)) return;



			try
			{
				bool connected = SpreadsheetManager.connectToGoogleAPI(true);
				sendEmbed(connected ? Color.Green : Color.LightOrange, connected ? $"Successfully reconnected to Google sheets API." : $"{_alert} **ReconnectGoogleSheets Error** {_alert}", connected ? "" : "Error reconnecting to Google sheets API.", interaction: si);
			}
			catch (Exception e) { sendEmbed(Color.Red, $"{_alert} **{commandName} Error** {_alert}", $"Error reconnecting to Google sheets API.\n\n`{e}`", interaction: si); }
		}

		/// <summary> change the DB that +Set writes to for the current server | =SwapSetDB [DB Name] </summary>
		public void swapsetdb(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = true;
            string commandName = "SwapSetDB";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



            try
            {
                IGuild server = Bot.getGuild(message.Channel.Id);
                if (Config._dbSwap.Exists(d => d.serverID == server.Id))
                {
                    Config._dbSwap.Remove(Config._dbSwap.Find(d => d.serverID == server.Id));
                    if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Reset Schedule Database to Default for {server.Name}");
                }
                else
                {
                    Config._dbSwap.Add((server.Id, Config._testDB));
                    if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Set Schedule Database to `{Config._testDB}` for {server.Name}");
                }
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description + errorMsg, message: message);
			}
        }

		/// <summary> shuts down bot | =shutdown </summary>
		public void shutdown(SocketMessage message, bool reply, bool _0)
		{
			string commandName = "ShutDown";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, message, commandName)) return;



            if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: "Shutting Down.");
            Bot._client.StopAsync();
		}
		public void shutDown(SocketSlashCommand si)
		{
			string commandName = "ShutDown";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, si, commandName)) return;


			bool restart = false;
			if (si.Data.Options.Count > 0) restart = (bool)si.Data.Options.First().Value;

            if (!restart)
			{
				sendEmbed(Color.Green, "Shutting Down.", interaction: si);
				Bot._client.StopAsync();
			}		
            else
            {
				sendEmbed(Color.DarkGreen, $"Restarting... {Bot.getEmote(Config._emojiNames["Loading"])}", interaction: si);
				Bot._restartedSlash = si;
				Bot._client.StopAsync().Wait();
				do
				{
					try
					{
						Bot.Main();
						return;
					}
					catch { }
				} while (Bot._client.CurrentUser.Status == UserStatus.Offline);
			}
		}

		/// <summary> restarts bot | =restart </summary>
		public void restart(SocketMessage message, bool reply, bool _0)
        {
            string commandName = "Restart";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

            if (!confirmPerms(command.perms, message, commandName)) return;



            if (reply) Bot.sendEmbed(message.Channel, Color.DarkGreen, title: $"Restarting... {Bot.getEmote(Config._emojiNames["Loading"])}");
            Bot._restarted = message.Channel;
            Bot._client.StopAsync().Wait();
            do
            {
                try
                {
                    Bot.Main();
                    return;
                }
                catch { }
            } while (Bot._client.CurrentUser.Status == UserStatus.Offline);
		}

		#region showadmins
		/// <summary> prints admins | =showAdmins or =showAdmins serverID </summary>
		public void showadmins(SocketMessage message, bool reply, bool delete)
        {
            string commandName = "ShowAdmins";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{Config._adminCommandPrefix}{(Bot._testBot ? Config._adminCommandPrefix : "")}showAdmins".Length), reply, delete).Trim();
            bool inDMs = Bot.isDMs(message.Channel);

            sendShowAdmins(command, msg, inDMs, message);
        }

        public void showAdmins(SocketSlashCommand si)
        {
            string commandName = "ShowAdmins";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

            string msg = si.Data.Options.Count > 0 ? si.Data.Options.First().Value.ToString().Trim() : "";
            bool inDMs = Bot.isDMs(si.Channel);

            sendShowAdmins(command, msg, inDMs, interaction: si);
        }

        public static void sendShowAdmins((string commandName, string category, string description, string properFormat, Perms perms) command, string msg, bool inDMs, IMessage message = null, SocketSlashCommand interaction = null)
        {

            List<EmbedFieldBuilder> print = new();
            List<SocketGuildUser> users = new();
            string printContains = "";
            ulong serverID = 0;
            print.Add(new EmbedFieldBuilder
            {
                Name = "Bot Owner:",
                Value = Bot._client.GetUser(Bot._client.GetApplicationInfoAsync().Result.Owner.Id).Username
            });
            printContains += print[0].Value;

            if (Config._admins.Count > 0)
            {
                EmbedFieldBuilder staticAdmins = new() { Name = "Static Admins:" };
                foreach (ulong id in Config._admins)
                {
                    IUser u = Bot._client.GetUserAsync(id).Result;
                    if (u != null) staticAdmins.Value += u.Username + "\n";
                }

                if (staticAdmins.Value != null) printContains += staticAdmins.Value;
                print.Add(staticAdmins);
            }

            if (msg.Length > 11)
            {
                if (ulong.TryParse(msg, out serverID)) Bot.addServer(serverID, Bot.getServerName(message != null ? message.Channel : interaction.Channel), inDMs); //create server if it's not in list
                else sendEmbed(Color.LightOrange, $"{_alert} **{command.commandName} Error** {_alert}", $"Couldn't find server with ID: [`{msg}`].", message: message, interaction: interaction);
            }
            else if (!inDMs) //For current Server
                serverID = Bot.getGuild(message != null ? message.Channel.Id : interaction.Channel.Id).Id;

            if ((message != null && !confirmPerms(command.perms, message, command.commandName, serverID)) || (interaction != null && !confirmPerms(command.perms, interaction, command.commandName, serverID))) return;



            users = Bot._client.GetGuild(serverID).Users.ToList().FindAll(u => u.GuildPermissions.Administrator || u.GuildPermissions.ManageGuild || u.GuildPermissions.BanMembers || u.GuildPermissions.ViewAuditLog);

            if (users.Count > 0)
            {
                EmbedFieldBuilder serverAdmins = new() { Name = $"Admins of **{Bot._client.GetGuild(serverID).Name}**:" };

                foreach (SocketGuildUser user in users)
                { if (!printContains.Contains(user.Username)) serverAdmins.Value += user.Username + "\n"; }

                if (serverAdmins.Value != null) printContains += serverAdmins.Value;
                print.Add(serverAdmins);
            }

            sendEmbed(Color.Green, "Admins:", fields: print, message: message, interaction: interaction);
        }
        #endregion

        /// <summary> resets admin list | =resetAdmins </summary>
        public void resetadmins(SocketMessage message, bool reply, bool _0)
        {
            string commandName = "ResetAdmins";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, message, commandName)) return;



            Config._admins = new List<ulong>();
            if (Config._admins.Count == 0)
            {
                if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: "Reset admins.");
            }
            else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", "Unable to reset admins.", message: message);
		}
		public void resetAdmins(SocketSlashCommand si)
		{
			string commandName = "ResetAdmins";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, si, commandName)) return;



			Config._admins = new List<ulong>();
			if (Config._admins.Count == 0) sendEmbed(Color.Green, "Reset admins.", interaction: si);
			else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", "Unable to reset admins.", interaction: si);
		}

		/// <summary> resets mimic list | =resetMimics </summary>
		public void resetmimics(SocketMessage message, bool reply, bool _0)
        {
            string commandName = "ResetMimics";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, message, commandName)) return;



            Bot._mimic = new List<ulong>();
            if (Bot._mimic.Count == 0)
            {
                if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: "Stopped mimicking.");
            }
            else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", "Unable to reset mimics.", message: message);
		}
		public void resetMimics(SocketSlashCommand si)
		{
			string commandName = "ResetMimics";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, si, commandName)) return;



			Bot._mimic = new List<ulong>();
			if (Bot._mimic.Count == 0) sendEmbed(Color.Green, "Stopped mimicking.", interaction: si);
			else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", "Unable to reset mimics.", interaction: si);
		}
		#endregion

		#region elevated user commands
		#region help
		/// <summary> shows admin commands | =help [Optional: Command category (show categories if blank)] </summary>
		public void help(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "Help";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



            try
            {
                string[] entries = msg.Split('\\');
                Perms? usersPerms = getUsersPerms(message, Bot.getGuild(message.Channel.Id).Id);

                if (entries.Length == 1 && !string.IsNullOrWhiteSpace(entries[0].Trim()))
                {
                    bool useCategory = entries[0].ToLower().Trim() != "all";
                    string category = !useCategory ? "" : _adminCommands.Find(c => c.category.ToLower().Contains(entries[0].ToLower().Trim())).category;
                    List<(string commandName, string category, string description, string properFormat, Perms perms)> commands = _adminCommands.FindAll(c => (int)c.perms >= (int)usersPerms && (!useCategory || (c.category == category)));

                    if ((!useCategory || (useCategory && !string.IsNullOrWhiteSpace(category))) && commands.Count > 0)
                        sendHelp(commands, useCategory, category, message);
                    else
                        Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: "Invalid Category.", subtitle: "Please use one of the following categories:", text: getAdminCategoriesStringFormat(this, message, includeAll: true));
                }
                else if (entries.Length == 0 || string.IsNullOrWhiteSpace(entries[0].Trim()))
                    Bot.sendEmbed(message.Channel, Color.Blue, title: $"{commandName} Categories", description: getAdminCategoriesStringFormat(this, message, includeAll: true));
                else
                    Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

        public void Help(SocketSlashCommand si)
        {
            bool consoleWrite = false;
            string commandName = "Help";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";

            if (!confirmPerms(command.perms, si, commandName)) return;



            try
            {
                string category = si.Data.Options.Count > 0 ? si.Data.Options.First().Value.ToString().Trim() : "";
                Perms? usersPerms = getUsersPerms(si, Bot.getGuild(si.Channel.Id).Id);

                if (!string.IsNullOrWhiteSpace(category) && category != "categories")
                {
                    bool useCategory = category != "all";
                    category = !useCategory ? "" : _adminCommands.Find(c => c.category.ToLower().Contains(category.ToLower().Trim())).category;
                    List<(string commandName, string category, string description, string properFormat, Perms perms)> commands = _adminCommands.FindAll(c => (int)c.perms >= (int)usersPerms && (!useCategory || (c.category == category)));

                    if ((!useCategory || (useCategory && !string.IsNullOrWhiteSpace(category))) && commands.Count > 0)
                        sendHelp(commands, useCategory, category, interaction: si);
                    else si.RespondAsync(embed: new EmbedBuilder()
                    {
                        Color = Color.LightOrange,
                        Title = $"{_alert} **{commandName} Error** {_alert}",
                        Description = "Invalid Category.",
                        Fields = new()
                        {
                            new()
                            {
                                Name = "Please use one of the following categories:",
                                Value = getAdminCategoriesStringFormat(this, interaction: si, includeAll: true)
                            }
                        }
                    }.Build(), ephemeral: true);
                }
                else si.RespondAsync(embed: new EmbedBuilder()
                {
                    Color = Color.Blue,
                    Title = $"{commandName} Categories",
                    Description = getAdminCategoriesStringFormat(this, interaction: si, includeAll: true)
                }.Build());
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                si.RespondAsync(embed: new EmbedBuilder()
                {
                    Color = Color.LightOrange,
                    Title = $"{_alert} **{commandName} Error** {_alert}",
                    Description = description + errorMsg
                }.Build(), ephemeral: true);
            }
        }

        private static void sendHelp(List<(string commandName, string category, string description, string properFormat, Perms perms)> commands, bool useCategory, string category, IMessage message = null, IDiscordInteraction interaction = null)
        {
            List<(List<EmbedFieldBuilder> embeds, string count)> fieldsList = new();
            (List<EmbedFieldBuilder> embeds, string count) fields = (null, "");

            foreach ((string commandName, string category, string description, string properFormat, Perms perms) cmd in commands)
            {
                if ((message != null && confirmPerms(cmd.perms, message, cmd.commandName, runningHelpCommand: true)) || (interaction != null && confirmPerms(cmd.perms, interaction, cmd.commandName, runningHelpCommand: true)))
                {
                    if ((useCategory && (fields.embeds == null || fields.embeds.Count >= 24) && cmd.category == category) ||
                    (!useCategory && (fields.embeds == null || fields.embeds.Count >= 24 || commands.IndexOf(cmd) == 0 || (commands.IndexOf(cmd) > 0 && commands[commands.IndexOf(cmd) - 1].category != cmd.category))))
                    {
                        if (commands.IndexOf(cmd) > 0 && fields.embeds != null) fieldsList.Add(fields);

                        fields = (new List<EmbedFieldBuilder>(), cmd.category);
                    }

                    if (!useCategory || (useCategory && cmd.category == category))
                    {
                        fields.embeds.Add(new EmbedFieldBuilder()
                        {
                            Name = cmd.commandName,
                            Value = cmd.description + cmd.properFormat
                        });
                    }
                }
            }

            fieldsList.Add(fields);

            foreach ((List<EmbedFieldBuilder> embeds, string count) entry in fieldsList)
            {
                Bot.sendEmbed(message != null ? message.Channel : (IMessageChannel)Bot._client.GetChannel(interaction.ChannelId.Value), color: Color.Green, fields: entry.embeds,
                    title: $"{entry.count} Commands{(fieldsList.Count > 1 ? $" [{(fieldsList.IndexOf(entry) + 1)}/{fieldsList.Count}]" : "")}:");
            }

			if (message == null)
			{
				interaction.DeferAsync().Wait();
				interaction.DeleteOriginalResponseAsync().Wait();
			}
		}
        #endregion

        /// <summary> sets _advancedResults to true, and then calls -findschedules </summary>
        public void findschedules(SocketMessage message, bool _0, bool _1)
        {
            string commandName = "FindSchedules";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, message, commandName)) return;



            Bot.getServer(message.Channel)._SH._advancedResults = true;
            UserCommand.findschedules(message);
        }
		#endregion

		#region server management
		#region nickname
		/// <summary> Set a user's nickname | =setNickname serverID\userId\nickname </summary>
		public void setnickname(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "SetNickname";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

            try
            {
                string[] entries = msg.Split('\\');
                if (entries.Length == 3 &&
                    ulong.TryParse(entries[1].Trim(), out ulong userID) &&
                    ulong.TryParse(entries[0].Trim(), out ulong serverID))
                {
                    if (!confirmPerms(command.perms, message, commandName, serverID)) return;



                    SocketGuildUser user = Bot._client.GetGuild(serverID).GetUser(userID);
                    setNicknameUpdate(serverID, user, entries[2], message, reply: reply);
                }
                else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description, "Incorrect parameters:", errorMsg, message: message);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                if (!confirmPerms(command.perms, message, commandName)) return;
                sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description + errorMsg, message: message);
            }
        }

        public static void setNickname(SocketUserCommand ui)
		{
			string commandName = "SetNickname";
			(string commandName, string category, string description, string properFormat, Perms perms) command = Bot._ac._adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
            ulong serverID = ui.GuildId ?? Bot.getGuildChannel(ui.Channel.Id).Guild.Id;

			try
			{
				if (serverID > 0)
				{
					if (!confirmPerms(command.perms, ui, commandName, serverID)) return;



                    ModalBuilder modal = new()
                    {
                        CustomId = $"SetNickname[{ui.Data.Member.Id}]",
                        Title = "Update Nickname",
                        Components = new()
                    };

                    IGuildUser user = Bot.getGuildUser(ui.Channel.Id, ui.Data.Member.Id);

                    modal.Components.WithTextInput(new()
                    {
                        CustomId = "nickname",
                        Label = "Enter new nickname",
                        MaxLength = 32,
                        MinLength = 0,
                        Required = false,
                        Style = TextInputStyle.Short,
                        Placeholder = "No value resets nickname.",
                        Value = user?.Nickname ?? null
                    });

                    ui.RespondWithModalAsync(modal.Build()).Wait();
                    return;
				}
			}
			catch
			{
				if (!confirmPerms(command.perms, ui, commandName)) return;
				sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description + errorMsg, interaction: ui);
			}
		}

        public static void setNickname_Selected(IModalInteraction mi)
		{
			string newNickname = mi.Data.Components.First().Value.ToString();
            string userIDValue = mi.Data.CustomId[(mi.Data.CustomId.IndexOf('[') + 1)..^1];
            IGuildUser user;

			if (ulong.TryParse(userIDValue, out ulong userID) && (user = Bot.getGuildUser(mi.ChannelId.Value, userID)) != null)
			{
                Bot.changeNickname(user, newNickname).Wait();

                mi.DeferAsync().Wait();
                mi.DeleteOriginalResponseAsync().Wait();
			}
		}

        private static void setNicknameUpdate(ulong serverID, IGuildUser user, string newNickname, IMessage message = null, SocketSlashCommand interaction = null, bool reply = true)
        {
			Bot.changeNickname(user, newNickname).Wait();

			user = Bot._client.GetGuild(serverID).GetUser(user.Id);
            if ((user.Nickname ?? user.Username) == newNickname)
            {
                if (reply && message != null) Bot.sendEmbed(message.Channel, Color.Green, title: $"**{user.Username}'s** nickname updated to `{(user.Nickname ?? user.Username)}`.");
                else interaction?.RespondAsync(embed: new EmbedBuilder()
                {
                    Color = Color.Green,
                    Title = $"**{user.Username}'s** nickname updated to `{(user.Nickname ?? user.Username)}`."
                }.Build(), ephemeral: true);
            }
            else sendEmbed(Color.LightOrange, $"{_alert} **Error** {_alert}", subtitle: $"Unable to upate **{user.Username}'s** nickname to `{newNickname}`", 
                text: Bot._client.GetGuild(serverID) == null ? "*Note that you cannot change nicknames in DMs*" : "*Check the log for more details.*", message: message, interaction: interaction); 
                
		}
		#endregion

		#region get info
		/// <summary> Get channels from a specific server | =getChannels [Optional: server ID (if blank, current server)]\[Optional: searchFor] </summary>
		public void getchannels(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetChannels";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				ulong serverID = 0;
				string searchFor = "";
				SocketGuild server;

                if (msg.Trim().Length > 0)
                {
                    if (msg.Contains('\\'))
                    {
                        string[] args = msg.Split('\\');
                        searchFor = args[1];
                        if (ulong.TryParse(args[0], out ulong sID)) serverID = sID;
                    }
                    else if (!ulong.TryParse(msg.Trim(), out serverID)) searchFor = msg.Trim();
                }

                if (serverID == 0) serverID = Bot.getGuild(message.Channel.Id).Id;

				if (!confirmPerms(command.perms, message, commandName, serverID)) return;



                if ((server = Bot._client.GetGuild(serverID)) != null)
                {
                    List<string> list = new();
                    string currentList = "";

                    foreach (SocketGuildChannel c in server.Channels)
                    {
                        if (searchFor == "" || c.Name.Contains(searchFor))
                        {
                            string toAdd = $"[{c.Name}]({Bot.getChannelLink(c.Id)}): `{c.Id}`\n";
                            if (currentList.Length + toAdd.Length >= 4096)
                            {
                                list.Add(currentList);
                                currentList = "";
                            }

                            currentList += toAdd;
                        }
                    }

					if (!list.Exists(s => s == currentList)) list.Add(currentList);

					foreach (string s in list)
						Bot.sendEmbed(message.Channel, Color.Green, title: $"Channels in {server.Name}{(searchFor != "" ? $" Containing `{searchFor}`" : "")}:", description: s);

                }
                else sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", $"Couldn't find server with ID: [`{msg}`].", message: message);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                if (!confirmPerms(command.perms, message, commandName)) return;
				sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description + errorMsg, message: message);
			}
        }

		/// <summary> Get roles from a specific server | =getRoles [Optional: server ID (if blank, current server)]\[Optional: searchFor] </summary>
		public void getroles(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetRoles";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				ulong serverID = 0;
				string searchFor = "";
				SocketGuild server;

                if (msg.Trim().Length > 0)
                {
                    if (msg.Contains('\\'))
                    {
                        string[] args = msg.Split('\\');
                        searchFor = args[1];
                        if (ulong.TryParse(args[0], out ulong sID)) serverID = sID;
					}
                    else if (!ulong.TryParse(msg.Trim(), out serverID)) searchFor = msg.Trim();
                }

                if (serverID == 0) serverID = Bot.getGuild(message.Channel.Id).Id;

				if (!confirmPerms(command.perms, message, commandName, serverID)) return;



                if ((server = Bot._client.GetGuild(serverID)) != null)
                {
                    List<string> list = new();
                    string currentList = "";

                    foreach (SocketRole r in server.Roles)
                    {
                        if (searchFor == "" || r.Name.Contains(searchFor))
                        {
                            string toAdd = $"{r.Name}: `{r.Id}`\n";
                            if (currentList.Length + toAdd.Length >= 4096)
                            {
                                list.Add(currentList);
                                currentList = "";
                            }

                            currentList += toAdd;
                        }
                    }

					if (!list.Exists(s => s == currentList)) list.Add(currentList);

                    foreach (string s in list)
                        Bot.sendEmbed(message.Channel, Color.Green, title: $"Roles in {server}{(searchFor != "" ? $" Containing `{searchFor}`" : "")}:", description: s);
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: $"Couldn't find server with ID: [`{msg}`].");
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                if (!confirmPerms(command.perms, message, commandName)) return;
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

        public static void getList(SocketSlashCommand si)
		{
			string commandName = "GetChannels";
			(string commandName, string category, string description, string properFormat, Perms perms) command = Bot._ac._adminCommands.Find(c => c.commandName == commandName);
			string description = command.description,
                  list = "", searchFor = "";
            ulong serverID = si.GuildId.Value;

            foreach (var option in si.Data.Options)
            {
                string name = option.Name, value = option.Value.ToString();

                switch (name)
                {
                    case "list": list = value; 
                        break;

                    case "server":
                    {
                        if (ulong.TryParse(value.Trim(), out ulong sid)) serverID = sid;

                        SocketGuild server = null;
                        if ((server = Bot._client.GetGuild(serverID)) == null || !server.GetUsersAsync().FlattenAsync().Result.ToList().Exists(u => u.Id == si.User.Id))
                        {
                            si.RespondAsync(embed: new EmbedBuilder()
                            {
                                Color = Color.Red,
                                Title = $"{_alert} GetList Error {_alert}",
                                Description = $"Sorry, I don't have access to any servers with ID `{serverID}`."
                            }.Build(), ephemeral: true);
                        }

                        break;
                    }

                    case "searchfor": searchFor = value; 
                        break;
                }
            }

            (string title, string info) = getInfo(list, Bot.getServer(Bot._client.GetGuild(serverID).TextChannels.First().Id), searchFor, serverID == si.GuildId.Value);

            string[] infoLines = info.Split('\n');
            string[] infoEmbeds = new string[] { "", "", "" };
            int index = 0;

            foreach(string i in infoLines)
            {
                if (infoEmbeds[index].Length + i.Length + 1 >= 4096) index++;

                infoEmbeds[index] += (i.Replace("[^v]", "\n") + "\n");
            }

            List<Embed> embeds = new();
            foreach(string i in infoEmbeds)
            {
                if (i.Length > 0) embeds.Add(new EmbedBuilder()
                {
                    Color = !string.IsNullOrWhiteSpace(i) ? Color.Green : Color.Orange,
                    Title = infoEmbeds.Length > 1 ? title : "",
                    Description = !string.IsNullOrWhiteSpace(i) ? i : "No results."
                }.Build());
            }

            si.RespondAsync(embeds: embeds.ToArray()).Wait();
		}

        public static (string, string) getInfo(string type, Server server, string searchFor = "", bool sameServer = true)
		{
			string title = $"__X on {server._name}__{(searchFor.Length > 0 ? $" \n🔍 Searching for: [{searchFor}]" : "")}", info = "";

            switch (type)
            {
                case "channels":
                {
                    title = title.Replace("X", "Channels");
                    foreach (SocketGuildChannel channel in Bot._client.GetGuild(server._id).Channels.ToList().Where(c => c.Name.Contains(searchFor) || c.Name.ToLower().Contains(searchFor.ToLower())))
                    {
                        ChannelType channelType = channel.GetChannelType().Value;
                        string icon = channelType switch
                        {
                            ChannelType.Category => "C",
                            ChannelType.Forum    => "F",
                            ChannelType.Group    => "G",
                            ChannelType.Stage    => "S",
                            ChannelType.Store    => "S",
                            ChannelType.News     => $"{Bot.getEmote(Config._emojiNames["News"])}",
                            ChannelType.Text     => $"{Bot.getEmote(Config._emojiNames["Channel"])}",
                            ChannelType.Voice    => $"{Bot.getEmote(Config._emojiNames["Voice"])}",
                            ChannelType.NewsThread or ChannelType.PrivateThread or ChannelType.PublicThread => $"{Bot.getEmote(Config._emojiNames["Thread"])}",
                            _ => "?"
                        };

                        info += $"{(channelType.ToString().Contains("hread") ? "> " : "")}{icon} [{channel.Name}]({Bot.getChannelLink(channel.Id)})\n";
                    }

                    break;
                }

				case "roles":
                {
                    title = title.Replace("X", "Roles");
                    foreach (SocketRole role in Bot._client.GetGuild(server._id).Roles.ToList().Where(r => (r.Name.Contains(searchFor) || r.Name.ToLower().Contains(searchFor.ToLower())) && !r.IsEveryone).OrderByDescending(r => r.Position))
                    {
                        string count = $"`{role.Members.Count()} {(role.Members.Count() < 10 ? "  " : role.Members.Count() < 100 ? " " : "")} user{(role.Members.Count() != 1 ? "s" : " ")}`";
						info += $"{count} {(role.Emoji != null ? $"{role.Emoji} " : "")}{(sameServer ? role.Mention : $"@{role.Name}")}\n";
                    }

                    break;
                }

				case "emotes":
                {
                    title = title.Replace("X", "Emotes");
                    foreach (GuildEmote emote in Bot._client.GetGuild(server._id).Emotes.ToList().Where(e => e.Name.Contains(searchFor) || e.Name.ToLower().Contains(searchFor.ToLower())))
                        info += $"{emote} `{emote.Name}`\n";
                    break;
                }

				case "users":
                {
                    bool firstBot = true;
                    title = title.Replace("X", "Users");
                    foreach (SocketGuildUser user in Bot._client.GetGuild(server._id).Users.ToList().Where(u => u.Nickname != null ?
                        u.Nickname.Contains(searchFor) || u.Nickname.ToLower().Contains(searchFor.ToLower()) : u.Username.Contains(searchFor) || u.Username.ToLower().Contains(searchFor.ToLower())).
                        OrderByDescending(u => u.Hierarchy).OrderBy(u => u.IsBot))
                    {
                        string icon = $"{Bot.getEmote(Config._emojiNames["Member"])}";
						if (user.GuildPermissions.ManageGuild) icon = $"{Bot.getEmote(Config._emojiNames["Mod"])}";
						if (user.IsBot)
						{
							icon = firstBot ? $"[^v]**━━━━━━ __Bots__ ━━━━━━**[^v]\n" : "";

							icon += $"{Bot.getEmote(Config._emojiNames["Bot"])}";
							firstBot = false;
						}

						if (user.Hierarchy == int.MaxValue) icon = $"{Bot.getEmote(Config._emojiNames["Owner"])}";

						info += $"{icon} {user.DisplayName}#{user.DiscriminatorValue}[^v]{user.Mention}[^v]\n";
                    }

                    break;
                }

				case "userswrole":
                {
                    title = title.Replace("X", $"Users with roles similar to `'{searchFor}'`");
                    foreach (SocketGuildUser user in Bot._client.GetGuild(server._id).Users.ToList().Where(u => u.Roles.ToList().Exists(r => r.Name.Contains(searchFor) || r.Name.ToLower().Contains(searchFor.ToLower()))))
                        info += $"{user.DisplayName}{user.DiscriminatorValue} | <@{user.Id}>\n";
                    break;
                }
			}

            return (title, info);
		}
		#endregion

		/// <summary> Leave a server | =leaveServer [Optional: server ID (if blank, current server)] </summary>
		public void leaveserver(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "LeaveServer";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			if (!confirmPerms(command.perms, message, commandName)) return;



			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

            try
            {
                ulong serverID = 0;

                if (msg.Trim().Length > 0)
                {
                    if (ulong.TryParse(msg.Trim(), out ulong sID)) serverID = sID;
                }
                else serverID = Bot.getGuild(message.Channel.Id).Id;

                if (serverID > 0)
                {
                    IGuild server = Bot._client.GetGuild(serverID);
                    server.LeaveAsync();
                    Bot.sendEmbed(message.Channel, Color.Green, title: $"Left {server.Name}");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

        public void updatedividerroles(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "UpdateDividerRoles";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				ulong serverID = 0;

				if (msg.Trim().Length > 0)
				{
					if (ulong.TryParse(msg.Trim(), out ulong sID)) serverID = sID;
				}
				else serverID = Bot.getGuild(message.Channel.Id).Id;

				if (!confirmPerms(command.perms, message, commandName, serverID)) return;




				if (serverID > 0)
				{
					(int count, int updated) = RoleManager.updateExistingUsers(Bot._client.GetGuild(serverID));

					if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Updated `{updated}`/`{count}` users.");
				}
				else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
			}
			catch
			{
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
			}
		}

        public static void createtestingserver(SocketMessage message, bool reply, bool delete)
        {
            confirmPerms(Perms.Owner, message, "CreateTestingServer");

            IGuild server = Bot.getGuild(message.Channel.Id);
            IGuild existingServer = Bot._client.GetGuild(Config._napGodServerID);

            foreach (IRole role in existingServer.Roles.OrderByDescending(r => r.Position))
            {
				server.CreateRoleAsync(role.Name, role.Permissions, role.Color, role.IsHoisted, role.IsMentionable).Wait();
                MM.Print(true, $"Created Role @{role.Name}");
			}

            server.CreateCategoryAsync("Meta").Wait();
            server.CreateTextChannelAsync("welcome").Wait();
            server.CreateTextChannelAsync("get_started").Wait();
			server.CreateCategoryAsync("Polyphasic Discussion").Wait();
			server.CreateTextChannelAsync("polyphasic-questions").Wait();
			server.CreateTextChannelAsync("beginners").Wait();
			server.CreateTextChannelAsync("general-polyphasic").Wait();
			server.CreateCategoryAsync("Misc").Wait();
			server.CreateTextChannelAsync("botspam").Wait();
		}

        #region welcome
        public static void _welcome(SocketSlashCommand sc)
		{
			string commandName = "Welcome";
			(string commandName, string category, string description, string properFormat, Perms perms) command = Bot._ac._adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			if (!confirmPerms(command.perms, sc, commandName)) return;



			try
			{
				sc.RespondWithModalAsync(new ModalBuilder()
				{
					CustomId = "Welcome[Create]",
					Title = "Create Welcome Message",
					Components = new ModalComponentBuilder()
						.WithTextInput(new()
						{
							CustomId = "title",
							Label = "Enter the Welcome Message's title",
							MaxLength = 200,
							MinLength = 0,
							Required = false,
							Style = TextInputStyle.Short
						})
						.WithTextInput(new()
						{
							CustomId = "message",
							Label = "Enter the message that will be displayed",
							MaxLength = 2000,
							MinLength = 0,
							Required = true,
							Style = TextInputStyle.Paragraph
						})
				}.Build()).Wait();
			}
			catch
			{
				if (!confirmPerms(command.perms, sc, commandName)) return;
				sendEmbed(Color.LightOrange, $"{_alert} **{commandName} Error** {_alert}", description + errorMsg, interaction: sc);
			}
		}

        public static void welcomeSlashSubmit_CreateMessage(IModalInteraction mi)
		{
            string title = mi.Data.Components.ToList().Find(c => c.CustomId == "title")?.Value,
			     message = mi.Data.Components.ToList().Find(c => c.CustomId == "message")?.Value;

			message = Bot.replaceUserMentions_ModalSubmit(message, mi.GuildId.Value);
			message = Bot.replaceTextRoleMentions_ModalSubmit(message, mi.GuildId.Value);
			message = Bot.replaceChannelMentions_ModalSubmit(message, mi.GuildId.Value);
			message = Bot.replaceTextEmoji_ModalSubmit(message);

			Bot.getIMessageChannel(mi.ChannelId.Value).SendMessageAsync(
                embed: new EmbedBuilder()
			    {
				    Color = Color.Green,
				    Title = title,
				    Description = message
			    }.Build(), 
                components: new ComponentBuilder().WithButton(new ButtonBuilder()
			    {
				    CustomId = "Welcome:start",
				    Style = ButtonStyle.Primary,
				    Label = "Get Started!"
			    }).Build()).Wait();

			mi.DeferAsync().Wait();
			//mi.DeleteOriginalResponseAsync().Wait();
		}

		public static void welcomeButtonClick_SendQuestionnaire(SocketMessageComponent mc)
		{
            if (Bot.getGuildUser(mc.Channel.Id, mc.User.Id).RoleIds.Contains(Bot.getRole("New Member", mc.GuildId.Value).Id))
                mc.RespondWithModalAsync(new ModalBuilder()
                {
                    CustomId = "Welcome[User]",
                    Title = "Welcome Questionnaire",
                    Components = new ModalComponentBuilder()
                    .WithTextInput(new TextInputBuilder()
                    {
                        CustomId = "age",
                        Label = "What is your age?",
                        Placeholder = "Important for finding schedules you can do.",
                        Style = TextInputStyle.Short,
                        MaxLength = 3,
                        Required = true
                    })
                    .WithTextInput(new TextInputBuilder()
                    {
                        CustomId = "activity",
                        Label = "Current and desired physical activity?",
                        Placeholder = "Important for finding schedules you can do.",
                        Style = TextInputStyle.Paragraph,
                        MaxLength = 500,
                        Required = true
                    })
                    .WithTextInput(new TextInputBuilder()
                    {
                        CustomId = "hours",
                        Label = "Current sleep hours, need, and sleep quality?",
                        Placeholder = "Hrs you sleep, Hrs to feel rested, Wake freq.",
                        Style = TextInputStyle.Paragraph,
                        MaxLength = 500,
                        Required = true
                    })
                    .WithTextInput(new TextInputBuilder()
                    {
                        CustomId = "experience",
                        Label = "Past polyphasic sleeping experience, if any?",
                        Placeholder = "Any schedules you've attempted or adapted to.",
                        Style = TextInputStyle.Paragraph,
                        MaxLength = 1000,
                        Required = true
                    })
                    .WithTextInput(new TextInputBuilder()
                    {
                        CustomId = "reasons",
                        Label = "Why you're interested in poly sleep?",
                        Style = TextInputStyle.Paragraph,
                        MaxLength = 1000,
                        Required = true
                    })
                }.Build()).Wait();
            
            else mc.RespondAsync($"If you want to send a new welcome embed, please ask a mod to give you the <@&{Bot.getRole("New Member", mc.GuildId.Value).Id}> role.", ephemeral: true);
		}

		internal static List<(EmbedBuilder embed, ulong userID, int age, SocketMessageComponent mc)> _welcomeEmbeds = new();
        public static void welcomeQuestionnaireSubmit_AskAdvisingQ(IModalInteraction mi)
        {
            bool consoleWrite = true;

            string age = "", activity = "", hours = "", experience = "", reasons = "";

            foreach (IComponentInteractionData parameter in mi.Data.Components)
            {
                string name = parameter.CustomId, value = parameter.Value.Trim();

                switch (name)
                {
                    case "age": age = value; break;
                    case "activity": activity = value; break;
                    case "hours": hours = value; break;
                    case "experience": experience = value; break;
                    case "reasons": reasons = value; break;
                }
            }

            IGuildUser user = Bot.getGuildUser(mi.ChannelId.Value, mi.User.Id);

            if (!int.TryParse(age, out int ageNum))
                MM.Print(consoleWrite, $"{mi.User.Username} - invalid age [{age}]");

            _welcomeEmbeds.Add((new()
            {
                Color = Color.LighterGrey,
				Title = "Welcome!",
                Author = new ()
                {
                    Name = $"{user.Username}#{user.Discriminator}",
                    IconUrl = user.GetDisplayAvatarUrl()
				},
				Fields = new List<EmbedFieldBuilder>()
                {
                    new() { Name = "__Age:__", Value = age },
                    new() { Name = "__Current & desired physical activity:__", Value = activity },
                    new() { Name = "__Current sleep hours__; __Hours to feel rested__; __Wake frequency:__", Value = hours },
                    new() { Name = "__Polyphasic experience:__", Value = experience },
                    new() { Name = "__Reasons for wanting to sleep polyphasically:__", Value = reasons }
                }
            }, user.Id, ageNum, null));

            mi.RespondAsync(ephemeral: true,
                embed: new EmbedBuilder()
                {
                    Author = new EmbedAuthorBuilder() { Name = "Last step!" },
                    Color = Bot.getColorByHash("#A69B00"),
                    Title = $"{Bot.getEmote(Config._emojiNames["Question"])} Do you need help choosing or creating a schedule?",
					Description = $"If not, skip the rest of this message and click `No` below!\n\n" +
                    $"If you do, it's crucial that we know when you can and can't sleep to be able to help you.\n" +
                    $"**1.** Fill out this napchart [https://napchart.com/qcpfj](https://napchart.com/qcpfj).\n" +
                    $"**2.** Click `Generate Snapshot Link` and copy the link.\n" +
                    $"**3.** Click `Yes` below and paste the link in the window that appears.\n" +
                    $"**4.** Done! 🥳"
				}.Build(),
                components: new ComponentBuilder()
                    .WithButton(new ButtonBuilder() 
                    {
                        Label = "Yes",
                        CustomId = "Welcome:y",
                        Style = ButtonStyle.Success
                    })
				    .WithButton(new ButtonBuilder()
				    {
                        Label = "No",
					    CustomId = "Welcome:n",
					    Style = ButtonStyle.Danger
				    }).Build());
        }

		public static void welcomeYesAdvising_SendModal(SocketMessageComponent mc)
		{
			(EmbedBuilder e, ulong u, int a, SocketMessageComponent _) = _welcomeEmbeds.Find(e => e.userID == mc.User.Id);
			_welcomeEmbeds.RemoveAll(e => e.userID == mc.User.Id);
            _welcomeEmbeds.Add((e, u, a, mc));
            
			mc.RespondWithModalAsync(new ModalBuilder()
			{
				CustomId = "Welcome[Napchart]",
				Title = "Availability Napchart Submission",
				Components = new ModalComponentBuilder()
					.WithTextInput(new TextInputBuilder()
					{
						CustomId = "link",
						Label = "Submit napchart link.",
						Placeholder = "Click 'Generate Snapchot Link' and send that.",
						Style = TextInputStyle.Short,
						MinLength = 16,
                        MaxLength = 130,
						Required = true
					})
			}.Build()).Wait();
		}

		public static void welcomeScheduleHelpClicked_SendLastMessage(IDiscordInteraction i)
        {
            bool selectedYes = i is IModalInteraction;

            (EmbedBuilder embed, ulong userID, int age, SocketMessageComponent mc) = _welcomeEmbeds.Find(e => e.userID == i.User.Id);
			mc ??= i as SocketMessageComponent;

			try
			{
				IGuildUser user = Bot.getGuildUser(i.ChannelId.Value, userID);
				string underageLink = "";
                ulong serverID = i.GuildId.Value;

				if (age is > 0 and < 19)
				{
					user.AddRoleAsync(Bot.getRole("Underage", serverID)).Wait();

					(string commandName, NapGodText.Category _, string text) = NapGodText._help.Find(o => o.commandName == "underage");

					IUserMessage message = Bot.sendNGHelpCommand(Bot.getIMessageChannel(Bot.getChannel("botspam", serverID).Id), commandName, text, user);

					if (message != null) underageLink = Bot.getMessageLink(message);
				}

                if (selectedYes)
				{
					string napchartLink = (i as IModalInteraction).Data.Components.First().Value;
                    bool isNCOnline  = NapchartInterface.isNapchartOnline(),
						 notBlank = false, isLink = false, isNapchart = false, isValidNapchart = false, notOriginal = false, notEmpty = false;

					if (isNCOnline)
					{
						notBlank = !string.IsNullOrWhiteSpace(napchartLink);
						isLink = Uri.IsWellFormedUriString(napchartLink, UriKind.Absolute);
						if (notBlank && isLink)
						{
                            isNapchart = napchartLink.ToLower().Contains("napchart.com/");
							isValidNapchart = NapchartInterface.isLinkValidNapchart(napchartLink);
							notOriginal = napchartLink != "http://napchart.com/qcpfj";
                            if (isValidNapchart)	
                                notEmpty = !NapchartInterface.getChart(napchartLink).Contains("\"elements\":[],");
						}
					}

					if (isNCOnline && notBlank && isLink && isValidNapchart && notOriginal && notEmpty)
                    {
                        embed.WithImageUrl(NapchartInterface.getChartImg(napchartLink));
                        _welcomeEmbeds.RemoveAll(e => e.userID == userID);
					}
                    else
                    {
						mc.ModifyOriginalResponseAsync(m =>
                        {
                            m.Embed = new EmbedBuilder()
                            {
                                Color = isNCOnline ? Color.Red : Color.Orange,
                                Title = isNCOnline ? $"{_alert} Invalid Napchart Link {_alert}" : "Napchart Site Error",
                                Description = !isNCOnline ? "Napchart appears to be offline, please press the button below to skip this step for now." :
                                (
                                    $"**You Entered:** __[[{napchartLink}]({napchartLink})]__\n\n" +
                                    (
                                        !notBlank || !isLink ?
                                            "Hmm... It doesn't seem you entered a link at all.\n" +
                                            "Please submit a valid napchart link." :

                                        isNapchart && !isValidNapchart ?
											"Hm, something went wrong with the link you provided, and it doesn't seem to work.\n" +
											"Please check your link and make sure it's a valid napchart link!" :

										!isNapchart ?
                                            "That's... not a napchart link.\n" +
											"Please submit a valid napchart link." :

									    !notOriginal ?
										    "Whoops, it looks like you submitted the original link!\n" +
										    "Make sure you click the `Generate Snapshot Link` button, and copy the link that it gives in the popup." :

									    !notEmpty ?
										    "Whoops, it looks like you didn't add anything to the napchart.\n\n" +
										    "Please at least add Brown segments signifying where you can sleep and Blue segments where you're busy." :

										    //unknown error
										    "Something went wrong, please try resubmitting. If that doesn't work, please contact a moderator."
								    ) +
								"\n\nWhen you're ready, click the button below!"
							    )
							}.Build();

                            m.Components = new ComponentBuilder().WithButton(new ButtonBuilder()
                            {
                                CustomId = isNCOnline ? "Welcome:y" : "Welcome:napchartoffline",
                                Label = isNCOnline ? "Retry" : "Continue",
                                Style = isNCOnline ? ButtonStyle.Success : ButtonStyle.Primary
                            }).Build();
                        }).Wait();
                        i.DeferAsync().Wait();

                        return;
                    }

					embed
						.WithColor(Bot.getColorByHash("#e7007e"))
						.WithFooter("💡 Requested scheduling help.");
				}
				else _welcomeEmbeds.RemoveAll(e => e.userID == userID);

				user.RemoveRoleAsync(Bot.getRole("New Member", serverID)).Wait();

                Embed updateEmbed = new EmbedBuilder()
                {
                    Color = Color.Green,
                    Title = "You're All Set!",
                    Description = "Before you go, here are some links to help you get started!\n\n" +
                    (
                        string.IsNullOrEmpty(underageLink) || !Uri.IsWellFormedUriString(underageLink, UriKind.Absolute) ? "" :
                        $"{_alert} __**Since you're under 19 years old:**__\n" +
                        $"[Please read this]({underageLink}) for important information regarding polyphasic sleeping for minors.\n\n"
                    ) +
                    $"{Bot.getEmote(Config._emojiNames["Tag"])} __**Want to update your roles?**__\n" +
                    $"- Select [**Channel Access**]({Config._channelAccessLink[serverID]}) roles.\n" +
                    $"- Select [**Notification Toggle**]({Config._notificationsLink[serverID]}) roles.\n\n" +
                    $"ℹ️ __**Looking for sleep advice or info?**__\n" +
                    $"__[#beginners]({Bot.getChannelLink(Bot.getChannel("beginners", serverID).Id)}):__\n" +
                    $"Scheduling help, quick questions to help getting started.\nYour welcome questionnaire answers will be here.\n\n" +
                    $"__[#polyphasic_questions]({Bot.getChannelLink(Bot.getChannel("polyphasic-questions", serverID).Id)}):__\n" +
                    $"Our forum; ask more specific queries and general advice.\n\n" +
                    $"__[#botspam]({Bot.getChannelLink(Bot.getChannel("botspam", serverID).Id)}):__\n" +
                    $"Use `!help` commands, set your schedule, and use the `-FindSchedules` command."
                }.Build();

                if (selectedYes)
                {
                    mc.ModifyOriginalResponseAsync(m =>
                    {
                        m.Embed = updateEmbed;
                        m.Components = null;

                    }).Wait();
                    i.DeferAsync().Wait();

                    embed.AddField(new EmbedFieldBuilder()
                    {
                        Name = Config._blank,
                        Value = $"__**{user.DisplayName}'s sleep availability:**__"
					});
				}
                else mc.UpdateAsync(m =>
                {
                    m.Embed = updateEmbed;
                    m.Components = null;
                }).Wait();

				Bot.getIMessageChannel(Bot.getChannel("beginners", serverID).Id).SendMessageAsync($"<@{i.User.Id}>", embed: embed.Build()).Wait();
			}
            catch { _welcomeEmbeds.Add((embed, userID, age, mc)); }
		}
		#endregion
		#endregion

		#region meme management
		/// <summary> resets meme list and local variables for current or specified server | =resetMemes or =resetMemes serverID </summary>
		public void resetmemes(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "ResetMemes";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);
			bool inDMs = Bot.isDMs(message.Channel);

			if (!confirmPerms(command.perms, message, commandName)) return;



			Server server = null;
			ulong serverID = 0;

			try
			{
				if (msg.Trim().Length > 11) //For specific Server
				{
					if (ulong.TryParse(msg.Trim(), out serverID))
						server = Bot.addServer(serverID, Bot.getServerName(message.Channel), inDMs); //create server if it's not in list                    
				}
				else //For current Server
				{
					server = Bot.getServer(message.Channel);
					serverID = server._id;
				}

                if (server != null && serverID > 0)
                {
                    server.createMemeList(message);
                    if ((Bot._client.GetGuild(serverID).GetUser(Bot._client.CurrentUser.Id) as IGuildUser).Nickname == "MemeHelper") Bot.changeNickname(Bot._client.GetGuild(serverID).GetUser(Bot._client.CurrentUser.Id), Bot._client.CurrentUser.Username).Wait();
                    if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Reset memes for **{Bot._client.GetGuild(serverID).Name}**");
                }
                else Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: $"Couldn't find server with ID: [`{msg}`].");
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> sets memeExhaustion for current or specified server | =setME # or =setME #\serverID </summary>
		public void setme(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "SetMe";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);
			bool inDMs = Bot.isDMs(message.Channel);

			if (!confirmPerms(command.perms, message, commandName)) return;



			Server server = null;
			ulong serverID = 0;
			int memeExhaustion = -1;

            try
            {
                if (msg.Length > 0)
                {
                    if (msg.Contains('\\')) //For specific Server
                    {
                        string[] entries = msg.Split('\\');
                        if (int.TryParse(entries[0], out int me))
                            memeExhaustion = me;
                        if (ulong.TryParse(entries[1].Trim(), out serverID))
                            server = Bot.addServer(serverID, Bot.getServerName(message.Channel), inDMs); //create server if it's not in list                    
                    }
                    else //For current Server
                    {
                        if (int.TryParse(msg.Trim(), out int me)) memeExhaustion = me;
                        server = Bot.getServer(message.Channel);
                        serverID = server._id;
                    }

                    if (server != null && serverID > 0)
                    {
                        server.memeExhaustion = memeExhaustion;
                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Set memeExhaustion to `{server.memeExhaustion}` for **{Bot._client.GetGuild(serverID).Name}**");
                    }
                    else Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: $"Couldn't find server with ID: [`{msg}`].");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> clears local variables for current server or specified server | =clear or =clear serverID </summary>
		public void clear(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "Clear";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);
			bool inDMs = Bot.isDMs(message.Channel);

			if (!confirmPerms(command.perms, message, commandName)) return;




			Server server = null;
			ulong serverID = 0;

			try
			{
				if (msg.ToString().Trim().Length > 6) //For specific Server
				{
					if (ulong.TryParse(msg.Trim(), out serverID))
						server = Bot.addServer(serverID, Bot.getServerName(message.Channel), inDMs); //create server if it's not in list\                    
				}
				else //For current Server
				{
					server = Bot.getServer(message.Channel);
					serverID = server._id;
				}

                if (server != null && serverID > 0)
                {
                    server.clear();
                    if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Cleared variables for **{(inDMs ? "DMs" : Bot._client.GetGuild(server._id).Name)}**");
                }
                else Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: $"Couldn't find server with ID: [`{msg}`].");
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }
        #endregion

		#region logging
		/// <summary> toggle logging for specified server | =logging bool or =logging bool\serverID </summary>
		public void logging(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "Logging";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);
			bool inDMs = Bot.isDMs(message.Channel);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				if (msg.Length > 0)
				{
					if (msg.Contains('\\')) //For specific Server
					{
						string[] entries = msg.Split('\\');
						msg = entries[0];
						if (ulong.TryParse(entries[1], out ulong serverID))
						{
							Server srv = Bot.addServer(serverID, Bot.getServerName(message.Channel), inDMs); //create server if it's not in list

                            if (bool.TryParse(msg.Trim(), out bool log)) srv._logging = log;

                            if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Set logging to `{srv._logging}` for **{Bot._client.GetGuild(serverID).Name}**");
                        }
                    }
                    else //For current Server
                    {
                        Server server = Bot.getServer(message.Channel);
                        if (bool.TryParse(msg.Trim(), out bool log)) server._logging = log;

                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Set logging to `{server._logging}` for **{(inDMs ? "DMs" : Bot._client.GetGuild(server._id).Name)}**");
                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> prints messages sent in specified channel to current channel | =tail to see tails, =tail channelID, or for simple tailing =tail channelID\s </summary>
		public void tail(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "Tail";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



            try
            {
                if (msg.Trim().Length == 0) //Print tails for current channel
                {
					List<List<(ulong channelsTailed, ulong channelViewingFrom)>> tailingLists = new()
					{
						Bot._tailing,
						Bot._tailingS
					};
					bool simple = false;

                    foreach (List<(ulong channelsTailed, ulong channelViewingFrom)> tailingList in tailingLists)
                    {
                        if (tailingList.FindAll(p => p.channelViewingFrom == message.Channel.Id).Count > 0)
                        {
                            List<(ulong channelsTailed, ulong channelViewingFrom)> serverChannel = new();
                            List<ulong> servers = new();

                            foreach ((ulong channelsTailed, ulong channelViewingFrom) in tailingList.FindAll(p => p.channelViewingFrom == message.Channel.Id))
                            {
                                serverChannel.Add((channelsTailed, Bot.getGuild(channelsTailed).Id));
                                if (!servers.Exists(u => u == serverChannel[^1].channelViewingFrom))
                                    servers.Add(serverChannel[^1].channelViewingFrom);
                            }

                            List<EmbedFieldBuilder> fields = new();

                            foreach (ulong server in servers)
                            {
								EmbedFieldBuilder field = new()
								{
									Name = Bot._client.GetGuild(server).Name + ":"
								};

								foreach ((ulong channelsTailed, ulong channelViewingFrom) in serverChannel)
                                {
                                    if (channelViewingFrom == server) field.Value += $"`{channelsTailed}`: {Bot._client.GetChannelAsync(channelsTailed).Result}\n";
                                }

                                fields.Add(field);
                            }

							Bot.sendEmbed(message.Channel, Color.Green, title: $"Tailing{(simple ? " (Simple)" : "")}", fields: fields);
						}
						else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"No channels are being tailed{(simple ? " (simple)" : "")} from this channel.");

						simple = true; //second iteration looks at simple
					}
				}
				else //Begin tailing
				{
					bool simple = false;
					if (msg.Contains("\\s"))
					{
						simple = true;
						msg = msg.Replace("\\s", "");
					}

					if (ulong.TryParse(msg.Trim(), out ulong channelID))
					{
						if (simple) Bot._tailingS.Add((channelID, message.Channel.Id));
						else Bot._tailing.Add((channelID, message.Channel.Id));

                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Now tailing{(simple ? " (simple)" : "")} **{Bot._client.GetChannel(channelID)}** on **{(Bot._client.GetChannel(channelID) as SocketGuildChannel).Guild.Name}**");
                    }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
                }
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> stops tailing specified channel or all channels | =stopTail to stop tailing all channels or =stopTail channelID </summary>
		public void stoptail(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "StopTail";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				if (msg.Length > 0) //specific channel to current channel
				{
					if (ulong.TryParse(msg.Trim(), out ulong channelID))
					{
						int count = Bot._tailing.Count;
						int countS = Bot._tailingS.Count;
						Bot._tailing.RemoveAll(p => p.channelsTailed == channelID && p.channelViewingFrom == message.Channel.Id);
						Bot._tailingS.RemoveAll(p => p.channelsTailed == channelID && p.channelViewingFrom == message.Channel.Id);
						if (count > Bot._tailing.Count || countS > Bot._tailingS.Count)
						{
							if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Stopped tailing **{Bot._client.GetChannel(channelID)}** on **{(Bot._client.GetChannel(channelID) as SocketGuildChannel).Guild.Name}**");
						}
						else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **StopTail Error** {_alert}", description: Bot._tailing.Count + Bot._tailingS.Count > 0 ? "Unable to stop tailing." : "No channels to stop tailing.");

                    }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
                }
                else //all to current channel
                {
                    int count = Bot._tailing.Count;
                    int countS = Bot._tailingS.Count;
                    Bot._tailing.RemoveAll(p => p.channelViewingFrom == message.Channel.Id);
                    Bot._tailingS.RemoveAll(p => p.channelViewingFrom == message.Channel.Id);
                    if (count > Bot._tailing.Count || countS > Bot._tailingS.Count)
                    {
                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, title: $"Stopped tailing {(count - Bot._tailing.Count) + (countS - Bot._tailingS.Count)} channel{((count - Bot._tailing.Count) + (countS - Bot._tailingS.Count) == 1 ? "" : "s")}.");
                    }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **StopTail Error** {_alert}", description: Bot._tailing.Count + Bot._tailingS.Count > 0 ? "Unable to stop tailing." : "No channels to stop tailing.");
                }
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }
		#endregion

		#region message management
		#region edit
		/// <summary> edit a message | /edit </summary>
		public void editMessage(SocketMessageCommand mi)
		{
			bool consoleWrite = false;
            IMessage m = mi.Data.Message;
            IGuildChannel channel = Bot.getGuildChannel(mi.Channel.Id);
			string commandName = "Edit";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";

			try
			{
				if (!confirmPerms(command.perms, mi, commandName, (m.Channel as IGuildChannel).Guild.Id)) return;



                if (m != null)
                {
                    if (m.Author.Id != Bot._client.CurrentUser.Id)
                    {
                        mi.RespondAsync("You cannot edit this message.", ephemeral: true).Wait();
                        return;
                    }
                    else
                    {
                        ModalBuilder modal = new()
                        {
                            Title = "Editing Message",
                            CustomId = $"Edit[{channel.Id}|{m.Id}]"
                        };
                        modal.Components.WithTextInput(new TextInputBuilder()
                        {
                            CustomId = "messagetext",
                            Label = "Edit this to change the message content.",
                            Placeholder = "Removing all text will result in an empty message.",
                            Value = m.Content,
                            Required = true,
                            MaxLength = 2000,
                            Style = TextInputStyle.Paragraph
                        });

                        mi.RespondWithModalAsync(modal.Build()).Wait();
                    }
                }
                else mi.RespondAsync(embed: new EmbedBuilder()
                    {
                        Color = Color.LightOrange,
                        Title = $"{_alert} **{commandName} Error** {_alert}",
                        Description = description,
                        Fields = new List<EmbedFieldBuilder>
                        {
                            new EmbedFieldBuilder()
                            {
                                Name = "Incorrect parameters:",
                                Value = errorMsg
                            }
                        }
                    }.Build(), ephemeral: true);
			}
			catch
			{
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				if (!confirmPerms(command.perms, mi, commandName)) return;

				mi.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.LightOrange,
					Title = $"{_alert} **{commandName} Error** {_alert}",
					Description = description + errorMsg
				}.Build(), ephemeral: true);
			}
		}

		public static void editMessage_Editing(IModalInteraction mi)
        {
            string commandName = "Edit";
			string newContent = mi.Data.Components.First().Value;
            if (string.IsNullOrWhiteSpace(newContent)) newContent = Config._blank;
            string[] ids = mi.Data.CustomId[(mi.Data.CustomId.IndexOf("[") + 1)..^1].Split('|');

            IMessage m = (Bot._client.GetChannelAsync(ulong.Parse(ids[0])).Result as IMessageChannel).GetMessageAsync(ulong.Parse(ids[1])).Result;

            if (m != null)
			{
				string content = m.Content;

                Bot.editMessage(m, newContent);

                string contentAfter = m.Channel.GetMessageAsync(m.Id).Result.Content;

                if (content != contentAfter && newContent == contentAfter)
                    mi.RespondAsync(embed: new EmbedBuilder() { Color = Color.Green, Title = $"Edit successful." }.Build(), ephemeral: true);
				else
					mi.RespondAsync(embed: new EmbedBuilder()
					{
						Color = Color.Red,
						Title = $"{_alert} **{commandName} Error** {_alert}",
						Description = "Edit unsuccessful."
					}.Build(), ephemeral: true);
			}
		}

		/// <summary> edit a message | =edit messageLink\new text </summary>
		public void edit(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "Edit";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				string[] entries = msg.Split('\\');
				IUserMessage m = (IUserMessage)Bot.getMessageFromLink(entries[0]);

				if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(m.Channel.Id).Id)) return;



				if (m != null)
				{
					string newContent = entries[1].Replace("^bs ", "\\").Replace("^v ", "\n");
					try
					{
						Bot.editMessage(m, newContent, null);
						if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Edited [message]({Bot.getMessageLink(m)}).");
					}
					catch
					{
						Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **Error** {_alert}", description: $"Failed to edit [message]({Bot.getMessageLink(m)}).");
					}
				}
				else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
			}
			catch
			{
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				if (!confirmPerms(command.perms, message, commandName)) return;
				Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
			}
		}


		/// <summary> edit an embed | /editEmbed </summary>
		public void editEmbed(SocketMessageCommand mi)
        {
            bool consoleWrite = false;
            IMessage m = mi.Data.Message;
            IGuildChannel channel = Bot.getGuildChannel(mi.Channel.Id);
            string commandName = "EditEmbed";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";

            try
            {
                if (!confirmPerms(command.perms, mi, commandName, (m.Channel as IGuildChannel).Guild.Id)) return;



                if (m != null)
                {
                    if (m.Author.Id != Bot._client.CurrentUser.Id)
                    {
                        mi.RespondAsync("You cannot edit this message.", ephemeral: true).Wait();
                        return;
                    }
                    else if (m.Embeds.Count == 0)
                    {
                        mi.RespondAsync("There are no embeds in this message to edit.", ephemeral: true).Wait();
                        return;
                    }
                    else
                    {
                        IEmbed embed = m.Embeds.First();

                        ModalBuilder modal = new()
                        {
                            Title = "Editing Embed",
                            CustomId = $"EditEmbed[{channel.Id}|{m.Id}]"
                        };

                        //color | description
                        modal.Components.WithTextInput(new TextInputBuilder()
                        {
                            CustomId = "color_description",
                            Label = "Color | Description",
                            Value = embed.Color.Value.ToString() + "¬" +
                            (!string.IsNullOrEmpty(embed.Description) ? embed.Description : ""),
                            Required = false,
                            Style = TextInputStyle.Paragraph
                        });

                        //author name | URL | Icon
                        modal.Components.WithTextInput(new TextInputBuilder()
                        {
                            CustomId = "authorname_url_icon",
                            Label = "Author Name | URL | Icon",
                            Value = embed.Author.HasValue ? 
                                (!string.IsNullOrWhiteSpace(embed.Author.Value.Name) ? embed.Author.Value.Name : "") + "¬" +
								(!string.IsNullOrWhiteSpace(embed.Author.Value.Url) ? embed.Author.Value.Url : "") + "¬" +
								(!string.IsNullOrWhiteSpace(embed.Author.Value.IconUrl) ? embed.Author.Value.IconUrl : "") : "¬¬",
                            MaxLength = 256,
                            Required = false,
                            Style = TextInputStyle.Paragraph
                        });

                        //title | url
                        modal.Components.WithTextInput(new TextInputBuilder()
                        {
                            CustomId = "title_url_thumbnail_image",
                            Label = "Title | URL | Thumbnail | Image",
                            Value = (!string.IsNullOrEmpty(embed.Title) ? embed.Title : "") + "¬" + 
                                    (!string.IsNullOrEmpty(embed.Url) ? embed.Url : "") + "¬" + 
                                    (embed.Thumbnail.HasValue && !string.IsNullOrWhiteSpace(embed.Thumbnail.Value.Url) ? embed.Thumbnail.Value.Url : "") + "¬" +
									(embed.Image.HasValue && !string.IsNullOrWhiteSpace(embed.Image.Value.Url) ? embed.Image.Value.Url : ""),
                            MaxLength = 256,
                            Required = false,
                            Style = TextInputStyle.Paragraph
                        });

						//fields
						#region fields
						string fields = "";
                        foreach (EmbedField e in embed.Fields) { fields += $"{e.Name}¬{e.Value}\n"; }

                        modal.Components.WithTextInput(new TextInputBuilder()
                        {
                            CustomId = "fields",
                            Label = "Fields",
                            Placeholder = "Enter [Null] to remove fields.",
                            Value = fields,
                            Required = false,
                            Style = TextInputStyle.Paragraph
                        });
						#endregion

                        //footer text | URL
                        modal.Components.WithTextInput(new TextInputBuilder()
                        {
                            CustomId = "footertext_url",
                            Label = "Footer Text | URL",
                            Value = embed.Footer.HasValue ? 
                                (!string.IsNullOrEmpty(embed.Footer.Value.Text) ? embed.Footer.Value.Text : "") + "¬" +
								(!string.IsNullOrEmpty(embed.Footer.Value.IconUrl) ? embed.Footer.Value.IconUrl : "") : "¬",
                            MaxLength = 2048,
                            Required = false,
                            Style = TextInputStyle.Paragraph
                        });

                        mi.RespondWithModalAsync(modal.Build()).Wait();
                    }
                }
                else mi.RespondAsync(embed: new EmbedBuilder()
                {
                    Color = Color.LightOrange,
                    Title = $"{_alert} **{commandName} Error** {_alert}",
                    Description = description,
                    Fields = new List<EmbedFieldBuilder>
                        {
                            new EmbedFieldBuilder()
                            {
                                Name = "Incorrect parameters:",
                                Value = errorMsg
                            }
                        }
                }.Build(), ephemeral: true);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                if (!confirmPerms(command.perms, mi, commandName)) return;

                mi.RespondAsync(embed: new EmbedBuilder()
                {
                    Color = Color.LightOrange,
                    Title = $"{_alert} **{commandName} Error** {_alert}",
                    Description = description + errorMsg
                }.Build(), ephemeral: true);
            }
        }

        public static void editEmbed_Editing(IModalInteraction mi)
        {
            string commandName = "EditEmbed";

            string[] IDs = mi.Data.CustomId[(mi.Data.CustomId.IndexOf('[') + 1)..^1].Split('|');
            Color color = Color.Default;
            List<EmbedFieldBuilder> fields = new();
            Dictionary<string, string> values = mi.Data.Components.ToList().Select(c => (c.CustomId, c.Value)).ToDictionary(c => c.CustomId, c => c.Value);

            if (values.TryGetValue("fields", out string value))
			{
                if (value == "") values.Remove("fields");
                else
				{
					string[] fieldList = value.Split('\n');

					foreach (string field in fieldList)
					{
						if (field.Contains('|'))
						{
							string[] nameValue = field.Split('|');

							fields.Add(new EmbedFieldBuilder()
							{
								Name = !string.IsNullOrWhiteSpace(nameValue[0]) ? nameValue[0] : Config._blank,
								Value = !string.IsNullOrWhiteSpace(nameValue[1]) ? nameValue[1] : Config._blank
							});
						}
					}
				}
			}

            if ((Bot._client.GetChannelAsync(ulong.Parse(IDs[0])).Result as IMessageChannel).GetMessageAsync(ulong.Parse(IDs[1])).Result is IUserMessage m)
			{
                IEmbed e = m.Embeds.First();
				Dictionary<string, string> originalValues = checkValues(e);

                m.ModifyAsync(x =>
                {
                    EmbedBuilder embedBuilder = new();

                    string[] color_description = (values.TryGetValue("color_description", out string cd) ? cd : e.Color + "¬" + e.Description).Split("¬");
                    embedBuilder.Color = !string.IsNullOrWhiteSpace(color_description[0]) ? Bot.getColorByHash(color_description[0]) : e.Color;
                    embedBuilder.Description = !string.IsNullOrWhiteSpace(color_description[1]) ? color_description[1] : e.Description;

                    string[] authorname_url_icon = (values.TryGetValue("authorname_url_icon", out string anui) ? anui : e.Author.HasValue ? (
                        (!string.IsNullOrEmpty(e.Author.Value.Name) ? e.Author.Value.Name : "") + "¬" + (!string.IsNullOrEmpty(e.Author.Value.Url) ? e.Author.Value.Url : "" ) + "¬" +
					    (!string.IsNullOrEmpty(e.Author.Value.IconUrl) ? e.Author.Value.IconUrl : "" )) : "¬").Split("¬");
                    if (authorname_url_icon.Length > 0 || e.Author.HasValue)
					{
						EmbedAuthorBuilder author = new();

						if (!string.IsNullOrEmpty(authorname_url_icon[0])) author.Name = !string.IsNullOrWhiteSpace(authorname_url_icon[0]) ? authorname_url_icon[0] : Config._blank;
                        else if (e.Author.HasValue && !string.IsNullOrEmpty(e.Author.Value.Name)) author.Name = e.Author.Value.Name;

                        if (!string.IsNullOrEmpty(authorname_url_icon[1])) author.Url = !string.IsNullOrWhiteSpace(authorname_url_icon[1]) ? authorname_url_icon[1] : Config._blank;
                        else if (e.Author.HasValue && !string.IsNullOrEmpty(e.Author.Value.Url)) author.Url = e.Author.Value.Url;

                        if (!string.IsNullOrEmpty(authorname_url_icon[2])) author.IconUrl = !string.IsNullOrWhiteSpace(authorname_url_icon[2]) ? authorname_url_icon[2] : Config._blank;
                        else if (e.Author.HasValue && !string.IsNullOrEmpty(e.Author.Value.IconUrl)) author.IconUrl = e.Author.Value.IconUrl;

						embedBuilder.Author = author;
					} 
					
					string[] title_URL_thumbnail_image = (values.TryGetValue("title_url_thumbnail_image", out string tuti) ? tuti : 
                        e.Title + "¬" + e.Url + "¬" + (e.Thumbnail.HasValue && !string.IsNullOrEmpty(e.Thumbnail.Value.Url) ? e.Thumbnail.Value.Url : "") + "¬" + 
                        (e.Image.HasValue && !string.IsNullOrEmpty(e.Image.Value.Url) ? e.Image.Value.Url : "")).Split("¬");
                    if (!string.IsNullOrWhiteSpace(title_URL_thumbnail_image[0])) embedBuilder.Title = title_URL_thumbnail_image[0];
                    if (!string.IsNullOrWhiteSpace(title_URL_thumbnail_image[1])) embedBuilder.Url = title_URL_thumbnail_image[1];
                    if (!string.IsNullOrWhiteSpace(title_URL_thumbnail_image[2])) embedBuilder.ThumbnailUrl = title_URL_thumbnail_image[2];
                    if (!string.IsNullOrWhiteSpace(title_URL_thumbnail_image[3])) embedBuilder.ImageUrl = title_URL_thumbnail_image[3];

					string[] footertext_url = (values.TryGetValue("footertext_url", out string ftu) ? ftu : e.Footer.HasValue ? (
						(!string.IsNullOrEmpty(e.Footer.Value.Text) ? e.Footer.Value.Text : "") + "¬" + (!string.IsNullOrEmpty(e.Footer.Value.IconUrl) ? e.Footer.Value.IconUrl : "")) : "¬¬").Split("¬");
					if (footertext_url.Length > 0 || e.Footer.HasValue)
					{
						EmbedFooterBuilder footer = new();

						if (!string.IsNullOrEmpty(footertext_url[0])) footer.Text = !string.IsNullOrWhiteSpace(footertext_url[0]) ? footertext_url[0] : Config._blank;
						else if (e.Footer.HasValue && !string.IsNullOrEmpty(e.Footer.Value.Text)) footer.Text = e.Footer.Value.Text;

						if (!string.IsNullOrEmpty(footertext_url[1])) footer.IconUrl = !string.IsNullOrWhiteSpace(footertext_url[1]) ? footertext_url[1] : Config._blank;
						else if (e.Footer.HasValue && !string.IsNullOrEmpty(e.Footer.Value.IconUrl)) footer.IconUrl = e.Footer.Value.IconUrl;

						embedBuilder.Footer = footer;
					}

                    if (values.TryGetValue("fields", out string fields) || (e.Fields != null && e.Fields.Length > 0))
					{
                        if (fields is not "Null" and not "[Null]")
                        {
							List<EmbedFieldBuilder> fieldsBuilder = new();

                            try
                            {
								if (fields.Length > 0)
								{
									string[] fieldValues = fields.Split('\n');

									foreach (string field in fieldValues)
									{
										string[] nameValue = field.Split("¬");
										fieldsBuilder.Add(new EmbedFieldBuilder() { Name = nameValue[0], Value = nameValue[1] });
									}
								}
								else { foreach (EmbedField field in e.Fields) fieldsBuilder.Add(new EmbedFieldBuilder() { Name = field.Name, Value = field.Value }); }
							}
                            catch
                            {
								mi.RespondAsync(embed: new EmbedBuilder()
								{
									Color = Color.Red,
									Title = $"{_alert} **{commandName} Error** {_alert}",
									Description = "Edit unsuccessful - fields were in an invalid format."
								}.Build(), ephemeral: true);
							}
							

							embedBuilder.Fields = fieldsBuilder;
						}                        
					}

                    try
                    {
						x.Embed = embedBuilder.Build();
					}
                    catch 
                    {
						mi.RespondAsync(embed: new EmbedBuilder()
						{
							Color = Color.Red,
							Title = $"{_alert} **{commandName} Error** {_alert}",
							Description = "Edit unsuccessful - Invalid Parameters."
						}.Build(), ephemeral: true);
					}                    
				}).Wait();

				Dictionary<string, string> newValues = checkValues((Bot._client.GetChannelAsync(m.Channel.Id).Result as IMessageChannel).GetMessageAsync(m.Id).Result.Embeds.First());

				if (!MM.compareDictionaries(newValues, originalValues, "¬") && MM.compareDictionaries(values, newValues, "¬"))
                    mi.RespondAsync(embed: new EmbedBuilder() { Color = Color.Green, Title = $"Edit successful." }.Build(), ephemeral: true);
                else
                    mi.RespondAsync(embed: new EmbedBuilder()
                    {
                        Color = Color.Red,
                        Title = $"{_alert} **{commandName} Error** {_alert}",
                        Description = "Edit unsuccessful."
                    }.Build(), ephemeral: true);
            }
        }

        internal static Dictionary<string, string> checkValues(IEmbed embed)
		{
			Dictionary<string, string> values = new();
            string fields = "";

			//color_description
			if (!string.IsNullOrEmpty(embed.Description)) values.Add("color_description", embed.Color + "¬" + embed.Description);

			//authorname_url_icon
			if (embed.Author.HasValue)
			{
				EmbedAuthor author = embed.Author.Value;
                values.Add("authorname_url_icon",
                    (!string.IsNullOrEmpty(author.Name) ? author.Name : "") + "¬" +
					(!string.IsNullOrEmpty(author.Url) ? author.Url : "") + "¬" +
					(!string.IsNullOrEmpty(author.IconUrl) ? author.IconUrl : ""));
            }

			//title_url_thumbnail_image
			#region title, url, thumbnail, image
			values.Add("title_url_thumbnail_image", 
                (!string.IsNullOrEmpty(embed.Title) ? embed.Title : "") + "¬" +
				(!string.IsNullOrEmpty(embed.Url) ? embed.Url : "") + "¬" +
				(embed.Thumbnail.HasValue && !string.IsNullOrEmpty(embed.Thumbnail.Value.Url) ? embed.Thumbnail.Value.Url : "") + "¬" +
				(embed.Image.HasValue && !string.IsNullOrEmpty(embed.Image.Value.Url) ? embed.Image.Value.Url : ""));
			#endregion

            //fields
			if (embed.Fields != null && embed.Fields.Length > 0)
			{
				foreach (EmbedField e in embed.Fields) fields += $"{e.Name}¬{e.Value}\n";
				values.Add("fields", fields);
			}

			//footertext_url
			if (embed.Footer.HasValue)
			{
				EmbedFooter ftr = embed.Footer.Value;
                values.Add("footertext_url",
                    (!string.IsNullOrEmpty(ftr.Text) ? ftr.Text : "") + "¬" +
                    (!string.IsNullOrEmpty(ftr.IconUrl) ? ftr.IconUrl : ""));
			}

            return values;
		}
		#endregion

		#region send
		/// <summary> send a message | =send [optional: replyTo link]\[channelID/userID for DMs]\message or for current channel =send [optional: replyTo link]\message </summary>
		public void send(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "Send";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

            //Alter from NapGod '|Say' format
            if (message.Content.Trim().ToLower().StartsWith(Config._napGodCommandPrefixes["NapGod Admin Command Prefix"]))
                msg = message.ToString().Trim().Remove(0, $"{Config._napGodCommandPrefixes["NapGod Admin Command Prefix"]}say".Length);

			try
			{
				string[] entries = msg.Split('\\');
				string link = "";
				ulong channelID = 0;

				IMessage replyTo = null;

				if (entries.Length > 0)
				{
					msg = entries.Last().Replace("^bs ", "\\").Replace("^v ", "\n");
					string tempLink = entries[0].Replace("<", "").Replace(">", "").Trim();
					if (Uri.IsWellFormedUriString(tempLink, UriKind.Absolute))
					{
						link = tempLink;
						if (!string.IsNullOrWhiteSpace(link)) replyTo = Bot.getMessageFromLink(link);

                        if (entries.Length > 2)
                        {
                            if (ulong.TryParse(entries[1], out ulong cID))
                                channelID = cID;
                        }
                        else channelID = message.Channel.Id;
                    }
                    else if (ulong.TryParse(entries[0], out ulong cID)) channelID = cID;
                }
                
                if (channelID == 0) channelID = message.Channel.Id;

                if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(channelID).Id)) return;



                if (((entries.Length == 3 && replyTo != null) || entries.Length == 2) && channelID > 0)
                {
                    bool isUser = Bot._client.GetUserAsync(channelID).Result != null;
                    IMessage sent = !isUser
						? (Bot._client.GetChannel(channelID) as IMessageChannel).SendMessageAsync(msg,
						messageReference: replyTo != null ? new MessageReference(replyTo.Id, replyTo.Channel.Id, Bot.getGuild(replyTo.Channel.Id).Id) : null).Result
						: (IMessage)Bot._client.GetUser(channelID).SendMessageAsync(msg).Result;
					if (sent != null)
                    {
                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Sent [message]({Bot.getMessageLink(sent)}).");
                    }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **Send Error** {_alert}", description: $"Unable to send message in (this channel)[{Bot.getChannelLink(channelID)}]");
                }
                else if ((entries.Length == 1 || (entries.Length == 2 && replyTo != null)) && msg.Length > 0)
                {
                    Bot.send(message, entries.Last().Replace("^bs ", "\\").Replace("^v ", "\n"), reference: replyTo);
                    if (!delete && !Bot.isDMs(message.Channel)) message.DeleteAsync().Wait();
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                if (!confirmPerms(command.perms, message, commandName)) return;
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

        public static void send(SocketSlashCommand si)
		{
			bool consoleWrite = false;
			string commandName = "Send";
			(string commandName, string category, string description, string properFormat, Perms perms) command = Bot._ac._adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";

			try
			{
                string msg = "", tempLink = "", link = "";
				AllowedMentions ping = AllowedMentions.None;
				ulong channelID = 0;
				IMessage replyTo = null;

				foreach (var data in si.Data.Options)
                {
                    switch (data.Name)
                    {
                        case "message": msg = data.Value.ToString(); break;

                        case "replyto": tempLink = data.Value.ToString(); break;

                        case "channelid": if (ulong.TryParse(data.Value.ToString(), out ulong tempChannelID)) channelID = tempChannelID; break;
					}
                }

				msg = msg.Replace("^v ", "\n");

				if (!string.IsNullOrEmpty(tempLink) && tempLink.Trim().EndsWith('@'))
				{
					ping = AllowedMentions.All;
                    ping.MentionRepliedUser = true;
					tempLink = tempLink.Trim()[0..^1];
				}

				if (Uri.IsWellFormedUriString(tempLink, UriKind.Absolute))
				{
					link = tempLink;

					if (!string.IsNullOrWhiteSpace(link)) replyTo = Bot.getMessageFromLink(link);
				}

				if (channelID == 0) channelID = si.Channel.Id;

				if (!confirmPerms(command.perms, si, commandName, Bot.getGuild(channelID).Id)) return;


                if (replyTo != null && channelID != replyTo.Channel.Id)
                {
                    si.RespondAsync(embed: new EmbedBuilder()
                    {
                        Color = Color.Red,
                        Title = $"{_alert} **{commandName} Error** {_alert}",
                        Description = $"The channel of the [message to reply to]({Bot.getChannelLink(replyTo.Channel.Id)}) and the [channel to send the message in]({Bot.getChannelLink(channelID)}) must be the same."
                    }.Build(), ephemeral: true);

                    return;
                }

				bool isUser = Bot._client.GetUserAsync(channelID).Result != null;
				IMessage sent = !isUser
					? (Bot._client.GetChannel(channelID) as IMessageChannel).SendMessageAsync(msg,
					messageReference: replyTo != null ? new MessageReference(replyTo.Id, replyTo.Channel.Id, Bot.getGuild(replyTo.Channel.Id).Id) : null, allowedMentions: ping).Result
					: (IMessage)Bot._client.GetUser(channelID).SendMessageAsync(msg, allowedMentions: ping).Result;

                if (channelID != si.Channel.Id)
                {
                    if (sent != null) si.RespondAsync(embed: new EmbedBuilder() { Color = Color.Green, Title = $"Message Sent.", Url = Bot.getMessageLink(sent) }.Build(), ephemeral: true).Wait();
                    else si.RespondAsync(embed: new EmbedBuilder()
                    {
                        Color = Color.LightOrange,
                        Title = $"{_alert} **{commandName} Error** {_alert}",
                        Description = $"Unable to send message in (this channel)[{Bot.getChannelLink(channelID)}]"
                    }.Build(), ephemeral: true).Wait();
                }
                else
                {
                    si.DeferAsync().Wait();
                    si.GetOriginalResponseAsync().Result.DeleteAsync().Wait();
                }
			}
			catch
			{
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				if (!confirmPerms(command.perms, si, commandName)) return;

				si.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.LightOrange,
					Title = $"{_alert} **{commandName} Error** {_alert}",
					Description = description + errorMsg
				}.Build(), ephemeral: true);
			}
		}
        
        /// <summary> send an embed | =sendEmbed [channelID or 'this' for current channel]\[conditions: separated by new lines]
        /// color: 
        /// message: (text)
        /// title: (text)
        /// description: (text)
        /// fields: (<(name)><[value]><&> etc.)
        /// url: (url)
        /// image: (url)
        /// thumbnail: (url)
        /// footer: (text)
        /// author: (UserID)
        /// reply: (messageLink) 
        /// </summary>
        public void sendembed(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "SendEmbed";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

            try
            {
                string[] entries = msg.Split('\\');
                if (!ulong.TryParse(entries[0].Trim(), out ulong channelID) && entries[0].Trim().ToLower() == "this")
                {
                    channelID = Bot.isDMs(message.Channel.Id) ? message.Author.Id : message.Channel.Id;
                    reply = false;
                    delete = true;
                }

                bool inDMs = Bot._client.GetUserAsync(channelID).Result != null;

                if (entries.Length > 1 && channelID > 0)
                {
                    if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(channelID).Id)) return;



                    string messageText = "", title = "", descriptionText = "", fieldsText = "", text = "", url = "", imageURL = "", thumbnailURL = "", footer = "";
                    Color color = Color.Default;
                    ulong userID = 0;
                    IGuildUser user = null;
                    IMessage replyTo = null;
                    List<string> rawConditions = entries[1].Split('\n').ToList();
                    List<string> errorList = new();
                    List<EmbedFieldBuilder> fields = new();

                    foreach (string condition in rawConditions)
                    {
                        if (condition.Length > 0)
                        {
                            string conditionName = condition[..condition.IndexOf(":")];
                            string conditionValue = condition[(condition.IndexOf(":") + 1)..].Trim();
                            if (conditionName.Length > 0 && conditionValue.Length > 0)
                            {
                                switch (conditionName)
                                {
                                    case "color":
                                        color = Bot.getColorByString(conditionValue);
                                        break;

									case "message":
										messageText = conditionValue.Replace("^v ", "\n");
										break;

									case "title":
										title = conditionValue.Replace("^v ", "\n");
										break;

									case "description":
										descriptionText = conditionValue.Replace("^v ", "\n");
										break;

									case "fields":
                                    {
                                        fieldsText = conditionValue.Replace("^v ", "\n");

                                        foreach (string field in fieldsText.Split("<&>"))
                                        {
                                            if (field.Trim().StartsWith("<(") && field.Contains(")>") && field.Contains("<[") && field.Trim().EndsWith("]>"))
                                            {
												string name = field.Trim()[2.. field.IndexOf(")>")],
												      value = field.Replace($"<({name})>", "")[2..field.Replace($"<({name})>", "").IndexOf("]>")];

                                                fields.Add(new() { Name = name, Value = value });
											}
										}

                                        break;
                                    }

									case "url":
										url = conditionValue.Replace("<", "").Replace(">", "").Replace("\n", "").Trim();
										break;

									case "image":
										imageURL = conditionValue.Replace("<", "").Replace(">", "").Replace("\n", "").Trim();
										break;

									case "thumbnail":
										thumbnailURL = conditionValue.Replace("<", "").Replace(">", "").Replace("\n", "").Trim();
										break;

									case "footer":
										footer = conditionValue.Replace("^v ", "\n");
										break;

									case "author":
										if (ulong.TryParse(conditionValue, out userID))
											user = Bot.getGuildUser(channelID, userID);
										break;

									case "reply":
										if (!inDMs) replyTo = Bot.getMessageFromLink(conditionValue);
										break;
								}
							}
						}
					}

                    SocketUser x = Bot._client.GetUser(channelID);
                    IUser y = Bot._client.GetUserAsync(channelID).Result;

					#region send
					IMessage sent = !inDMs
						? Bot.sendEmbed(Bot._client.GetChannel(channelID) as IMessageChannel,
							color,
							message: messageText,
							title: title,
							description: descriptionText,
							fields: fields,
							text: text,
							url: Uri.IsWellFormedUriString(url, UriKind.Absolute) ? url : "",
							imageURL: Uri.IsWellFormedUriString(imageURL, UriKind.Absolute) ? imageURL : "",
							thumbnailURL: Uri.IsWellFormedUriString(thumbnailURL, UriKind.Absolute) ? thumbnailURL : "",
							footer: footer,
							author: user,
							replyTo: replyTo)
						: Bot.sendEmbed(Bot._client.GetUserAsync(channelID).Result,
							color,
							message: messageText,
							title: title,
							description: descriptionText,
							fields: fields,
							url: Uri.IsWellFormedUriString(url, UriKind.Absolute) ? url : "",
							imageURL: Uri.IsWellFormedUriString(imageURL, UriKind.Absolute) ? imageURL : "",
							thumbnailURL: Uri.IsWellFormedUriString(thumbnailURL, UriKind.Absolute) ? thumbnailURL : "",
							footer: footer,
							author: user);
                    #endregion
                    if (sent != null) { if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Sent [message]({Bot.getMessageLink(sent)})."); }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **SendEmbed Error** {_alert}", description: $"Unable to send message in (this channel)[{Bot.getChannelLink(channelID)}]");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                if (!confirmPerms(command.perms, message, commandName)) return;
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> copy an embed | =cpEmbed [channelID or 'this' for current channel]\messageLink\embed index\[conditions: separated by new lines]
		/// color: 
		/// message: (text)
		/// title: (text)
		/// description: (text)
		/// fields: (<(name)><[value]>, etc.)
		/// url: (url)
		/// image: (url)
		/// thumbnail: (url)
		/// footer: (text)
		/// author: (UserID)
		/// reply: (messageLink) 
		/// </summary>
		public void cpembed(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "CPEmbed";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

            try
            {
                string[] entries = msg.Split('\\');
                if (!ulong.TryParse(entries[0].Trim(), out ulong channelID) && entries[0].Trim().ToLower() == "this")
                {
                    channelID = Bot.isDMs(message.Channel.Id) ? message.Author.Id : message.Channel.Id;
                    reply = false;
                    delete = true;
                }

                bool inDMs = Bot._client.GetUserAsync(channelID).Result != null;

                if (entries.Length >= 3 && channelID > 0)
                {
                    if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(channelID).Id)) return;



					string colorName = "", messageText = "", title = "", descriptionText = "", fieldsText = "", url = "", imageURL = "", thumbnailURL = "", footerText = "";
					ulong userID = 0;
					int embedIndex = 0;
					if (int.TryParse(entries[2].Trim(), out int indx)) embedIndex = indx;
					IEmbed embed = null;
					IGuildUser user = null;
					IMessage replyTo = null,
						embedMessage = Bot.getMessageFromLink(entries[1].Trim());
					List<string> rawConditions = null;
					List<EmbedFieldBuilder> fields = new();

					if (embedMessage != null && embedMessage.Embeds.Count >= embedIndex + 1)
					{
						embed = embedMessage.Embeds.ToList()[embedIndex];
						if (embed.Fields.ToList().Count > 1)
						{
							foreach (var field in embed.Fields.ToList())
								fields.Add(new EmbedFieldBuilder() { Name = field.Name, Value = field.Value, IsInline = field.Inline });
						}
					}

                    if (entries.Length > 3)
                    {
                        rawConditions = entries[3].Contains('\n') ? entries[3].Split('\n').ToList() : new List<string>() { entries[3] };

						foreach (string condition in rawConditions)
                        {
                            if (condition.Length > 0)
                            {
                                string conditionName = condition[..condition.IndexOf(":")];
                                string conditionValue = condition[(condition.IndexOf(":") + 1)..].Trim();
                                if (conditionName.Length > 0 && conditionValue.Length > 0)
                                {
                                    switch (conditionName.ToLower())
                                    {
                                        case "color":       colorName = conditionValue.Trim(); 
                                            break;

										case "message":     messageText = conditionValue.Replace("^v ", "\n"); 
                                            break;

										case "title":       title = conditionValue.Replace("^v ", "\n"); 
                                            break;

										case "description": descriptionText = conditionValue.Replace("^v ", "\n"); 
                                            break;

										case "url":         url = conditionValue.Replace("<", "").Replace(">", "").Replace("\n", "").Trim(); 
                                            break;

										case "image":       imageURL = conditionValue.Replace("<", "").Replace(">", "").Replace("\n", "").Trim(); 
                                            break;

										case "thumbnail":   thumbnailURL = conditionValue.Replace("<", "").Replace(">", "").Replace("\n", "").Trim(); 
                                            break;

										case "footer":      footerText = conditionValue.Replace("^v ", "\n"); 
                                            break;

										case "author":      if (ulong.TryParse(conditionValue, out userID)) user = Bot.getGuildUser(channelID, userID);
											break;

										case "reply":       if (!inDMs) replyTo = Bot.getMessageFromLink(conditionValue);
											break;

										case "fields":
										{
											fieldsText = conditionValue.Replace("^v ", "\n");

											foreach (string field in fieldsText.Split("<&>"))
											{
												if (field.Trim().StartsWith("<(") && field.Contains(")>") && field.Contains("<[") && field.Trim().EndsWith("]>"))
												{
													//string name = field.Trim().Substring(2, field.IndexOf(")>")),
													//	  value = field.Replace($"<({name})>", "").Substring(2, field.Replace($"<({name})>", "").IndexOf("]>"));
													string name = field.Trim()[2.. field.IndexOf(")>")],
													  value = field.Replace($"<({name})>", "")[2..field.Replace($"<({name})>", "").IndexOf("]>")];

													fields.Add(new() { Name = name, Value = value });
												}
											}

											break;
										}
									}
								}
							}
						}
					}

					Embed newEmbed = Bot.copyEmbed(embed, colorName, title, descriptionText, fields.Select(f => f.Build()).ToList(), url, imageURL: imageURL, thumbnailURL: thumbnailURL, footerText: footerText, user: user);

                    IMessage sent = !inDMs
						? (Bot._client.GetChannel(channelID) as IMessageChannel).SendMessageAsync(text: messageText, embed: newEmbed, allowedMentions: AllowedMentions.None,
							messageReference: replyTo == null ? null : new MessageReference(replyTo.Id, replyTo.Channel.Id, Bot.getGuild(replyTo.Channel.Id).Id)).Result
						: Bot._client.GetUserAsync(channelID).Result.SendMessageAsync(
							text: messageText, embed: newEmbed, allowedMentions: AllowedMentions.None).Result;
					if (sent != null) { if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Sent [message]({Bot.getMessageLink(sent)})."); }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **SendEmbed Error** {_alert}", description: $"Unable to send message in (this channel)[{Bot.getChannelLink(channelID)}]");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                if (!confirmPerms(command.perms, message, commandName)) return;
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }
		#endregion

		#region delete
		internal void deleteMessage(SocketMessageCommand mc)
		{
			bool consoleWrite = false;
			IMessage m = mc.Data.Message;
			IGuildChannel channel = Bot.getGuildChannel(mc.Channel.Id);
			string commandName = "Delete";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";

			try
			{
				if (!confirmPerms(command.perms, mc, commandName, (m.Channel as IGuildChannel).Guild.Id)) return;



				if (m != null)
				{
					if (m.Author.Id != Bot._client.CurrentUser.Id)
					{
						mc.RespondAsync("You cannot delete this message.", ephemeral: true).Wait();
						return;
					}
					else
					{
						m.DeleteAsync().Wait();
						mc.RespondAsync(embed: new EmbedBuilder() { Color = Color.Green, Title = $"Message Deleted." }.Build(), ephemeral: true);
					}
				}
			}
			catch
			{
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				if (!confirmPerms(command.perms, mc, commandName)) return;

				mc.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.LightOrange,
					Title = $"{_alert} **{commandName} Error** {_alert}",
					Description = description + errorMsg
				}.Build(), ephemeral: true);
			}
		}

		/// <summary> delete a message | =delete messageLink </summary>
		public void delete(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "Delete";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				IMessage m = Bot.getMessageFromLink(msg);

                if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(m.Channel.Id).Id)) return;



				if (m != null)
				{
					m.DeleteAsync().Wait();

                    if ((Bot._client.GetChannel(m.Channel.Id) as IMessageChannel).GetMessageAsync(m.Id).Result == null)
                    {
                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"[Message]({Bot.getChannelLink(m.Channel.Id)}) deleted.");
                    }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **Delete Error** {_alert}", description: $"Unable to delete [message]({Bot.getMessageLink(m)}).");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                if (!confirmPerms(command.perms, message, commandName)) return;
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> delete a certain number of messages | =deleteCount [optional: channelID, if blank, current channel]\#\[optional: userID] </summary>
		public void deletecount(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "DeleteCount";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');
				ulong userID = 0;
				ulong channelID = 0;
				int count = 0;

                if (entries.Length == 1 || (entries.Length == 2 && entries[0].Length < 18 && entries[1].Length == 18))
                {
                    channelID = message.Channel.Id;
                    if (int.TryParse(entries[0], out int c)) count = c;
                    if (entries.Length == 2 && ulong.TryParse(entries[1], out ulong uID)) userID = uID;
                }
                else if (entries.Length is 2 or 3)
                {
                    if (ulong.TryParse(entries[0], out ulong cID)) channelID = cID;
                    if (int.TryParse(entries[1], out int c)) count = c;
                    if (entries.Length == 3 && ulong.TryParse(entries[2], out ulong uID)) userID = uID;
                }

                if (count > 0 && channelID > 0)
                {
                    IEnumerable<IMessage> messages = (Bot._client.GetChannel(channelID) as ISocketMessageChannel).GetMessagesAsync(count).FlattenAsync().Result;
                    int deleted = 0;
                    foreach (IMessage m in messages)
                    {
                        if (userID == 0 || (userID > 0 && m.Author.Id == userID))
                        {
                            try
                            {
                                m.DeleteAsync().Wait();
                                Thread.Sleep(500);
                                if ((Bot._client.GetChannel(channelID) as IMessageChannel).GetMessageAsync(m.Id).Result == null) deleted++;
                            }
                            catch (Exception e) { MM.Print(consoleWrite, $"\n{e.ToString().Split('\n')[0]}\n"); }
                        }
                    }

                    if (deleted > 0)
                    {
                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Deleted `{deleted}` [message{(deleted == 1 ? "" : "s")}]({ Bot.getChannelLink(channelID)}).");
                    }
                    else if (deleted <= 0) Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **DeleteCount Error** {_alert}", description: $"Unable to delete any [messages]({Bot.getChannelLink(channelID)}).");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

        #region deletemessages
        internal (List<IMessage> allMatches, IMessage lastMessageChecked) _deleteMessagesResults = (new List<IMessage>(), null);
        public void deletemessages(SocketMessage message, bool reply, bool delete) => new Thread(() => deleteMessages(message, reply, delete)).Start();
        /// <summary> Search the message history and delete specific messages (max 1000 at once) | =deletemessages [channel id]\[conditions: separated by new lines]
        /// start: (message link)
        /// end: (message link)
        /// direction: (recent or oldest; defaults to oldest)
        /// user: (ID or name)
        /// max results: (#; defaults to 20)
        /// show details: (true or false; defaults to false)
        /// content: (string)
        /// case sensitive: (true or false; defaults to false) [only relevant if using a content search]
        /// show final: (true or false; defaults to false)
        /// </summary>
        public void deleteMessages(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "DeleteMessages";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');
				List<string> rawConditions = entries[1].Split('\n').ToList();

                if (entries.Length > 1)
                {
                    List<string> errorText = new();

					IMessage start = null, end = null;
					ISocketMessageChannel channel = null;
					string userName = "", content = "";
					ulong userID = 0;
					bool showDetails = false, caseSensitive = false, backwards = false,
						startNotFound = false, endNotFound = false, showFinal = false;
					int maxResults = 20;

                    foreach (string condition in rawConditions)
                    {
                        if (condition.Length > 0)
                        {
                            string conditionName = condition[..condition.IndexOf(":")];
                            string conditionValue = condition[(condition.IndexOf(":") + 1)..].Trim();
                            IMessage startTemp = null, endTemp = null;
                            switch (conditionName)
                            {
                                case "start":
                                    if ((startTemp = Bot.getMessageFromLink(conditionValue)) != null)
                                        start = startTemp;
                                    else startNotFound = true;
                                    break;

								case "end":
									if ((endTemp = Bot.getMessageFromLink(conditionValue)) != null)
										end = endTemp;
									else endNotFound = true;
									break;

								case "direction":
									if (conditionValue == "recent")
										backwards = true;
									break;

								case "show details":
									if (bool.TryParse(conditionValue, out bool sd))
										showDetails = sd;
									break;

                                case "max results":
                                    if (int.TryParse(conditionValue, out int mr))
                                        maxResults = mr;
                                    break;

								case "user":
									if (ulong.TryParse(conditionValue, out ulong userIDTemp))
										userID = userIDTemp;
									else userName = conditionValue;
									break;

								case "content":
									content = conditionValue;
									break;

								case "case sensitive":
									if (bool.TryParse(conditionValue, out bool cs))
										caseSensitive = cs;
									break;

								case "show final":
									if (bool.TryParse(conditionValue, out bool sf))
										showFinal = sf;
									break;
							}
						}
					}

                    if (ulong.TryParse(entries[0].Trim(), out ulong channelID))
                        channel = Bot._client.GetChannel(channelID) as ISocketMessageChannel;
                    else errorText.Add($"Channel not found; [{entries[0].Trim()}] is an invalid channel ID.");
                    if (userID == 0 && !string.IsNullOrEmpty(userName))
                    {
                        userID = Bot._msm.getUser((Bot._client.GetChannel(channelID) as IGuildChannel).Guild as SocketGuild, userName);
                        if (userID == 1)
                            errorText.Add($"Too many users found with the username or nickname {userName}. Please try a user ID");
                        else if (userID == 0)
                            errorText.Add($"No users found with the username or nickname {userName}. Please try a user ID");
                    } //get user

                    if (startNotFound)
                        errorText.Add($"Starting message not found.");
                    
                    if (endNotFound)
                    {
                        errorText.Add($"Ending message not found.");
                    }

                    if (channel != null && !startNotFound && !endNotFound)
                    {
                        Bot.sendEmbed(message.Channel, color: Color.Blue, title: $"Beginning search... {Bot.getEmote(Config._emojiNames["Loading"])}");
                        (List<IMessage> allMatches, IMessage lastMessageChecked) results =
							messageSearch(channel, start, end, userID, content, 100, maxResults, startNotFound, endNotFound, caseSensitive, backwards, showFinal);

						if (results.allMatches.Count > 0)
						{
							if (backwards) results.allMatches.Reverse();

							_deleteMessagesResults = results;
							sendDeleteMessagesEmbed(message.Channel, _deleteMessagesResults.allMatches.First());
						}
						else errorText.Add($"No results found. [Last Message Checked]({Bot.getMessageLink(_deleteMessagesResults.lastMessageChecked)}).");
					}

					if (errorText.Count > 0)
					{
						string errors = "";

                        foreach (string error in errorText)
                            errors += $"{(errors.Length > 0 ? "\n" : "")}{error}";
                        
                        Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: errors);
                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

        internal static void DM_componentInteractionHandler(SocketMessageComponent ci)
        {
			if (!Bot.confirmOwner(ci.User.Id)) return;

			string data = ci.Data.CustomId[(ci.Data.CustomId.IndexOf(":") + 1)..];
			List<ButtonComponent> btns = ci.Message.Components.First().Components.OfType<ButtonComponent>().ToList();

			if (ci.Data.CustomId.StartsWith("Delete:"))
			{
				string[] IDs = data.Split('/');
				if (ulong.TryParse(IDs[0], out ulong channelID) && ulong.TryParse(IDs[1], out ulong messageID))
				{
					string content = ci.Message.Content;
					(Bot._client.GetChannelAsync(channelID).Result as IMessageChannel).GetMessageAsync(messageID).Result.DeleteAsync().Wait();
					ci.Message.ModifyAsync(msg =>
					{
						msg.Components = btns.Count == 2
							? (Optional<MessageComponent>)new ComponentBuilder()
							.WithButton(btns[0].Label, btns[0].CustomId, btns[0].Style, disabled: true)
							.WithButton(btns[1].Label, btns[1].CustomId, btns[1].Style, disabled: btns[1].IsDisabled).Build()
							: (Optional<MessageComponent>)new ComponentBuilder().WithButton(btns[0].Label, btns[0].CustomId, btns[0].Style, disabled: true).Build();

						msg.Content = $"❌ `DELETED` ❌\n{content}";
					}).Wait();
				}
			}
			else
			{
				if (int.TryParse(data, out int currentIndex))
				{
					Bot._ac.sendDeleteMessagesEmbed(ci.Channel, Bot._ac._deleteMessagesResults.allMatches[currentIndex + 1]);
					ci.Message.ModifyAsync(msg => msg.Components = new ComponentBuilder()
						.WithButton(btns[0].Label, btns[0].CustomId, btns[0].Style, disabled: btns[0].IsDisabled)
						.WithButton(btns[1].Label, btns[1].CustomId, btns[1].Style, disabled: true).Build()).Wait();
				}
			}
		}
		#endregion

		#region dm
		public void dm(SocketMessage message, bool reply, bool delete) => new Thread(() => DM(message, reply, delete)).Start();
        /// <summary> DeleteMessages but with simplified parameters | =dm [content]\[max results]\[start (message link)] </summary>
        public void DM(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "DM";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



            try
            {
                string[] entries = msg.Split('\\');
                string content = entries[0].Trim();
                int maxResults = int.Parse(entries[1].Trim());
                IMessage start = Bot.getMessageFromLink(entries[2].Trim());
                
                if (content != "" && start != null)
                {
                    List<string> errorText = new();

					if (Bot._client.GetChannel(start.Channel.Id) is not ISocketMessageChannel channel) errorText.Add($"Channel not found; [{entries[0].Trim()}] is an invalid channel ID.");
					else
					{
						Bot.sendEmbed(message.Channel, color: Color.Blue, title: $"Beginning search... {Bot.getEmote(Config._emojiNames["Loading"])}");
                        (List<IMessage> allMatches, IMessage lastMessageChecked) results =
							messageSearch(channel, start, null, 0, content, 100, maxResults, false, false, false, false, true);

						if (results.allMatches.Count > 0)
						{
							_deleteMessagesResults = results;
							sendDeleteMessagesEmbed(message.Channel, _deleteMessagesResults.allMatches.First());
						}
						else errorText.Add($"No results found. [Last Message Checked]({Bot.getMessageLink(_deleteMessagesResults.lastMessageChecked)})");
					}

					if (errorText.Count > 0)
					{
						string errors = "";

						foreach (string error in errorText)
							errors += $"{(errors.Length > 0 ? "\n" : "")}{error}";

                        Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: errors);
                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }
		#endregion
		#endregion

		#region findmessages
		public void findmessages(SocketMessage message, bool reply, bool delete) => new Thread(() => findMessages(message, reply, delete)).Start();
        /// <summary> Search the message history for specific messages (max 1000 at once) | =findmessages [channel id]\[conditions: separated by new lines]
        /// start: (message link)
        /// end: (message link)
        /// direction: (recent or oldest; defaults to oldest)
        /// user: (ID or name)
        /// max results: (#; defaults to 20)
        /// show details: (true or false; defaults to false)
        /// content: (string)
        /// case sensitive: (true or false; defaults to false) [only relevant if using a content search]
        /// show final: (true or false; defaults to false)
        /// </summary>
        public void findMessages(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "FindMessages";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');
				List<string> rawConditions = entries[1].Split('\n').ToList();

                if (entries.Length > 1)
                {
                    List<string> errorText = new();

					IMessage start = null, end = null;
					ISocketMessageChannel channel = null;
					string userName = "", content = "";
					ulong userID = 0;
					bool showDetails = false, caseSensitive = false, backwards = false,
						startNotFound = false, endNotFound = false, showFinal = false;
					int maxResults = 20;

                    foreach (string condition in rawConditions)
                    {
                        if (condition.Length > 0)
                        {
                            string conditionName = condition[..condition.IndexOf(":")];
                            string conditionValue = condition[(condition.IndexOf(":") + 1)..].Trim();
                            IMessage startTemp = null, endTemp = null;
                            switch (conditionName)
                            {
                                case "start":
                                    if ((startTemp = Bot.getMessageFromLink(conditionValue)) != null)
                                        start = startTemp;
                                    else startNotFound = true;
                                    break;

								case "end":
									if ((endTemp = Bot.getMessageFromLink(conditionValue)) != null)
										end = endTemp;
									else endNotFound = true;
									break;

								case "direction":
									if (conditionValue == "recent")
										backwards = true;
									break;

								case "show details":
									if (bool.TryParse(conditionValue, out bool sd))
										showDetails = sd;
									break;

                                case "max results":
                                    if (int.TryParse(conditionValue, out int mr))
                                        maxResults = mr;
                                    break;

								case "user":
									if (ulong.TryParse(conditionValue, out ulong userIDTemp))
										userID = userIDTemp;
									else userName = conditionValue;
									break;

								case "content":
									content = conditionValue;
									break;

								case "case sensitive":
									if (bool.TryParse(conditionValue, out bool cs))
										caseSensitive = cs;
									break;

								case "show final":
									if (bool.TryParse(conditionValue, out bool sf))
										showFinal = sf;
									break;
							}
						}
					}

                    if (ulong.TryParse(entries[0].Trim(), out ulong channelID))
                        channel = Bot._client.GetChannel(channelID) as ISocketMessageChannel;
                    else errorText.Add($"Channel not found; [{entries[0].Trim()}] is an invalid channel ID.");
                    if (userID == 0 && !string.IsNullOrEmpty(userName))
                    {
                        userID = Bot._msm.getUser(Bot.getGuild(channelID) as SocketGuild, userName);
                        if (userID == 1)
                            errorText.Add($"Too many users found with the username or nickname {userName}. Please try a user ID");
                        else if (userID == 0)
                            errorText.Add($"No users found with the username or nickname {userName}. Please try a user ID");
                    } //get user

                    if (startNotFound)
                        errorText.Add($"Starting message not found.");

                    if (endNotFound)
                        errorText.Add($"Ending message not found.");
                    
                    if (channel != null && !startNotFound && !endNotFound)
                    {
                        IUserMessage beginningSearch = Bot.sendEmbed(message.Channel, color: Color.Blue, title: $"Beginning search... {Bot.getEmote(Config._emojiNames["Loading"])}");
                        (List<IMessage> messages, IMessage currentMessage) =
							messageSearch(channel, start, end, userID, content, 100, maxResults, startNotFound, endNotFound, caseSensitive, backwards, showFinal);

						Bot.editEmbed(beginningSearch, beginningSearch.Embeds.First(), messages.Count > 0 ? Color.Green : Color.Orange, title: "Search complete.", description: $"`{messages.Count}` results.");

                        if (messages.Count > 0)
                        {
                            ulong serverID = Bot.getGuild(channel.Id).Id;
                            List<List<EmbedFieldBuilder>> efList = new();
                            List<EmbedFieldBuilder> efs = new();
                            int messageCount = 0;
                            string resultTitle = $"Messages{(userID > 1 ? $" from `{(!string.IsNullOrEmpty(userName) && userName != userID.ToString() ? userName : messages[0].Author.Username)}`" : "")} " +
                                $"in `{messages[0].Channel.Name}` " + $"on `{Bot._client.GetGuild(serverID).Name}`";

							if (backwards) messages.Reverse();

                            foreach (IMessage m in messages)
                            {
                                if (maxResults > 0 && messageCount >= maxResults) break;
                                else
                                {
                                    string numSize = new('x', maxResults > 0 ? maxResults.ToString().Length : messages.Count);
                                    int embedSize = $"{resultTitle} [`{numSize}`-`{numSize}`/`{numSize}`]".Length,
                                    rowIndex = messages.IndexOf(m);
                                    foreach (EmbedFieldBuilder e in efs) embedSize += e.Name.Length + e.Value.ToString().Length;
                                    int embedCount = m.Embeds.Count;
                                    int attachmentCount = m.Attachments.Count;
                                    string newFieldName = $"``` ```";
                                    string newFieldContent = $"**#{messageCount + 1}: [[{m.Id}]({Bot.getChannelLink(m.Channel.Id)}/{m.Id})] ({m.Timestamp})\n" +
                                        $"__<@{m.Author.Id}> ({m.Author.Username}{(showDetails ? $" : `{m.Author.Id}`)\n" : ")")} " +
                                        $"in [{m.Channel.Name}]({Bot.getChannelLink(ulong.Parse(m.Channel.Id.ToString()))}){(showDetails ? $" : (`{m.Channel.Id}`)" : "")}{(embedCount > 0 || attachmentCount > 0 ? "\n" : ":__**\n")}" +
                                        $"{(embedCount > 0 ? $"[Embeds: `{embedCount}`]{(attachmentCount > 0 ? "\n" : ":__**\n")}" : "")}" +
                                        $"{(attachmentCount > 0 ? $"[Attachments: `{attachmentCount}`]:__**\n" : "")}";
                                    int numOfFields = (int)Math.Ceiling((double)m.Content.ToString().Length / 1023);
                                    int newFieldSize = newFieldName.Length + newFieldContent.Length + m.Content.ToString().Length + ("previous message continued".Length * 3);

									if (efs.Count + numOfFields >= 24 || embedSize + newFieldSize > 6000)
									{
										efList.Add(efs);
										efs = new List<EmbedFieldBuilder>();
									}

                                    string cntnt = $"{newFieldContent}\n{(string.IsNullOrEmpty(m.Content.ToString()) ? "" : $"{m.Content}\n ")}";
                                    List<string> contentSplit = new();

									while (cntnt.Length > 0)
									{
										int toRemove = cntnt.Length;
										if (cntnt.Length > 1023) toRemove = 1023;

                                        contentSplit.Add(cntnt[..toRemove]);
                                        cntnt = cntnt.Remove(0, toRemove);
                                    }

                                    if (contentSplit.Count == 0) contentSplit.Add("`[NULL MSG CONTENT]`");
                                    foreach (string c in contentSplit)
                                    {
                                        efs.Add(new EmbedFieldBuilder()
                                        {
                                            Name = contentSplit.IndexOf(c) == 0 ? newFieldName : "previous message continued",
                                            Value = c,
                                            IsInline = false
                                        });
                                    }

                                    messageCount++;
                                }
                            }

                            if (!efList.Exists(e => e == efs)) efList.Add(efs);

							bool successfulSendForAllEmbeds = true;
							int index = 0, indexEnd = 0;

							for (int i = 0; i < efList.Count; i++)
							{
								foreach (EmbedFieldBuilder e in efList[i])
								{
									if (e.Name != "previous message continued") indexEnd++;
								}

								IMessage m = Bot.sendEmbed(message.Channel, Color.Blue,
									title: $"{resultTitle} [`{index}`-`{indexEnd}`/`{messageCount}`]",
									author: userID > 1 ? Bot._client.GetGuild(serverID).GetUser(userID) : null,
									fields: efList[i],
									footer: showFinal && indexEnd == messageCount ? Bot.getMessageLink(currentMessage) : null);
								if (m == null) successfulSendForAllEmbeds = false;

								index = indexEnd + 1;
							}

							if (!successfulSendForAllEmbeds) errorText.Add("One or more embeds failed to send.");
						}
						else errorText.Add("No results found.");
					}

					if (errorText.Count > 0)
					{
						string errors = "";

                        foreach (string error in errorText)
                            errors += $"{(errors.Length > 0 ? "\n" : "")}{error}";
                        
                        Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: errors);
                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }
		#endregion

		#region non-command methods
		internal static (List<IMessage> allMatches, IMessage lastMessageChecked) messageSearch(IMessageChannel channel, IMessage start, IMessage end, 
            ulong userID, string content, int numberPerSearch, int maxResults, bool startNotFound, bool endNotFound, bool caseSensitive, bool backwards, bool showFinal)
		{
			bool consoleWrite = true;

            //keeps track of the list's count each search so that I can see how many times it may have failed to add values to the list to prevent too much time spent hogging resources
            List<int> previousCount = new();
            List<IMessage> messages = new();
            IMessage currentMessage = null;

			//Find within start-end range
			if (!startNotFound && !endNotFound && start != null && end != null)
			{
				if (start.Timestamp > end.Timestamp) (end, start) = (start, end);
				if (containedInMessage(start, userID, content, caseSensitive)) messages.Add(start);
				previousCount.Add(messages.Count);
				currentMessage = start;

				while (currentMessage.Timestamp < end.Timestamp || messages.Count <= 1000 || previousCount.FindAll(i => i == previousCount.Last()).Count > 1000)
				{
					string log = $"{previousCount.Count}: ";

					List<IMessage> retrieved = channel.GetMessagesAsync(currentMessage, Direction.After, 1).FlattenAsync().Result.ToList();
					if (retrieved.Count == 1)
					{
						log += $"Found [{Bot.getMessageLink(currentMessage)}] ";
						List<IMessage> matches = retrieved.FindAll(m => containedInMessage(m, userID, content, caseSensitive));
						log += $"\tMatched {matches.Count}\t({messages.Count} Total)";

						messages.AddRange(matches);
						currentMessage = retrieved.Last();
					}
					else log += "No results. END ---------------------------------";
					if (currentMessage.Id == end.Id)
						log += "Reached End message. END ---------------------------------";
					if (!backwards && messages.Count >= maxResults)
						log += "End of channel. END ---------------------------------";
					if (previousCount.Count > 500)
						log += "500 attempts exceeded. END ---------------------------------";

					previousCount.Add(messages.Count);

                    MM.Print(consoleWrite, log);
                    if (log.Contains("END")) break;
                }
            }
            //Find 1000 messages after start or before end (or up to most recent/first message)
            else if ((!startNotFound && start != null) || (!endNotFound && end != null) || (start == null && end == null))
            {
                bool after = !startNotFound && start != null && end == null;
                currentMessage = (start == null && end == null) ? channel.GetMessagesAsync(1).FlattenAsync().Result.ToList().First() : after ? start : end;
                if (containedInMessage(currentMessage, userID, content, caseSensitive)) messages.Add(currentMessage);
                previousCount.Add(messages.Count);

				while (messages.Count <= 1000 || previousCount.FindAll(i => i == previousCount.Last()).Count > 10)
				{
					string log = $"{previousCount.Count}: ";

					List<IMessage> retrieved = channel.GetMessagesAsync(currentMessage, after ? Direction.After : Direction.Before, numberPerSearch).FlattenAsync().Result.ToList();
					if (retrieved.Count > 0)
					{
						log += $"Found [{(retrieved.Count > 1 ? $"{retrieved.Count} messages" : Bot.getMessageLink(currentMessage))}] ";

						if (after) retrieved.Reverse();

						List<IMessage> matches = retrieved.FindAll(m => containedInMessage(m, userID, content, caseSensitive));
						log += $"\tMatched {matches.Count}\t({messages.Count} Total)";

						messages.AddRange(matches);
						currentMessage = retrieved.Last();

						//Couldn't retrieve full amount bc reached the first or last message in channel
						if (retrieved.Count < numberPerSearch)
						{
							log += "End of channel. END ---------------------------------";
							break;
						}
					}
					else log += "No results. END ---------------------------------";
					if (((!backwards && !startNotFound && start != null) || (backwards && !endNotFound && end != null)) && messages.Count >= maxResults)
						log += "End of channel. END ---------------------------------";
					if (previousCount.Count > 500)
						log += "500 attempts exceeded. END ---------------------------------";

					previousCount.Add(messages.Count);

                    MM.Print(consoleWrite, log);
                    if (log.Contains("END")) break;
                }
            }

			return (messages, showFinal ? currentMessage : null);
		}

		internal static bool containedInMessage(IMessage message, ulong userID, string content, bool caseSensitive) =>
			(userID <= 1 || message.Author.Id == userID) && (string.IsNullOrWhiteSpace(content) ||
			(caseSensitive ?
			message.Content.Contains(content) || message.Embeds.ToList().Exists(e =>
				(e.Title != null && e.Title.Contains(content)) ||
				(e.Footer != null && e.Footer.Value.Text != null && e.Footer.Value.Text.Contains(content)) ||
				(e.Author != null && e.Author.Value.Name != null && e.Author.Value.Name.Contains(content)) ||
				(e.Description != null && e.Description.Contains(content)) ||
				e.Fields.ToList().Exists(f => f.Value.Contains(content) || f.Name.Contains(content)))
			:
			message.Content.ToLower().Contains(content.ToLower()) || message.Embeds.ToList().Exists(e =>
				(e.Title != null && e.Title.ToLower().Contains(content.ToLower())) ||
				(e.Footer != null && e.Footer.Value.Text != null && e.Footer.Value.Text.ToLower().Contains(content.ToLower())) ||
				(e.Author != null && e.Author.Value.Name != null && e.Author.Value.Name.ToLower().Contains(content.ToLower())) ||
				(e.Description != null && e.Description.ToLower().Contains(content.ToLower())) ||
				e.Fields.ToList().Exists(f => f.Value.ToLower().Contains(content.ToLower()) || f.Name.ToLower().Contains(content.ToLower())))));

		internal void sendDeleteMessagesEmbed(IMessageChannel channel, IMessage messageToShow)
		{
            string header = $"> __**Message from `{messageToShow.Author.Username}` in `{messageToShow.Channel.Name}` " + 
                $"on `{Bot.getGuild(messageToShow.Channel.Id).Name}` on `{messageToShow.Timestamp}`**__" +
				$"> {Bot.getMessageLink(messageToShow)}\n\n";

            ComponentBuilder options = new();
            options.WithButton(new ButtonBuilder("Delete", $"Delete:{messageToShow.Channel.Id}/{messageToShow.Id}", ButtonStyle.Danger));

			//Don't show "Next" button if there are no more messages
			if (_deleteMessagesResults.allMatches.Last().Id != messageToShow.Id)
				options.WithButton(new ButtonBuilder("Next ->", $"NextMessage:{_deleteMessagesResults.allMatches.IndexOf(messageToShow)}", ButtonStyle.Primary));
			else if (_deleteMessagesResults.lastMessageChecked != null)
				header = header.Replace("\n\n", $"\n> __End of search:__ `{Bot.getMessageLink(_deleteMessagesResults.lastMessageChecked)}`\n\n");

			channel.SendMessageAsync(header + messageToShow.Content, components: options.Build(), embed: messageToShow.Embeds.Count > 0 ? Bot.copyEmbed(messageToShow.Embeds.First()) : null);
		}
		#endregion
		#endregion

		#region reactions
		#region react
		public void React(SocketMessageCommand mi)
		{
			string commandName = "React";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);

			try
			{
				if (!confirmPerms(command.perms, mi, commandName, Bot.getGuildChannel(mi.Channel.Id).Guild.Id)) return;



                ModalBuilder modal = new()
                {
                    Title = "Select Reaction",
                    CustomId = $"React[{mi.Channel.Id}|{mi.Data.Message.Id}]",
                    Components = new()
                };

                modal.Components
                    .WithTextInput(new()
                    {
                        CustomId = "emoji",
                        Label = "Emoji Name",
                        MinLength = 1,
                        Required = true,
                        Style = TextInputStyle.Short
                    });

                mi.RespondWithModalAsync(modal.Build()).Wait();
			}
			catch { }
		}

        internal static void react_EmojiSelected(IModalInteraction mi)
		{
			string emoteName = mi.Data.Components.First().Value.Trim();
			string[] ids = mi.Data.CustomId[(mi.Data.CustomId.IndexOf("[") + 1)..^1].Split('|');
			IMessage m = (Bot._client.GetChannelAsync(ulong.Parse(ids[0])).Result as IMessageChannel).GetMessageAsync(ulong.Parse(ids[1])).Result;
            IEmote emote = null;
            Emoji tempEmote = null;

			if (m != null)
			{
                if ((emote = Bot.getEmote(emoteName.Replace(":", ""))) != null || 
                    Emoji.TryParse(emoteName.StartsWith(":") && emoteName.EndsWith(":") ? emoteName : $":{emoteName.Replace(":", "")}:", out tempEmote))
                {
                    if (tempEmote != null) emote = tempEmote;

                    m.AddReactionAsync(emote).Wait();

                    if (m.Channel.GetMessageAsync(m.Id).Result.GetReactionUsersAsync(emote, 100).FlattenAsync().Result.ToList().Exists(u => u.Id == Bot._client.CurrentUser.Id))
                    {
                        mi.DeferAsync().Wait();
                        mi.DeleteOriginalResponseAsync().Wait();
                    }
                    else sendEmbed(Color.Red, $"{_alert} **React Error** {_alert}", $"Unable to react with {emote}.", interaction: mi);
                }
                else sendEmbed(Color.Red, $"{_alert} **React Error** {_alert}", $"Unable to find emote with name [`{emoteName}`].", interaction: mi);
			}
            else sendEmbed(Color.Red, $"{_alert} **React Error** {_alert}", "Unable to find message.", interaction: mi);
		}

		/// <summary> react to a message | =react messageLink\d\:emoji_name: or =react messageLink\c\emoji_name </summary>
		public void react(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "React";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				string[] entries = msg.Split('\\');
				IMessage m = Bot.getMessageFromLink(entries[0]);

                if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(m.Channel.Id).Id)) return;



                if (entries.Length == 3 && m != null)
                {
                    if (entries[1].Trim() == "d") //Default emojis
                    {
                        Emoji e = new(entries[2].Trim());
                        int reactionCount = m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount;

						m.AddReactionAsync(e).Wait();

						m = (Bot._client.GetChannel(m.Channel.Id) as IMessageChannel).GetMessageAsync(m.Id).Result;
						if (m.Reactions.ToList().Exists(r => r.Key.Name == e.Name && r.Value.IsMe) && m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount == reactionCount + 1)
						{
							if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Reacted to [the message]({Bot.getMessageLink(m as IUserMessage)}) with {e}.");
						}
						else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **React Error** {_alert}", description: $"Failed to react to [the message]({Bot.getMessageLink(m as IUserMessage)}) with {e}.");
					}
					else //Custom emojis
					{
						GuildEmote e = Bot.getEmote(entries[2]);
						int reactionCount = m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount;

						m.AddReactionAsync(e).Wait();

						m = (Bot._client.GetChannel(m.Channel.Id) as IMessageChannel).GetMessageAsync(m.Id).Result;



						if (m.Reactions.ToList().Exists(r => r.Key.Name == e.Name && r.Value.IsMe) && m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount == reactionCount + 1)
						{
							if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Reacted to [the message]({Bot.getMessageLink(m as IUserMessage)}) with {e}.");
						}
						else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **React Error** {_alert}", description: $"Failed to react to [the message]({Bot.getMessageLink(m as IUserMessage)}) with {e}.");

                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }
		#endregion

		#region rreact
		/// <summary> edit a message | /edit </summary>
		public void rreact(SocketMessageCommand mi)
		{
			rreactInteractions.Add(mi);

			bool consoleWrite = false;
			IMessage m = mi.Data.Message;
			IGuildChannel channel = Bot.getGuildChannel(mi.Channel.Id);
			string commandName = "RReact";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";

			try
			{
				if (!confirmPerms(command.perms, mi, commandName, (m.Channel as IGuildChannel).Guild.Id)) return;



                if (m != null)
                {
                    IMessage msg = Bot.getMessageFromMessage(mi.Data.Message);

                    if (msg.Reactions.Count > 0)
                    {
                        ComponentBuilder components = new();
                        List<SelectMenuOptionBuilder> options = msg.Reactions.ToList().Select(r => new SelectMenuOptionBuilder(
                            $"{r.Key.Name} ({(r.Value.ReactionCount > 1 ? r.Value.ReactionCount : Bot.getUsersName(msg.GetReactionUsersAsync(r.Key, 1).ToListAsync().Result.First().First(), Bot.getServer(m.Channel.Id)))})", 
                            r.Key.Name, emote: r.Key)).ToList();

                        components.WithSelectMenu(new()
                        {
                            CustomId = $"RReact:{msg.Channel.Id}|{msg.Id}",
                            Options = options
                        });

                        mi.RespondAsync("Select a reaction.", components: components.Build(), ephemeral: true);
                    }
                    else mi.RespondAsync(embed: new EmbedBuilder()
					{
						Color = Color.LightOrange,
						Title = $"{_alert} **{commandName} Error** {_alert}",
						Description = "No reactions found."
					}.Build(), ephemeral: true);
				}
				else mi.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.LightOrange,
					Title = $"{_alert} **{commandName} Error** {_alert}",
					Description = description,
					Fields = new List<EmbedFieldBuilder>
						{
							new EmbedFieldBuilder()
							{
								Name = "Incorrect parameters:",
								Value = errorMsg
							}
						}
				}.Build(), ephemeral: true);
			}
			catch
			{
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				if (!confirmPerms(command.perms, mi, commandName)) return;

				mi.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.LightOrange,
					Title = $"{_alert} **{commandName} Error** {_alert}",
					Description = description + errorMsg
				}.Build(), ephemeral: true);
			}
		}

		public static void rreact_SelectedEmote(SocketMessageComponent mci)
		{
			string emoteName = mci.Data.Values.First();
			string[] ids = mci.Data.CustomId[(mci.Data.CustomId.IndexOf(":") + 1)..].Split('|');
			IMessage m = (Bot._client.GetChannelAsync(ulong.Parse(ids[0])).Result as IMessageChannel).GetMessageAsync(ulong.Parse(ids[1])).Result;

			if (m != null)
			{
				ComponentBuilder components = new();
                IEmote emote = m.Reactions.ToList().Find(e => e.Key.Name == emoteName.Trim()).Key;
                IReadOnlyCollection<IUser> users = m.GetReactionUsersAsync(emote, 100).ToListAsync().Result.First();

                if (users.Count == 1) rreact_Remove(m, emote, users.First(), mci);
                else
				{
					List<SelectMenuOptionBuilder> options = users.Select(u =>
					new SelectMenuOptionBuilder($"{Bot.getUsersName(u, Bot.getServer(mci.Channel.Id))}", u.Id.ToString())).ToList();

					components.WithSelectMenu(new()
					{
						CustomId = mci.Data.CustomId.Replace("RReact", "RReact2") + $"|{emote.Name}",
						Options = options
					});

					mci.RespondAsync($"Select a user to remove their {emote} reaction.", components: components.Build(), ephemeral: true);
				}
			}

			update_rreactInteractions(mci);
		}

		public static void rreact_SelectedUser(SocketMessageComponent mci)
		{
			string userIDValue = mci.Data.Values.First();
			string[] ids = mci.Data.CustomId[(mci.Data.CustomId.IndexOf(":") + 1)..].Split('|');

			IMessage m = (Bot._client.GetChannelAsync(ulong.Parse(ids[0])).Result as IMessageChannel).GetMessageAsync(ulong.Parse(ids[1])).Result;
            IEmote e = m.Reactions.ToList().Find(r => r.Key.Name == ids[2]).Key;

			if (m != null && e != null && ulong.TryParse(userIDValue.Trim(), out ulong userID))
			{
                IUser u = Bot._client.GetUserAsync(userID).Result;
                rreact_Remove(m, e, u, mci);
			}

			update_rreactInteractions(mci);
		}
        public static void rreact_Remove(IMessage msg, IEmote emote, IUser user, SocketMessageComponent mci)
		{
			msg.RemoveReactionAsync(emote, user);

			if (!Bot.getMessageFromMessage(msg).GetReactionUsersAsync(emote, 100).ToListAsync().Result.First().Contains(user))
				mci.RespondAsync(embed: new EmbedBuilder() { Color = Color.Green, Title = $"Removal successful." }.Build(), ephemeral: true);
			else
				mci.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.Red,
					Title = $"{_alert} **RReact Error** {_alert}",
					Description = "Removal unsuccessful."
				}.Build(), ephemeral: true);

            update_rreactInteractions(mci, false);
		}

		internal static List<IDiscordInteraction> rreactInteractions = new();
		internal static void update_rreactInteractions(IDiscordInteraction toAdd, bool add = true)
        {
			List<IDiscordInteraction> toRemoveList = rreactInteractions.FindAll(i => i.User.Id == toAdd.User.Id);
			foreach (IDiscordInteraction toRemove in toRemoveList)
			{
				if (toRemove.GetOriginalResponseAsync().Result != null)
					toRemove.DeleteOriginalResponseAsync().Wait();
				rreactInteractions.Remove(toRemove);
			}

			if (add) rreactInteractions.Add(toAdd);
		}

		/// <summary> remove all reactions | =rReact messageLink </summary>
		public void rreact(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "RReact";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				IMessage m = Bot.getMessageFromLink(msg);

                if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(m.Channel.Id).Id)) return;



				if (m != null)
				{
					m.RemoveAllReactionsAsync().Wait();

					m = (Bot._client.GetChannel(m.Channel.Id) as IMessageChannel).GetMessageAsync(m.Id).Result;


                    if (m.Reactions.Count == 0)
                    {
                        if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Removed all reactions from [the message]({Bot.getMessageLink(m as IUserMessage)}).");
                    }
                    else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **RReact Error** {_alert}",
                            description: $"Unable to remove all reactions from [the message]({Bot.getMessageLink(m as IUserMessage)}). {m.Reactions.Count} reaction{(m.Reactions.Count == 1 ? "" : "s")} remain.",
                            footer: (Bot._client.GetDMChannelAsync(m.Channel.Id) != null ? "*Note that this command does not work on messages in DMs.*" : ""));
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }


        /// <summary> remove all reactions added by user | =rUReact messageLink\d\:emoji_name:\userID or =rUReact messageLink\c\emoji_name\userID </summary>
        public void rureact(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "RUReact";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			try
			{
				string[] entries = msg.Split('\\');
				IMessage m = Bot.getMessageFromLink(entries[0]);

                if (!confirmPerms(command.perms, message, commandName, Bot.getGuild(m.Channel.Id).Id)) return;



                if (entries.Length == 4 && m != null &&
                ulong.TryParse(entries[3].Trim(), out ulong userID))
                {
                    if (entries[1] == "d") //Default emojis
                    {
                        Emoji e = new(entries[2].Trim());
                        int reactionCount = m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount;

						m.RemoveReactionAsync(e, Bot._client.GetUser(userID)).Wait();

						m = (Bot._client.GetChannel(m.Channel.Id) as IMessageChannel).GetMessageAsync(m.Id).Result;

						if (m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount == reactionCount - 1)
						{
							if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Removed {Bot._client.GetUser(userID).Username}{(Bot._client.GetUser(userID).Username.EndsWith("s") ? "'" : "'s")} {new Emoji(entries[2].Trim())} reaction from [the message]({Bot.getMessageLink(m as IUserMessage)}).");
						}
						else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **RUReact Error** {_alert}",
								description: $"Unable to remove {Bot._client.GetUser(userID).Username}{(Bot._client.GetUser(userID).Username.EndsWith("s") ? "'" : "'s")} {new Emoji(entries[2].Trim())} reaction from [the message]({Bot.getMessageLink(m as IUserMessage)}).");
					}
					else //Custom emojis
					{
						GuildEmote e = Bot.getEmote(entries[2].Trim());
						int reactionCount = m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount;

						m.RemoveReactionAsync(e, Bot._client.GetUser(userID)).Wait();

						m = (Bot._client.GetChannel(m.Channel.Id) as IMessageChannel).GetMessageAsync(m.Id).Result;

						if (m.Reactions.ToList().Find(r => r.Key.Name == e.Name).Value.ReactionCount == reactionCount - 1)
						{
							if (reply) Bot.sendEmbed(message.Channel, Color.Green, description: $"Removed {Bot._client.GetUser(userID).Username}{(Bot._client.GetUser(userID).Username.EndsWith("s") ? "'" : "'s")} {new Emoji(entries[2].Trim())} reaction from [the message]({Bot.getMessageLink(m as IUserMessage)}).");

                        }
                        else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **RUReact Error** {_alert}",
                                description: $"Unable to remove {Bot._client.GetUser(userID).Username}{(Bot._client.GetUser(userID).Username.EndsWith("s") ? "'" : "'s")} {new Emoji(entries[2].Trim())} reaction from [the message]({Bot.getMessageLink(m as IUserMessage)}).");
                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }
		#endregion
		#endregion

		#region SQL
		/// <summary> Use a specific command | =executeCommand [query results (true or false)]\query\max results (optional) </summary>
		public void executecommand(SocketMessage message, bool reply, bool delete)
        {
            bool consoleWrite = false;
            string commandName = "ExecuteCommand";
            (string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');

                if (entries.Length >= 2)
                {
                    int maxResults = 0;
                    bool queryResults = false;
                    if (bool.TryParse(entries[0], out bool qr)) queryResults = qr;
                    if (entries.Length == 3 && int.TryParse(entries[2], out int max)) maxResults = max;

					string query = entries[1].Trim();

					if (queryResults)
					{
						DataSet result = Bot._msm.executeQuery(query);

                        if (result.Tables.Count > 0 && result.Tables[0] != null && result.Tables[0].Rows.Count > 0)
                        {
                            List<string> columns = new();
                            foreach (DataColumn col in result.Tables[0].Columns)
                            {
                                if (col != null) columns.Add(col.ColumnName);
                            }

                            Bot._msm.printDataEmbeds(message.Channel, columns, result, maxResults: maxResults);
                        }
                        else Bot.sendEmbed(message.Channel, Color.Red,
                            title: $"{_alert} **{commandName} Error** {_alert}",
                            description: description,
                            subtitle: "No results found for the following query:",
                            text: $"```{query}```");
                    }
                    else
                    {
                        try
                        {
                            bool rowsAffected = Bot._msm.executeNonQuery(query);
                            if (rowsAffected) Bot.sendEmbed(message.Channel, Color.Green, title: $"Successfully executed command.", description: $"{rowsAffected} rows affected.");
                            else Bot.sendEmbed(message.Channel, Color.Red, subtitle: "No effect from executing :", text: $"```{query}```");
                        }
                        catch (Exception e)
                        {
                            Bot._msm.closeIfPossible();
                            Bot.sendEmbed(message.Channel, Color.Red,
                            title: $"{_alert} **{commandName} Error** {_alert}",
                            description: description,
                            subtitle: "Error executing the following command:",
                            text: $"```{query}```\n\n**Error:** ```{e}```");
                        }
                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> Query the SQL database | =getData schema\table\columns, separated by commas\max results (optional) </summary>
		public void getdata(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetData";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');

                if (entries.Length is 3 or 4)
                {
                    int maxResults = 0;
                    if (entries.Length == 4 && int.TryParse(entries[3], out int max)) maxResults = max;

					string schema = entries[0].Trim();
					string table = entries[1].Trim();
					string columnList = entries[2].Replace(", ", ",");
					List<string> columns = entries[2].Split(',').ToList();
					DataSet result = Bot._msm.simpleSelect(schema, columns, table);

                    if (result.Tables.Count > 0 && result.Tables[0] != null && result.Tables[0].Rows.Count > 0)
                        Bot._msm.printDataEmbeds(message.Channel, columns, result, maxResults: maxResults);
                    else Bot.sendEmbed(message.Channel, Color.Red,
                        title: $"{_alert} **{commandName} Error** {_alert}",
                        description: description,
                        subtitle: "No results found for the following query:",
                        text: $"```SELECT {MySQLManager.combineColumns(columns)} FROM `{schema}`.{table};```");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> Query the SQL database for specific messages | =getmessages [server name or server id]\[conditions: separated by new lines]
		/// start: (datetime)
		/// end: (datetime)
		/// user: (ID or name)
		/// channel: (ID or name)
		/// max results: (#)
		/// show details: (true or false)
		/// message types: (S, E, D)
		/// content: (string)
		/// </summary>
		public void getmessages(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetMessages";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');
				List<string> rawConditions = entries[1].Split('\n').ToList();

                if (entries.Length > 1)
                {
                    List<string> errorText = new();

                    DateTime? start = null, end = null;
                    string serverName = entries[0].Trim(), userName = "", channelName = "", content = "";
                    ulong userID = 0, serverID = 0, channelID = 0;
                    bool showDetails = false;
                    List<string> types = new();
                    int maxResults = 0;

					if (serverID == 0)
					{
						serverID = Bot._msm.getServer(serverName);

                        if (serverID is 1 or 0)
                        {
                            Bot.sendEmbed(message.Channel, color: Color.Red, title: $"{_alert} **{commandName} Error** {_alert}",
                                description: serverID == 1 ? $"Too many servers with the name {serverName}. Please use the server ID instead." : $"No servers were found with the name {serverName}. Please check your entry or try the server ID instead.");
                            return;
                        }
                    } //get server

                    SocketGuild server = Bot._client.GetGuild(serverID);

                    foreach (string condition in rawConditions)
                    {
                        if (condition.Length > 0)
                        {
                            string conditionName = condition[..condition.IndexOf(":")];
                            string conditionValue = condition[(condition.IndexOf(":") + 1)..].Trim();
                            switch (conditionName)
                            {
                                case "start":
                                    if (DateTime.TryParse(conditionValue.Trim(), out DateTime startTemp))
                                        start = startTemp;
                                    break;

								case "end":
									if (DateTime.TryParse(conditionValue, out DateTime endTemp))
										end = endTemp;
									break;

								case "show details":
									if (bool.TryParse(conditionValue, out bool sd))
										showDetails = sd;
									break;

                                case "max results":
                                    if (int.TryParse(conditionValue, out int mr))
                                        maxResults = mr;
                                    break;

                                case "message types":
                                    foreach (string t in conditionValue.Split(','))
                                    {
                                        string type = t.Trim().ToUpper();
                                        if (type is "S" or "D" or "E")
                                            types.Add(type);
                                    }

                                    break;

								case "user":
									if (ulong.TryParse(conditionValue, out ulong userIDTemp))
										userID = userIDTemp;
									else userName = conditionValue;
									break;

								case "channel":
									if (ulong.TryParse(conditionValue, out ulong channelIDTemp))
										channelID = channelIDTemp;
									else channelName = conditionValue;
									break;

								case "content":
									content = conditionValue;
									break;
							}
						}
					}

                    if (channelID == 0 && !string.IsNullOrEmpty(channelName))
                    {
                        channelID = Bot._msm.getChannel(server, channelName);
                        if (channelID == 1)
                            errorText.Add($"Too many channels found with the name {channelName}. Please try a channel ID");
                        else if (channelID == 0)
                            errorText.Add($"No channels found with the name {channelName}. Please try a channel ID");
                    } //get channel

                    if (userID == 0 && !string.IsNullOrEmpty(userName))
                    {
                        userID = Bot._msm.getUser(server, userName);
                        if (userID <= 1)
                        {
                            if (!string.IsNullOrEmpty(userName))
                            {
                                List<IGuildUser> users = Bot.getUser(userName, serverID);
                                if (users.Count == 1) userID = users.First().Id;
                                else if (users.Count > 1) userID = 1;
                                else if (users.Count == 0) userID = 0;
                            }

							if (userID == 1)
								errorText.Add($"Too many users found with the username or nickname {userName}. Please try a user ID");
							else if (userID == 0)
								errorText.Add($"No users found with the username or nickname {userName}. Please try a user ID");
						}
					} //get user

                    List<string> conditions = new();
                    if (start != null) conditions.Add($"`datetime` >= '{start.Value:yyyy-MM-dd HH:mm:ss}'");
                    if (end != null) conditions.Add($"`datetime` < '{end.Value:yyyy-MM-dd HH:mm:ss}'");
                    if (userID != 0) conditions.Add($"`authorID` = '{userID}'");
                    if (userID == 0 && userName != "") conditions.Add($"`authorName` = '{userName.Trim()}'");
                    if (channelID != 0) conditions.Add($"`channelID` = '{channelID}'");
                    if (channelID == 0 && userName != "") conditions.Add($"`channelName` = '{channelName.Trim()}'");
                    if (types.Count > 0)
                    {
                        string typeCondition = "(";
                        foreach (string type in types)
                        {
                            typeCondition += $"`type` = '{type.Trim()}'";
                            if (types.IndexOf(type) + 1 != types.Count && types.Count > 1) typeCondition += " OR ";
                        }

                        typeCondition += ")";
                        conditions.Add(typeCondition);
                    }

                    if (content.Length > 0) conditions.Add($"`content` like '%{content}%'");
                    string query = $"SELECT `datetime`, `authorName`, `type`, `channelName`, `content`, `messageID`, `authorID`, `channelID`, `attachmentCount`, `embedCount` FROM `{serverID}`.`messages` WHERE\n";

                    foreach (string condition in conditions)
                    {
                        query += $"{condition}";
                        if (conditions.IndexOf(condition) + 1 != conditions.Count) query += " AND\n";
                    }

                    query += $"{(query.Last() != '\n' ? "\n" : "")}ORDER BY `datetime` ASC;";

                    List<string> columns = new() { "datetime", "authorName", "type", "channelName", "content", "messageID", "authorID", "channelID", "attachmentCount", "embedCount" };
                    if (showDetails) columns.Add("showDetails");

					DataSet results = Bot._msm.executeQuery(query);
					if (results.Tables.Count > 0 && results.Tables[0] != null && results.Tables[0].Rows.Count > 0)
						Bot._msm.printDataEmbeds(message.Channel, columns, results, maxResults: maxResults, resultTitle:
							$"Messages{(userID > 1 ? $" from `{(!string.IsNullOrEmpty(userName) && userName != userID.ToString() ? userName : results.Tables[0].Rows[0]["authorName"])}`" : "")} " +
							$"in `{(!string.IsNullOrEmpty(channelName) && channelName != channelID.ToString() ? channelName : results.Tables[0].Rows[0]["channelName"])}` " +
							$"on `{(!string.IsNullOrEmpty(serverName) && serverName != serverID.ToString() ? serverName : Bot._client.GetGuild(serverID).Name)}`",
							formatAsMessage: true, serverID: serverID);
					else errorText.Add($"No results found for the following query:\n```{query}```");

					if (errorText.Count > 0)
					{
						string errors = "";

						foreach (string error in errorText)
						{
							errors += $"{(errors.Length > 0 ? "\n" : "")}{error}";
						}

                        Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: errors);
                    }
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> Get all references to a specific message from the SQL database | =getMsg server name or id\message id or link </summary>
		public void getmsg(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetMsg";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				IMessage m;
				List<string> entries = msg.Split('\\').ToList();
				ulong serverID = 0, messageID = 0;

				if (Uri.IsWellFormedUriString(msg, UriKind.Absolute))
				{
					if ((m = Bot.getMessageFromLink(msg)) != null)
					{
						serverID = (m.Channel as IGuildChannel).Guild.Id;
						messageID = m.Id;
					}
					else
					{
						serverID = Bot.getGuild(message.Channel.Id).Id;
						messageID = ulong.Parse(msg.Split('/').Last().Trim());
					}
				}
				else if (entries.Count == 2)
				{
					if (!ulong.TryParse(entries[0].Trim(), out serverID)) serverID = Bot._msm.getServer(entries[0].Trim());					

					if (ulong.TryParse(entries[1].Trim(), out ulong mID)) messageID = mID;
				}

				if (serverID != 0 && messageID != 0)
				{
					DataSet results = Bot._msm.executeQuery($"SELECT authorName, authorID, channelName, channelID, type, content, datetime, embedCount, attachmentCount FROM `{serverID}`.`messages` WHERE messageID = '{messageID}' ORDER BY datetime ASC;");

					if (results.Tables.Count > 0 && results.Tables[0] != null && results.Tables[0].Rows.Count > 0)
					{
						DataRow data = results.Tables[0].Rows[0];

						string channelLink = Bot.getChannelLink(ulong.Parse(data["channelID"].ToString())),
							   messageLink = $"{channelLink}/{messageID}";

						List<EmbedFieldBuilder> fields = new();
						bool deleted = false;
						int editCount = 0;

						if (Bot.getMessageFromLink(messageLink) == null) deleted = true;

						foreach (DataRow row in results.Tables[0].Rows)
						{
							string type = row["type"].ToString();
							bool hasEmbeds = row["embedCount"].ToString() != "0",
								 hasAttchments = row["attachmentCount"].ToString() != "0";

							if (type == "E") editCount++;

							fields.Add(new EmbedFieldBuilder()
							{
								Name = $"`[{row["datetime"]}]`: {(type == "S" ? "Original" : type == "E" ? $"Edit {editCount}" : "Deleted")}" + (!hasEmbeds && !hasAttchments ? "" :
									("\n`[" +
										(hasEmbeds ? $"Embeds: {row["embedCount"]}" : "") +
										(hasEmbeds && hasAttchments ? " | " : "") +
										(hasAttchments ? $"Attachments: {row["attachmentCount"]}" : "")
									+ "]`")),
								Value = type != "D" ? row["content"].ToString() : "‎"
							});
						}

						Bot.sendEmbed(message.Channel, Color.Green, title: "GetMsg Results", fields: fields, url: !deleted ? messageLink : null,
							description: $"<@{data["authorID"]}> ({data["authorName"]}) in {(channelLink != null ? $"[{data["channelName"]}]({channelLink})" : $"{data["channelName"]} (deleted channel)")}");
					}
				}
				else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
			}
			catch (Exception e)
			{
				MM.Print(true, $"\n{commandName}:\n{e}\n");
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
			}
		}

		/// <summary> Query the SQL database to get x most recent messages from a certain channel and/or user | =getRecent # of messages\show details (true or false)\server name or id\[Channel: channel name or id (optional)]\[User: user name or id (optional)] </summary>
		public void getrecent(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetRecent";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');

                if (entries.Length >= 3)
                {
                    int numOfResults = 0;
					if (int.TryParse(entries[0], out int num)) numOfResults = num;
                    string serverName = entries[2].Trim(),
                        channelName = "", userName = "";
                    List<string> errorText = new();
                    bool showDetails = false;
                    if (bool.TryParse(entries[1].Trim(), out bool show)) showDetails = show;
                    ulong channelID = 0, userID = 0, serverID = 0;
                    if (ulong.TryParse(entries[2].Trim(), out ulong sID)) serverID = sID;

                    if (entries.Length >= 4)
                    {
                        int channelIndex = entries[3][1..].StartsWith("hannel:") ? 3 : entries.Length == 5 && entries[4][1..].StartsWith("channel:") ? 4 : -1;
                        int userIndex = entries[3][1..].StartsWith("ser:") ? 3 : entries.Length == 5 && entries[4][1..].StartsWith("ser:") ? 4 : -1;

                        if (channelIndex != -1)
                        {
                            channelName = entries[channelIndex].Replace("channel:", "").Replace("Channel:", "").Trim();
                            if (ulong.TryParse(channelName, out ulong cID)) channelID = cID;
                        }

                        if (userIndex != -1)
                        {
                            userName = entries[userIndex].Replace("user:", "").Replace("User:", "").Trim();
                            if (ulong.TryParse(userName, out ulong uID)) userID = uID;
                        }

						if (channelIndex + userIndex == -2) Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: $"Incorrect parameters {(entries.Length == 5 ? "for options 4 & 5" : "for option 4")}:", text: errorMsg);
					}

					if (serverID == 0)
					{
						serverID = Bot._msm.getServer(serverName);

                        if (serverID is 1 or 0)
                        {
                            Bot.sendEmbed(message.Channel, color: Color.Red, title: $"{_alert} **{commandName} Error** {_alert}",
                                description: serverID == 1 ? $"Too many servers with the name {serverName}. Please use the server ID instead." : $"No servers were found with the name {serverName}. Please check your entry or try the server ID instead.");
                            return;
                        }
                    } //get server

                    SocketGuild server = Bot._client.GetGuild(serverID);

                    if (channelID == 0 && !string.IsNullOrEmpty(channelName))
                    {
                        channelID = Bot._msm.getChannel(server, channelName);
                        if (channelID == 1)
                            errorText.Add($"Too many channels found with the name {channelName}. Please try a channel ID");
                        else if (channelID == 0)
                            errorText.Add($"No channels found with the name {channelName}. Please try a channel ID");
                    } //get channel

                    if (userID == 0 && !string.IsNullOrEmpty(userName))
                    {
                        userID = Bot._msm.getUser(server, userName);
                        if (userID <= 1)
                        {
                            if (!string.IsNullOrEmpty(userName))
                            {
                                List<IGuildUser> users = Bot.getUser(userName, serverID);
                                if (users.Count == 1) userID = users.First().Id;
                                else if (users.Count > 1) userID = 1;
                                else if (users.Count == 0) userID = 0;
                            }

							if (userID == 1)
								errorText.Add($"Too many users found with the username or nickname {userName}. Please try a user ID");
							else if (userID == 0)
								errorText.Add($"No users found with the username or nickname {userName}. Please try a user ID");
						}
					} //get user

                    string query = "SELECT " +
                        $"`datetime`, `authorName`, `type`, `channelName`, `content`, `attachmentCount`, `embedCount`, `FID`, `messageID`, `authorID`, `channelID` " +
                        $"FROM `{serverID}`.messages{(userID > 1 || channelID > 1 ? " WHERE\n" : "")}" +
                        $"{(channelID > 1 ? $" `channelID` = '{channelID}'" : "")}{(userID > 1 ? $"{(channelID > 0 ? " AND" : "")} `authorID` = '{userID}'" : "")}\n" +
                        $"ORDER BY `datetime` ASC;";
                    DataSet results = Bot._msm.executeQuery(query);
                    List<string> columns = new() { "datetime", "authorName", "type", "channelName", "content", "attachmentCount", "embedCount", "messageID", "authorID", "channelID", "FID"};
                    if (showDetails) columns.Add("showDetails");

                    if (results.Tables.Count > 0 && results.Tables[0] != null && results.Tables[0].Rows.Count > 0)
                        Bot._msm.printDataEmbeds(message.Channel, columns, results, maxResults: numOfResults, resultTitle:
                            $"Messages{(userID > 1 ? $" from `{(!string.IsNullOrEmpty(userName) && userName != userID.ToString() ? userName : results.Tables[0].Rows[0]["authorName"])}`" : "")} " +
                            $"{(channelID > 1 ? $"in `{(!string.IsNullOrEmpty(channelName) && channelName != channelID.ToString() ? channelName : results.Tables[0].Rows[0]["channelName"])}` " : "")}" +
                            $"on `{(!string.IsNullOrEmpty(serverName) && serverName != serverID.ToString() ? serverName : Bot._client.GetGuild(serverID).Name)}`",
                            formatAsMessage: true, indexFromEnd: numOfResults, serverID: serverID);
                    else Bot.sendEmbed(message.Channel, color: Color.Red, title: $"{_alert} **{commandName} Error** {_alert}",
                                description: $"No results found for the following query: \n```{query}```");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> Query the SQL database to get x most recent deleted messages from a certain channel and/or user | =getDeleted # of messages\show details (true or false)\server name or id\[Channel: channel name or id (optional)]\[User: user name or id (optional)] </summary>
		public void getdeleted(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetDeleted";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');

                if (entries.Length >= 3)
                {
                    int numOfResults = 0;
					if (int.TryParse(entries[0], out int num)) numOfResults = num;
                    string serverName = entries[2].Trim(),
                        channelName = "", userName = "";
                    List<string> errorText = new();
                    bool showDetails = false;
                    if (bool.TryParse(entries[1].Trim(), out bool show)) showDetails = show;
                    ulong channelID = 0, userID = 0, serverID = 0;
                    if (ulong.TryParse(entries[2].Trim(), out ulong sID)) serverID = sID;

                    if (entries.Length >= 4)
                    {
                        int channelIndex = entries[3][1..].StartsWith("hannel:") ? 3 : entries.Length == 5 && entries[4][1..].StartsWith("channel:") ? 4 : -1;
                        int userIndex = entries[3][1..].StartsWith("ser:") ? 3 : entries.Length == 5 && entries[4][1..].StartsWith("ser:") ? 4 : -1;

                        if (channelIndex != -1)
                        {
                            channelName = entries[channelIndex].Replace("channel:", "").Replace("Channel:", "").Trim();
                            if (ulong.TryParse(channelName, out ulong cID)) channelID = cID;
                        }

                        if (userIndex != -1)
                        {
                            userName = entries[userIndex].Replace("user:", "").Replace("User:", "").Trim();
                            if (ulong.TryParse(userName, out ulong uID)) userID = uID;
                        }

						if (channelIndex + userIndex == -2) Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: $"Incorrect parameters {(entries.Length == 5 ? "for options 4 & 5" : "for option 4")}:", text: errorMsg);
					}

					if (serverID == 0)
					{
						serverID = Bot._msm.getServer(serverName);

                        if (serverID is 1 or 0)
                        {
                            Bot.sendEmbed(message.Channel, color: Color.Red, title: $"{_alert} **{commandName} Error** {_alert}",
                                description: serverID == 1 ? $"Too many servers with the name {serverName}. Please use the server ID instead." : $"No servers were found with the name {serverName}. Please check your entry or try the server ID instead.");
                            return;
                        }
                    } //get server

                    SocketGuild server = Bot._client.GetGuild(serverID);

                    if (channelID == 0 && !string.IsNullOrEmpty(channelName))
                    {
                        channelID = Bot._msm.getChannel(server, channelName);
                        if (channelID == 1)
                            errorText.Add($"Too many channels found with the name {channelName}. Please try a channel ID");
                        else if (channelID == 0)
                            errorText.Add($"No channels found with the name {channelName}. Please try a channel ID");
                    } //get channel

                    if (userID == 0 && !string.IsNullOrEmpty(userName))
                    {
                        userID = Bot._msm.getUser(server, userName);
                        if (userID <= 1)
                        {
                            if (!string.IsNullOrEmpty(userName))
                            {
                                List<IGuildUser> users = Bot.getUser(userName, serverID);
                                if (users.Count == 1) userID = users.First().Id;
                                else if (users.Count > 1) userID = 1;
                                else if (users.Count == 0) userID = 0;
                            }

							if (userID == 1)
								errorText.Add($"Too many users found with the username or nickname {userName}. Please try a user ID");
							else if (userID == 0)
								errorText.Add($"No users found with the username or nickname {userName}. Please try a user ID");
						}
					} //get user

					string query = $"SELECT * FROM `{serverID}`.messages \n" +
						"WHERE `messageID` IN \n" +
							"(SELECT `messageID` FROM \n" +
								$"(SELECT `type`, `messageID` FROM `{serverID}`.messages \n" +
								"WHERE `type` = 'S' OR `type` = 'D') AS a \n" +
							"GROUP BY `messageID` HAVING COUNT(`messageID`) > 1) \n" +
						$"AND (`type` = 'S' OR `type` = 'E'){(channelID > 1 || userID > 1 ? " \n" : "")}" +
						$"{(channelID > 1 ? $" AND `channelID` = '{channelID}'" : "")}{(userID > 1 ? $" AND `authorID` = '{userID}'" : "")}\n" +
						$"ORDER BY `datetime` ASC;";
					DataSet results = Bot._msm.executeQuery(query);

                    List<string> columns = new() { "datetime", "authorName", "type", "channelName", "content", "attachmentCount", "embedCount", "messageID", "authorID", "channelID", "FID" };
                    if (showDetails) columns.Add("showDetails");

                    if (results.Tables.Count > 0 && results.Tables[0] != null && results.Tables[0].Rows.Count > 0)
                        Bot._msm.printDataEmbeds(message.Channel, columns, results, maxResults: numOfResults, resultTitle:
                            $"Deleted Messages{(userID > 1 ? $" from `{(!string.IsNullOrEmpty(userName) && userName != userID.ToString() ? userName : results.Tables[0].Rows[0]["authorName"])}`" : "")} " +
                            $"{(channelID > 1 ? $"in `{(!string.IsNullOrEmpty(channelName) && channelName != channelID.ToString() ? channelName : results.Tables[0].Rows[0]["channelName"])}` " : "")}" +
                            $"on `{(!string.IsNullOrEmpty(serverName) && serverName != serverID.ToString() ? serverName : Bot._client.GetGuild(serverID).Name)}`",
                            formatAsMessage: true, indexFromEnd: numOfResults, serverID: serverID);
                    else Bot.sendEmbed(message.Channel, color: Color.Red, title: $"{_alert} **{commandName} Error** {_alert}",
                                description: $"No results found for the following query: \n```{query}```");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> Query the SQL database to get the number of messages sent with certain parameters | =getMessageCount server name or id\[Channel: channel name or id (optional)]\[User: user name or id (optional)] </summary>
		public void getmessagecount(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = false;
			string commandName = "GetMessageCount";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');

                if (entries.Length >= 1 && !string.IsNullOrWhiteSpace(entries[0]))
                {
                    string serverName = entries[0].Trim(),
                        channelName = "", userName = "";
                    List<string> errorText = new();
                    ulong channelID = 0, userID = 0, serverID = 0;
                    if (ulong.TryParse(entries[0].Trim(), out ulong sID)) serverID = sID;

                    if (entries.Length >= 2)
                    {
                        int channelIndex = entries[1][1..].StartsWith("hannel:") ? 1 : entries.Length == 3 && entries[2][1..].StartsWith("channel:") ? 2 : -1;
                        int userIndex = entries[1][1..].StartsWith("ser:") ? 1 : entries.Length == 3 && entries[2][1..].StartsWith("ser:") ? 2 : -1;

                        if (channelIndex != -1)
                        {
                            channelName = entries[channelIndex].Replace("channel:", "").Replace("Channel:", "").Trim();
                            if (ulong.TryParse(channelName, out ulong cID)) channelID = cID;
                        }

                        if (userIndex != -1)
                        {
                            userName = entries[userIndex].Replace("user:", "").Replace("User:", "").Trim();
                            if (ulong.TryParse(userName, out ulong uID)) userID = uID;
                        }

						if (channelIndex + userIndex == -2) Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: $"Incorrect parameters {(entries.Length == 5 ? "for options 4 & 5" : "for option 4")}:", text: errorMsg);
					}

					if (serverID == 0)
					{
						serverID = Bot._msm.getServer(serverName);

                        if (serverID is 1 or 0)
                        {
                            Bot.sendEmbed(message.Channel, color: Color.Red, title: $"{_alert} **{commandName} Error** {_alert}",
                                description: serverID == 1 ? $"Too many servers with the name {serverName}. Please use the server ID instead." : $"No servers were found with the name {serverName}. Please check your entry or try the server ID instead.");
                            return;
                        }
                    } //get server

                    SocketGuild server = Bot._client.GetGuild(serverID);

                    if (channelID == 0 && !string.IsNullOrEmpty(channelName))
                    {
                        channelID = Bot._msm.getChannel(server, channelName);
                        if (channelID == 1)
                            errorText.Add($"Too many channels found with the name {channelName}. Please try a channel ID");
                        else if (channelID == 0)
                            errorText.Add($"No channels found with the name {channelName}. Please try a channel ID");
                    } //get channel

                    if (userID == 0 && !string.IsNullOrEmpty(userName))
                    {
                        userID = Bot._msm.getUser(server, userName);
                        if (userID <= 1)
                        {
                            if (!string.IsNullOrEmpty(userName))
                            {
                                List<IGuildUser> users = Bot.getUser(userName, serverID);
                                if (users.Count == 1) userID = users.First().Id;
                                else if (users.Count > 1) userID = 1;
                                else if (users.Count == 0) userID = 0;
                            }

							if (userID == 1)
								errorText.Add($"Too many users found with the username or nickname {userName}. Please try a user ID");
							else if (userID == 0)
								errorText.Add($"No users found with the username or nickname {userName}. Please try a user ID");
						}
					} //get user

                    string query = "SELECT " +
                        $"`datetime`, `authorName`, `type`, `channelName`, `attachmentCount`, `embedCount`, `messageID`, `authorID`, `channelID` " +
                        $"FROM `{serverID}`.messages{(userID > 1 || channelID > 1 ? " WHERE\n" : "")}" +
                        $"{(channelID > 1 ? $"`channelID` = '{channelID}'" : "")}{(userID > 1 ? $"{(channelID > 0 ? " AND " : "")}`authorID` = '{userID}'" : "")}\n" +
                        $"ORDER BY `authorID`, `type` ASC;";
                    DataSet results = Bot._msm.executeQuery(query);
                    List<string> columns = new() { "datetime", "authorName", "type", "channelName", "content", "attachmentCount", "embedCount", "messageID", "authorID", "channelID", "FID" };

                    if (results.Tables.Count > 0 && results.Tables[0] != null && results.Tables[0].Rows.Count > 0)
                    {
                        DataRowCollection rows = results.Tables[0].Rows;
                        List<(string authorID, string authorName, int s, int e, int d, int index)> counts = new();

						foreach (DataRow row in rows)
						{
							if (counts.Count == 0 || counts.Last().authorID != row["authorID"].ToString())
								counts.Add((row["authorID"].ToString(), row["authorName"].ToString(), 0, 0, 0, 0));

							string type = row["type"].ToString();
							(string authorID, string authorName, int s, int e, int d, int index) update = (
								counts.Last().authorID,
								row["authorName"].ToString(),
								counts.Last().s + (type == "S" ? 1 : 0),
								counts.Last().e + (type == "E" ? 1 : 0),
								counts.Last().d + (type == "D" ? 1 : 0),
								counts.Last().index + 1);

                            counts.Remove(counts.Last());
                            counts.Add(update);
                        }

                        counts = new List<(string authorID, string authorName, int s, int e, int d, int index)>(counts.OrderBy(c => c.index));

                        string title = $"Messages sent" +
                                $"{(channelID > 0 ? $" in {(channelName != channelID.ToString() ? $"__{channelName}__" : $"__{rows[^1]["channelName"]}__")}" : "")}" +
                                $"{(userID > 0 ? $" by {(userName != userID.ToString() ? $"__{userName}__" : $"__{rows[^1]["authorName"]}__")}" : "")}";
                        int estimatedEmbedSizes = (int)Math.Ceiling((double)((counts.Count *
                            ($"{new string('x', 25)}".Length +
                            $"<@{new string('0', 18)}>\n```Sent:    10000\nEdited:  10000\nDeleted: 1000```".Length))
                            - title.Length) / 6000);
                        string numSize = new('x', estimatedEmbedSizes);
                        int embedLength = title.Length + $"{(estimatedEmbedSizes > 1 ? $" [{numSize}/{numSize}]" : "")}".Length,
                            fieldsLength = 0;

                        List<List<EmbedFieldBuilder>> fieldsList = new();
                        List<EmbedFieldBuilder> fields = new();

                        foreach ((string authorID, string authorName, int s, int e, int d, int index) in counts)
                        {
                            string name = $"{authorName}",
                                value = $"<@{authorID}>\n```Sent:    {s}\nEdited:  {e}\nDeleted: {d}```";
                            EmbedFieldBuilder field = new() { Name = name, Value = value };
                            int fieldLength = name.Length + value.Length;

							if (fieldsLength + fieldLength >= 6000 || fields.Count >= 24)
							{
								fieldsList.Add(fields);
								fields = new List<EmbedFieldBuilder>();
								fieldsLength = 0;
							}

                            fields.Add(field);
                            fieldsLength += fieldLength;
                        }

                        if (!fieldsList.Contains(fields)) fieldsList.Add(fields);

                        foreach (List<EmbedFieldBuilder> e in fieldsList)
                        {
                            Bot.sendEmbed(message.Channel, color: Color.Blue, fields: e,
                                title: title + $"{(fieldsList.Count > 1 ? $" [{fieldsList.IndexOf(e) + 1}/{fieldsList.Count}]" : "")}");
                        }
                    }
                    else Bot.sendEmbed(message.Channel, color: Color.Red, 
                        title: $"{_alert} **{commandName} Error** {_alert}",
                        description: $"No results found for the following query: \n```{query}```");
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		/// <summary> Transfer Google Sheets data to MySQL | =googletosql Server ID\Tab Name </summary>
		public void googletosql(SocketMessage message, bool reply, bool delete)
		{
			bool consoleWrite = true;
			string commandName = "GoogleToSQL";
			(string commandName, string category, string description, string properFormat, Perms perms) command = _adminCommands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";
			string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length), reply, delete);

			if (!confirmPerms(command.perms, message, commandName)) return;



			try
			{
				string[] entries = msg.Split('\\');

                if (entries.Length == 2)
                {
                    ulong serverID = 0;
					if (ulong.TryParse(entries[0], out ulong sID)) serverID = sID;
                    string sheetName = entries[1].Trim();
                    if (serverID > 0)
                    {
                        string serverName = Bot._client.GetGuild(serverID)?.Name,
                            messageArea = Config._spreadsheetVars["Message area"];
                        Server server = new(serverID, serverName, false);
                        List<List<string>> data = SpreadsheetManager.readCellsFromSheet(sheetName, messageArea.Remove(messageArea.Length - 1));
                        DataSet results = Bot._msm.executeQuery($"SELECT `FID` FROM `{server._id}`.messages;");
                        int count = results.Tables.Count > 0 && results.Tables[0] != null ? results.Tables[0].Rows.Count : 0,
                            index = 0;

						foreach (List<string> row in data)
						{
							server._logger.messageBacklog.Add(new LogEntry(
								server,             //server
								$"{server._id}",    //tab name
								row[0],             //date
								row[1],             //time
								string.IsNullOrWhiteSpace(row[2]) ? "S" : row[2],   //input type
								string.IsNullOrWhiteSpace(row[3]) ? "0" : row[3],   //authorID
								string.IsNullOrWhiteSpace(row[4]) ? "0" : row[4],   //channelID
								string.IsNullOrWhiteSpace(row[5]) ? "0" : row[5].Replace("'", "").Replace("`", ""), //messageID
								string.IsNullOrWhiteSpace(row[6]) ? "NULL" : row[6], //channelName
								string.IsNullOrWhiteSpace(row[7]) ? "NULL" : row[7], //authorName
								row.Count >= 9 ? row[8] : "", //content
								true));

                            index++;
                            if (index >= 500 || data.IndexOf(row) == data.Count - 1)
                            {
                                MM.Print(consoleWrite, $"{serverName} Logging: {data.IndexOf(row) + 1}/{data.Count}");
                                while (server._logger.messageBacklog.Count > 0) { }

                                index = 0;
                            }
                        }

                        results = Bot._msm.executeQuery($"SELECT `FID` FROM `{server._id}`.messages;");
                        if (results.Tables.Count > 0 && results.Tables[0] != null) count = results.Tables[0].Rows.Count - count;
                        if (count == data.Count) Bot.sendEmbed(message.Channel, Color.Green, title: "Data successfully transfered.");
                        else Bot.sendEmbed(message.Channel, Color.Red, title: $"{_alert} **{commandName} Error** {_alert}", description: $"Missing {data.Count - count} entries.");
                    }                    
                }
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch (Exception e)
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}\n{e}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }


        internal static bool SQL_componentHandler(SocketMessageComponent ci)
        {
			if(Bot.confirmOwner(ci.User.Id) && 
                !Bot._msm.sendAttachmentsOrEmbeds(ci.Channel, ci.Data.CustomId, ci.Data.Values.First()))
            {
				Bot.sendEmbed(ci.Channel, Color.Red,
					title: $"{_alert} **GetRecent Error** {_alert}",
					description: $"Error retrieving selected {(ci.Data.Values.First().ToLower().StartsWith("a") ? "attchment" : "embed")}.");

                return true;
			}
            else return false;
        }
        #endregion
    }
}