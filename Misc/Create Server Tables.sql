USE `SERVERNAME`;

CREATE TABLE `messages` (
	`datetime` DATETIME NOT NULL,
	`type` VARCHAR(6) NOT NULL,
	`authorName` VARCHAR(32) NOT NULL,
	`channelName` VARCHAR(100) NOT NULL,
	`content` VARCHAR(4000) DEFAULT NULL,
	`embedCount` int NOT NULL DEFAULT '0',
	`attachmentCount` int NOT NULL DEFAULT '0',
	`FID` VARCHAR(32) NOT NULL,
	`authorID` BIGINT NOT NULL,
	`channelID` BIGINT NOT NULL,
	`messageID` BIGINT NOT NULL,
	PRIMARY KEY (`FID`),
	UNIQUE KEY `ID_UNIQUE` (`FID`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

CREATE TABLE `attachments` (
	`datetime` DATETIME NOT NULL,
    `messageFID` VARCHAR(32) NOT NULL,
    `messageID` VARCHAR(20) NOT NULL,
	`attachmentID` VARCHAR(32) NOT NULL,
	`filename` VARCHAR(1000) DEFAULT NULL,
	`url` VARCHAR(1000) DEFAULT NULL,
	`proxyUrl` VARCHAR(1000) DEFAULT NULL,
	PRIMARY KEY (`attachmentID`),
	UNIQUE KEY `attachmentID_UNIQUE` (`attachmentID`),
	KEY `message_idx` (`messageFID`),
	CONSTRAINT `message_attachments` FOREIGN KEY (`messageFID`)
		REFERENCES `messages` (`FID`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

CREATE TABLE `embeds` (
	`datetime` DATETIME NOT NULL,
	`embedHash` BIGINT NOT NULL AUTO_INCREMENT,
    `messageFID` VARCHAR(32) NOT NULL,
    `messageID` VARCHAR(20) NOT NULL,
	`color` VARCHAR(45) DEFAULT NULL,
	`author_name` VARCHAR(256) DEFAULT NULL,
	`author_url` VARCHAR(500) DEFAULT NULL,
	`author_iconUrl` VARCHAR(500) DEFAULT NULL,
	`author_proxyIconUrl` VARCHAR(500) DEFAULT NULL,
	`title` VARCHAR(256) DEFAULT NULL,
	`description` VARCHAR(4096) DEFAULT NULL,
	`field_count` INT DEFAULT NULL,
	`url` VARCHAR(500) DEFAULT NULL,
	`thumbnailUrl` VARCHAR(500) DEFAULT NULL,
	`thumbnailProxyUrl` VARCHAR(500) DEFAULT NULL,
	`videoUrl` VARCHAR(500) DEFAULT NULL,
	`footer_text` VARCHAR(2048) DEFAULT NULL,
	`footer_iconUrl` VARCHAR(500) DEFAULT NULL,
	`footer_proxyUrl` VARCHAR(500) DEFAULT NULL,
	`provider_name` VARCHAR(100) DEFAULT NULL,
	`provider_url` VARCHAR(500) DEFAULT NULL,
	`timestamp` TIME DEFAULT NULL,
	PRIMARY KEY (`embedHash`),
	UNIQUE KEY `pk_UNIQUE` (`embedHash`),
	KEY `message_idx` (`messageFID`),
	CONSTRAINT `message_embeds` FOREIGN KEY (`messageFID`)
		REFERENCES `messages` (`FID`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI ROW_FORMAT=DYNAMIC;

CREATE TABLE `embed_fields` (
    `datetime` DATETIME NOT NULL,
    `pk` INT NOT NULL AUTO_INCREMENT,
    `embedHash` BIGINT NOT NULL,
    `name` VARCHAR(256) NOT NULL,
    `value` VARCHAR(1024) NOT NULL,
    `index` INT NOT NULL,
    PRIMARY KEY (`pk`),
    KEY `embed_idx` (`embedHash`),
    CONSTRAINT `embed` FOREIGN KEY (`embedHash`)
        REFERENCES `embeds` (`embedHash`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

CREATE TABLE `interactions` (
    `type` VARCHAR(45) NOT NULL,
    `count` INT NOT NULL DEFAULT '0',
    PRIMARY KEY (`type`),
    UNIQUE KEY `type_UNIQUE` (`type`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4 COLLATE = UTF8MB4_0900_AI_CI;

INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [getdeleted]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [reconnectmysql]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [googletosql]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [getrecent]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [getdata]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [executecommand]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [getmessagecount]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [help]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [cpembed]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [nc]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [set]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [adapted]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [cnd]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [cn]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [fs]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [findmessages]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [getroles]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [getchannels]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [deletemessages]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [dm]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [shutdown]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [updatebot]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [leaveserver]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [get]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [reconnectgooglesheets]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [restart]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [swapsetdb]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [randomschedule]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [rs]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [getmsg]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('ping', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('error report', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('poly inquiry', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('help request', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('thanks', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('compliment', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('insult', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('greeting', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('goodbye', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('meme request', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('RICKROLL', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('AGNA!', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('KURONA:', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('PESTO', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('LOKI', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [findschedules]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [exit]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [createnapchart]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [cnexample]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [dp]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [help]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('command [ban]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [findschedules]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [showadmins]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [resetadmins]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [refreshconfig]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [resetmimics]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [resetmemes]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [setme]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [clear]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [logging]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [tail]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [stoptail]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [send]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [sendembed]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [edit]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [delete]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [deletecount]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [react]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [rreact]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [rureact]', '0');
INSERT INTO `interactions` (`type`, `count`) VALUES ('admin command [setnickname]', '0');