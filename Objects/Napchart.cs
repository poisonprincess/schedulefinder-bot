﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PolyphasicScheduleFinder_Bot
{
	/// <summary> Napchart object with all relevant information </summary>
	class Napchart
    {
        #region attributes
        /// <summary> Napchart object constructor </summary>
        internal Napchart(List<Objective> inObjectives, List<ColorTag> colorTags, string inName, string inDescription)
        {
            objectives = inObjectives;
            chartData = formatData(colorTags);
            title = inName;
            description = inDescription;
        }

        internal Napchart(Napchart inChart)
        {
            objectives = new(inChart.objectives);
            chartData = formatData(new(inChart.chartData.colorTags));
            title = new(inChart.title);
            description = new(inChart.description);
            publicLink = new(inChart.publicLink);
            chartID = new(inChart.chartID);
            customColors = new(inChart.customColors);
        }

        /// <summary> A list of groups of elements </summary>
        internal List<Objective> objectives;
        /// <summary> The Napchart's Element, ColorTag, lane count, and shape information </summary>
        internal ChartData chartData = new();
        /// <summary> Napchart's title </summary>
        internal string title;
        /// <summary> Napchart's description </summary>
        internal string description;
        /// <summary> The link to access the Napchart </summary>
        internal string publicLink;
        /// <summary> The unique identifier postfix of the Napchart link </summary>
        internal string chartID;
		#endregion

		/// <summary> Formats the ColorTag information into Element lists, Objectives and ChartData </summary>
		internal ChartData formatData(List<ColorTag> colorTags)
        {
            List<Element> timeElems = new();

            int maxLaneNum = 0;

            foreach (Objective objective in objectives)
            {
                if (objective != null)
                {
                    foreach (SleepBlock sb in objective.schedule.sleeps)
                    { timeElems.Add(new Element(sb.endTime, objective.laneNum, sb.blockText, objective.color, sb.startTime, objective.customColor)); }

                    if (objective.laneNum > maxLaneNum) maxLaneNum = objective.laneNum;
                }
            }

            if (colorTags.Exists(ct => ct == null)) colorTags.Remove(colorTags.Find(ct => ct == null));

            return new ChartData(timeElems.ToArray(), maxLaneNum + 1, colorTags.ToArray());
        }

        #region color
        /// <summary> The default and preset custom colors for napchart objectives </summary>
        internal enum Color { red, blue, brown, green, gray, yellow, purple, pink, 
            black, white, crimson, orange, cyan, indigo, lime, magenta, tan, custom };

        /// <summary> The hash values of the preset custom colors </summary>
        internal static List<(string colorName, string hex)> extraColors = new()
		{
            ("black", "#000000"),
            ("white", "#ffffff"),
            ("crimson", "#dc143c"),
            ("orange", "#ffa500"),
            ("cyan", "#00ffff"),
            ("indigo", "#4b0082"),
            ("lime", "#00ff00"),
            ("magenta", "#ff00ff"),
            ("tan", "#d2b48c")
        };
        internal List<(string colorName, string hex)> customColors;

		/// <summary> The hash values of each of the colors </summary>
		internal static Dictionary<string, string> colors = new()
		{
			{ "red", "#D02516" },
			{ "blue", "#4285F4" },
			{ "brown", "#B15911" },
			{ "green", "#34A853" },
			{ "gray", "#949494" },
			{ "yellow", "#FBBC05" },
			{ "purple", "#730B73" },
			{ "pink", "#FF94D4" },
			{ "black", "#000000" },
			{ "white", "#ffffff" },
			{ "crimson", "#dc143c" },
			{ "orange", "#ffa500" },
			{ "cyan", "#00ffff" },
			{ "indigo", "#4b0082" },
			{ "lime", "#00ff00" },
			{ "magenta", "#ff00ff" },
			{ "tan", "#d2b48c" }
		};

		/// <summary> Formatted list of available default and preset custom colors </summary>
		private static string colorOptions;

        /// <summary> Creates and returns the list of formatted color options </summary>
        internal static string getColorOptions()
        {
            bool consoleWrite = false;

            if (string.IsNullOrWhiteSpace(colorOptions))
            {
                foreach (System.Reflection.MemberInfo c in typeof(Color).GetMembers().ToList().FindAll(x => x.ToString().Contains("NapchartColor")))
                {
                    MM.Print(consoleWrite, c.ToString());
                    if (string.IsNullOrWhiteSpace(colorOptions)) colorOptions = "";
                    else colorOptions += ", ";

                    string tempC = c.ToString().Replace("NapchartColor ", "");
                    colorOptions += char.ToUpper(tempC[0]) + tempC[1..];
                }

                colorOptions += "(#colorHex)";
            }

            return colorOptions;
        }

        /// <summary> Returns the custom color's name based on a ColorTag's values </summary>
        internal static string getCustomColorName(ColorTag colorTag)
        {
            string colorName = "";

            if (colorTag != null)
            {
                colorName = colorTag.color.ToString();

                if (colorName == Color.custom.ToString())
                    colorName += $"({colorTag.customColor})";
            }

            return colorName;
        }

        /// <summary> Returns the custom color's name based on an objective's information </summary>
        internal static string getCustomColorName(Objective objective)
        {
            string colorName = "";

            if (objective != null)
            {
                colorName = objective.color.ToString();

                if (colorName == Color.custom.ToString())
                    colorName += $"({objective.customColor})";
            }

            return colorName;
        }

        /// <summary> Returns the custom color's name based on an element's information </summary>
        internal static string getCustomColorName(Element element)
        {
            string colorName = "";

            if (element != null)
            {
                colorName = element.color.ToString();

                if (colorName == Color.custom.ToString())
                    colorName += $"({element.customColor})";
            }

            return colorName;
        }

        internal static Discord.Color getDominantColor(Napchart napchart)
        {
			List<(string hex, int length)> countPerColor = new();
			foreach (Objective o in napchart.objectives)
			{
				int length = SFAlgorithm.stringToMinutesInt(o.schedule.tst != "" ? o.schedule.tst : o.schedule.getTST());

				if (!colors.TryGetValue(o.color.ToString(), out string hex))
				{
					if (!string.IsNullOrEmpty(o.customColor))
						hex = o.customColor;
				}

				if (!countPerColor.Exists(c => c.hex == hex))
					countPerColor.Add((hex, length));
				else
				{
					var prev = countPerColor.Find(c => c.hex == hex);
					countPerColor.Add((hex, prev.length + length));
					countPerColor.Remove(prev);
				}
			}

			return countPerColor.Count > 0
                ? new Discord.Color(Bot.getColorByHash(countPerColor.OrderBy(c => c.length).Last().hex[1..]))
                : Discord.Color.Default;
        }

        #endregion
    }

    /// <summary> Representation of a group of blocks of time in a Napchart representing times which an objective is occuring </summary>
    class Objective
    {
        #region attributes
        /// <summary> Constructor for a representation of a group of blocks of time in a Napchart representing times which an objective is occuring </summary>
        internal Objective(Schedule inSchedule, int inLaneNum, Napchart.Color inColor, string inCustomColor = "")
        {
            schedule = inSchedule;
            laneNum = inLaneNum;
            color = inColor;
            customColor = inCustomColor;
        }

        /// <summary> The schedule that the Objective will be representing </summary>
        internal Schedule schedule;
        /// <summary> The lane index that this objective will be on in the Napchart </summary>
        internal int laneNum;
        /// <summary> The color of the Objective (if default) </summary>
        internal Napchart.Color color;
        /// <summary> The color of the Objective (if custom) </summary>
        internal string customColor;
		#endregion
	}

	/// <summary> A Napchart's Element, ColorTag, lane count, and shape information </summary>
	class ChartData
    {
        #region attributes
        /// <summary> Constructor for a Napchart's Element, ColorTag, lane count, and shape information </summary>
        internal ChartData(Element[] inElements = null, int inLaneCount = 1, ColorTag[] inColors = null)
        {
            elements = inElements;
            lanes = inLaneCount;
            colorTags = inColors;
        }

        /// <summary> The Elements associated with the Napchart </summary>
        internal Element[] elements;
        /// <summary> The ColorTags associated with the Napchart </summary>
        internal ColorTag[] colorTags;
        /// <summary> The number of lanes in the Napchart </summary>
        internal int lanes;
        /// <summary> The Napchart's shape (defaults to Circle) </summary>
        internal string shape = "circle";
		#endregion
	}

	/// <summary> Representation of a block of time in an Objective in a Napchart </summary>
	class Element
    {
        #region attributes
        /// <summary> Constructor for the representation of a block of time in an Objective in a Napchart </summary>
        internal Element(string inEnd, int inLane, string inText, Napchart.Color inColor, string inStart, string inCustomColor = "")
        {
            end = (int)Math.Round(SFAlgorithm.stringToMinutesDouble(inEnd, 0, true) * 5);
            lane = inLane;
            text = inText;
            color = inColor;
            start = (int)Math.Round(SFAlgorithm.stringToMinutesDouble(inStart, 0, true) * 5);
            customColor = inCustomColor;
        }

        internal Element() {}

        /// <summary> The end time of the Element </summary>
        internal int end;
        /// <summary> The lane the Element will be on </summary>
        internal int lane;
        /// <summary> Any text displayed next to the Element </summary>
        internal string text;
        /// <summary> The color of the Element (if default) </summary>
        internal Napchart.Color color;
        /// <summary> The start time of the Element </summary>
        internal int start;
        /// <summary> The color of the Element (if custom) </summary>
        internal string customColor;
		#endregion
	}

	/// <summary> Representation of Color information associated with an Objective </summary>
	class ColorTag
    {
        #region attributes
        /// <summary> Constructor for the representation of Color information associated with an Objective </summary>
        internal ColorTag(string inTag, Napchart.Color inColor, string inCustomColor = "")
        {
            tag = inTag;
            color = inColor;
            customColor = inCustomColor;
        }

        internal ColorTag(ColorTag inColorTag)
        {
            tag = new(inColorTag.tag);
            color = MM.findEnum(inColorTag.color);
            customColor = new(inColorTag.customColor);
        }

        /// <summary> The label for the Color </summary>
        internal string tag;
        /// <summary> The color (if default) </summary>
        internal Napchart.Color color;
        /// <summary> The color (if custom) </summary>
        internal string customColor;
		#endregion
	}
}
