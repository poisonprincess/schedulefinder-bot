﻿using System;
using System.Collections.Generic;

namespace PolyphasicScheduleFinder_Bot
{
	/// <summary> All information required to log to a DB or Google Sheet </summary>
	class LogEntry
	{
		#region attributes
		/// <summary> Constructor for an object that holds all information required to log to a DB or Google Sheet </summary>
		public LogEntry(Server server, string tabName, string date, string time, string inputType,
			string authorId, string channelId, string messageId, string channelName, string authorName,
			string content, bool consoleWrite, string attachmentDataString = "", string embedDataString = "",
			List<LogAttachment> attachmentData = null, List<LogEmbed> embedData = null)
		{
			_server = server;
			_tabName = tabName;
			_date = date;
			_time = time;
			_inputType = inputType;
			_authorId = authorId;
			_channelId = channelId;
			_messageId = messageId;
			_channelName = channelName;
			_authorName = authorName;
			_content = content;
			_attachmentData = attachmentData;
			_attachmentDataString = attachmentDataString;
			_embedData = embedData;
			_embedDataString = embedDataString;
			_consoleWrite = consoleWrite;
		}

		/// <summary> The server the message was sent/edited/deleted on </summary>
		internal Server _server;
		/// <summary> The tab in the Logging Google Sheet to write this log entry to </summary>
		internal string _tabName;
		/// <summary> The date that the message was sent/edited/deleted </summary>
		internal string _date;
		/// <summary> The time the message was sent/edited/deleted at </summary>
		internal string _time;
		/// <summary> What type of interaction is being logged (message sent, edited, or deleted) </summary>
		internal string _inputType;
		/// <summary> The message's Author's ID </summary>
		internal string _authorId;
		/// <summary> The ID of the channel the message was in </summary>
		internal string _channelId;
		/// <summary> The Message's ID </summary>
		internal string _messageId;
		/// <summary> The name of the channel the message was in </summary>
		internal string _channelName;
		/// <summary> The message's Author's name </summary>
		internal string _authorName;
		/// <summary> Any text associated with the message </summary>
		internal string _content;
		/// <summary> Any attachment data associated with the message, in string format </summary>
		internal string _attachmentDataString;
		/// <summary> Any attachment data associated with the message, in AttachmentData format </summary>
		internal List<LogAttachment> _attachmentData;
		/// <summary> Any embed data associated with the message, in string format </summary>
		internal string _embedDataString;
		/// <summary> Any embed data associated with the message, in EmbedData format </summary>
		internal List<LogEmbed> _embedData;
		/// <summary> Whether or not to log this message to the console </summary>
		internal bool _consoleWrite;
		#endregion
	}

	/// <summary> All information required to log an Attachment associated with a message being logged </summary>
	class LogAttachment
	{
		#region attributes
		/// <summary> Constructor for the object that holds all information required to log an Attachment associated with a message being logged </summary>
		public LogAttachment(ulong id, string filename, string url, string proxyUrl)
		{
			_id = id;
			_filename = filename;
			_url = url;
			_proxyUrl = proxyUrl;
		}

		/// <summary> Attachment's ID </summary>
		internal ulong _id;
		/// <summary> FileName of Attachment </summary>
		internal string _filename;
		/// <summary> Url that the Attachment is being hosted at </summary>
		internal string _url;
		/// <summary> Proxy Url that the Attachment is being hosted at </summary>
		internal string _proxyUrl;
		#endregion
	}

	/// <summary> All information required to log an Embed associated with a message being logged </summary>
	class LogEmbed
	{
		#region attributes
		/// <summary> Constructor for the object that holds all information required to log an Embed associated with a message being logged </summary>
		public LogEmbed(int embedHash, string color, string authorName, string authorUrl,
			string authorIconUrl, string authorProxyIconUrl, string title, string description,
			List<LogEmbedField> fields, string url, string thumbnailUrl, string thumbnailProxyUrl,
			string videoUrl, string footerText, string footerIconUrl, string footerProxyUrl,
			string providerName, string providerUrl, DateTime? timestamp)
		{
			_embedHash = embedHash;
			_color = color;
			_authorName = authorName;
			_authorUrl = authorUrl;
			_authorIconUrl = authorIconUrl;
			_authorProxyIconUrl = authorProxyIconUrl;
			_title = title;
			_description = description;
			_fields = fields;
			_url = url;
			_thumbnailUrl = thumbnailUrl;
			_thumbnailProxyUrl = thumbnailProxyUrl;
			_videoUrl = videoUrl;
			_footerText = footerText;
			_footerIconUrl = footerIconUrl;
			_footerProxyUrl = footerProxyUrl;
			_providerName = providerName;
			_providerUrl = providerUrl;
			_timestamp = timestamp;
		}

		/// <summary> The hash for the Embed </summary>
		internal int _embedHash;
		/// <summary> The color of the Embed </summary>
		internal string _color;
		/// <summary> The message author's Name </summary>
		internal string _authorName;
		/// <summary> The Url of the image associated with the Embed's Author </summary>
		internal string _authorUrl;
		/// <summary> The Url of the icon of the Embed's Author </summary>
		internal string _authorIconUrl;
		/// <summary> The Proxy Url of the icon of the Embed's Author </summary>
		internal string _authorProxyIconUrl;
		/// <summary> The Title field of the embed </summary>
		internal string _title;
		/// <summary> The Description field of the embed </summary>
		internal string _description;
		/// <summary> A list of all information required to log the Embed's Fields </summary>
		internal List<LogEmbedField> _fields;
		/// <summary> The Url hyperlink associated with the Embed's Title </summary>
		internal string _url;
		/// <summary> The Url of the Embed's thumbnail </summary>
		internal string _thumbnailUrl;
		/// <summary> The Proxy Url of the Embed's thumbnail </summary>
		internal string _thumbnailProxyUrl;
		/// <summary> The Url of the Embed's Video </summary>
		internal string _videoUrl;
		/// <summary> The text in the Embed's Footer </summary>
		internal string _footerText;
		/// <summary> The Url of the Icon in the Embed's Footer </summary>
		internal string _footerIconUrl;
		/// <summary> The Proxy Url of the Icon in the Embed's Footer </summary>
		internal string _footerProxyUrl;
		/// <summary> Unsure </summary>
		internal string _providerName;
		/// <summary> Unsure </summary>
		internal string _providerUrl;
		/// <summary> Timestamp of the Embed </summary>
		internal DateTime? _timestamp;
		#endregion
	}

	/// <summary> All information required to log an an Embed's Fields associated with a message being logged </summary>
	class LogEmbedField
	{
		#region attributes
		/// <summary> Constructor for the object that holds all information required to log an an Embed's Fields associated with a message being logged </summary>
		public LogEmbedField(string name, string value, int index)
		{
			_name = name;
			_value = value;
			_index = index;
		}

		/// <summary> First parameter of the Field </summary>
		internal string _name;
		/// <summary> Second parameter of the Field </summary>
		internal string _value;
		/// <summary> Whether the field is inline with other Fields </summary>
		internal int _index;
		#endregion
	}
}
