﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PolyphasicScheduleFinder_Bot
{
	internal class MM
	{
		#region print
		public static string Method(MethodBase method) => $"{method.DeclaringType.Name}.{method.Name}";

		public static void Print(bool consoleWrite, string print, bool newLine = true, bool useMethod = false, string methodModifier = "")
		{
			if (consoleWrite)
				Debug.Write($"{(useMethod ? $"[{Method(new StackTrace().GetFrame(2).GetMethod())}]->[{Method(new StackTrace().GetFrame(1).GetMethod())}{(!string.IsNullOrWhiteSpace(methodModifier) ? $" ({methodModifier})" : "")}]: " : "")}{print}{(newLine ? "\n" : "")}");
		}

		private static List<string> _relevantFiles = null;
		public static bool isRelevantMethod(StackFrame frame)
		{
			//create _relevantFiles
			if (_relevantFiles == null)
			{
				_relevantFiles = new List<string>();
				DirectoryInfo[] di = new DirectoryInfo(@".\..\..\").GetDirectories();

				foreach (DirectoryInfo d in di)
				{
					if (d.Name.ToLower()[0] != d.Name[0])
					{
						foreach (FileInfo f in d.GetFiles())
						{
							if (f.Name.Length >= 2 && f.Name.EndsWith("cs")) _relevantFiles.Add(f.Name);
						}

						foreach (DirectoryInfo d2 in d.GetDirectories())
						{
							foreach (FileInfo f in d.GetFiles())
							{
								if (f.Name.Length >= 2 && f.Name.EndsWith("cs")) _relevantFiles.Add(f.Name);
							}
						}
					}
				}
			}

			return frame != null && frame.GetMethod().Name[0] != '.' && _relevantFiles.Exists(n => n[..n.IndexOf('.')] == frame.GetMethod().DeclaringType.Name);
		}
		#endregion

		#region misc
		internal static List<enumType> getEnums<enumType>() => Enum.GetValues(typeof(enumType)).Cast<enumType>().ToList();
		internal static enumType findEnum<enumType>(enumType toFind) => getEnums<enumType>().Find(t => t.ToString() == toFind.ToString());

		internal static bool compareDictionaries(Dictionary<string, string> dict, Dictionary<string, string> dict2, string remove = "")
		{
			bool equal = false;

			if (dict.Count == dict2.Count) // Require equal count.
			{
				equal = true;
				foreach (var pair in dict)
				{
					if (dict2.TryGetValue(pair.Key, out string value))
					{
						// Require value be equal.
						if (value.Replace(remove, "") != pair.Value.Replace(remove, ""))
						{
							equal = false;
							break;
						}
					}
					else
					{
						// Require key be present.
						equal = false;
						break;
					}
				}
			}

			return equal;
		}

		internal static string getCurrentTimeStamp() => 
			$"{(DateTime.Now.Hour	< 10 ? "0" : "") + DateTime.Now.Hour.ToString()}:" +
			$"{(DateTime.Now.Minute < 10 ? "0" : "") + DateTime.Now.Minute.ToString()}:" +
			$"{(DateTime.Now.Second < 10 ? "0" : "") + DateTime.Now.Second.ToString()}";
		#endregion
	}
}
