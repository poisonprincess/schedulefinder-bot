﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace PolyphasicScheduleFinder_Bot
{
    class Config
    {
        #region config management
        /// <summary> Retrieves values from Google Sheet Config and writes them to the variables, and calls other config update methods </summary>
        public static void setupConfig(bool consoleWrite, bool skipGSheets = false)
        {
            bool consoleWriteVars = false;
            MM.Print(consoleWrite, $"{MM.getCurrentTimeStamp()} Config      [{_configTabName}!{_configRange}]");

            if (!skipGSheets)
            {
                List<List<string>> configData = SpreadsheetManager.readCellsFromSheet(_configTabName, _configRange);

                foreach (List<string> row in configData)
                {
                    if (row != null && row.Count == 2)
                    {
                        if (row[0] == _staticAdminIDs)
                        {
                            if (ulong.TryParse(row[1], out ulong id))
                            {
                                _admins.Add(id);
                                MM.Print(consoleWriteVars, $"\tAdmin:\t\t\t\t{_admins[^1]}");
                            }
                        }
                        else if (row[0] == _blacklistedIDs)
                        {
                            if (ulong.TryParse(row[1], out ulong id))
                            {
                                _adminBlacklist.Add(id);
                                MM.Print(consoleWriteVars, $"\tBlacklisted:\t\t{_adminBlacklist[^1]}");
                            }
                        }
                        else if (row[0] == _SHIDs)
                        {
                            _possibleSHIDs.Add(row[1]);
                            MM.Print(consoleWriteVars, $"\tSchedlueHelper ID: {_possibleSHIDs[^1]}");
                        }
                        else if (row[0] == _serversForAlerts)
                        {
                            if (ulong.TryParse(row[1], out ulong id))
                            {
                                _alertServers.Add(id);
                                MM.Print(consoleWriteVars, $"\tAlert Servers:\t\t{_alertServers[^1]}");
                            }
                        }
                        else if (row[0] == _keywordsForAlerts)
                        {
                            _alertKeywords.Add(row[1]);
                            MM.Print(consoleWriteVars, $"\tAlert Words:\t\t{_alertKeywords[^1]}");
                        }
                        else if (row[0] == _SkeywordsForAlerts)
                        {
                            _SalertKeywords.Add(row[1]);
                            MM.Print(consoleWriteVars, $"\t!Alert Words:\t\t{_SalertKeywords[^1]}");
                        }
                        else if (row[0] == _keywordsForKotoAlerts)
                        {
                            _kotoAlertKeywords.Add(row[1]);
                            MM.Print(consoleWriteVars, $"\tKoto Alert Words:\t\t{_kotoAlertKeywords[^1]}");
                        }
                        else if (row[0] == _IDForKotoAlertsChannel)
                        {
                            if (ulong.TryParse(row[1].Trim(), out ulong id))
                            {
                                _kotoAlertsChannelID = id;
                                MM.Print(consoleWriteVars, $"\tKoto Alert Channel ID: {_kotoAlertsChannelID}");
                            }
                        }
                        else if (row[0] == _keywordsForElohAlerts)
                        {
                            _elohAlertKeywords.Add(row[1]);
                            MM.Print(consoleWriteVars, $"\tEloh Alert Words:\t\t{_elohAlertKeywords[^1]}");
                        }
                        else if (row[0] == _dnlServerIDs)
                        {
                            if (ulong.TryParse(row[1].Trim(), out ulong id))
                            {
                                _doNotLog.Add(id);
                                MM.Print(consoleWriteVars, $"\tDo Not Log:\t\t{_doNotLog[^1]}");
                            }
                        }
                        else if (row[0] == _adminCommandPfx)
                        {
                            _adminCommandPrefix = row[1].Trim();
                            MM.Print(consoleWriteVars, $"\tAdmin Command Prefix: {_adminCommandPrefix}");
                        }
                        else if (row[0] == _commandPfx)
                        {
                            _commandPrefix = row[1].Trim();
                            MM.Print(consoleWriteVars, $"\tCommand Prefix: {_commandPrefix}");
                        }
                        else if (row[0] == _napgodServerID && ulong.TryParse(row[1], out ulong serverID))
                        {
                            _napGodServerID = serverID;
                        }
                        else if (row[0] == _napgodBotID && ulong.TryParse(row[1], out ulong botID))
                        {
                            _napGodBotID = botID;
                        }
                        else if (_napGodCommandPrefixes.TryGetValue(row[0], out string napGodPrefix) && napGodPrefix != row[1])
                        {
                            _napGodCommandPrefixes[row[0]] = row[1];
                            MM.Print(consoleWriteVars, $"\tSpreadsheet Vars [{row[0]}]: :{_napGodCommandPrefixes[row[0]]}:");
                        }
                        else if (_scheduleFinderVars.TryGetValue(row[0], out string scheduleFinderVar) && scheduleFinderVar != row[1])
                        {
                            _scheduleFinderVars[row[0]] = row[1];
                            MM.Print(consoleWriteVars, $"\tSpreadsheet Vars [{row[0]}]: :{_scheduleFinderVars[row[0]]}:");
                        }
                        else if (_napchartInterfaceVars.TryGetValue(row[0], out string napchartInterfaceVar) && napchartInterfaceVar != row[1])
                        {
                            _napchartInterfaceVars[row[0]] = row[1];
                            MM.Print(consoleWriteVars, $"\tSpreadsheet Vars [{row[0]}]: :{_napchartInterfaceVars[row[0]]}:");
                        }
                        else if (_spreadsheetVars.TryGetValue(row[0], out string spreadsheetVar) && spreadsheetVar != row[1])
                        {
                            _spreadsheetVars[row[0]] = row[1];
                            MM.Print(consoleWriteVars, $"\tSpreadsheet Vars [{row[0]}]: :{_spreadsheetVars[row[0]]}:");
                        }
                        else if (_emojiNames.TryGetValue(row[0], out string emojiName) && emojiName != row[1])
                        {
                            _emojiNames[row[0]] = row[1];
                            MM.Print(consoleWriteVars, $"\tEmoji Names: :{_emojiNames[row[0]]}:");
                        }
                    }
                }
            }

            NapGodText.updateText();
            Schedule.updateScheduleVars();
            Bot.updateScheduleDBVars();
            Bot._ac.updateConfigVars();
            Bot._uc.updateConfigVars();
            RoleManager.updateConfigVars();
            NapGodCommand.updateConfigVars();
            ScheduleFinder.updateScheduleFinderConfigVars();
            NapchartInterface.updateNapchartConfigVars();

            if (Bot._testBot)
            {
                _napGodServerID = 879179596703096913; //Poison's Test Server
                _napGodBotID = 322749155313319939; // Poison
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Biphasic)._id = 883394480097624065;
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Dual_Core)._id = 883394184537571329;
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Everyman)._id = 883394222999371817;
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Experimental)._id = 883394723304333325;
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Monophasic)._id = 883394681352900628;
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Nap_Only)._id = 883394262455189524;
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Tri_Core)._id = 883394321334812713;
                _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Random)._id = 883394515128438855;
                _napGodSchedules.Find(s => s._nameClean == "Bimaxion")._role._id = 883344816954568745;
                _napGodSchedules.Find(s => s._nameClean == "E3")._role._id = 884895195454377985;
            }

            MM.Print(consoleWrite, $"{MM.getCurrentTimeStamp()} Config      Configured");
        }

        /// <summary> Resets config variables and re-reads them from the Google Sheet Config </summary>
        public static void resetConfig(bool refreshScheduleDB)
        {
            _spreadsheetVars = new()
            {
                { "Max Date cell", "A71" },
                { "Date Formula cells", "B73:C73" },
                { "Interaction area", "A4:B45" },
                { "Message area", "E2:M2" },
                { "ScheduleDB tab name", "ScheduleDB" },
                { "ScheduleDB area", "$A$3:$AV" }
            };

            _dbSwap = new();
            _testDB = "test821806294108602379";
            _admins = new List<ulong>();
            _adminBlacklist = new List<ulong>();
            _possibleSHIDs = new List<string>();
            _adminCommandPrefix = "=";
            _commandPrefix = "-";

            _testingServers = new List<ulong>
            {
                804338250202218496, /*B R U H*/
                879179596703096913, /*Poison's Test Server*/
                417043325451894787 /*PS Testing*/
		    };

            _emojiNames = new()
            {
                { "Blob Thumbs-Up", "BlobThumbsUp" },
                { "Blob uwu", "BlobUwU" },
                { "Blob Insomnia", "blobSomnia" },
                { "Blob Banhammering", "blobBanHammering" },
                { "Blob Dramatic", "BlobDunDunDun" },
                { "Blob Unamused", "blobUnamused" },
                { "Blob Eye Roll", "blobEyeroll" },

                { "Concerned", "ohGosh" },
                { "Hell Yeah", "hellYeah" },
                { "Arch", "arch" },
                { "Crispy Crimson", "crispycrimson" },
                { "Pesto", "pesto" },
                { "Garlic Bread", "wombogarlicbread" },

                { "Up Rainbow", "RainbowUp" },
                { "Alert", "alert" },
                { "Question", "question" },
                { "Discord", "discord" },
                { "NapGod", "napgod" },
                { "Nitro", "nitro" },
                { "Loading", "loading_Discord" },
                { "Tag" , "Tag" },

                { "Channel", "channel" },
                { "News", "channel_News" },
                { "Rules", "channel_Rules" },
                { "Thread", "channel_Thread" },
                { "Voice", "channel_Voice" },
                { "Locked Channel", "channel_Locked" },

                { "Owner", "role_Owner" },
                { "Mod", "role_Mod" },
                { "Member", "role_Members" },
                { "Bot", "role_Bot" },

                { "Online", "status_online" },
                { "Away", "status_away" },
                { "DND", "status_dnd" },
                { "Offline", "status_offline" },

                { "Yes", "selection_Yes" },
                { "Empty", "selection_None" },
                { "No", "selection_No" }
            };

            _alertServers = new List<ulong>();
            _alertKeywords = new List<string>();
            _SalertKeywords = new List<string>();
            _kotoAlertKeywords = new List<string>();
            _kotoAlertsChannelID = 1057699903247241327;
            _elohAlertKeywords = new List<string>();
            _doNotLog = new List<ulong> { 473338327810965514 };

            _scheduleFinderVars = new()
            {
                { "Minimum Age", "13" },
                { "Maximum Age", "100" },
                { "Minimum Mono", "13" },
                { "Maximum Mono", "100" },
                { "Default Mono", "8" },
                { "Max Schedule Name Length", "19" }
            };

            _napchartInterfaceVars = new()
            {
                { "Napchart API Endpoint", "https://api.napchart.com/v1/" },
                { "Create Snapshot", "createSnapshot" },
                { "Get Image", "getImage" },
                { "Get Chart", "getChart" },
                { "Napchart Regex", @"^{ { \w|\d }{5,6} }$&^snapshot\/{ { \w|\d }{9} }$&^\w{6,100}\/.*\-\w{6,9}$" }
            };

            _napGodServerID = 249219704655183876;
            _napGodBotID = 791661693910384690;

            _napGodCommandPrefixes = new()
            {
                { "NapGod Command Prefix", "+" },
                { "NapGod Admin Command Prefix", "|" },
                { "NapGod Help Command Prefix", "!" }
            };

            if (refreshScheduleDB)
            {
                SFAlgorithm._scheduleDB = new List<Schedule>();
                Bot.populateScheduleDB(Bot._printScheduleInfo);
            }
        }
        #endregion

        #region spreadsheet constants
        /// <summary> Name of the tab that has the Config information </summary>
        const string _configTabName = "Config";
        /// <summary> Range that holds the Config info </summary>
        const string _configRange = "A:B";


        /// <summary> String to look for associated with the Admin Command Prefix </summary>
        const string _adminCommandPfx = "Admin Command Prefix";
        /// <summary> String to look for associated with the Command Prefix </summary>
        const string _commandPfx = "Command Prefix";
        /// <summary> String to look for associated with Static Admin IDs </summary>
        const string _staticAdminIDs = "Static Admin IDs";
        /// <summary> String to look for associated with Blacklisted IDs </summary>
        const string _blacklistedIDs = "Blacklisted IDs";
        /// <summary> String to look for associated with Possible ScheduleHelper IDs </summary>
        const string _SHIDs = "Possible SH IDs";
        /// <summary> String to look for associated with Servers to watch for and send Alerts about </summary>
        const string _serversForAlerts = "Alert Servers";
        /// <summary> String to look for associated with Keywords to send Alerts about </summary>
        const string _keywordsForAlerts = "Keywords";
        /// <summary> String to look for associated with Keywords to send urgent Alerts about </summary>
        const string _SkeywordsForAlerts = "Keywords!";
        /// <summary> String to look for associated with Keywords to send Alerts to Koto about </summary>
        const string _keywordsForKotoAlerts = "Koto Keywords";
        /// <summary> String to look for associated with ID of channel to send alerts to Koto in </summary>
        const string _IDForKotoAlertsChannel = "Koto Alert Channel";
        /// <summary> String to look for associated with Keywords to send Alerts to Eloh about </summary>
        const string _keywordsForElohAlerts = "Eloh Keywords";
        /// <summary> String to look for associated with IDs of servers that shouldn't be logged </summary>
        const string _dnlServerIDs = "DNL Server";
        /// <summary> String to look for associated with the NapGod server ID </summary>
        const string _napgodServerID = "NapGod Server ID";
        /// <summary> String to look for associated with the NapGod Bot ID </summary>
        const string _napgodBotID = "NapGod Bot ID";
        #endregion

        #region config variables
        /// <summary> Determines whether to use Google Sheets to log info or MySQL </summary>
        internal static bool _LoggingWithGSheets = false;

        /// <summary> Spreadsheet Variables </summary>
        internal static Dictionary<string, string> _spreadsheetVars = new()
        {
            { "Max Date cell", "A71" },
            { "Date Formula cells", "B73:C73" },
            { "Interaction area", "A4:B45" },
            { "Message area", "E2:M2" },
            { "ScheduleDB tab name", "ScheduleDB" },
            { "ScheduleDB area", "$A$3:$AV" }
        };

        internal static List<(ulong serverID, string newDBName)> _dbSwap = new();
        internal static string _testDB = "test821806294108602379";
        internal static List<ulong> _testingServers = new()
        {
            804338250202218496, /*B R U H*/
            879179596703096913, /*Poison's Test Server*/
            417043325451894787, /*PS Testing*/
            1085660044374315189 /*Polyphasic Testing*/
		};
        /// <summary> List of Static Admin User IDs for tracking perms </summary>
        internal static List<ulong> _admins = new();
        /// <summary> List of Blacklisted User IDs to prevent admin command usage </summary>
        internal static List<ulong> _adminBlacklist = new();
        /// <summary> Possible SH IDs to catch any uncaught mentions </summary>
        internal static List<string> _possibleSHIDs = new();
        /// <summary> Prefix for admin commands </summary>
        internal static string _adminCommandPrefix = "=";
        /// <summary> Prefix for commands </summary>
        internal static string _commandPrefix = "-";

        /// <summary> Emoji Names </summary>
        internal static Dictionary<string, string> _emojiNames = new()
        {
            { "Blob Thumbs-Up", "BlobThumbsUp" },
            { "Blob uwu", "BlobUwU" },
            { "Blob Insomnia", "blobSomnia" },
            { "Blob Banhammering", "blobBanHammering" },
            { "Blob Dramatic", "BlobDunDunDun" },
            { "Blob Unamused", "blobUnamused" },
            { "Blob Eye Roll", "blobEyeroll" },

            { "Concerned", "ohGosh" },
            { "Hell Yeah", "hellYeah" },
            { "Arch", "arch" },
            { "Crispy Crimson", "crispycrimson" },
            { "Pesto", "pesto" },
            { "Garlic Bread", "wombogarlicbread" },

            { "Up Rainbow", "RainbowUp" },
            { "Alert", "alert" },
            { "Question", "question" },
            { "Discord", "site_discord" },
            { "NapGod", "napgod" },
            { "Nitro", "nitro" },
            { "Loading", "loading_Discord" },
            { "Tag" , "Tag" },

            { "Channel", "channel" },
            { "News", "channel_News" },
            { "Rules", "channel_Rules" },
            { "Thread", "channel_Thread" },
            { "Voice", "channel_Voice" },
            { "Locked Channel", "channel_Locked" },

            { "Owner", "role_Owner" },
            { "Mod", "role_Mod" },
            { "Member", "role_Members" },
            { "Bot", "role_Bot" },

            { "Online", "status_online" },
            { "Away", "status_away" },
            { "DND", "status_dnd" },
            { "Offline", "status_offline" },

            { "Yes", "selection_Yes" },
            { "Empty", "selection_None" },
            { "No", "selection_No" }
        };
        internal static Regex _emojiString = new(@"<:[A-z_]{2,32}:[0-9]{18,19}>");
        internal static Regex _userString  = new(@"<:@[0-9]{18,19}>");

        internal const string _blank = "‎";

        /// <summary> Servers for Alerts </summary>
        internal static List<ulong> _alertServers = new();
        /// <summary> Keywords for Alerts </summary>
        internal static List<string> _alertKeywords = new();
        /// <summary> Serious Keywords for Alerts </summary>
        internal static List<string> _SalertKeywords = new();
        /// <summary> Keywords for Koto Alerts </summary>
        internal static List<string> _kotoAlertKeywords = new();
        /// <summary> Servers that should not be logged </summary>
        internal static ulong _kotoAlertsChannelID = 1057699903247241327;
        /// <summary> Keywords for Eloh Alerts </summary>
        internal static List<string> _elohAlertKeywords = new();
        /// <summary> Servers that should not be logged </summary>
        internal static List<ulong> _doNotLog = new() { 473338327810965514 };

        /// <summary> List of variables associated with ScheduleFinding </summary>
        internal static Dictionary<string, string> _scheduleFinderVars = new()
        {
            { "Minimum Age", "13" },
            { "Maximum Age", "100" },
            { "Minimum Mono", "13" },
            { "Maximum Mono", "100" },
            { "Default Mono", "8" },
            { "Max Schedule Name Length", "19" }
        };

        /// <summary> List of variables associated with Napchart Interfacing </summary>
        internal static Dictionary<string, string> _napchartInterfaceVars = new()
        {
            { "Napchart API Endpoint", "https://api.napchart.com/v1/" },
            { "Create Snapshot", "createSnapshot" },
            { "Get Image", "getImage" },
            { "Get Chart", "getChart" },
            { "Napchart Regex", @"^{ { \w|\d }{5,6} }$&^snapshot\/{ { \w|\d }{9} }$&^\w{6,100}\/.*\-\w{6,9}$" }
        };

        internal static Dictionary<ulong, List<ulong>> _roleDividers = new() 
        {
            { 249219704655183876,  new() { 0, 0, 0 } },
            { 879179596703096913,  new() { 1068625696081059846, 1068624662252232804, 1068622682570104923 } },
            { 1085660044374315189, new() { 1085667051370184755, 1085667081212678204, 1085667163764953138 } }
        };

        /// <summary> NapGod Polyphasic Sleeping server's ID </summary>
        internal static ulong _napGodServerID = 249219704655183876;
        /// <summary> NapGod Bot's ID </summary>
        internal static ulong _napGodBotID = 791661693910384690;
		/// <summary> #adaptation_logs channel ID </summary>
		internal static ulong _napGodLogsChannelID = 279277678375469056;
		/// <summary> Crimson's ID </summary>
		internal static ulong _crimsonUserID = 380207783171194882;
        internal static Dictionary<ulong, string> _channelAccessLink = new()
        {
            { 249219704655183876,  "" },
			{ 879179596703096913,  "https://discord.com/channels/879179596703096913/1083614929451487343/1083615053854543883" },
			{ 1085660044374315189, "https://discord.com/channels/1085660044374315189/1085751484567801877/1085753464199585834" }
		};
		internal static Dictionary<ulong, string> _notificationsLink = new()
		{
			{ 249219704655183876,  "" },
			{ 879179596703096913,  "https://discord.com/channels/879179596703096913/1083614929451487343/1083615085982920705" },
			{ 1085660044374315189, "https://discord.com/channels/1085660044374315189/1085751484567801877/1085754180989370428" }
		};
		/// <summary> NapGod Bot's Help Command Prefix </summary>
		internal static Dictionary<string, string> _napGodCommandPrefixes = new()
		{
			{ "NapGod Command Prefix", "+" },
			{ "NapGod Admin Command Prefix", "|" },
			{ "NapGod Help Command Prefix", "!" }
		};
        /// <summary> Roles for SH to assign </summary>
        internal static List<NapGodRole> _napGodRoles = new()
		{
            new NapGodRole(NapGodSchedule.Category.Biphasic,        "Biphasic",     255100745643327491),
            new NapGodRole(NapGodSchedule.Category.Everyman,        "Everyman",     255092679946403841),
            new NapGodRole(NapGodSchedule.Category.Dual_Core,       "Dual Core",    255092956917137410),
            new NapGodRole(NapGodSchedule.Category.Tri_Core,        "Tri Core",     327006805987164161),
            new NapGodRole(NapGodSchedule.Category.Monophasic,      "Monophasic",   320351910173474818),
            new NapGodRole(NapGodSchedule.Category.Nap_Only,        "Nap Only",     255093149171449856),
            new NapGodRole(NapGodSchedule.Category.Experimental,    "Experimental", 362774904959008788),
            new NapGodRole(NapGodSchedule.Category.Random,          "Random",       409016823019995147)
        };
        /// <summary> Schedules for SH to use with the +set command </summary>
        internal static List<NapGodSchedule> _napGodSchedules = new()
		{
            new NapGodSchedule("cama;camayl",                    "CAMAYL",       new NapGodRole(NapGodSchedule.Category.Experimental, "CAMAYL",       0),                  _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Experimental)),
            new NapGodSchedule("e1;biphasic;everyman1",          "E1",           new NapGodRole(NapGodSchedule.Category.Biphasic,     "E1",           336522416836575242), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Biphasic)),
            new NapGodSchedule("segmented",                      "Segmented",    new NapGodRole(NapGodSchedule.Category.Biphasic,     "Segmented",    336522434364702721), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Biphasic)),
            new NapGodSchedule("siesta",                         "Siesta",       new NapGodRole(NapGodSchedule.Category.Biphasic,     "Siesta",       336522432930119681), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Biphasic)),
            new NapGodSchedule("bix;biphasicx",                  "Biphasic-X",   new NapGodRole(NapGodSchedule.Category.Biphasic,     "Biphasic-X",   701689185844723733), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Biphasic)),
            new NapGodSchedule("e2;everyman2",                   "E2",           new NapGodRole(NapGodSchedule.Category.Everyman,     "E2",           336522424419876864), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Everyman)),
            new NapGodSchedule("e3;everyman3",                   "E3",           new NapGodRole(NapGodSchedule.Category.Everyman,     "E3",           336522426026557440), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Everyman)),
            new NapGodSchedule("e4;everyman4",                   "E4",           new NapGodRole(NapGodSchedule.Category.Everyman,     "E4",           336522427280523267), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Everyman)),
            new NapGodSchedule("e5;everyman5",                   "E5",           new NapGodRole(NapGodSchedule.Category.Everyman,     "E5",           336522429113565185), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Everyman)),
            new NapGodSchedule("seva;sevamayl",                  "SEVAMAYL",     new NapGodRole(NapGodSchedule.Category.Everyman,     "SEVAMAYL",     400975678905778178), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Everyman)),
            new NapGodSchedule("trimax;trimaxion",               "Trimaxion",    new NapGodRole(NapGodSchedule.Category.Everyman,     "Trimaxion",    336522441323053057), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Everyman)),
            new NapGodSchedule("bimax;bimaxion",                 "Bimaxion",     new NapGodRole(NapGodSchedule.Category.Dual_Core,    "Bimaxion",     336522430887755776), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Dual_Core)),
            new NapGodSchedule("dc1;dualcore1",                  "DC1",          new NapGodRole(NapGodSchedule.Category.Dual_Core,    "DC1",          335601225623928834), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Dual_Core)),
            new NapGodSchedule("dc2;dualcore2",                  "DC2",          new NapGodRole(NapGodSchedule.Category.Dual_Core,    "DC2",          336522418468421632), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Dual_Core)),
            new NapGodSchedule("dc3;dualcore3",                  "DC3",          new NapGodRole(NapGodSchedule.Category.Dual_Core,    "DC3",          336522420141686784), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Dual_Core)),
            new NapGodSchedule("dc4;dualcore4",                  "DC4",          new NapGodRole(NapGodSchedule.Category.Dual_Core,    "DC4",          336522421366423556), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Dual_Core)),
            new NapGodSchedule("duca;ducamayl",                  "DUCAMAYL",     new NapGodRole(NapGodSchedule.Category.Dual_Core,    "DUCAMAYL",     777644582229901343), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Dual_Core)),
            new NapGodSchedule("tc1;tricore1",                   "TC1",          new NapGodRole(NapGodSchedule.Category.Tri_Core,     "TC1",          336522437992906753), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Tri_Core)),
            new NapGodSchedule("tc2;tricore2",                   "TC2",          new NapGodRole(NapGodSchedule.Category.Tri_Core,     "TC2",          336880489451487232), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Tri_Core)),
            new NapGodSchedule("triphasic;tc0;tricore;tricore0", "Triphasic",    new NapGodRole(NapGodSchedule.Category.Tri_Core,     "Triphasic",    336522442640064512), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Tri_Core)),
            new NapGodSchedule("mono;monophasic",                "Mono",         new NapGodRole(NapGodSchedule.Category.Monophasic,   "Monophasic",   0),                  _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Monophasic)),
            new NapGodSchedule("dymax;dymaxion",                 "Dymaxion",     new NapGodRole(NapGodSchedule.Category.Nap_Only,     "Dymaxion",     336522423040081920), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Nap_Only)),
            new NapGodSchedule("naptation",                      "Naptation",    new NapGodRole(NapGodSchedule.Category.Nap_Only,     "Naptation",    0),                  _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Nap_Only)),
            new NapGodSchedule("spamayl;spam",                   "SPAMAYL",      new NapGodRole(NapGodSchedule.Category.Nap_Only,     "SPAMAYL",      336522436222779393), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Nap_Only)),
            new NapGodSchedule("tesla;u4",                       "Tesla",        new NapGodRole(NapGodSchedule.Category.Nap_Only,     "Tesla",        336522439611645952), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Nap_Only)),
            new NapGodSchedule("uberman;u6",                     "Uberman",      new NapGodRole(NapGodSchedule.Category.Nap_Only,     "Uberman",      336522443407753219), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Nap_Only)),
            new NapGodSchedule("qc0;quadcore;quadcore0;qc",      "QC0",          new NapGodRole(NapGodSchedule.Category.Experimental, "QC0",          701689291201445969), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Experimental)),
            new NapGodSchedule("random",                         "Random",       new NapGodRole(NapGodSchedule.Category.Random,       "Random",       465865767406272522), _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Random)),
            new NapGodSchedule("experimental",                   "Experimental", new NapGodRole(NapGodSchedule.Category.Experimental, "Experimental", 0),                  _napGodRoles.Find(r => r._category == NapGodSchedule.Category.Experimental))
        };
        #endregion
    }

    internal class NapGodText
    {
        internal enum NapGodCommandType { Admin, Help, User };
        internal enum Category { Category, GenPoly, Scheduling, Adaptation, Challenges, SScience, Hidden };
        internal static List<(string commandName, Category category, string text)> _help = new();
        internal static List<(string commandName, NapGodCommandType type, string text)> _commands = new();
        internal static List<(string nameLookup, NapGodSchedule.Category scheduleCategory, string description, string napchartLink, bool show)> _schedules = new();

        internal static void updateText()
        {
			#region help
			_help = new List<(string commandName, Category category, string text)>
            {
                ("generalpolyphasicinfo", Category.Category, @"|__**General Polyphasic Info Commands**__

The following commands are intended to provide generalized information about polyphasic sleeping.

```
!beforeyoustart  !jumpintoschedule 
!ns              !oss 
!polyprep        !products 
!recovery        !underage ```"),
                ("scheduling",            Category.Category, @"|__**Scheduling Commands**__

The following commands are intended to provide a more focused look at the process of schedule selection and personalization.

```
!bestschedule    !dayblock 
!dualcore        !everyman 
!graveyard       !latecore 
!naplength       !nightshift ```"),
                ("adaptationinfo",        Category.Category, @"|__**Adaptation Info Commands**__

The following commands are intended to describe the process of adaptation, from beginning to end.

```
!adaptation      !adapt-methods 
!adapt-success   !alarms 
!discipline      !drugs 
!eating          !exercise 
!flexing         !howtobegin ```"),
                ("adaptationchallenges",  Category.Category, @"|__**Adaptation Challenges Commands**__

The following commands are intended to describe the difficulties you may face during adaptation, and provide potential solutions for them.

```
!adhd            !dst 
!cantadapt       !eyestrain 
!fallasleep      !incompatible 
!napspot         !prematurewake 
!sickness        !skipnap 
!sleepdebt       !stayawake 
!travel          !zombiemode ```"),
                ("sleepscience",          Category.Category, @"|__**Sleep Science Commands**__

The following commands are intended to provide an overview of the science behind sleep.

```
!circadian       !compression 
!darkperiod      !experiments 
!genetics        !melatonin 
!memory          !microsleep 
!napaware        !repartition 
!secondwind      !sleepstages 
!sleeptracker    !smartalarm 
!tankphasic ```"),

                ("adaptation",      Category.Adaptation, @"__**The Stages of Adaptation**__

**Stage 1:**
Falling asleep and waking up are easy in each sleep (core sleep and naps) because sleep deprivation hasn't kicked in. Sleep quality is the same as in mono sleep. Sleep stages haven't compressed. Sleep cycle is still the same (typically 90m one cycle).

**Stage 2:**
Tiredness is present some of the day. Waking up becomes harder as total sleep is reduced. Feeling of sleepiness becomes more apparent, and sleep debts accumulate. 

**Stage 3:**
This is where the screwups begin. Sleep debts accumulate enough to often cause an oversleep. It's important to maximize your alarms to prevent this before entering this stage (see `!alarms`). If and how much you oversleep depends on how much sleep you get in your schedule: the shorter it is, the more likely you are to oversleep. This is also the stage where many start to remember dreams in their nap(s)/core(s). Falling asleep becomes very hard for some sleeps and very easy for others, but waking up from those sleeps is almost always much harder. Repartitioning has begun. Your sleep cycle can be reduced with the reduction of light sleep. 

**Stage 4:**
If you bypass stage 3 (by oversleeping only minimally/not at all) you enter stage 4. There's a clear difference between stage 3 and 4, where stage 4 feels more like stage 2. During this stage, your sleep deprivation symptoms will get progressively better. As time progresses, you may start to wake naturally before your alarm from time to time, have more vivid dreams, and falling asleep won't be an issue any longer. Waking up also becomes very easy, and you will start to feel alert in the day. When nap times come, sleepiness sharply rises at that moment. When you wake up from nap, you feel fresh and rested until the next sleep. When you meet all `!adapt-success` signs you've adapted!"),
                ("adaptmethods",    Category.Adaptation, @"__**Adaptation Methods**__

**Cold turkey:**
Jump straight from mono into your desired schedule.

**Naptation:**
Stay awake for around 36h, then start napping for 20m (adapting to nap-only schedules like Uberman), to practice napping.

**Gradual adaptation:**
Start with easy schedules, then move to harder ones by cutting down total sleep after each adaptation."),
                ("adaptsuccess",    Category.Adaptation, @"__**Have You Adapted?**__

**Signs to recognize when you have adapted to a schedule, in order of importance:**

**1.** Feel energized and productive when awake.

**2.** No memory loss or microsleeps.

**3.** Wake up feeling refreshed from both naps and cores; no sleep inertia.

**4.** Fall asleep quickly in all sleeps, even if you don't prepare some time beforehand to sleep. 


**The following are not necessary to be adapted, but are common in polyphasic schedule adaptations:**

**5.** Elevation in mood and good appetite.

**6.** Wake naturally without the need for alarms. This requires long entrainment with a sleep cycle, often months for it to be consistent.

**7.** Remember more dreams from your sleep, and have more vivid dreams. Dreams occur most often from the end of your core sleep, but it's possible to dream shortly after you fall asleep as well. (**Disclaimer:** Some people remember fewer dreams due to consistently waking from light sleep as an adaptation to rigid sleep times.)"),
                ("adhd",            Category.Challenges, @"__**ADHD and Polyphasic Sleep**__

In a small number of cases on this server, people with ADHD have been documented to be unable to adapt to polyphasic schedules with a sleep total below 6-7h. There are [proven](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2276739/#:~:text=Sleep%20problems%20are%20frequently%20associated,not%20consistent%20over%20all%20studies) effects of ADHD on sleep, which include reduced REM gain (difficulty meeting REM need), increased nocturnal movements (potentially causing wakes during sleep), sleep onset issues, and fragmented (low quality) vital stages. In this community there is mounting evidence that people with ADHD are more prone to microsleeping and have an inability to reduce their light sleep totals (typically below around 50%), regardless of their schedule's sleep total. As a result, the process of repartitioning removing light sleep to make room for vital sleep stages (the fundamental concept that makes polyphasic sleep possible) cannot occur. 
Stimulant medications may complicate adaptation progress as well, but some sleepers are able to sleep during their sleeps regardless. 

If you have ADHD, it is recommended to try Dual-Core schedules (for optimizing the inefficient REM gain) with higher sleep totals (to accomodate your possible light sleep need) to increase your chance to adapt."),
                ("alarms",          Category.Adaptation, @"__**Alarm Optimization**__

**1.** Have multiple devices with multiple alarms go off at 0, 1, 2, 5, 10, 15, 20m after wake. Have the first alarm be some upbeat music that makes sure you wake up with the feeling of being able to do anything! 

**2.** Take advantage of location-based alarms such as barcode alarms. For example, you could place a code in the fridge (putting you in a position to easily drink a glass of cold water), or in the shower (to encourage a quick wakeup shower). In any case, forcing yourself to walk around before the alarm can be turned off is helpful for avoiding zombie mode oversleeps (see `!zombiemode`).

**3.** Turn off the alarms after they each go off, don't turn them off all at once. 

**4.** It's imperative to use more than one device, since if that device fails, you likely won't wake up. 

**5.** Keep each alarm device away from your bed so that you are required to get out of bed to turn it off.


Popular app options for alarms are ""Alarmy"", and ""Can't wake up"". Ask about what different people use in |#sleep_tech#| if you want something special. If you have a person that could wake you, make a backup alarm that's loud enough for them to hear, and make sure they know to wake you no matter what."),
                ("alcohol",         Category.Hidden, @"__**Alcohol and Sleep**__

Alcohol consumption leads to changes in the balance between REM and SWS, and can mess with cycle lengths. Higher consumption wrecks sleep quality and makes you feel groggy after waking.

Moderate alcohol consumption with some time before sleep shouldn't cause issues. However, it's best avoided during adaptation completely."),     //drugs
                ("beforeyoustart",  Category.GenPoly, @"__**Before You Start...**__

**Before attempting a polyphasic lifestyle and selecting a schedule, please make sure you think through the following:**

**1.** Are you 18 or younger? If you are, it's still possible to be polyphasic, but it's important not to reduce sleep below 7h total. For more info, see `!underage`.

**2.** Do you have any sleep disorders? If you have insomnia, polyphasic sleep will likely help you. If you have other severe disorders like narcolepsy, sleep apnea, or something else, consult a physician about your conditions first before becoming polyphasic. 

**3.** Do you have any physical illnesses? If you have chronic diseases like diabetes, again, consult your physician. 

**4.** Can you be polyphasic long term? Look several months ahead in your work/study schedule, and your future life. Adaptation to a sleep schedule usually takes 1-2 months, if you're strict. So if you only have a month and a half (for example) it would be wise to choose not to attempt polyphasic sleep at all, because you'd have little time to enjoy the period of success before having to change your schedule (if you're able to adapt in that time at all). It's typically not worth it to spend so much effort and time suffering the reduced productivity that occurs during adaptation if you can't stay on the schedule for very long."),
                ("bestschedule",    Category.Scheduling, @"__**Best Polyphasic Schedule?**__

This depends entirely on your own needs. Important considerations are your other appointments and obligations, as well as age, exercise amount, and if you wake up easily during the night.
You can look at the schedules on [our website](https://www.polyphasic.net/polyphasic-sleep-schedules/), or in |#botspam#| using the `+schedules` command.
For schedule recommendations, try ScheduleHelper's `-FindSchedules` command in |#botspam#|, and ask for assistance in |#beginners#|. One of our knowledgeable advisors or experienced members will assist you."),
                ("caffeine",        Category.Hidden, @"__**Caffeine and Sleep**__

Caffeine may help you feel better in the short-term, but you'll feel worse after the effects of the caffeine fade out. Caffeine prevents or limits entry into deeper vital sleep stages, effectively replacing valuable REM and SWS with light sleep. This means that that even if you were able to fall asleep, your sleep quality would suffer substantially. Caffeine's half-life (the time it takes for the caffeine amount in your body to be halved) is on average 6.5h, meaning that it takes about 12-18h to be mostly removed.

Caffiene should be completely avoided on all schedules, with the exception of Segmented due to the exceptionally long wake gap during the day. If you monitor your caffeine levels (the [CoffeeCalc tool](https://coffeecalc.cc) is helpful for this), you can find the maximum amount that you may consume without affecting your sleep. Generally it's okay to consume a moderate amount of caffeine in the morning, but best to avoid it after noon."),     //drugs
                ("cannabis",        Category.Hidden, @"__**Cannabis and Sleep**__

A very limited amount of research has been done with regards to cannabis affecting sleep. Research and anecdotal evidence points to cannabis reducing the amount of REM sleep and increasing SWS, possibly also increasing its daily need. In addition, many users report sleepiness as a side effect.

Cannabis should be completely avoided during adaptation, and used with moderation once adapted."),     //drugs
                ("cantadapt",       Category.Challenges, @"__**When Adaptation Never Happens**__

Sometimes, polyphasic sleepers have trouble adapting to a schedule and may not be quite certain why. 


**There are a few common reasons for why this may occur:**

**1.** Oversleeping is typically the main culprit for an endless adaptation. If you frequently oversleep, your body can never achieve the sleep repartitioning required to adapt (see `!repartition` and `!oss`). Recovery is usally the best course of action in these situations (see `!recovery`).

**2.** Inconsistency is another common reason you may not be able to adapt. Discipline and consistency are extremely important for adapting, as skipping or flexing sleeps prevents your body from learning the schedule, which is vital for repartitioning (see `!discipline`, `!skipnap`, and `!flexing`). In addition, it's important to have a consistent dark period every day so that your circadian rhythm is stable (an unstable circadian rhythm has many negative consequences, some of which can impact your ability to adapt).

**3.** Overwhelming sleep debt upon beginning a schedule can result in a failure to adapt, as you will be more prone to oversleeps due to your body's increased need for vitals. This is why recovery is important before beginning a schedule.

**4.** Excessive microsleeps may prevent you from adapting as well. If you microsleep more than a few times a day, your body may be preventing itself from reaching the sleep debt threshold (see `!sleepdebt`) required for repartitioning to occur, since microsleeps can alleviate small amounts of sleep deprivation. Cumulatively, this can result in a perpetual stage 2 or 3. See `!microsleep` for more information.


See `!incompatible` to learn about cases where you can't adapt because your schedule simply isn't right for you."),
                ("circadian",       Category.SScience, @"__**Circadian Rhythm**__

Your circadian rhythm is maintained and shifted by blue light and food intake (and to a lesser extent, temperature and physical activity). To keep your circadian intact, it's wise to keep a 6-12h (preferably 8-10h) dark period (with only red light allowed, see `!darkperiod`) coinciding with an 8-12 hour fasting period. Both the dark period and fasting should start 2h prior to the core. Not doing so has a highly negative influence on your core's sleep quality, and leads to elevated risks of diabetes, cancer, and cardiovascular diseases. 

The length of the dark period can directly affect the amount of SWS in the core(s), which means minor tweaking of the length of the dark period can be benefitial in order to get high amounts of quality sleep. The effects are easiest seen with a sleep tracker (see `!sleeptracker`). 

If your core doesn't start within the hours of 20:00-24:00, it is wise to have the dark period be slightly cooler than the rest of the day. In this case, getting a daylight lamp to use outside of the dark period is also recommended.

Try to limit physical activities that raise your heart rate during your dark period."),
                ("compression",     Category.SScience, @"__**Sleep Cycle Compression**__

Sleep cycle compression is mostly observed in Dual-Core schedules. Segmented creates a gap between the two cores which allows both the SWS peak (typically 21:00-24:00) and REM peak (typically 06:00-09:00) to be overlapped during sleep allowing for the removal of a sleep cycle. This causes the sleep cycles to increase in length (105m). DC1 removes one REM-heavy cycle and adds one 20m nap, and the cycles compress some (100m). DC2 adds another nap, and compresses the cycles down further. (80m)

All schedules except DC1 and Segmented should first be scheduled to have 90m cycles from the start. If you start to regularly naturally wake before your alarm after the third week of adaptation, you can assume the cycles have compressed. In that case the alarms can be altered to accommodate this."),
                ("darkperiod",      Category.SScience, @"__**Dark Period**__

Melatonin is essential for entering SWS, however its production is delayed by all light wavelengths except red. Because of this, all non-red light should be completely blocked off during the dark period, which should start 2h prior to the core (first core on DC schedules, night core on Tri-Core) and continue for 6-12h (preferably 8-10h) after that. 
Time spent sleeping is included as part of the dark period. 
The dark period should continue even after your wake time (if you have a short core) to ensure the stability of the circadian rhythm. 
The light blocking can be done by either wearing blue-green light blocking glasses or by using programs such as F.lux, sunsetscreen, etc. on a low kelvin setting (around 1000K), though these programs do not entirely block out the inherent white backlight of screens, which means that they are not nearly as effective as the previously mentioned glasses. Installing red LED bulbs is also an option if you're avoiding screens or cannot use/buy DP glasses and are using one of the previously mentioned programs instead.

It is important to avoid excessive exercise, extreme temperatures, and consuming anything xenobiotic (essentially anything other than water) during this time, as these things have a high likelihood to disrupt your circadian rhythm (see `!circadian` and `!exercise` for more information), which is unhealthy and can suppress melatonin.


**Schedule Line Specifics:**

`DC:` If your cores are within 5h of each other, the dark period should cover the whole core gap.

`E:` The dark period should extended until the end of the first nap as long as the total duration of the dark period does not exceed 12h.

`TC:` The gap between the night and dawn cores should be covered in the dark period."),
                ("dayblock",        Category.Scheduling, @"__**Sleeping During the Day**__

If you can't nap at all during the day, or you can nap during the day but you can't sleep at the same exact particular time every day, you may have issues adapting to many schedules. If you are in this position, consider Segmented sleep or a biphasic variant with its nap after work (17:00-18:00) with another sleep late in the morning."),
                ("diet",            Category.Hidden, @"__**Diet and Sleep**__

Intermittent fasting is acceptable for polyphasic sleep, and a ketogenic (low-carb) diet should pose no problems. Make sure to keep at least an 8h fasting period (no food or non-water liquids) that coincides with the dark period. 
On E2 and E3, it's best to not eat until after the first nap. On DC schedules, it's important not to eat between the cores.

For more advice, ask in |#fitness_and_nutrition#|."),     //eating
                ("discipline",      Category.Adaptation, @"__**Discipline and Polyphasic Sleep**__

When adapting to a schedule, make sure to stick precicely to ALL sleep times in your schedule until you are adapted. Go to bed at the same time every day without exception. Even if you don't fall asleep fast at first, keep sticking with your scheduled sleep times, and eventually you will fall asleep asleep quickly when sleep times come. Avoid oversleeping at all costs during adaptation. Small mess-ups should still be tolerable and you can still adapt after around one to two months. See `!alarms` to learn how to minimize your chances of oversleeping. "),
                ("drugs",           Category.Adaptation, @"__**Drugs and Sleep**__

Avoid any drug that alters your sleep either directly (by altering sleep stages/cycle lengths, making you sleepier/more alert, etc) or indirectly (by making you you lose control or motivation in some way). This is especially important during adaptation. Check in |#fitness_and_nutrition#| or read some studies if you're unsure about a certain drug.


__**Common drugs and their impact on polyphasic sleep:**__

**Alcohol:**
Alcohol consumption leads to changes in the balance between REM and SWS, and can mess with cycle lengths. Higher consumption wrecks sleep quality and makes you feel groggy after waking. 
__Verdict:__ Moderate alcohol consumption with some time before sleep shouldn't cause issues. However, it's best avoided during adaptation completely.

**Caffeine:**
Caffeine may help you feel better in the short-term, but you'll feel worse after the effects of the caffeine fade out. Caffeine prevents or limits entry into deeper vital sleep stages, effectively replacing valuable REM and SWS with light sleep. This means that that even if you were to fall asleep, your sleep quality would suffer substantially. Caffeine's half-life (the time it takes for the caffeine amount in your body to be halved) is on average 6.5h, meaning that it takes about 12-18h to be mostly removed.
__Verdict:__  Caffiene should be completely avoided on all schedules, with the exception of Segmented due to the exceptionally long wake gap during the day. If you monitor your caffeine levels (the [CoffeeCalc tool](https://coffeecalc.cc) is helpful for this), you can find the maximum amount that you may consume without affecting your sleep. Generally it's okay to consume a moderate amount of caffeine in the morning, but best to avoid it after noon.

**Cannabis:**
A very limited amount of research has been done with regards to cannabis affecting sleep. Research and anecdotal evidence points to cannabis reducing the amount of REM sleep and increasing SWS, possibly also increasing its daily need. In addition, many users report sleepiness as a side effect. 
__Verdict:__ Cannabis should be completely avoided during adaptation, and used with moderation once adapted.

**Nicotine:** Nicotine has a half-life of about 2h after which it gets turned into cotine with a half-life of about 19h. Both of these are stimulants, which means they will make it harder to fall asleep and reduce the quality of any sleep you do get (replacing SWS and REM with light sleep). Nicotine can also alter your circadian rhythm, which is both unhealthy and harmful to your adaptation.
__Verdict:__ Nicotine should be completely avoided while doing a Polyphasic Sleeping schedule."),
                ("dst",             Category.Challenges, @"__**Daylight Saving Time (DST)**__

DST can be hard on your schedule because altering sleep times during adaptation will confuse your body as to when it's supposed to sleep. Tiredness/grogginess during the wrong times is not uncommon. 


**There are 2 ways to deal with DST:**

**1.** Rotate the schedule 1h in a single day.**
**Pros:** Fast change, works if schedule doesn't allow anything else.
**Cons:** Sets your adaptation back several days, and you'll feel groggy for a while, increasing the risk of oversleeps. This can also initiate a stage 3/4 loop that prevents adaptation entirely in some cases.

**2. Stick to the old schedule:**
**Pros:** No setback whatsoever.
**Cons:** Requires you to have a schedule that allows it to be shifted by an hour as DST changes the time around it.


**Do not** rotate your schedule by a small increment of time over the course of a number of days, as this has been shown to destabilize your circadian rhythm more than shifting all at once.

If you're planning to start a polyphasic sleep schedule but DST is coming up soon, it's wise to either go with the schedule you'll end up with in the end or wait until DST passes before switching schedules."),
                ("dualcore",        Category.Scheduling, @"__**Dual-Core Schedules**__

The Dual-Core (DC) schedules operate on the premise that splitting the night core into 2 parts allows each core to overlap with a vital sleep peak. This improves the potential for higher quality and quantity for both REM and SWS. The first core overlaps with the SWS peak (typically 21:00-24:00) and second core overlaps with the REM peak (typically 06:00-09:00) to achieve the highest quality sleep. Due to the placement of the cores, the first core will contain mostly SWS, while the second will contain mostly REM.
People who often wake up during the night without being able to fall back asleep easily are often naturally Segmented, and the DC line is recommended for them.
Cycle compression is often present in the DC line (see `!compression`).
If you aren't able to sleep at these times ask in |#beginners#| if you will be able to adapt to a DC schedule."),
                ("eating",          Category.Adaptation, @"__**Eating and Polyphasic Sleep**__

The act of digestion activates your nervous system, which increases sleep onset times and prevents or limits your body from entering the deeper vital stages of sleep. As a result, it is best to eat meals at least 1-2h before a nap (2-3h before a core), or after you sleep (as long as you aren't in your dark period; see `!darkperiod` to learn why). Some light snacks such as fruit are okay within 1h before a nap.

Consuming too much sugar should be avoided. After consumption of mainly sugar you get a boost in energy for up to 45m, during which it's difficult to fall asleep. Abundant sugar mixed with plenty of fiber, protein, and/or fat should reduce the intensity of the sugar high, and can make it last up to a couple hours. When this wears off, you'll reach an energy low for as long as a couple hours where you risk oversleeping. The quality of your sleep will also suffer as a result of too much sugar. SWS will be replaced by light sleep and you'll be more prone to waking up in the middle of your sleep (which will also result in a lower quality sleep).

Intermittent fasting is acceptable for polyphasic sleep, and a ketogenic (low-carb) diet should pose no problems. Make sure to keep at least an 8h fasting period (no food or non-water liquids) that coincides with the dark period. 
On E2 and E3, it's best to not eat until after the first nap. On DC schedules, it's important not to eat between the cores.

For more advice about eating while on a polyphasic schedule, ask in |#fitness_and_nutrition#|.
"),
                ("everyman",        Category.Scheduling, @"__**Everyman Schedules**__

Everyman (E) schedules have a single core and naps during the day. The premise of this schedule line is that 90m chunks of the core are traded for an equivalent number of 20m naps during the day.

The core of the Everyman schedules can be scheduled slightly later than the first core in DC schedules, since the core should contain both SWS and REM. *However, a core that is scheduled too late will not allow you to achieve sufficient SWS.*

Once adapted to, the naps of E1 and E2 can be quite flexible. In order to gain some flexibility with E3, the core could be extended to 3.5h, or you could try E3-extended, which has a 4.5h core and generally high flexibility. **Do not** flex at all during adaptation.
E4 and E5 are below the minimum sleep threshold, and should generally be avoided."),
                ("exercise",        Category.Adaptation, @"__**Exercise and Sleep**__

People who do a lot of intense muscle building (HIIT, lifting, etc.) need at least 3 sleep cycles in their core (first core for DC) to handle the increased SWS need. Cardio doesn't increase the SWS need significantly.

Most people should have at least 2h after exercising before sleeping in order to avoid having the exercising interfere with their sleep.
It is advised to do cardio in the morning and lifting in the evening post-adaptation. 

During adaptation it's best to avoid increasing the SWS need, since it can cause an oversleep later. Because sleep deprivation hinders muscle gain, you also won't get the desired results.

Exercising can be beneficial to you during adaptation, if it's integrated into your morning routine to avoid oversleeping.

It is best to avoid raising your heart rate during dark period, so avoid cardio and intense workouts during this time (see `!darkperiod` for more information)."),
                ("experiments",     Category.SScience, @"__**Experimenting**__

The Experiments group aims to study how different substances and activities (such as caffeine, meditation, and sugar) affect sleep with the aid of EEG sleep trackers. This improves the legitimacy of the community and helps exchange speculative claims into scientific ones. After enough data is gathered, the results will be posted on the polyphasic website. The Experiments group is looking for adapted polyphasic or monophasic sleepers with sleep trackers, people who plan on getting sleep trackers, or people who are willing to help contribute to the community by analyzing data, programming, or are otherwise just curious for what happens in the R&D side of the server. If any of these criteria fit you please tag a moderator and ask them to give you the |@Experimenter@| role! "),
                ("eyestrain",       Category.Challenges, @"__**Eyestrain and Polyphasic Sleep**__

If you have problems with eyestrain during screen use, it is suggested that you take regular breaks and switch to paper media for reading, and to look at somethinig farther away for 20s every 20m or so to allow your eyes to relax. Also for a period of 6-12h (preferably 8-10), beginning 2h prior to the first core, you should avoid light (especially blue light). Red light is the least harmful, and does not have to be avoided (but high levels of red light are still undesirable). Do this by using programs such as F.lux on your computer, having red light in your house, or wearing red-tinted glasses. See `!darkperiod` for more information. 

Eyestrain occurs at higher rates for polyphasic sleepers the shorter their schedule is, as shorter overall sleep means less opportunity for a sleeper's eyes to get the rest they are used to when sleeping more. If you experience this issue, try eyedrops or putting a wet, warm towel over your eyes for a few minutes at a time."),
                ("fallasleep",      Category.Challenges, @"__**Tips for Falling Asleep**__

Try to lower your body temperature to fall asleep more easily. Create a sleep-inducing environment by lowering the light level in the room, remain calm, and try focusing on your breath. Inhale and exhale at regular intervals.
Try to avoid getting caught in stimulating trains of thought, and perhaps try reading something entertaining or calming before bed. Have a hot shower around 1h before sleep time. Listen to lulling music, brown/white noise, or ASMR. Stay away from electronics (especially screens) for at least 15m before bedtime.
Remaining calm is essential for falling asleep, so it is most helpful to accept that you may not fall asleep quickly (or at all for naps at first), and enjoy the relaxing, meditative personal time. Remember that laying down and attempting to sleep will help the body learn when it's time to sleep, causing you to fall asleep faster in the future. In addition, simply laying down and resting provides a wakefulness boost, albeit smaller than if you were to fall asleep. Don't get discouraged- keep trying!

If you fail to fall asleep even after a long time on the schedule, spending some time on a harder schedule like Naptation, E3, etc. until you fall asleep reliably can be helpful. The ability to nap is a skill that has been anecdotally proven to stick even after going back to mono sleep for a long time, so recovering after learning to fall asleep is recommended. See `!recovery` for more information."),
                ("flexing",         Category.Adaptation, @"__**Flexing Sleep Blocks**__

Flexing is when you start one or more scheduled sleeps at different times than scheduled, but keeping the sleep length the same. This is strongly discouraged for beginner polysleepers (especially during adaptation), as it can prevent adaptation or even cause you to unadapt. Flexing should only be done in small increments, after being adapted for a month or two to a strict schedule. 

Nap times can be flexed as little as 15m in either direction, or as much as 2-3h either direction, which takes longer to adapt to. Always keep at least 2-3h gaps between sleeps even when flexing. Gradually increasing flex range is recommended, and it's recommended to start by flexing just one nap by 15m to be safe.
The length of a flexing adaptation takes a varying weeks that increases with difficulty of base schedule, number of sleeps flexed, range of flexing, and how much (if at all) you are flexing your core(s).


**Tricks to make flexing a schedule easier:**

**1.** Avoid napping in peak SWS pressure of circadian rhythm (broadly 20:00-02:00).

**2.** Avoid flexing your core(s) unless you are a experienced polysleeper, and/or are adapted to a higher TST schedule.

**3.** Increase core lengths 30m (e.g. to 3.5h for E3 or 5h for E2).

**4.** Increase core lengths by a 90m cycle (e.g. to 4.5h for E3 or 6h for E2).

**5.** Avoid flexing your first nap (if your schedule has more than one).

**6.** Choose a schedule with at least 5.5h total sleep per day. (Several people have adapted to schedules with a 4.5h TST and flexed naps, such as E3 with a 3.5h core and a stable first nap.)"),
                ("genetics",        Category.SScience, @"__**Genetics and Sleep**__

Some people have specific genetic mutations that allow them to function at optimal levels with much less sleep than the average person. Most people need 7-8h of sleep per day on monophasic schedules to be fully functional, but some people need less than 6h of total sleep each day. A common gene which affects the general sleep requirement is the DEC2 gene. Their sleep compression is often much higher than most people, enabling them to attempt schedules like Bimaxion, Trimaxion, SPAMAYL (and in rare cases even Dymaxion or Uberman), with a higher possibility to succeed long term than most. If you have lower sleep requirements, you would likely know that by the time you're an adult. Therefore, think twice before attempting these extreme schedules, as they may have negative health effects in the long term."),
                ("graveyard",       Category.Scheduling, @"__**Graveyard Hours**__

The graveyard hours typically last from midnight to 08:00. The need to rest during this period is greater than during the day, and subsequently sleepiness is typically higher than any other time of day. It's generally best to have the majority of your sleep in these hours, which can also have benefits of improved health. "),
                ("help",            Category.Hidden, @"__**Informational Commands**__

**General Polyphasic Info**```
!beforeyoustart  !jumpintoschedule
!ns              !oss
!products        !polyprep
!recovery        !underage
```
**Scheduling**```
!bestschedule    !dayblock
!dualcore        !everyman
!graveyard       !latecore
!naplength       !nightshift
```
**Adaptation Info**```
!adaptation      !adapt-methods
!adapt-success   !alarms
!discipline      !drugs
!eating          !exercise
!flexing         !howtobegin
```
**Adaptation Challenges**```
!adhd            !dst
!cantadapt       !eyestrain
!fallasleep      !incompatible
!napspot         !prematurewake
!sickness        !skipnap
!sleepdebt       !stayawake
!travel          !zombiemode
```
**Sleep Science**```
!circadian       !compression
!darkperiod      !experiments
!genetics        !melatonin
!memory          !microsleep
!napaware        !repartition
!secondwind      !sleepstages
!sleeptracker    !smartalarm
!tankphasic
```"),
                ("howtobegin",      Category.Adaptation, @"__**Tips for Beginning Polyphasic Sleep**__

Now that you've got a schedule and learned other tips, how do you begin?


**Here are some important things to keep in mind:**

**1.** You would want to fall asleep quickly in your first core sleep, or first nap. If you have your core/nap earlier than your usual sleep time, try sleeping at the scheduled time anyways. If you aren't able to sleep that day you'll fall asleep easier the next day!

**2.** Prepare and make sure your alarms work! Lack of knowledge of alarm usage is recipe for adaptation failure! See `!alarms` for more information.

**3.** If you oversleep or undersleep, keep sticking to the schedule! If you consistently oversleep, see `!oss`.

**4.** Make sure you look through other tips (use `!help` to see a full list)."),
                ("incompatible",    Category.Challenges, @"__**Incompatible Polyphasic Schedules**__

Occasionally, regardless of how rigid, consistent, and disciplined a polyphasic sleeper may be, a fully adapted state seems to be just out of reach. Often times this occurs when attempting a schedule that is incompatible with your body. 


**There are a few ways this may present:**

**1.** An incompatible schedule is usually a schedule where there is either not enough sleep to fit all of your vitals, or where the distribution of sleep may be unideal for you (such as someone with high REM needs on an everyman schedule that has a core in the SWS peak, where REM might be pushed out by the more prevalent SWS). When this is the case, you're most likely to simply oversleep shortly after reaching stage 2 or 3 due to the lack of vitals. 

**2.** If your schedule is too short to fit your vitals but you avoid oversleeping at first, you might suffer an endless stage 3 until you do oversleep. This is because your body becomes increasingly sleep deprived until it reaches a point where it forces you to oversleep (generally due to zombie mode occuring when your alarms go off; see `!zombiemode`) in order to get the needed vital sleeps. 

**3.** Less commonly, you may be able to get *just* under what your vital needs are when on a schedule. In this case, you'd likely reach stage 4 and begin to feel better, but still never fully adapt. Even if you are sleeping on time for every sleep and never oversleeping, your body will never be able to meet your vital needs on a daily basis. If you record your sleep with an EEG (see `!sleeptracker`), you'll likely see your sleeps flip-flop between prioritizing REM and SWS on a daily basis in an effort to try to make up for the deficit of that vital sleep stage the previous day. 

**4.** Theoretically, if your schedule is *exactly* enough to meet your vital needs, you may again reach stage 4, but never adapt. In this case, it's possible that your body may be unable to pay off the sleep debt that it accumulated during the rest of the adaptation proccess, in spite of the fact that you're getting vital totals equivalent to your usual needs. This is because sleep debt increases your vital needs, and as such you aren't *actually* meeting your vital needs when reaching stage 4- you're perpetuating a sleep-deprived state."),
                ("jumpintoschedule",Category.GenPoly, @"__**Tips for Schedule Creation and Preparation**__

Before you start your schedule, you should get used to the future bedtime before starting to minimize the amount of initial sleep deprivation you build up from the first night.

For example, you want to do mono->E1, but your current bedtime deviates from the one you want to have on E1:
**Current mono bedtime:** 24:00
**Desired E1 bedtime:** 23:00

Jumping into the schedule right away would most likely lead to you not being able to fall asleep for at least an hour, so you would right away start with a lot more sleep deprivation. This will make oversleeping later in adaptation more likely.


**If you have troubles shifting back your bedtime, you can try a few things to aid this process:**

**1.** Apply a proper dark period about 2-3hrs prior your bedtime. This is a good practice for beginning any polyphasic schedule.

**2.** Shift by small amounts each day by ~30m intervals.

**3.** Take your time with it and do it gradually- laying in bed for hours straight without sleeping won't help you."),
                ("latecore",        Category.Scheduling, @"__**Late Cores**__

Having a late core is possible in theory, but it's hard to maintain practically. If the core doesn't start between 20:00-24:00 (21:00-23:00 for DC schedules), optimal circadian management is required (see `!circadian`). Shorter cores are much more difficult to schedule late than longer cores due to the limited time for SWS gain. 

It's easier to pull off a late core if the bedtime doesn't change after starting adaptation. If there's a big switch in the time when the core starts, the risk of getting an SWS rebound increases a lot, even for schedules with over 2 cycles in the core."),
                ("melatonin",       Category.SScience, @"__**Melatonin and Sleep**__

Melatonin is useful for shifting core bedtimes earlier or later, just like its common use for jetlag. This is especially useful for setting your circadian rhythm on the first 1-4 days of your new schedule.

For polyphasic schedules, it's recommended NOT to use extended time-release melatonin, as those are designed for mono-length sleeps (~8h). Typically only take melatonin for a few nights at a time. The optimal amount of melatonin will vary per person. Toxicity of melatonin is known to be extremely low, but common doses range from 1-5mg, and 10mg in rare cases."),
                ("memory",          Category.SScience, @"__**Memory and Sleep**__

During adaptation your memory can be expected to be impaired due to lowered amounts of REM and SWS, but after adaptation it will restored to that of a natural wakes mono or possibly better due to the more frequent breaks throughout the day (naps or cores). 

SWS is responsible for storage of declarative/explicit memory, i.e. recall of explicit factual information. For example: if you study for a history, or a biology test; X happened on date Y; A in the body does thing B.

REM is responsible for storage of procedural memory, i.e. remembering how to do certain things by following a procedure. For example: how to walk, how to ride a bike, how to play a specific piano piece. REM is also responsible for emotionally related memory consolidation, and for spatial memory consolidation (together with light sleep), i.e. which way to walk to your friend's house. 

If you're trying to remember procedures by repetition, napping is a good way to boost that. If you're trying to remember chunks of information, you'll experience the most improvement after your core sleep. It is best to study these types of things before your core(s).

If you have to follow some procedure every time, for example you do X, then Y, then Z, and it leads to Q, and you're following the procedure over and over, REM sleep will help. If you need to recall some piece of information off the top of your head, SWS sleep will help. 

Many things require a bit of both SWS and REM. When you are learning something, you probably have some sort of impact on most of the memory types, so getting good quality sleep is going to help you regardless. It's better to study, sleep, and then take the test, than it is to pull an all nighter. You're much more likely to have better recall after sleeping."),
                ("microsleep",      Category.SScience, @"__**Microsleeps**__

Microsleeping occurs when the body is so sleep deprived that it forcibly shuts itself down for a period of time lasting from a few seconds up to a minute or two (any longer is called a crash) in order to regain some vital sleep. It's essentially your body's way of saying ""if you won't sleep enough, I'll force you to!"".

Microsleeps are generally preceeded by an alertness dip, and often occur in cycles of 2-5 microsleeps in a row. It's best to stand up and move around (see `!stayawake`) to prevent getting caught in microsleep cycles and then subsequently crashing.


**Microsleeps can be difficult to avoid, but it's imperative that you limit them as much as possible for the following reasons:**

**1.** If you have consistent microsleeps every day, they can become repartitioned into your sleep schedule, causing you to microsleep at the same time every day. 

**2.** The more you microsleep on a daily basis, the more likely you are to continue doing so in subsequent days, which may lead to crashes (oversleeps).

**3.** If you microsleep an extreme amount, it can even delay your adaptation by preventing you from achieving high enough levels of sleep deprivation to begin the repartitioning process (see `!repartition`)."),
                ("napaware",        Category.SScience, @"__**Napawares**__

During a nap, it can sometimes be difficult to determine whether you are awake or asleep. If you find that you are unsure, it is very likely that you were indeed sleeping, but in a state of lucidity called a Napaware. In the lucid dreaming community, this is known as 'WILD', where one enters a lucid state as they fall asleep. 
Some napaware experiencers report experiencing involuntary rapid eye movements as they rest, as well as vivid images that feel like a daydream, but are likely lucid dreams. When monitoring napawares using an EEG (see `!sleeptracker`), it often shows up as REM or light sleep, and napaware experiencers report their readings showing wakes that coincide with moments where they adjusted their sleeping position. 

Napawares seem to be common whilst learning to nap, as well as when nappers of any level attempt to nap in an unfamiliar environment. As a result, it has been speculated that napawares may be an evolutionary development where the body can rest, but the brain is still in an alert state, ready to respond to any potential threats in the unfamiliar environment/unfamiliar state of daytime napping.

To help provide some cognitive feedback to during this experience and to help yourself fall into a deeper sleep is to have some kind of auditory stimuli end (fan, rain, brown/white noise, binaural beats, etc.) about 15 minutes into a 20 minute nap. When you wake up, try to recall if you remember the exact point the noise turned off. If you had a napaware, you likely won't have any recollection of the noise turning off, providing your brain feedback. Do this for a few weeks and the experience will improve. You may still have lucid experiences but they will likely feel much deeper, providing an experience of what it's like to be lucid and sleeping at the same time."),
                ("napeat",          Category.Hidden, @"__**Eating Before Sleep**__

The act of digestion activates your nervous system, which increases sleep onset times and prevents or limits your body from entering the deeper vital stages of sleep.
It is best to eat meals at least 1-2h before a nap (to allow time for digestion before sleep), or after a nap. Some light snacks such as fruit are okay within 1h before a nap."),     //eating
                ("naplength",       Category.Scheduling, @"__**Nap Lengths**__

30m naps are notoriously difficult to wake up from because you enter SWS around 25m in on average, and SWS can cause bad sleep inertia, zombie mode (see `!zombiemode`), or worse, simply sleeping through your alarm in some extreme cases. It's also unhealthy to interrupt SWS. For these reasons, we usually recommend 20m over 30m for naps.

Don't allow yourself more than 2m to fall asleep (ideally, stick with 20m total nap time). Once sleep deprivation starts to kick in, you will fall asleep within minutes, and if your naps are any longer you risk entering SWS. If you want to sleep longer, try aiming for a full cycle of 90m.

If you don't have the ability to take 20m naps, two 15m naps could replace one 20m. It is best to keep all naps the same length."),
                ("napspot",         Category.Challenges, @"__**Where to Sleep?**__

If you're struggling with oversleeping, avoid sleeping in a bed as it increases the risk of oversleeping. Sleeping on a flat surface (with carpet) is recommended for spine health, waking on time, and has been known to provide other health benefits. It's possible to train yourself to sleep while sitting in a chair or car, or laying on an uncomfortable surface. Stick with it and you'll fall asleep eventually! (See `!fallasleep` if you need help.)"),
                ("nicotine",        Category.Hidden, @"__**Nicotine and Sleep**__

Nicotine has a half-life of about 2h after which it gets turned into cotine with a half-life of about 19h. Both of these are stimulants, which means they will make it harder to fall asleep and reduce the quality of any sleep you do get (replacing SWS and REM with light sleep). Nicotine can also alter your circadian rhythm, which is both unhealthy and harmful to your adaptation.

Nicotine should be completely avoided while doing a Polyphasic Sleeping schedule."),     //drugs
                ("nightshift",      Category.Scheduling, @"__**Night Shifts and Polyphasic Sleep**__

**For those who work night shifts, polyphasic sleep can be possible, if:**

**1.** You can have a short nap at work. 

**2.** Your night shift schedule is stable every day for the foreseeable future.

**3.** You don't have any health problems (night shift workers tend to have higher blood pressure, heart rate, even some sleep disorders, due to destabilized circadian rhythm; this is often known as [shift work disorder](https://www.sleepfoundation.org/shift-work-disorder)).


For health benefits, any form of biphasic sleep is a good choice. A good approach is to have one core sleep after work when you go home, and a small nap before you go to work. Everyman schedules are viable, but beware of their tougher adaptation process in this context."),
                ("ns",              Category.GenPoly, @"__**Newcomer Syndrome**__ 

Newcomer Syndrome occurs in new polyphasic sleepers at very high rates. Newcomers often underestimate adaptation difficulty or overestimate their willpower/physical limits, and decide to the hardest sleep schedules like Uberman, Tesla, and Dymaxion, knowing that they *don't* have the short sleeper genes that would allow them to sleep less in the day without feeling tired. (If you have this gene, you would have known by the time you became an adult.) This syndrome is severe because it cultivates the misconception that everyone can sleep as little as 2h per day while gaining superhuman productivity (you most likely cannot). In addition, the often inevitable failure to adapt often discourages these newcomers from continuing to attempt polyphasic sleep, and adds to the public perception of polyphasic sleep as impossible, unhealthy, and limited to the nap-only schedules.

The adaptation to these hostile schedules is extremely severe, and sticking with them long term can cause severe physical/mental exhaustion due to the extreme lack of amount of vital sleep stages that are normally present.

If you're a newcomer, please be realistic in choosing a suitable sleep schedule for yourself, and take your time to consider what's best for your own well-being.
Try to sleep for more than 4h a day if you're over 21. If you're between 18 and 21, you should get at least 5h of sleep. If you're underage, see `!underage`."),
                ("oss",             Category.GenPoly, @"__**OverSleeping Syndrome**__

Oversleeping Syndrome is a psychological phenomenon that has been documented numerous times in the polyphasic sleeping community. It occurs when a polyphasic sleeper experiences consistent oversleeps, but nevertheless continues attempting to adapt, expecting to succeed eventually. 


**The syndrome is fatal for adaptation because:**

**1.** Continuing to oversleep while trying to adapt will inevitably mess up one's circadian rhythm and sleep compression entirely. The body is trying to adjust to a certain sleep cycle length, but this can only happen with **no** oversleeping.

**2.** As sleep is not stabilized (oversleeping during intended waking time), the person will fail to adapt to a schedule and thus will constantly be plagued in sleep deprivation. Long-term sleep deprivation leads to obesity, diabetes, increased heart rate, headaches, fatigue, frequent anger, and more negative symptoms.


The solution to this syndrome is generally to go back to monophasic sleep or another non-reducing schedule for at least 1-2 weeks to regain homeostasis, then attempt to adapt to a polyphasic schedule again. This is called ""recovery"" (see `!recovery`)."),
                ("polyprep",        Category.GenPoly, @"__**Tips for Preparing for a Polyphasic Adaptation Attempt**__

**1.** Take some time to prepare for your schedule if possible. Quit caffeine, nicotine, drugs, and excess sugar before starting (see `!drugs` and `!sugar` to learn more). Start eating healthier and exercising. 

**2.** Begin teaching yourself to get up the moment you wake up. Start having both a night and morning routine.

**3.** Inform people around you about what you will be attempting and how this may affect both you and them. 

**4.** Start scouting out possible locations where you can nap.

**5.** Read up to understand polyphasic adaptation and what to expect. That includes other `!help` commands like `!adaptation` and `!oss`, <https://polyphasic.net/>, and asking any questions in |#general_polyphasic#|, |#beginners#|, or another relevant channel.

**6.** Consider shifting your current core wake time or bedtime toward your new core's bedtime or wake time if either is possible, and implement the dark period you will be following when you begin your schedule.

**7.** Install and practice using all apps you may need, like alarm apps (see `!alarms`), F.lux/Twilight/Sunsetscreen, or productivity apps.

**8.** Ensure your health and safety if you have any stress-aggravated medical conditions, like cardiovascular problems or seizures or suicidal thoughts, as you will be going through weeks of mild to severe sleep deprivation. Severity depends on the person and the schedule. See a doctor to determine if short-term sleep deprivation is an acceptable risk for you.

**9.** Create at least two huge lists:

       **a.** Tasks toward major, motivating goals that inspire your interest in more time or better sleep.

       **b.** Easy, time-consuming tasks that you can make yourself do while tired and unmotivated, like cleaning or cooking."),
                ("prematurewake",   Category.Challenges, @"__**What to do When You Wake Before Your Alarm?**__

If you naturaly wake prematurely (<5m on a nap, <30m on a core), get out of bed. Don't try to cram in more sleep until the alarm rings. You could fall asleep and wake up in SWS (see `!zombiemode`), or sleep through your alarm(s). Falling back asleep could also be difficult, resulting in you wasting the time spent trying to do so.

Premature wakes that fall within the ranges detailed above are often referred to as ""natural wakes"", and they can be a sign that your body is learning your schedule and anticipating the wakes, which is good news for an adaptation! Natural wakes usually result in a comfortable waking process with minimal sleep inertia, and as a result are usually coveted by polyphasic sleepers. In other cases these wakes may simply be random, caused by cortisol spikes or external stimulation, such as noises around you."),
                ("products",        Category.GenPoly , $@"__**Polyphasic Sleep Products**__

Some fellow polyphasic sleepers now offer redistributions of items with long shipping times, or exclusive items. Check [here](https://docs.google.com/spreadsheets/d/1w8uIIdOcAymnsmBfHd-gENqZWczCChmku9LaEKHJrOo/edit?usp=sharing).


{(Bot._client.GetUserAsync(Config._crimsonUserID).Result != null ? $"<@{Config._crimsonUserID}>" : Bot.getUser("Crimson", Config._napGodServerID) != null ? $"<@{Bot.getUser("Crimson", Config._napGodServerID)[0].Id}>" : "Crimsonflwr")}: *Amazon requires that the following sentence is mentioned:*
__As an Amazon Associate I earn from qualifying purchases.__

**Red Glasses:** Use during the dark period, if you normally wear glasses you can buy two and remove the frames from one to wear between the glasses and the face.
[Purchase Here](https://amzn.to/31PCObu) *(ad)*

**Daylight Lamp:** Use if your core doesn't start at 20:00-24:00. Have atleast 5000 LUX. Use when it's gotten dark outside/during the early morning to set the circadian rhythm. Use until 3h from the dark period.
[Purchase Here](https://amzn.to/2MPOxCj) *(ad)*

**Red-Light Bulbs:** Alternative to the red glasses (if you aren't using screens or any other sources of blue/white light).
[Purchase Here](https://amzn.to/345DvPy) *(ad)*

**Sonic Bomb:** A very loud alarm clock. 
[Purchase Here](https://amzn.to/2peTYSI) *(ad)*

**Zeo EEG:** (Mobile, Mobile Pro, or Bedside). A sleep tracker, only available secondhand (see |#marketplace#|). 
[More Information](https://polyphasic.net/polyphasic-sleep/sleep-tracking/zeo-sleep-tracking/)

**Olimex openEEG:** A sleep tracker that requires building, but is more accurate than the Zeo. It costs roughly €200 with shipping. You will need electrodes, and it's recommended to get a hollow frame ski mask to thread the electrodes through.
[Purchase Here](https://www.olimex.com/Products/EEG/OpenEEG/EEG-SMT/open-source-hardware) | [More Information](https://polyphasic.net/polyphasic-sleep/sleep-tracking/olimex-openeeg/)


More information regarding products can be found [on our website](https://polyphasic.net/polyphasic-sleep/adaptation/products/).
The Polyphasic Community does not endorse any specific products from specific sellers. For alternatives, ask in |#sleep_tech#|."),
                ("pronap",          Category.SScience, @"__**Pronaps**__
Pronaps are naps typically 40m in length intended to be taken during the REM peak (see `!sleepstages`) in order to gain more REM than a traditional 20m nap may allow. This 40m time is longer than the recommended nap length of 20m (see `!naplength`), but there is a lot of evidence supporting the validity of this type of nap, given the correct conditions.

The correct conditions stipulate that the sleeper will have already met their SWS need in previous sleeps. As a result, they should not have any in the pronap. This is important because SWS is difficult to wake from, and typically begins at about 25m into sleep. If the sleeper has met their SWS need, they should not have any in the pronap, meaning they won’t have to worry about waking from that. This provides a solid 40m of time that can be up to 100% REM in the best cases. *This is notably highly ideal for lucid dreamers.* If one will not have met their SWS need in time for a pronap, or a pronap is scheduled outside of the REM Peak, the pronap would not be recommended."),
                ("recovery",        Category.GenPoly, @"__**Recovery**__

Recovery is the concept of sleeping with natural wakes until all sleep deprivation is gone. This usually takes about 1-2 weeks, but it can take more or less time depending on the amount of sleep deprivation accumulated. While doing recovery, one should have a consistent start time for each sleep.


**Recovery is usually done:**

**1.** Before doing a polyphasic sleep schedule, if one is sleep deprived.

**2.** After falling into OSS. See `!oss`.

**3.** After an oversleep big enough that you know you won't be able to continue.


Mono-recovery (one core) is the prefered recovering schedule, but it's also possible to recover on biphasic schedules (will likely take longer due to the alleviated sleep deprivation during the day):

**E1-recovery:** One 20m nap during the day and a core in the night without an alarm.

**Siesta-recovery:** One 90m core during the day and a core in the night without an alarm.

**Segmented-recovery:** One 3.5h core, followed by a break and another core without an alarm.


**Recovering from a polyphasic schedule often follows these phases:**

**Phase 1.** A few days with a short core (due to the body remembering to wake up early) 

**Phase 2.** Many days with a long core

**Phase 3.** Core times of the usual length


You are likely done recovering when the sleep has stabilized at a set length that's of similar length to before attempting polyphasic sleep, around 6-8h for most adults.

If recovery with natural wakes is impossible due to scheduling limitations, you can recover simply by attempting to get as much sleep as you can get, but recovery will take longer."),
                ("repartition",     Category.SScience, @"__**Sleep Repartitioning**__

After successfully adaptating to a polyphasic schedule, sleep stage repartition is complete. Repartition means the rearrangement of vital sleep stages into each sleep (core/nap) in your schedule. This results in situations where a sleeper may have a nap full of SWS in the evening, and a nap full of REM around dawn. 


__**Before Repartitioning:**__ When one first begins polyphasic sleep, sleep stages have not been repartitioned. The process of falling asleep (for both polyphasic and monophasic sleepers) is as follows: 

**1.** Sleeper enters NREM1 (Non-REM; light sleep), with wakeful state for some time. 

**2.** Sleeper enters NREM2, starting to fall asleep and reality of surrounding fades and awareness gradually becomes null.

**3.** Sleeper enters NREM3 (or SWS: Slow-Wave Sleep), the deepest sleep stage. 

**4.** Sleeper enters REM (Rapid Eye Movement), where dreams occur. 


__**After Repartitioning:**__ Once adaptation is complete, your sleep schedule is stabilized in most sleeps. 
It's common to expect a very dreamy nap around dawn, suggesting that the nap is REM dominant. You will obviously go through NREM1 and NREM2 (takes some time to fall asleep), but you bypass NREM3 to enter REM. Since adaptation is successful, you wake up easily and after around 15m of REM (for example), you will likely enter NREM1 and wake up comfortably. 
If you nap late in the day, results vary - you might enter REM very quickly after lying down, or it could be an SWS dominant nap, in which you won't recall dreams and you'll sleep very deeply. You may also get both SWS and REM in just a 20m nap. 
NREM1 and NREM2 are reduced in your sleeps, saving space for SWS and REM which are more important for physical and mental functions."),
                ("secondwind",      Category.SScience, $@"__**Second Wind**__

A second wind is a sleep phenomenon which occurs when someone stays up for a long time (typically about 24h), and suddenly loses all sense of tiredness for some time. Read more [here]({"https://en.wikipedia.org/wiki/Second_wind_(sleep)"})."),
                ("sickness",        Category.Challenges, @"__**Sicknesses and Polyphasic Sleep**__

When you're sick with anything more than a mild flu (with symptoms above the neck), it's best to return to mono to fully recover before continue polyphasic sleep. 

If you are adapted/adapting to a poly schedule and get sick, you can still be polyphasic. However, while sleep deprived, it is harder for your immune system to resist sickness. Consequently, it is advisable to lengthen* your core sleep(s) (or nap(s) in circumstances where core(s) cannot be extended) to acommodate the elevated vital needs required for recovering. 
If you're adapted, you may not need to extend your schedule if it has a high enough sleep total to accommodate the increased vital needs. 
If you are on a nap-only schedule, it is best that you return to a non-reducing schedule immediately. 

Try boosting your immune system before trying to adapt again. Make sure you're eating healthily, exercising, and so on!

**Naps should be replaced by an extra full cycle, and cores should be extended by a full cycle as well; do not use a random length.*"),
                ("skipnap",         Category.Challenges, @"__**Skipping Naps**__

During adaptation (typically the first month), it's imperative to follow your schedule strictly. This means that you need to avoid oversleeping (any sleep outside of scheduled sleep times), skipping naps, or changing your sleep schedule at all. Failing to do so can result in your body failing to learn the schedule as quickly or at all, which can delay, reset, or halt adaptation progress.
If you cannot avoid missing a sleep, it's best to skip rather than to move, because moving a sleep counts as an oversleep.

Losing a nap means you'll increase the risk of an oversleep later, so be sure you're prepared for this. Set up extra alarms or ask someone to check on you to make sure you're awake.

Move your sleeps only if you know not doing so will result in an even bigger oversleep later. Make sure it's not too close to the next nap/core sleep (spare around 4h before the next core sleep, if your nap is 30m or shorter).

If you have to skip some parts of your core, it's best to do so in chunks of 90m (full cycles). Doing so will help you avoid waking up in the middle of a cycle, which is important because waking during a cycle results in higher risk of waking during SWS sleep, which increases risk of oversleeping. If you have to skip your whole core, try to at least take a nap or two during that time."),
                ("sleepdebt",       Category.Challenges, @"__**Sleep Debt**__

Depending on how long you're on monophasic schedule, sleep debt can hit you hard or not at all when you are adapting to a polyphasic schedule. Generally, expect sleep debt to build and hit you around week 2 or 3 if you don't oversleep. Unfortunately, oversleeping becomes more likely the more sleep debt accumulates, as your body becomes more and more desperate to meet its vital needs (see `!alarms` for help avoiding oversleeps). In the long term, sleep debt can be extremely harmful to your health, so make sure you recover if you have accumulated a large amount of sleep debt but adaptation is still eluding you (see `!recovery` and `!cantadapt`). On the plus side, eventualy the sleepiness caused by sleep debt will allow you to fall asleep instantly for your sleeps.

When enough sleep debt has built up, you'll get a REM rebound. This causes Sleep-Onset REM (SOREM), which is when REM is present the beginning of most (if not all) sleeps. 
On schedules with a split SWS core or with a low sleep total, you can also expect an SWS rebound to occur (typically accompanied by SOSWS).

Repartitioning occurs when enough sleep debt has accumulated if you rigidly adhere to your schedule (see `!repartition`)."),
                ("sleepstages",     Category.SScience, @"__**Sleep Stages**__

SWS (aka deep sleep or NREM3), REM, and Light Sleep (aka NREM1 and NREM2) are all stages of sleep. 

**SWS** (Slow-Wave Sleep) is a vital sleep stage, and the deepest stage of sleep, during which the glymphatic system is activated. Read more [here](https://www.polyphasic.net/deep-sleep/).

**REM** (Rapid-Eye Movement) is a vital sleep stage, and is most well-known for having more frequent occurrences of dreaming. Read more [here](https://www.polyphasic.net/REM-sleep/).

**Light Sleep** is a non-vital sleep stage (though it is theorized that it is healthy to have at least 20% light sleep in your core(s)). It is mostly a transitional stage of sleep, though it serves other purposes, and is usually the most pleasant to wake from. Read more [here](https://www.polyphasic.net/nrem2/).

The average sleep cycle is 90m long, though they can vary person to person. They usually begin with Light Sleep, then transition to SWS, Light Sleep, and then REM, before restarting. Read more [here](https://www.polyphasic.net/sleep-cycle/), and see `!memory` for more about the roles different sleep stages play in memory."),
                ("sleeptracker",    Category.SScience, @"__**Sleep Trackers**__

Most products claiming to track sleep are inaccurate because they are solely based on body movement. This includes most fitness trackers and apps. Generally, anything that's not on your head cannot produce a reliable result. 

Sleep trackers that are often used by server members include the [Olimex openEEG](https://polyphasic.net/polyphasic-sleep/sleep-tracking/olimex-openeeg/) (+-200$); and the [Zeo](https://polyphasic.net/polyphasic-sleep/sleep-tracking/zeo-sleep-tracking/) (40-100$). 
The Olimex is a semi-DIY EEG that produces great results. The Zeo is a user-friendly solution, but is only available secondhand. The Zeo is less accurate than the Olimex but easier to use.
Other head-worn options may or may not provide accurate results. When in doubt, ask in |#sleep_tech#| for more information!


**Research Regarding Sleep Trackers:**

**(1)** Consumer sleep tracking devices... [[Link](http://www.tandfonline.com/doi/full/10.1586/17434440.2016.1171708?scroll=top&needAccess=true)]

**(2)** Consumer Sleep Tracking Devices... [[Link](https://pdfs.semanticscholar.org/a18d/14fbc724c2dfe982fa4b44fbae1d5948c68b.pdf)]

**(3)** Smartphone App vs Polysomnography [[Link](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4481053/)]

**(4)** Examining... Sleep Apps [[Link](https://www.ncbi.nlm.nih.gov/pubmed/28992831)]


**Abstract (1):** Reviewed: BodyMedia FIT, Fitbit Flex, Jawbone UP, Basis Band, Innovative Sleep Solutions SleepTracker, and Zeo Sleep Manager Pro. The review identified a critical lack of basic information about the devices: five out of six devices provided no supporting information on their sensor accuracy and four out of six devices provided no information on their output metrics accuracy. Peer reviewed articles wake detection accuracy was revealed to be quite low and to vary widely (BodyMedia, 49.9±3.6%; Fitbit, 19.8%; Zeo, 78.9% to 83.5%). 

**Abstract (2):** The growing literature comparing these devices against polysomnography/actigraphy shows that they tend to underestimate sleep disruptions and overestimate total sleep times and sleep efficiency in normal subjects.

**Conclusions (3):** Our study shows that the absolute parameters and sleep staging reported by the Sleep Time app (Azumio, Inc.) for iPhones correlate poorly with polysomnography."),
                ("smartalarm",      Category.SScience, @"__**Smart Alarms**__

App or smartband-based ""smart alarms"" are unreliable because they rely on sub-optimal sleeptracking to determine when to wake the user. (See `!sleeptracker` for the accuracy reports; most are below 50% accurate.) 
The Zeo Bedside's ""gentle wake up"" could be considered if on mono, and wake-up lights can be considered for mono, E1, Siesta, and the second core of Segmented. 

The different wake-up times of ""smart alarms"" may keep you from stabilizing to waking up the same time each day on your own. This means that in the long-term, you'd probably wake up feeling good on more days if you keep a regular alarm than if you'd use an unpredictable smart-alarm. Poly can't normally handle the random sleep reductions; cutting out 30m can be highly impactful."),
                ("stayawake",       Category.Challenges, @"__**Tips for Staying Awake**__

If you're grappling with microsleeps (see `!microsleep`) or sleepiness, try increasing body temperature to stay awake. That includes eating protein, exercising (jumping jacks, walking, pushups, planking, etc), or focusing on something you enjoy or that excites you, as sleepiness is increased when comfortable, sedentary, and/or bored. 
You can also improve alertness by increasing blood flow to the brain via methods like eating ice chips or doing handstands.

**Disclaimer:** Some of the tips mentioned above can disrupt your circadian rhythm and should be avoided during your dark period. See `!circadian`, `!darkperiod`, and `!exercise` for more information."),
                ("sugar",           Category.Hidden, @"__**Sugar and Sleep**__

Consuming too much sugar should be avoided. After consumption of mainly sugar you get a boost in energy for up to 45m, during which it's difficult to fall asleep. Abundant sugar mixed with plenty of fiber, protein, and/or fat should reduce the intensity of the sugar high, and can make it last up to a couple hours. When this wears off, you'll reach an energy low for as long as a couple hours where you risk oversleeping. 

The quality of your sleep will also suffer as a result of too much sugar. SWS will be replaced by light sleep and you'll be more prone to waking up in the middle of your sleep (which will also result in a lower quality sleep)."),     //eating
                ("tankphasic",      Category.SScience, @"__**Tankphasic Sleep Analogy**__

Polyphasic sleep can be understood more easily though battery charging principles. If you don't know what REM and SWS are, see `!sleepstages`. If you aren't familiar with how repartitioning works, see `!repartition`. 

This battery-charge principle shows the relationship of how our body gets REM, SWS and our performance on polyphasic sleep:

We have *two different batteries* (REM battery and SWS battery) and they usually refill when we're asleep. These two batteries have different capacities and they recharge at different speeds. The charging circuit has to switch between them to make sure they're both decently filled because it can only charge one at a time. 
If one of the batteries is completely empty (more often REM, since it runs out faster), you don't have enough power left to wake up, or you may just shut off without warning. When its very low, some stuff shuts off completely to save power (see `!zombiemode`) or it intermittently shuts off for small periods of time to try and slow down the battery drain (see `!microsleep`).

 When the battery is low, but not *very* low, you feel sleep deprived (like when you get the red power indicator light, or that annoying ""your battery is running low"" popup warning). When the battery is medium or higher, you feel okay (green light, no warnings). Like a normal battery, it takes way way more time to get from empty to full than it takes to get from empty to medium. The SWS battery doesn't usualy drain fully on schedules with >5h of sleep.

**The objective of polyphasic sleeping** is to set up the charging routine and battery level in such a way that you're batteries are refilled more frequently during the day so they never reach empty, and only have to be partially recharged at night, which requires less time spent charging overall."),
                ("travel",          Category.Challenges, @"__**Travelling as a Polyphasic Sleeper**__

Being on a polyphasic schedule while also travelling is difficult, but possible. 


**If time zones are switched, there are 3 recommend options:**

**1.** Sick with the original schedule.

**2.** Rotate the schedule to fit the new timezone.

**3.** Return to monophasic sleep for the duration of the trip.


The optimal route is to stick with the original schedule during the travel, but depending on the duration of the stay rotating the schedule should also be possible. Note that rotating the schedule will cause the adaptation to be set back. During adaptation it’s important to stick to all sleeping times, which is why many delay their adaptation until after they’ve travelled.

Jetlag occurs when you travel through different time zones. If you decide to change your schedule during the stay you will likely initially struggle to fall asleep quickly in the new time zone, and your schedule may deteriorate. Depending on how long your trip is, how long you stay in the new region, and how long you have been polyphasic, travelling may be difficult or relatively easy to handle.


**Tips for optimal polyphasic travelling:**

**1.** Plan the trip so your core aligns with the flight if possible.

**2.** Try not to core in the airports, as you will be vulnerable to thefts, personal safety, etc.

**3.** If you're adapted, you can flex the naps when possible. Skipping will also be more manageable, and small oversleeps tolerable.

**4.** If you haven't started your schedule yet, waiting until after you travel is often the best option."),
                ("underage",        Category.GenPoly, @"__**Underage Polyphasic Sleepers**__

Underage people (especially those under 16) should avoid reducing their daily sleep time entirely. Those over 16 are strongly recommended to do one of the schedules listed below, because both the brain and body are still developing at that age, and cutting light sleep may have unforseen negative long-term effects.


**Schedules for those between the ages of 16 and 19:**

**1.** E1 (6h20m)

**2.** Siesta (6h30, even Siesta-extended is plausable with 7.5h sleep)

**3.** Segmented (7h)


Siesta is good if you have no previous napping experience, since it has a full sleep cycle during the day. It's very easy to adapt to sleeping a full cycle, since both REM and SWS can easily be inserted into it. Siesta has a 5h core in the night, and a 1.5h core in the day. Siesta-extended is even more favorable for underaged people, since it gives the recommened 7h30m of sleep. Siesta-extended has a 6h night core (4 cycles), and a 1.5h day core (1 cycle). 

E1 is a strange option. It has the possibility to be too easy, and you could have to wait several weeks to be able to sleep for the nap, since so little sleep is actually cut (more sleep is actually cut for underaged people, so it might still be relatively easy to learn to fall asleep for the nap). If you attempt E1 without previous napping experience and you fail to fall asleep for the naps within a reasonable timeframe, see `!fallasleep`. 

Segmented is a bit harder, but still generally easy. It's better to do segmented if you naturally and consistently wake up during the night. Otherwise, the quality of the sleep should be the same as the other options.

Each of these options operate on different mechanics. Siesta takes advantage of the statistically likely REM period that happens at the 4.5-5h mark, and adds a cycle in the day. E1 trades a full cycle of light sleep and REM for a REM-filled nap, while segmented removes a cycle but makes the cycles longer to compensate.

You can look at each option in |#botspam#| with the commands `+siesta`, `+segmented`, and `+e1`."),
                ("zombiemode",      Category.Challenges, @"__**Zombie Mode**__

Zombie mode is a state characerized by a stark lack of awareness and autonomy upon waking, which often results in turning off alarms without fully waking up, and then going back to sleep and oversleeping (sometimes with no memory of ever waking up). This is more likely the more sleep deprived one is, and can be imagined as your body forcing you to get the sleep it thinks you need by preventing you from waking up. This most often occurs when waking from SWS, since this is the deepest sleep stage. See `!alarms` for help with minimizing your chances of zombie mode ruining your adaptation.")
            };
            _help.Add(("dp",                   Category.Hidden, _help.Find(c => c.commandName == "darkperiod").text));
            _help.Add(("newcomersyndrome",     Category.Hidden, _help.Find(c => c.commandName == "ns").text));
            _help.Add(("incompatibleschedule", Category.Hidden, _help.Find(c => c.commandName == "incompatible").text));
            _help.Add(("oversleepingsyndrome", Category.Hidden, _help.Find(c => c.commandName == "oss").text));
            _help.Add(("sleeptrackers",        Category.Hidden, _help.Find(c => c.commandName == "sleeptracker").text));
            _help.Add(("eegs",                 Category.Hidden, _help.Find(c => c.commandName == "sleeptracker").text));
            _help.Add(("eeg",                  Category.Hidden, _help.Find(c => c.commandName == "sleeptracker").text));
            _help.Add(("travelling",           Category.Hidden, _help.Find(c => c.commandName == "travel").text));
            _help.Add(("traveling",            Category.Hidden, _help.Find(c => c.commandName == "travel").text.Replace("ravell", "ravel")));
			#endregion

			_commands = new List<(string commandName, NapGodCommandType type, string text)>
            {
                ("help",            NapGodCommandType.User, @"__**Commands**__
**Schedule list:** `+schedules`
**Set a schedule:** `+set [schedule name]`
**Show a napchart:** `+nc [napchart-link]`

**Information:** `!help`"),
                ("schedules",       NapGodCommandType.User, @"To view information about a sleep schedule, type `+[schedule-name]` (e.g. `+DC1`).

-------------------------------------------

__**Non-polyphasic schedules:**__
`Mono`, `Random`

__**Biphasic schedules:**__
`Biphasic-X`, `Segmented`, `Siesta`, `E1`

__**Everyman schedules:**__
`E1`, `E2`, `E3`, `E4`, `E5`, `Trimaxion`, `SEVAMAYL`

__**Dual-Core schedules:**__
`DC1`, `DC2`, `DC3`, `DC4`, `Bimaxion`, `DUCAMAYL`

__**Tri-Core schedules:**__
`Triphasic`, `TC1`, `TC2`

__**Core-only schedules:**__
`QC0`, `CAMAYL`

__**Nap-only schedules:**__
`Naptation`, `Tesla`, `Uberman`, `Dymaxion`, `SPAMAYL`

-------------------------------------------

**Use `+set` followed by a schedule name to set a schedule such as:** `+set E3`


**You may set a schedule variant after a dash such as:** `+set DC1-extended`
Supported schedule variants are: 
`shortened`, `extended`, `flipped`, `modified`, `recovery`.


*Your schedule not listed? Feel free to consult an advisor or use `+set Experimental`.*"),
                ("tg",              NapGodCommandType.User, ""), //redirect to admin version
                ("adapted",         NapGodCommandType.User, ""), //redirect to admin version
                ("loghelp",         NapGodCommandType.User, $@"Feel free to message here with your logs: <#{Config._napGodLogsChannelID}>!"),
                ("log",             NapGodCommandType.User, $@"Feel free to message here with your logs: <#{Config._napGodLogsChannelID}>!"),
                ("freelog",         NapGodCommandType.User, $@"Feel free to message here with your logs: <#{Config._napGodLogsChannelID}>!"),
                ("logeeg",          NapGodCommandType.User, $@"Feel free to message here with your logs: <#{Config._napGodLogsChannelID}>!"),
                ("ping",            NapGodCommandType.Admin, "Sorry, NapGod is napping. ||(Pong)||")
            };

            _schedules = new List<(string nameLookup, NapGodSchedule.Category scheduleCategory, string description, string napchartLink, bool show)>
            {
                ("mono;monophasic",                             NapGodSchedule.Category.Monophasic,   @"**Monophasic**
------------------------------------------
**Also Known As:** Hibernation
**Invented By:** N/A; Historically slept throughout the world

**Schedule Classification:** Monophasic, Non-Reducing
**Total Sleep:** 7-9h
**Adaptation Difficulty:** Trivial
**Sleep Specification:** 1 Core (7-9h)
**Mechanism:** Discarding the polyphasic homeostatic pressure management that is sleeping multiple times each day, this schedule opts instead for one long sleep through the graveyard hours that contains all vitals and fully resets homestatic pressure for the next full day.
**Ideal Scheduling:** Core beginning around midnight and ending around dawn.
------------------------------------------", "https://napchart.com/poison/Mono-bwpqiw",                                      true),

                ("qc0;quadcore;quadcore0;nullmax;nullmaxion",   NapGodSchedule.Category.Experimental, @"**Quad-Core (QC)**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule and instead stick to something better tested (such as Triphasic/Triphasic-extended).*
------------------------------------------
**Also Known As:** Quadphasic, Quad-Core 0 (QC0), Four-Core 0 (FC0), 4CO, Nullmaxion, Nullmax
**Invented By:** GeneralNguyen

**Schedule Classification:** Quad-Core, Core-Only
**Total Sleep:** 6h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 4 Cores (1.5h)
**Mechanism:** First core before/after midnight for SWS gain, second core around graveyard hours to dawn for mixed vitals (or REM gain if scheduled later), third core around the morning to around noon for REM (or mixed vitals if scheduled later), and the fourth core in the afternoon for mixed vitals.
**Ideal Scheduling:** Three of the cores could be focused more in the night hours with 2.5-3h coregaps, and the last core in the afternoon with larger gaps between the first and third core.
__Alternative Scheduling:__ Distribution of sleep may resemble dymaxion: cores around midnight, 06:00, noon, and 18:00. This distribution can be referred to as Nullmaxion (dymaxion with no naps; following the naming convention of Bimaxion - 2 naps, and Trimaxion - 3 naps).
------------------------------------------
More information about QC can be found [here](https://www.polyphasic.net/quad-core-0/).", "https://napchart.com/poison/Quad-Core-oqmtsk",                                 true),
                ("cama;camayl",                                 NapGodSchedule.Category.Experimental, @"**CAMAYL (Core As Much As You Like)**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule and instead stick to something better tested (such as SEVAMAYL).*
------------------------------------------
**Also Known As:** CAMA
**Invented By:** GeneralNguyen

**Schedule Classification:** Core-Only, Flexible
**Total Sleep:** ~6h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 3-4 Cores (1.5h)
**Mechanism:** Variant of SPAMAYL that swaps all naps for all cores. The core sleeps provide more SWS than REM or vice versa depending on which hours they are placed in the day. Under emergency situations where an extended wake gap is required, a short power nap (10-15m) can be added to sustain wakefulness until the next core sleep. The adaptee must keep a close eye on their tiredness levels and have a strong discipline with regards to napping when needed regardless of the context. If either of those fail, the homeostatic pressure may increase and eventually cause a crash or oversleep.
**Ideal Scheduling:** 3-4 core sleeps on average from day to day, with 1-2 core sleeps around graveyard hours. Core spacing and placement should take into account natural periods of tiredness. In the afternoon 6+ hour wake gaps are common, whereas alertness may wane after 2-4 hours awake in the morning. One core sleep at night may be extended by up to 90m after adapted.
------------------------------------------
More information about CAMAYL can be found [here](https://www.polyphasic.net/camayl).", "https://www.polyphasic.net/wp-content/uploads/2021/04/CAMAYL-3.png",           true),

                ("siesta",                                      NapGodSchedule.Category.Biphasic,     @"**Siesta**
------------------------------------------
**Invented By:** N/A; Historically slept throughout the world

**Schedule Classification:** Biphasic, Dual-Core
**Total Sleep:** 6.5h
**Adaptation Difficulty:** Easy
**Sleep Specification:** 2 Cores (5h & 1.5h)
**Mechanism:** The main sleep (5h night core) is three cycles with a 30m extension that is refferred to as the ""statistically likely REM period"". This core is intended to contain the vast majority of vitals, and the ""siesta"" (1.5h afternoon core) contains mixed vitals.
**Ideal Scheduling:** Night core around midnight, siesta in the afternoon, though the scheduling ranges are quite extensive.
------------------------------------------
More information about Siesta can be found [here](https://www.polyphasic.net/siesta).", "https://napchart.com/poison/Siesta-52q1kw",                                    true),
                ("segmented",                                   NapGodSchedule.Category.Biphasic,     @"**Segmented**
------------------------------------------
**Invented By:** N/A; Historically slept throughout the world

**Schedule Classification:** Biphasic, Dual-Core
**Total Sleep:** 7h
**Adaptation Difficulty:** Easy
**Sleep Specification:** 2 Cores (3.5h)
**Mechanism:** The cores in the SWS and REM peaks mostly intended to gain the respective vitals, but both cores have *some* of both vitals. 3h coregap to trigger sleep stage division between the cores.
**Ideal Scheduling:** Cores in the SWS and REM peaks, no closer together than 2h (ideally 3h or more).
------------------------------------------
More information about Segmented can be found [here](https://www.polyphasic.net/segmented-sleep/).", "https://napchart.com/poison/Segmented-y4n4sf",                                 true),
                ("bix;biphasicx",                               NapGodSchedule.Category.Biphasic,     @"**Biphasic-X (Bi-X)**
------------------------------------------
**Also Known As:** Prototype X, Experimental X
**Invented By:** GeneralNguyen

**Schedule Classification:** Biphasic, Flexible, Non-Reducing
**Total Sleep:** 7-9h
**Adaptation Difficulty:** Easy
**Sleep Specification:** 1 Core (7-9h), 1 Core or Nap (20-90m)
**Mechanism:** Graveyard hours core sleep intended to cover all vital needs, with a sleep in the afternoon (typically a nap, but can be a short core sometimes). More than 1 core sleep or 1 nap (reduce total sleep) is allowed on busier days. Recovery day is done afterwards to recover from sleep deprivation (increase total sleep by extending either core length or nap length in Biphasic form) to keep up napping habits. Both core sleep and nap(s) are flexible and can be moved around somewhat as needed.
**Ideal Scheduling:** Consistent dark period everyday, core sleep starts 1-2h after dark period. Nap during daytime (generally afternoon), no later than 6 PM.
------------------------------------------
More information about Biphasic-X can be found [here](https://www.polyphasic.net/non-reducing-polyphasic-schedules/).", "https://www.polyphasic.net/wp-content/uploads/2021/04/Biphasic-X.png",         true),

                ("tesla;u4",                                    NapGodSchedule.Category.Nap_Only,     @"**Tesla**
------------------------------------------
*This schedule is **far below the minimum sleep threshold of most people** and consequently has a **very low success rate**. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule.*
------------------------------------------
**Also Known As:** Uberman 4 (U4)
**Invented By:** Sharif

**Schedule Classification:** Nap-Only
**Total Sleep:** 1h 20m
**Adaptation Difficulty:** Basically Impossible
**Sleep Specification:** 4 Naps (20m)
**Mechanism:** Four equidistant naps per day. Mixed  vitals in all 4 naps, or possibly pure-REM or pure-SWS nap.
**Ideal Scheduling:** Naps around midnight, 06:00, Noon, and 18:00.
------------------------------------------
More information about Tesla can be found [here](https://www.polyphasic.net/tesla).", "https://napchart.com/poison/Tesla-76hr7f",                                     true),
                ("uberman;u6",                                  NapGodSchedule.Category.Nap_Only,     @"**Uberman**
------------------------------------------
*This schedule is **below the minimum sleep threshold of most people** and consequently has a **very low success rate**. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule.*
------------------------------------------
**Also Known As:** Uber, U6
**Invented By:** Puredoxyk

**Schedule Classification:** Nap-Only
**Total Sleep:** 2h
**Adaptation Difficulty:** Extremely Hard
**Sleep Specification:** 6 Naps (20m)
**Mechanism:** Six equidistant naps per day. Mixed vitals are likely in 5 naps (one will likey be only SWS), though some may contain only REM.
**Ideal Scheduling:** Time slots free for rotation. Equidistant sleep is **highly** recommended.
------------------------------------------
More information about Uberman and related transitional schedules can be found [here](https://www.polyphasic.net/uberman/).", "https://napchart.com/poison/Uberman-4d1qmt",                                   true),
                ("dymax;dymaxion",                              NapGodSchedule.Category.Nap_Only,     @"**Dymaxion**
------------------------------------------
*This schedule is **below the minimum sleep threshold of most people** and consequently has a **very low success rate**. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule.*
------------------------------------------
**Also Known As:** Dymax, D4
**Invented By:** Buckminster Fuller

**Schedule Classification:** Nap-Only, Dymaxion-Line
**Total Sleep:** 2h
**Adaptation Difficulty:** Extremely Hard
**Sleep Specification:** 4 Naps (30m)
**Mechanism:** Four equidistant naps per day. Mixed vitals are possible in all 4 naps, though some may contain only REM or SWS.
**Ideal Scheduling:** Naps around midnight, 06:00, Noon, and 18:00.
------------------------------------------
More information about Dymaxion can be found [here](https://www.polyphasic.net/dymaxion).", "https://napchart.com/poison/Dymaxion-oa84uy",                                  true),
                ("naptation",                                   NapGodSchedule.Category.Nap_Only,     @"**Naptation**
------------------------------------------
*This schedule is intended as a transitional schedule only and should not be used as a permanent schedule.*
------------------------------------------
**Invented By:** Polyphasic Society

**Schedule Classification:** Nap-Only, Transitional
**Total Sleep:** <= 4h
**Adaptation Difficulty:** N/A
**Sleep Specification:** Variable number of Naps (20m)
**Mechanism:** Training schedule used by some people to train their body to fall asleep quickly during 20m naps before moving to Everyman, Dual-Core, or Tri-Core schedules. This may also be used as a transitional schedule before moving to other schedules such as SPAMAYL, Everyman, or Uberman.
**Ideal Scheduling:** Nap once every few hours (typically 1.7h). Naps may be removed as training/transition progresses.
------------------------------------------
More information about Naptation and related transitional schedules can be found [here](https://www.polyphasic.net/methods-to-prepare-for-adaptations/).", "https://www.polyphasic.net/wp-content/uploads/2021/04/naptation.png",          true),
                ("spam;spamayl",                                NapGodSchedule.Category.Nap_Only,     @"**SPAMAYL (Sleep Polyphasically As Much As You Like)**
------------------------------------------
*This schedule is **below the minimum sleep threshold of most people** and consequently has a **very low success rate**. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule.*
------------------------------------------
**Also Known As:** SPAM
**Invented By:** Rasmus

**Schedule Classification:** Nap-Only, Flexible
**Total Sleep:** ~2.5h
**Adaptation Difficulty:** Extremely Hard
**Sleep Specification:** 7-8 Naps (20m)
**Mechanism:** No rhythm, BRAC alignment, or rigidity; this schedule lets the sleeper nap for 20m whenever they become tired, which is intended to perpetually hold off the increasing homeostatic pressure. As such, the adaptee must keep a close eye on their tiredness levels and have a strong discipline with regards to napping when needed regardless of the context. If either of those fail, the homeostatic pressure may overwhelm the adaptee and cause a crash or oversleep.
**Ideal Scheduling:** Nap typically more than six times per day, whenever the adaptee is tired. SPAMAYL can be stabilized and formed into a rhythm if one has a stable work/schedule everyday (set nap times, like 2h rhythm, napping once every 1.7h at night for example). Most naps should be spread during graveyard hours to avoid the need to nap during the day with work, social life, etc.
------------------------------------------
More information about SPAMAYL can be found [here](https://www.polyphasic.net/spamayl).", "https://www.polyphasic.net/wp-content/uploads/2021/04/spamayl-default-5.png",  true),

                ("e5;everyman5",                                NapGodSchedule.Category.Everyman,     @"**Everyman 5 (E5)**
------------------------------------------
*This schedule is intended for people attempting to transition to Uberman or retain an Uberman sleep rhythm. People looking for a permanent schedule should consider E4 instead. It is also **below the minimum sleep threshold of most people** and consequently has a **very low success rate**. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule.*
------------------------------------------

**Invented By:** Tinytimrob

**Schedule Classification:** Everyman, Transitional
**Total Sleep:** 3h 10m
**Adaptation Difficulty:** Very Hard
**Sleep Specification:** 1 Core (1.5h), 5 Naps (20m)
**Mechanism:** Six equidistant sleeps per day, similar to uberman but with the pre-midnight nap as a core. Core before midnight (imperative) for SWS gain, and naps will likely be a combination of REM-only and mixed vitals.
**Ideal Scheduling:** Distribution of sleep resembles Uberman (Core in SWS peak (21:00-00:00), a graveyard, dawn, mid-morning, mid-afternoon, and dusk nap). Each sleep should be approximately 2.5-4h before the next sleep, ideally as close to equidistant as possible.
------------------------------------------
More information about Uberman and related transitional schedules can be found [here](https://www.polyphasic.net/uberman/).
More information about Everyman 5 can be found [here](https://www.polyphasic.net/everyman-5).", "https://napchart.com/poison/Everyman-5-m0jq12",                                true),
                ("e4;everyman4",                                NapGodSchedule.Category.Everyman,     @"**Everyman 4 (E4)**
------------------------------------------
*This schedule is **below the minimum sleep threshold of most people** and consequently has a **very low success rate**. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule.*
------------------------------------------
**Also Known As:** E1.5* 
||*(Deprecated Nomenclature; '1.5' is referencing core length in hours)||
**Invented By:** Puredoxyk

**Schedule Classification:** Everyman
**Total Sleep:** 2h 50m
**Adaptation Difficulty:** Very Hard
**Sleep Specification:** 1 Core (1.5h), 4 Naps (20m)
**Mechanism:** Core before midnight (imperative) for SWS gain, and the naps will likely contain only REM.
**Ideal Scheduling:** Core in SWS peak (21:00-00:00), 2 naps before 08:00, one at noon, and one in the afternoon (generally between 15:30-17:30).
------------------------------------------
More information about Everyman 4 can be found [here](https://www.polyphasic.net/everyman-4).", "https://napchart.com/poison/Everyman-4-9n2df7",                                true),
                ("e3;everyman3",                                NapGodSchedule.Category.Everyman,     @"**Everyman 3 (E3)**
------------------------------------------
**Also Known As:** Everyman
**Invented By:** Puredoxyk

**Schedule Classification:** Everyman
**Total Sleep:** 4-4.5h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 1 Core (3 or 3.5h), 3 Naps (20m)
**Mechanism:** Two-cycle core before midnight (very important) for SWS gain, and the naps will likely contain mostly REM.
**Ideal Scheduling:** Core beginning ideally at 21:00 (beginning of SWS peak), graveyard, morning, and early afternoon naps. First nap should be no more than 4.5h after the core ends, and gaps between sleeps increase in length as the day goes on.
------------------------------------------
More information about Everyman 3 can be found [here](https://www.polyphasic.net/everyman-3).", "https://napchart.com/poison/Everyman-3-50zl41",                                true),
                ("e2;everyman2",                                NapGodSchedule.Category.Everyman,     @"**Everyman 2 (E2)**
------------------------------------------
**Invented By:** Puredoxyk

**Schedule Classification:** Everyman
**Total Sleep:** 5h 10m
**Adaptation Difficulty:** Moderate
**Sleep Specification:** 1 Core (4.5h), 2 Naps (20m)
**Mechanism:** Three-cycle core beginning in SWS peak containing all SWS need and most REM need, naps may contain REM.
**Ideal Scheduling:** Core beginning between 21:00 and 02:00, first nap in the morning-noon range (no more than 4.5h after core), and second nap in the afternoon-dusk range (ideally 6-7.5h after first nap).
------------------------------------------
More information about Everyman 2 can be found [here](https://www.polyphasic.net/everyman-2).", "https://napchart.com/poison/Everyman-2-1215ia",                                true),
                ("e1;everyman1",                                NapGodSchedule.Category.Everyman,     @"**Everyman 1 (E1)**
------------------------------------------
**Also Known As:** Biphasic, E6* 
||*(Deprecated Nomenclature; '6' is referencing core length in hours)||
**Invented By:** Puredoxyk

**Schedule Classification:** Everyman, Biphasic
**Total Sleep:** 6h 20m-50m
**Adaptation Difficulty:** Moderate
**Sleep Specification:** 1 Core (6-6.5h), 1 Nap (20m)
**Mechanism:** The core is intended to contain all vital needs, with the afternoon nap having a small chance for REM. It has been speculated that extending the core by 30m may increase chances to adapt somewhat.
**Ideal Scheduling:** Core around midnight, nap around noon.
------------------------------------------
More information about Everyman 1 can be found [here](https://www.polyphasic.net/everyman-1).", "https://napchart.com/poison/Everyman-1-jle6so",                                true),
                ("trimax;trimaxion",                            NapGodSchedule.Category.Everyman,     @"**Trimaxion**
------------------------------------------
*This schedule is **below the minimum sleep threshold of most people** and consequently has a **very low success rate**. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule.*
------------------------------------------
**Also Known As:** Trimax
**Invented By:** GeneralNguyen

**Schedule Classification:** Everyman, Transitional, Dymaxion-Line
**Total Sleep:** 3h
**Adaptation Difficulty:** Very Hard
**Sleep Specification:** 1 Core (1.5h), 3 Naps (30m)
**Mechanism:** Four equidistant sleeps per day. Core before midnight (imperative) for SWS gain, and the first two naps will likely contain more REM, though all are likely to be mixed, and some may contain only REM or SWS.
**Ideal Scheduling:** Distribution of sleeps resembles Dymaxion (Core around midnight, naps around 06:00, noon, and 18:00).
------------------------------------------
More information about Trimaxion can be found [here](https://www.polyphasic.net/trimaxion).", "https://napchart.com/poison/Trimaxion-nc4doo",                                 true),
                ("seva;sevamayl",                               NapGodSchedule.Category.Everyman,     @"**SEVAMAYL (Sleep EVeryman As Much As You Like)**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule and instead stick to something better tested (such as E2 or E3).*
------------------------------------------
**Also Known As:** SEVA
**Invented By:** aethermind

**Schedule Classification:** Everyman, Flexible
**Total Sleep:** 5-7h
**Adaptation Difficulty:** Variable
**Sleep Specification:** 1 Core (4-6h), Variable number of Naps (10-20m)
**Mechanism:** Variant of SPAMAYL that includes a core. The core is intended to cover the majority of vital needs, with naps containing any missing REM. These naps can be taken any time the sleeper becomes tired, which is intended to hold off the increasing homeostatic pressure as many times as neccessary until it is more fully relieved by the core. As such, the adaptee must keep a close eye on their tiredness levels and have a strong discipline with regards to napping when needed regardless of the context. If either of those fail, the homeostatic pressure may increase and eventually cause a crash or oversleep.
**Ideal Scheduling:** 4-4.5h (sometimes longer if necessary) core around midnight, and 10-20m naps taken 2-6 times between dawn and late afternoon. Cycles may naturally compress to 80m with frequent sleeps, like E3; adaptees should plan for 90m cycles until there are many early natural wakes. Nap spacing should take into account natural periods of tiredness. In the evening 6h gaps are common, whereas alertness may wane after 3-4h awake in the morning. Some longer pronaps of <45m may be viable during REM peak (06:00-09:00) if all SWS has already been accounted for. The core might be flexed and/or varied by at most 90m in length, as optional variables after adapted to a base schedule.
------------------------------------------
More information about SEVAMAYL can be found [here](https://www.polyphasic.net/sevamayl).", "https://www.polyphasic.net/wp-content/uploads/2021/04/SEVAMAYL-1.png",         true),

                ("dc4;dualcore4",                               NapGodSchedule.Category.Dual_Core,    @"**Dual-Core 4 (DC4)**
------------------------------------------
*This schedule is intended for people attempting to transition to Uberman or retain an Uberman sleep rhythm. People looking for a permanent schedule should consider DC3 or E3 instead.*
------------------------------------------
**Invented By:** Tinytimrob

**Schedule Classification:** Dual-Core, Transitional
**Total Sleep:** 4h 20m
**Adaptation Difficulty:** Hard
**Sleep Specification:** 2 Cores (1.5h), 4 Naps (20m)
**Mechanism:** Six equidistant sleeps per day, similar to uberman but with the pre and post midnight naps as cores. First core before midnight for SWS gain, second core after midnight with mixed vitals, and naps will likely be REM-heavy. Cycle length on this schedule is likely to reduce to around 65m, so the second core might be able to be shortened to increase REM quantity in naps (although this is untested).
**Ideal Scheduling:** Distribution of sleep resembles Uberman (Cores before and after midnight, and dawn, mid-morning, mid-afternoon, and dusk naps). 3h coregap, with other sleeps approximately 2.5-4h before the next sleep, ideally as close to equidistant as possible.
------------------------------------------
More information about Uberman and related transitional schedules can be found [here](https://www.polyphasic.net/uberman/).
More information about Dual-Core 4 can be found [here](https://www.polyphasic.net/dual-core-4).", "https://napchart.com/poison/Dual-Core-4-2nx49d",                               true),
                ("dc3;dualcore3",                               NapGodSchedule.Category.Dual_Core,    @"**Dual-Core 3 (DC3)**
------------------------------------------
**Invented By:** Polyphasic Society

**Schedule Classification:** Dual-Core
**Total Sleep:** 4h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 2 Cores (1.5h), 3 Naps (20m)
**Mechanism:** First core before midnight for SWS gain, second after midnight with mixed vitals, and naps will likely be REM-heavy.
**Ideal Scheduling:** 3-5h coregap, with cores before and after midnight. Dawn (3-4h after core 2), noon, and afternoon naps. Gaps between sleeps increase in length as the day goes on.
------------------------------------------
More information about Dual-Core 3 can be found [here](https://www.polyphasic.net/dual-core-3).", "https://napchart.com/poison/Dual-Core-3-qoztbb",                               true),
                ("dc2;dualcore2",                               NapGodSchedule.Category.Dual_Core,    @"**Dual-Core 2 (DC2)**
------------------------------------------
**Invented By:** Polyphasic Society

**Schedule Classification:** Dual-Core
**Total Sleep:** 5h 10m
**Adaptation Difficulty:** Somewhat Hard
**Sleep Specification:** 2 Cores (3h & 1.5h), 2 Naps (20m)
**Mechanism:** First core before midnight (within SWS peak) for SWS gain, second core around dawn (within REM peak) for REM gain, and two naps with low potential for vitals, but possible to be REM-heavy or mixed vitals.
**Ideal Scheduling:** First Core around 22:00, second core around 5:30, noon and afternoon naps.
------------------------------------------
More information about Dual-Core 2 can be found [here](https://www.polyphasic.net/dual-core-2).", "https://napchart.com/poison/Dual-Core-2-2tfbgz",                               true),
                ("dc1;dualcore1",                               NapGodSchedule.Category.Dual_Core,    @"**Dual-Core 1 (DC1)**
------------------------------------------
**Invented By:** Polyphasic Society

**Schedule Classification:** Dual-Core
**Total Sleep:** 5h 20m
**Adaptation Difficulty:** Moderate
**Sleep Specification:** 2 Cores (3.5h & 1.5h *or* 3h 20m and 1h 40m)
**Mechanism:** First core in the SWS peak to meet SWS need, second core in the REM peak to meet REM need, and the nap is unlikely to contain vitals.
**Ideal Scheduling:** First core in the SWS peak, second in the REM peak, and the nap between noon and the late afternoon.
------------------------------------------
More information about Dual-Core 1 can be found [here](https://www.polyphasic.net/dual-core-1).", "https://napchart.com/poison/Dual-Core-1-472gdm",                               true),
                ("bimax;bimaxion",                              NapGodSchedule.Category.Dual_Core,    @"**Bimaxion**
------------------------------------------
*This schedule is intended for people attempting to transition to Dymaxion or retain a Dymaxion sleep rhythm. People looking for a permanent schedule should consider DC3 or E3 instead.*
------------------------------------------
**Also Known As:** Bimax, Quad, Quadphasic
**Invented By:** GeneralNguyen

**Schedule Classification:** Dual-Core, Transitional, Dymaxion-Line
**Total Sleep:** 4h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 2 Cores (1.5h), 2 Naps (30m)
**Mechanism:** Four equidistant sleeps per day. First core before midnight for SWS gain, second around dawn for REM gain. The second nap may contain mixed stages. This schedule is more flexible than Trimaxion and significantly more flexible than Dymaxion due to the elevated TST and dual cores which increases consistency and ease of meeting vital needs. As a result, naps can be flexed somewhat.
**Ideal Scheduling:** Distribution of sleeps resembles Dymaxion (First core around midnight, second around 06:00, and naps at noon and 18:00).
------------------------------------------
More information about Bimaxion can be found [here](https://www.polyphasic.net/bimaxion).", "https://napchart.com/poison/Bimaxion-uif902",                                  true),
                ("duca;ducamayl",                               NapGodSchedule.Category.Dual_Core,    @"**DUCAMAYL (DUal-Core As Much As You Like)**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule and instead stick to something better tested (such as DC1 or DC2).*
------------------------------------------
**Also Known As:** DUCA
**Invented By:** The Discord Polyphasic Community

**Schedule Classification:** Dual-Core, Flexible
**Total Sleep:** 5-6.5h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 2 Cores (1.5-4.5h), Variable number of Naps (10-20m)
**Mechanism:** Variant of SPAMAYL that includes two cores. The cores are intended to cover the majority of vital needs (night core covering more SWS and morning core covering more REM), with naps containing any missing REM. These naps can be taken any time the sleeper becomes tired, which is intended to hold off the increasing homeostatic pressure as many times as neccessary until it is more fully relieved by the cores. As such, the adaptee must keep a close eye on their tiredness levels and have a strong discipline with regards to napping when needed regardless of the context. If either of those fail, the homeostatic pressure may increase and eventually cause a crash or oversleep.
**Ideal Scheduling:** A 3-4.5h (sometimes longer if necessary) core around midnight, a 1.5-3h core around dawn, and 10-20m naps taken 1-3 times between dawn and late afternoon. Nap spacing should take into account natural periods of tiredness. Some longer pronaps of 30-40m may be viable during the morning hours (06:00-10:00) if all SWS has already been accounted for. The core might be flexed and/or varied by at most 90m in length, as optional variables after adapted to a base schedule, and coregap length may be adjusted as well.
------------------------------------------
More information about DUCAMAYL can be found [here](https://www.polyphasic.net/ducamayl).", "https://www.polyphasic.net/wp-content/uploads/2021/04/ducamayl-default-2.png", true),

                ("tc3;tricore3",                                NapGodSchedule.Category.Tri_Core,     @"**Tri-Core 3 (TC3)**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule. This schedule is intended for people attempting to transition to Uberman or retain an Uberman sleep rhythm. People looking for a permanent schedule should consider TC2 or E2 instead.*
------------------------------------------
**Also Known As:** Triphasic 3
**Invented By:** Poison

**Schedule Classification:** Tri-Core, Transitional
**Total Sleep:** 5.5h
**Adaptation Difficulty:** Unsure (Likely Moderate)
**Sleep Specification:** 3 Cores (1.5h), 3 Naps (20m)
**Mechanism:** Six equidistant sleeps per day, similar to uberman but with the pre-midnight, midnight, and post-midnight naps as cores. First core for SWS gain, second and third for REM gain, and naps may contain REM.
**Ideal Scheduling:** Distribution of sleep resembles uberman (Cores around dusk, midnight, and graveyard hours, and morning, noon, and afternoon naps). Sleeps approximately 2.5-4h before the next sleep, ideally as close to equidistant as possible.
------------------------------------------", "https://napchart.com/poison/Tri-Core-3-d79jgDTxA",                             false),
                ("tc2;tricore2",                                NapGodSchedule.Category.Tri_Core,     @"**Tri-Core 2 (TC2)**
------------------------------------------
**Also Known As:** Triphasic 2
**Invented By:** LichTerLoh

**Schedule Classification:** Tri-Core
**Total Sleep:** 5h 10m
**Adaptation Difficulty:** Moderate
**Sleep Specification:** 3 Cores (1.5h), 2 Naps (20m)
**Mechanism:** First core around dusk with mixed vitals, second core around midnight for SWS gain, and third around dawn for REM gain, and two naps that may contain REM. 
__Alternative scheduling:__ First core after dusk (near or within the SWS peak) for SWS gain, second (graveyard) and third (morning) for REM gain, and naps may contain REM.
**Ideal Scheduling:** First core around dusk, second around midnight, and third around dawn, with a mid-morning and early afternoon naps.
__Alternative scheduling:__ First core after dusk (near or entering the SWS peak), second in the graveyard hours, and third in the morning, with noon and afternoon naps.

In either case, cores are optimally scheduled equidistantly with about 4.5h between them, and the naps equidistant between the first and third cores. 
------------------------------------------
More information about Tri-Core 2 can be found [here](https://www.polyphasic.net/tri-core-2).", "https://napchart.com/poison/Tri-Core-2-4j18xz",                                true),
                ("tc1;tricore1",                                NapGodSchedule.Category.Tri_Core,     @"**Tri-Core 1 (TC1)**
------------------------------------------
**Also Known As:** Triphasic 1
**Invented By:** GeneralNguyen

**Schedule Classification:** Tri-Core
**Total Sleep:** 4h 50m
**Adaptation Difficulty:** Moderate
**Sleep Specification:** 3 Cores (1.5h), 1 Nap (20m)
**Mechanism:** First core before midnight for SWS gain, second (graveyard) and third (dawn) for REM gain, and the afternoon nap may contain REM. This schedule is likely more flexible than Dual-Core schedules due to the additional core providing more comfortable vital gains. All sleeps are likely to be comfortably shifted once adapted.
**Ideal Scheduling:** All cores at night to boost daytime alertness. First core in the SWS peak before midnight, second after midnight, and third in the REM peak around dawn, with the nap in the early afternoon. Cores can be spaced equidistantly between each other (with at least 2h between), and the nap can be placed equidistantly between the third and first cores.
------------------------------------------
More information about Tri-Core 1 can be found [here](https://www.polyphasic.net/tri-core-1).", "https://napchart.com/poison/Tri-Core-1-j4d3br",                                true),
                ("triphasic;tc0;tricore0;tricore",              NapGodSchedule.Category.Tri_Core,     @"**Triphasic**
------------------------------------------
**Also Known As:** Tri-Core 0 (TC0)
**Invented By:** Leif Weaver

**Schedule Classification:** Tri-Core
**Total Sleep:** 4.5h
**Adaptation Difficulty:** Somewhat Hard
**Sleep Specification:** 3 Cores (1.5h)
**Mechanism:** First core before midnight for SWS gain, second around dawn for REM gain, and afternoon core with mixed vitals.
**Ideal Scheduling:** Cores in the REM and SWS peaks, and another starting at some point in the 11:00-3:30 range. Can be scheduled equidistantly.
------------------------------------------
More information about Triphasic can be found [here](https://www.polyphasic.net/triphasic).", "https://napchart.com/poison/Triphasic-ucsl31",                                 true),
                ("monomax;monomaxion",                          NapGodSchedule.Category.Tri_Core,     @"**Monomaxion**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule. This schedule is intended for people attempting to transition to Dymaxion or retain an Dymaxion sleep rhythm. People looking for a permanent schedule should consider TC1 instead.*
------------------------------------------
**Also Known As:** Monomax
**Invented By:** Poison

**Schedule Classification:** Tri-Core, Transitional, Dymaxion-Line
**Total Sleep:** 5h
**Adaptation Difficulty:** Unsure (Likely Moderate)
**Sleep Specification:** 3 Cores (1.5h), 1 Nap (30m)
**Mechanism:** Four equidistant sleeps per day, similar to dymaxion but with the non-noon naps as cores. First core around dusk with mixed vitals, second core around midnight for SWS gain, and third around dawn for REM gain, and the noon nap may contain REM. This schedule is likely more flexible than Dual-Core schedules due to the additional core providing more comfortable vital gains. All sleeps are likely to be comfortably shifted once adapted.
**Ideal Scheduling:** Distribution of sleep resembles Dymaxion (Cores at 18:00, midnight, 06:00, and a nap at noon). 
------------------------------------------", "https://napchart.com/poison/Monomaxion-AQwOScnFT",                             false),
                ("trica;tricamayl;tricamayo",                   NapGodSchedule.Category.Tri_Core,     @"**TRICAMAYL (TRI-Core As Much As You Like)**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule and instead stick to something better tested (such as TC1 or TC2).*
------------------------------------------
**Also Known As:** TRICA, TRICAMAYO (TRI-Core As Much As You Ought)
**Invented By:** Eloh

**Schedule Classification:** Tri-Core, Flexible
**Total Sleep:** 5-7h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 3 Cores (1.5-3h), Variable number of Naps (10-20m)
**Mechanism:** Variant of SPAMAYL that includes three cores. The first (pre-midnight) and second (graveyard hours) cores cover SWS needs (though they may both contain low amounts of REM). The third core covers REM needs, with naps generally having no vitals (though REM may occur). These naps can be taken any time the sleeper becomes tired, which is intended to hold off the increasing homeostatic pressure as many times as neccessary until it is more fully relieved by the cores. As such, the adaptee must keep a close eye on their tiredness levels and have a strong discipline with regards to napping when needed regardless of the context. If either of those fail, the homeostatic pressure may increase and eventually cause a crash or oversleep.
**Ideal Scheduling:** A 1.5-3h (sometimes longer if necessary) core before midnight, during graveyard hours, and at dawn, and 10-20m naps taken 1-3 times between dawn and late afternoon. Nap spacing should take into account natural periods of tiredness. The core might be flexed and/or varied by at most 90m in length, as optional variables after adapted to a base schedule, and coregap lengths may be adjusted slightly as well.
------------------------------------------", "",                                                                             false),
                ("trix;triphasicx",                             NapGodSchedule.Category.Tri_Core,     @"**Triphasic-X (Tri-X)**
------------------------------------------
*This schedule is **experimental** and **not well tested**, with the specifics still being under debate and scrutiny. It is **strongly recommended** that inexperienced polyphasic sleepers do **NOT** attempt this schedule and instead stick to something better tested (such as TC1 or TC2).*
------------------------------------------
**Also Known As:** Tri-X
**Invented By:** PolyWiki Community

**Schedule Classification:** Tri-Core, Flexible
**Total Sleep:** 5-9h
**Adaptation Difficulty:** Hard
**Sleep Specification:** 3 Sleeps (20m-4.5h)
**Mechanism:** Variant of BiphasicX that includes three sleeps. The first (pre-midnight) core covers SWS needs (though it may contain low amounts of REM). The second (dawn hours) core covers REM needs, with third sleep (core or nap) containing low vitals, with REM possibly occurring in naps and both SWS and REM in cores. These sleeps can be taken any time the sleeper becomes tired, which is intended to hold off the increasing homeostatic pressure until it is more fully relieved by the cores. As such, the adaptee should pay attention to their tiredness levels and have discipline with regards to napping when needed regardless of the context.
**Ideal Scheduling:** A 1.5-4.5h (core before midnight and at dawn, and a 10m-3h core between mid-morning and late afternoon. Sleep spacing should take into account natural periods of tiredness. The sleeps might be flexed and/or varied by at most 90m in length, as optional variables after adapted to a base schedule, and coregap lengths may be adjusted slightly as well.
------------------------------------------", "",                                                                             false)
            };
        }
    }
}