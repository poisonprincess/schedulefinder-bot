﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static PolyphasicScheduleFinder_Bot.NapGodSchedule;

namespace PolyphasicScheduleFinder_Bot
{
	class NapGodCommand : Command
    {
		#region attributes
		/// <summary> updates any local stored config variables </summary>
		internal static void updateConfigVars()=> _alert = Bot.getEmote(Config._emojiNames["Alert"]);
        private bool _scheduledataCreated = false, _altScheduledataCreated = false;
		#endregion

		#region commands
		#region nc
		/// <summary> Returns napchart image | +nc [napchart link] </summary> 
		public static void nc(SocketMessage message)
        {
            string link = message.Content.Contains(' ') ? message.Content[message.Content.IndexOf(" ")..].Trim() : message.Content.Trim();

            if (NapchartInterface.isLinkValidNapchart(link))
            {
                message.DeleteAsync().Wait();
                string image = NapchartInterface.getChartImg(link);
                Bot.sendEmbed(message.Channel, 
                    color: Napchart.getDominantColor(NapchartInterface.getNapchartFromLink(link)),
					imageURL: image, 
                    author: Bot._client.GetGuild(Bot.getServer(message.Channel)._id).GetUser(message.Author.Id));
            }
            else Bot.send(message, "Invalid Napchart Link.");
        }

        public static void nc(SocketSlashCommand sInt)
		{
			string link = sInt.Data.Options.First().Value.ToString().Trim();
            Server server = Bot.getServer(sInt.Channel);
			SocketGuildUser author = Bot._client.GetGuild(server._id).GetUser(sInt.User.Id);

			if (NapchartInterface.isLinkValidNapchart(link))
			{
                EmbedBuilder embed = new()
                {
					Color = Napchart.getDominantColor(NapchartInterface.getNapchartFromLink(link)),
					ImageUrl = NapchartInterface.getChartImg(link),
					Author = new EmbedAuthorBuilder()
                    {
                        IconUrl = author.GetAvatarUrl(),
                        Name = $"Requested by: {Bot.getUsersName(author, server)}"
                    }
				};

				sInt.RespondAsync(embed: embed.Build()).Wait();
			}
			else sInt.RespondAsync("Invalid Napchart Link.", ephemeral: true);
		}
		#endregion

		#region set
		internal List<(string command, IUser author)> _setMergeCommands = new();
        public static void set(SocketMessage message)
        {
            bool none = false;
            string content = message.Content;

            if (content.ToLower().Trim().StartsWith($"{Config._napGodCommandPrefixes["NapGod Command Prefix"]}set none"))
				none = true;

			List<string> arguments = content.Split(' ').ToList();
			string schedule = "", napchartLink = "", scheduleName = "";
			Modifier modifier = Modifier.none;

            if (!none)
            {
                //Combines schedule and nap count if separated by spaces (e 2 => e2)
                if (arguments.Count == 3 && arguments[2].Length == 1 && int.TryParse(arguments[2], out int napCount))
                {
                    arguments[1] += napCount;
                    arguments.RemoveAt(2);
                }

                //Has Napchart or schedule, get value
                if (arguments.Count == 2)
                {
                    if (arguments[1].ToLower().Contains("napchart")) napchartLink = arguments[1].Trim();
                    else schedule = arguments[1].Replace(" ", "");
                }
                //Has Napchart and schedule, get schedule and napchart values
                else if (arguments.Count == 3)
                {
                    if (arguments[1].ToLower().Contains("napchart"))
                    {
                        schedule = arguments[2].Replace(" ", "");
                        napchartLink = arguments[1].Trim();
                    }
                    else if (arguments[2].ToLower().Contains("napchart"))
                    {
                        schedule = arguments[1].Replace(" ", "");
                        napchartLink = arguments[2].Trim();
                    }
                }

                //Has a potential schedule name
                if (schedule != "")
                {
                    int dashCount = schedule.Trim().Length - schedule.Trim().Replace("-", "").Length;
                    //Schedule name has modifier (or is biphasic-x), figure out which and get modifier if there is one
                    if (dashCount > 0)
                    {
                        string[] scheduleParameters = schedule.Split('-');

                        if (dashCount > 1)
                        {
                            scheduleName = scheduleParameters[0].Trim().ToLower() + scheduleParameters[1].Trim().ToLower();
                            if (!Enum.TryParse(scheduleParameters[2].Trim().ToLower(), out Modifier mod))
                            {
                                if (Enum.TryParse("_" + scheduleParameters[2].Trim().ToLower(), out mod))
                                    modifier = mod;
                            }
                            else modifier = mod;
                        }
                        else
                        {
                            scheduleName = Enum.TryParse(scheduleParameters[1].Trim().ToLower(), out modifier) || Enum.TryParse("_" + scheduleParameters[1].Trim().ToLower(), out modifier)
                                ? scheduleParameters[0].Trim().ToLower()
                                : scheduleParameters[0].Trim().ToLower() + scheduleParameters[1].Trim().ToLower();
                        }
                    }
                    else scheduleName = schedule.Trim().ToLower();
                }
            }
            else scheduleName = "none";

			Bot._ngc.Set(scheduleName, message.Channel, Bot.getGuildUser(message.Channel.Id, message.Author.Id), modifier, napchartLink, message: message);
        }

        /// <summary> Sets a user's username and roles to the included schedule | +set [schedule name] </summary>
        public async void Set(string scheduleName, IMessageChannel channel, IGuildUser user, Modifier modifier = Modifier.none, string napchartLink = "", SocketMessage message = null, SocketSlashCommand sc = null)
        {
            bool consoleWrite = true;
            bool none = scheduleName == "none";

            if (message != null && message.Content.ToLower().Trim().StartsWith($"{Config._napGodCommandPrefixes["NapGod Command Prefix"]}set adapted"))
            {
                Bot.send(message, "No.", true, message);
                return;
            }

			NapGodSchedule ngSchedule;
			bool validSchedule = (ngSchedule = getNapGodSchedule(scheduleName.ToLower())) != null;
            bool containsNapchart = !string.IsNullOrEmpty(napchartLink);

			//Valid schedule or napchart to update
			if (none || validSchedule || containsNapchart)
            {
                IRole role;
                int mergeSQLIndex = 0;
                List<bool> successes = new() { false, false, false, false, false, false, false };
                string napchartSetError = "", duplicateSchedule = "", nicknameText = "", scheduleText = "", newNickname = "", descriptionText = "";
                bool joker = validSchedule && ((ngSchedule._categoryRole._category == Category.Random && modifier == Modifier.flipped) ||
                    (ngSchedule._categoryRole._category == Category.Nap_Only && (modifier == Modifier.shortened || modifier == Modifier._short || modifier == Modifier.flipped)));

				#region changes
				//Create new nickname if changing it
				if (!none && validSchedule)
                {
                    nicknameText = (joker ? "🤡 " : "") + (user.Nickname != null ? Bot.getNicknameWithoutSchedule(user) : user.Username);
                    scheduleText = " [" + ngSchedule._nameClean;
                    scheduleText += modifier != Modifier.none ? (
                        modifier == Modifier._ext ? "-Extended" :
                        modifier == Modifier._short ? "-Shortened" :
                        modifier == Modifier._mod ? "-Modified" :
                        $"-{char.ToUpper(modifier.ToString()[0]) + modifier.ToString()[1..]}") : "";
                    scheduleText += "]";
                    if ((nicknameText + scheduleText).Length > 32)
                    {
                        nicknameText = nicknameText[..(32 - scheduleText.Length)];
                        successes[5] = true; //Shortened Nickname
                    }

                    newNickname = (nicknameText + scheduleText).Trim();
                }

                #region Database update [0]
                string dbName = "scheduledata";
                (ulong serverID, string newDBName) = Config._dbSwap.Find(d => d.serverID == Bot.getGuild(channel.Id).Id);
                if (serverID > 0) dbName = newDBName;
                else serverID = Bot.getGuild(channel.Id).Id;

				if (napchartLink != "")
				{
                    if (!NapchartInterface.isLinkValidNapchart(napchartLink))
                    {
                        napchartSetError = "`Invalid napchart link.`";
                        successes[6] = true; //one or more errors; invalid napchart link
                    }
                    else if (NapchartInterface.doesNapchartDataContainProfanity(napchartLink))
                    {
                        napchartSetError = "`Napchart text invalid, please try revising content.`";
                        successes[6] = true; //one or more errors; napchart contains profanity
                    }
                }

                if (napchartSetError == "")
                {
                    createScheduleDataTables(serverID > 0, serverID > 0 ? serverID : Bot.getGuild(channel.Id).Id);
                    //If not in db, add user
                    if (Bot._msm.executeQuery($"SELECT `userID` FROM `{dbName}`.`users` WHERE `userID` = {user.Id};").Tables?[0].Rows.Count == 0)
                    {
                        if (!Bot._msm.executeNonQuery($"INSERT INTO `{dbName}`.`users` (`userID`, `dateAdded`) VALUES ({user.Id}, current_timestamp());"))
                        {
                            napchartSetError = "Unable to add user to database.";
                            successes[6] = true; //one or more errors; unable to add user to db
                            MM.Print(consoleWrite, $"Unable to add user [{user.Username}][{user.Id}] to DB.");
                        }
                    }

                    //User in db already or adding succeeded
                    if (!successes[6])
                    {
                        if (!none)
                        {
                            if (!validSchedule)
                            {
                                if (user.Nickname != null)
                                {
                                    newNickname = user.Nickname;
                                    scheduleName = Bot.getNicknameWithoutSchedule(user);
                                }
                            }
                            else scheduleName = scheduleText.Replace("[", "").Replace("]", "").Trim();
                        }
                        else
                        {
                            scheduleName = "None";

                            if (user.Nickname != null && user.Nickname.Contains('[') && user.Nickname.EndsWith("]"))
							{
                                DataSet r = Bot._msm.executeQuery($"SELECT `userNickname` FROM `{dbName}`.`schedules` WHERE `userID` = {user.Id} ORDER BY `setDate` DESC LIMIT 1;");
                                DataRowCollection nicknameHist = null;

                                if (r.Tables != null && r.Tables.Count > 0 && (nicknameHist = r.Tables[0].Rows) != null && nicknameHist.Count > 0)
                                {
                                    string currentNickname = Bot.getNicknameWithoutSchedule(user);
                                    List<(string nickname, int levenshteinDistance)> nicknames = new();

                                    foreach (object nickname in nicknameHist)
                                    {
                                        if (!string.IsNullOrWhiteSpace(nickname.ToString()))
										{
                                            string nicknameClean = Bot.getNicknameWithoutSchedule(nickname.ToString());

                                            if (currentNickname.Length < nicknameClean.Length && nicknameClean.Contains(currentNickname))
                                                nicknames.Add((nickname.ToString(), levenshteinDistance(currentNickname, nicknameClean)));                                            
										}
									}

                                    newNickname = (nicknames.Count == 0 ? currentNickname : 
                                        nicknames.OrderBy(n => n.levenshteinDistance).First().nickname);
                                }
                            }
                        }

                        DataSet result = Bot._msm.executeQuery($"SELECT `entryID`, `scheduleName`, `napchartLink`, `setDate`, `napchartSetDate` FROM `{dbName}`.`schedules` WHERE `userID` = {user.Id} ORDER BY `setDate` DESC LIMIT 1;");
                        DataRowCollection scheduleHist = null;

                        bool hasData = result.Tables != null && result.Tables.Count > 0 && (scheduleHist = result.Tables[0].Rows) != null && scheduleHist.Count > 0;

                        if (Bot._msm.executeNonQuery($"INSERT INTO `{dbName}`.`schedules` (`userID`, `userNickname`, `scheduleName`, `napchartLink`, `setDate`, `napchartSetDate`) VALUES ({user.Id}, '{newNickname}', '{(!string.IsNullOrEmpty(scheduleName) ? scheduleName : "none")}', '{napchartLink}', current_timestamp(), {(!string.IsNullOrWhiteSpace(napchartLink) ? "current_timestamp()" : "null")});"))
                            successes[0] = true;
                        else
                        {
                            napchartSetError = "`Unable to update schedule and napchart in database.`";
                            successes[6] = true; //one or more errors; could not add entry to db
                        }
                                
                        if (hasData && (scheduleHist[0]["scheduleName"] != null && scheduleHist[0]["scheduleName"].ToString() == scheduleName)) //Same schedule as previous schedule
                        {
                            _setMergeCommands.Add(($"DELETE FROM `{dbName}`.`schedules` WHERE `entryID` = (SELECT `entryID` FROM (SELECT `entryID` FROM `{dbName}`.`schedules` WHERE `userID` = {user.Id} ORDER BY `setDate` DESC LIMIT 1) E);{(napchartLink != "" ? $"\r\nUPDATE `scheduledata`.`schedules` SET `napchartLink` = '{napchartLink}', `napchartSetDate` = current_timestamp() WHERE `entryID` = {scheduleHist[0]["entryID"]};" : "")}", user));
                            mergeSQLIndex = _setMergeCommands.Count - 1;

                            duplicateSchedule += $"The schedule you are attempting to set `[{scheduleName}]` is the same as the previous " +
								$"{(!string.IsNullOrWhiteSpace(scheduleHist[0]["napchartLink"].ToString()) ? $"[schedule]({scheduleHist[0]["napchartLink"]})" : "schedule")} you attempted (`[{scheduleHist[0]["scheduleName"]}]` started `{scheduleHist[0]["setDate"]}`).\n\n" +
                                $"Would you like to **merge the entries**{(napchartLink != "" ? " (keeping the most recent napchart),\n" : ", ")}or **create a new entry**?\n\n";

                            successes[6] = true; //one or more errors; duplicate schedule entry
						}                                    
                    }
                }
				#endregion

				//Nickname and roles update
				if (validSchedule)
                {
                    bool wasAdapted = false;

                    //Nickname update [1]
                    try
                    {
                        await user.ModifyAsync(n => n.Nickname = newNickname);
                        MM.Print(consoleWrite, $"Updated {user.Username}'s nickname to: [{newNickname}]");
                        successes[1] = true; //Successfully updated nickname
                    }
                    catch
                    {
                        MM.Print(consoleWrite, $"Issue updating {user.Username}'s Nickname to [{newNickname}]");
                        //Didn't shorten since nickname wasn't updated
                        successes[6] = true; //One or more errors
                    }

                    //Attempted-Schedule Role add [2]
                    ulong attSchedRoleID = 0;
                    if (ngSchedule._role._id > 0 && ((role = user.Guild.GetRole(ngSchedule._role._id)) != null || 
                        (role = Bot.getRole($"Attempted-{ngSchedule._nameClean.Replace("-", "")}", user.Guild.Id)) != null))
                    {
                        attSchedRoleID = ngSchedule._role._id;
                        if (!user.Guild.Roles.ToList().Exists(r => r.Id == attSchedRoleID)) attSchedRoleID = Bot.getRole($"Attempted-{ngSchedule._nameClean.Replace("-", "")}", user.Guild.Id).Id;

                        try
                        {
                            await user.AddRoleAsync(role);
                            MM.Print(consoleWrite, $"Gave {user.Username} the role: @{role.Name}");
                            successes[2] = true; //Successfully set schedule role
                        }
                        catch
                        {
                            MM.Print(consoleWrite, $"Unable to give {user.Username} the role: @{role.Name}");
                            successes[6] = true; //One or more errors
                        }

                        role = null;
                    }
                    else if (ngSchedule._role._id == 0) successes[2] = true; //No attempted-schedule role for this schedule
                    else successes[6] = true; //One or more errors; didn't find role

                    //Category Role removal+add [3]
                    ulong categoryRoleID = 0;
                    if (ngSchedule._categoryRole._id > 0 &&
                        ((role = user.Guild.GetRole(ngSchedule._categoryRole._id)) != null ||
                        (role = Bot.getRole(ngSchedule._categoryRole._name, user.Guild.Id)) != null))
                    {
                        foreach (NapGodRole r in Config._napGodRoles)
                        {
                            ulong toRemove = user.RoleIds.ToList().Find(rID => rID == r._id);
                            if (toRemove != 0 && r._name != role.Name)
                                user.RemoveRoleAsync(user.Guild.GetRole(toRemove)).Wait();
                        }

                        categoryRoleID = ngSchedule._categoryRole._id;
                        if (!user.Guild.Roles.ToList().Exists(r => r.Id == categoryRoleID)) categoryRoleID = Bot.getRole(ngSchedule._categoryRole._name.Replace("-", " "), user.Guild.Id).Id;

                        try
                        {
                            await user.AddRoleAsync(role);
                            MM.Print(consoleWrite, $"Gave {user.Username} the role: @{role.Name}");
                            successes[3] = true; //Successfully set category role
                        }
                        catch
                        {
                            MM.Print(consoleWrite, $"Unable to give {user.Username} the role: @{role.Name}");
                            successes[6] = true; //One or more errors
                        }

                        role = null;
                    }
                    else successes[6] = true; //One or more errors; didn't find category role

                    //Adapted Role removal [4]
                    ulong adaptedRoleID = 0;
                    if (((role = Bot.getRole("Currently Adapted", serverID)) != null ||
                        (role = Bot.getRole("Currently Adapted", user.Guild.Id)) != null)
                        && user.RoleIds.ToList().Exists(rID => rID == role.Id))
                    {
                        adaptedRoleID = role.Id;
                        if (!user.Guild.Roles.ToList().Exists(r => r.Id == adaptedRoleID)) adaptedRoleID = Bot.getRole("Currently Adapted", user.Guild.Id).Id;
                        wasAdapted = true;

                        try
                        {
                            await user.RemoveRoleAsync(role);
                            MM.Print(consoleWrite, $"Removed {user.Username}'s role: @{role.Name}");
                            successes[4] = true; //Successfully removed adapted role
                        }
                        catch
                        {
                            MM.Print(consoleWrite, $"Unable to remove {user.Username}'s role: @{role.Name}");
                            successes[6] = true; //One or more errors
                        }
                    }
                    else successes[4] = true; //Did not need to remove adapted role

                    //Build embed description with errors/successes
                    if (joker) descriptionText += "✅ Joke schedule, very funny :thumbsup:\n";
                    if (validSchedule && !successes[1]) descriptionText += $"{_alert} Unable to update your nickname.\n";
                    if (ngSchedule._role._id != 0) descriptionText += $"{(successes[2] ? "✅ You now have the" : $"{_alert} Unable to give you the")} <@&{(attSchedRoleID == 0 ? ngSchedule._role._id : attSchedRoleID)}> role.\n";
                    if (!successes[3]) descriptionText += $"{_alert} Unable to give you the <@&{(categoryRoleID == 0 ? ngSchedule._categoryRole._id : categoryRoleID)}> role.\n";
                    if (wasAdapted) descriptionText += successes[4] ? "✅ You are no longer Adapted.\n" : $"{_alert} Unable to remove your {(adaptedRoleID == 0 ? "@Currently Adapted" : $"<@&{adaptedRoleID}>")} role.\n";
                    if (successes[5]) descriptionText += $"⚠️ Your username had to be shortened because it was too long. Please contact moderators if you would like it modified.\n";
                }
                else successes[1] = successes[2] = successes[3] = successes[4] = successes[5] = true; //Nothing else to update
				#endregion

				ComponentBuilder buttons = null;

                if (duplicateSchedule != "")
				{
                    descriptionText += $"\n⚠️ {duplicateSchedule}";

                    buttons = new ComponentBuilder()
                        .WithButton("Merge", customId: $"Set:Merge:{mergeSQLIndex}", style: ButtonStyle.Primary, row: 0)
                        .WithButton("Create New", customId: $"Set:CreateNew:{mergeSQLIndex}", style: ButtonStyle.Success, row: 0);
                }
                else //Build embed description with errors/successes
                {
                    if (containsNapchart && successes[0]) descriptionText += "✅ Successfully updated your Napchart.\n";
                    else if (!successes[0]) descriptionText += $"{_alert} Unable to update database entry.\n__**ERROR:**__ {napchartSetError}\n\n";
                }

                int issueCount = successes.GetRange(0, 5).FindAll(e => e == false).Count + (duplicateSchedule != "" ? 1 : 0);

                EmbedBuilder embed = new()
                {
                    Color = successes[6] ? (!successes[1] || !successes[2] ? Discord.Color.Red : Discord.Color.Orange) : Discord.Color.Green,
                    Title = successes[6] ? (!successes[1] || !successes[2] ? "Unable To Set Schedule" : $"Schedule Set - {issueCount} Issue{(issueCount == 1 ? "" : "s")}") : "Schedule Set",
                    Description = descriptionText,
                    ImageUrl = containsNapchart ? NapchartInterface.getChartImg(napchartLink) : null,
                    Author = new()
                    {
                        Name = user.DisplayName,
                        IconUrl = user.GetDisplayAvatarUrl()
                    }
                };

                if (message != null) message.Channel.SendMessageAsync(embed: embed.Build(), components: buttons?.Build()).Wait();
                else sc.RespondAsync(embed: embed.Build(), components: buttons?.Build()).Wait();
                return;
            }

            //Send error message
            ComponentBuilder button = new();
            button.WithButton("Schedules", customId: "Set:Schedules", ButtonStyle.Secondary, row: 0);

            if (message != null) Bot.sendEmbed(channel, Discord.Color.Red, title: $"Set Command Invalid Input", description: $@"{_alert} **Improper Format**

**Proper Format:** `+set [schedule name] [napchart link]`, `+set [schedule name]`, or `+set [napchart link]`
For a list of documented schedules, type `+schedules` or click the ""Schedules"" button below.


**You may set a schedule modifier after a dash such as:** `+set DC1-extended`
Supported schedule variants are: 
`shortened`, `extended`, `flipped`, `modified`, `recovery`.


*Your schedule not listed? Feel free to consult an advisor or use `+set Experimental`.*", components: button);
		}
		internal static int levenshteinDistance(string target, string comparitor)
		{
			int tL = target.Length;
			int cL = comparitor.Length;
			int[,] d = new int[tL + 1, cL + 1];
			if (tL == 0) return cL;
			if (cL == 0) return tL;

			for (int i = 0; i <= tL; d[i, 0] = i++) ;
			for (int j = 0; j <= cL; d[0, j] = j++) ;
			for (int i = 1; i <= tL; i++)
			{
				for (int j = 1; j <= cL; j++)
				{
					int cost = (comparitor[j - 1] == target[i - 1]) ? 0 : 1;
					d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + cost);
				}
			}

			return d[tL, cL];
		}
		#endregion

		#region get
		public static void get(SocketMessage message)
        {
            string msg = message.Content[4..].Trim();
            IUser target;
            IGuild server = Bot.getGuild(message.Channel.Id);

            if (msg.Length == 0) target = message.Author;
            else if (message.MentionedUsers.Count == 1) target = message.MentionedUsers.First();
            else
			{
                List<IGuildUser> users = Bot.getUser(msg, server.Id);
                if (users.Count == 1) target = users.First();
                else
				{
                    ComponentBuilder componentMenu = null;

                    if (users.Count > 1)
					{
                        List<SelectMenuOptionBuilder> options = new();

                        foreach (IGuildUser user in users.GetRange(0, users.Count > 25 ? 25 : users.Count))
						{
                            if (user != null)
							{
                                options.Add(new SelectMenuOptionBuilder()
                                {
                                    Label = user.Nickname ?? user.Username,
                                    Value = $"Get:{user.Id}"
                                });
							}
                        }

                        componentMenu = new ComponentBuilder().WithSelectMenu(new SelectMenuBuilder($"Get:{Bot.getMessageLink(message)}", options, "Select a user."));
                    }

                    Bot.sendEmbed(message.Channel,
                        users.Count == 0 ? Discord.Color.Red : Discord.Color.Orange, 
                        title: $"{_alert} Get Error {_alert}",
                        description: users.Count == 0 ? 
                            $"No users found that match `[{msg}]`.\n*Try entering their full Username or Nickname, or tag them.*" :
                        users.Count <= 25 ? 
                            $"More than one user found that matches `[{msg}]`, please select one from the menu below." :
                            $"Too many users found that match `[{msg}]`.\n" +
							$"*If the user you are looking for is not in the list, try entering their full Username or Nickname, or tag them.*",
                        components: componentMenu);
                    return;
				}
            }

            if (target != null) Bot._ngc.Get(target, message: message);
        }

        public void Get(IUser target, IDiscordInteraction intrct = null, SocketMessage message = null)
        {
            ulong channelID = message != null ? message.Channel.Id : intrct.ChannelId.Value;
            IGuildUser user = Bot.getGuildUser(channelID, target.Id);
			IGuild server = Bot.getGuild(channelID);
            (ulong serverID, string newDBName) = Config._dbSwap.Find(d => d.serverID == server.Id);

            createScheduleDataTables(serverID > 0, serverID > 0 ? serverID : server.Id);

            string scheduleName, napchartLink, setDate, napchartSetDate;
            string dbName = !string.IsNullOrEmpty(newDBName) ? newDBName : "scheduledata";

            //Make Schedulehelper Schedule if necessary
            if (target.Id == Bot._client.CurrentUser.Id) setScheduleHelperSchedule(server, serverID, dbName);

            DataSet result = Bot._msm.executeQuery($"SELECT `scheduleName`, `napchartLink`, `setDate`, `napchartSetDate` FROM `{dbName}`.`schedules` WHERE `userID` = '{target.Id}' ORDER BY `setDate` DESC LIMIT 1;");
            DataRow record;
            EmbedBuilder embed = null;

			if (result.Tables != null && result.Tables.Count > 0 && result.Tables[0].Rows != null && result.Tables[0].Rows.Count > 0 && (record = result.Tables[0].Rows[0]) != null)
            {
                scheduleName = record["scheduleName"]?.ToString();
                napchartLink = record["napchartLink"]?.ToString();
                setDate = record["setDate"]?.ToString();
                napchartSetDate = record["napchartSetDate"]?.ToString();

                string description = napchartLink != "" ? $"**[Napchart]({napchartLink})** for <@{target.Id}>.\n" : "";

                int daysSinceScheduleSet = -1, daysSinceNapchartSet = -1;

                if (DateTime.TryParse(setDate, out DateTime date))
                    daysSinceScheduleSet = (DateTime.Today - date).Days;

                if (DateTime.TryParse(napchartSetDate, out DateTime ncDate))
                    daysSinceNapchartSet = (DateTime.Today - ncDate).Days;

                if (date == ncDate || daysSinceScheduleSet == daysSinceNapchartSet)
                    description = $"**Date Set:** [{setDate}] ({daysSinceScheduleSet} day{(daysSinceScheduleSet != 1 ? "s" : "")})\n\n" + description;
                else
                {
                    if (daysSinceScheduleSet > -1)
                        description = $"**Schedule Set:** [{setDate}] ({daysSinceScheduleSet} day{(daysSinceScheduleSet != 1 ? "s" : "")})\n\n" + description;

                    if (daysSinceNapchartSet > -1)
                        description += $"**Napchart Set:** [{napchartSetDate}] ({daysSinceNapchartSet} day{(daysSinceNapchartSet != 1 ? "s" : "")})";
                }

                embed = new()
                {
                    Color = Bot.getUsersColor(Bot._client.GetGuild(server.Id).Users.Where(u => u.Username == target.Username && u.Discriminator == target.Discriminator).First()),
                    Author = new ()
                    {
                        Name = user.DisplayName,
                        IconUrl = user.GetDisplayAvatarUrl()
                    },
                    Title = !string.IsNullOrEmpty(scheduleName) && scheduleName.ToLower() != "none" ? $"Schedule: {scheduleName}" : "No Schedule Set.",
                    Description = description.Trim(),
                    ImageUrl = napchartLink != "" ? NapchartInterface.getChartImg(napchartLink) : null,
                    Footer = new() { Text = napchartLink == "" ? $"{Bot.getUsersName(target, Bot.getServer(channelID))} does not have a Napchart set." : "" }
                };
            }
            else embed = new()
			{
				Color = Bot.getUsersColor(Bot._client.GetGuild(server.Id).Users.Where(u => u.Username == target.Username && u.Discriminator == target.Discriminator).First()),
				Author = new()
				{
					Name = user.DisplayName,
					IconUrl = user.GetDisplayAvatarUrl()
				},
				Title = $"No Schedule or Napchart Set."
			};

			if (message != null) message.Channel.SendMessageAsync(embed: embed.Build()).Wait();
			else intrct.RespondAsync(embed: embed.Build()).Wait();
		}
		#endregion

		#region adapted
		public static async void adapted(SocketMessage message)
		{
			if (!Bot.confirmAdmin(message, Bot.getGuildChannel(message.Channel.Id))) return;

			IGuildUser user = null;
			SocketGuild server = Bot._client.GetGuild((message.Channel as IGuildChannel).Guild.Id);
			NapGodSchedule schedule = null;
			string msg = message.Content.Trim(), username = "", scheduleName = "", errors = "";

			if (msg.Contains("+adapted "))
			{
				string info = msg.Replace("+adapted ", "");

				if (info.Contains(','))
				{
					username = info[..info.IndexOf(',')].Trim();
					scheduleName = info[(info.IndexOf(',') + 1)..].Trim();
				}
				else username = info.Trim();

				if (message.MentionedUsers.Count == 1)
					user = server.GetUser(message.MentionedUsers.First().Id);
				else if (!string.IsNullOrWhiteSpace(username))
					user = Bot.getUser(username, server.Id)[0];

				//found user
				if (user != null)
				{
					if (string.IsNullOrWhiteSpace(scheduleName))
					{
						if (user.Nickname != null && user.Nickname.Contains('[') && user.Nickname.Contains(']'))
							scheduleName = user.Nickname[(user.Nickname.IndexOf('[') + 1)..].Replace("]", "");

						else if (server.GetUser(user.Id).Roles.ToList().FindAll(r => r.Name.StartsWith("Attempted-")).Count == 1)
							scheduleName = server.GetUser(user.Id).Roles.ToList().Find(r => r.Name.StartsWith("Attempted-")).Name.Replace("Attempted-", "");
					}

					//found schedule name
					if (!string.IsNullOrWhiteSpace(scheduleName))
					{
						if (scheduleName.Contains('-'))
							scheduleName = scheduleName.Contains("Biphasic-X") ? "Biphasic-X" : scheduleName.Replace(scheduleName[scheduleName.IndexOf("-")..], "");

						//valid schedule
						if ((schedule = Config._napGodSchedules.Find(s => s._nameClean == scheduleName || s._nameClean.ToLower() == scheduleName.ToLower())) != null || (schedule = Config._napGodSchedules.Find(s => s._name.Contains(scheduleName) || s._nameClean.Contains(scheduleName.ToLower()))) != null)
						{
							IRole attemptedScheduleRole = server.GetRole(schedule._role._id);
							attemptedScheduleRole ??= Bot.getRole($"Attempted-{schedule._role._name.Replace("-", "")}", server.Id);
							IRole adaptedScheduleRole = Bot.getRole($"Adapted-{schedule._nameClean.Replace("-", "")}", server.Id);
							IRole currentlyAdaptedRole = Bot.getRole("Currently Adapted", server.Id);

							//remove attemped role
							if (attemptedScheduleRole != null)
							{
								if (user.RoleIds.ToList().Exists(r => r == attemptedScheduleRole.Id))
								{
									try
									{
										await user.RemoveRoleAsync(attemptedScheduleRole);
									}
									catch
									{
										errors += $"{_alert} <@&{attemptedScheduleRole.Id}> role unable to be removed.\n";
									}
								}
							}
							else errors += $"{_alert} Could not find Attempted-{scheduleName} role.\n";

							//add adapted role
							if (adaptedScheduleRole != null)
							{
								try
								{
									await user.AddRoleAsync(adaptedScheduleRole);
								}
								catch
								{
									errors += $"{_alert} <@&{adaptedScheduleRole}> role unable to be set.\n";
								}
							}
							else errors += $"{_alert} Could not find Adapted-{scheduleName} role.\n";

							//add currently adapted role
							if (currentlyAdaptedRole != null)
							{
								try
								{
									await user.AddRoleAsync(currentlyAdaptedRole);
								}
								catch
								{
									errors += $"{_alert} <@&{currentlyAdaptedRole}> role unable to be set.\n";
								}
							}
							else errors += $"{_alert} Could not find Currently Adapted role.\n";
						}
						else errors = $"{_alert} Could not find schedule: {scheduleName}.";
					}
					else errors = $"{_alert} Could not determine <@{user.Id}>'s schedule. Use:\n`+adapted [username or @user], [schedule name]`";
				}
				else errors = $"{_alert} Could not find user{(username.Length > 0 ? " " + username : "")}.";
			}
			else errors = $"{_alert} **Improper Format**\n\n`+adapted [username or @user]`\n\n *If user does not have a schedule name in their nickname or a single 'Attempted-Schedule' role, use:*\n`+adapted [username or @user], [schedule name]`";

			if (errors.Length > 0)
				Bot.sendEmbed(message.Channel, Discord.Color.Red, title: $"Adapted Command Error", description: errors);
			else
				Bot.sendEmbed(message.Channel, Discord.Color.Green, title: $"{user.Nickname} is now Adapted to {scheduleName}!", description: $"Congratulations, <@{user.Id}>! :partying_face:");
		}
		public static async void adapted(IDiscordInteraction c)
		{
			if (!Bot.confirmAdmin(c)) return;

			#region user
			IGuildUser user = c switch
            {
                SocketUserCommand  uc => Bot.getGuildUser(uc.Channel.Id, uc.Data.Member.Id),
                SocketSlashCommand sc => Bot.getGuildUser(sc.Channel.Id, (sc.Data.Options.First().Value as IUser).Id),
                _ => null
            };
			#endregion

			SocketGuild server = Bot._client.GetGuild(c.GuildId.Value);
			NapGodSchedule schedule = null;
			string scheduleName = "", errors = "";

			//found user
			if (user != null)
			{
                string username = user.DisplayName;
                if (c is SocketSlashCommand sc) scheduleName = sc.Data.Options.Last().Value.ToString().Trim();

				if (string.IsNullOrWhiteSpace(scheduleName))
				{
					if (user.Nickname != null && user.Nickname.Contains('[') && user.Nickname.Contains(']'))
						scheduleName = user.Nickname[(user.Nickname.IndexOf('[') + 1)..].Replace("]", "");

					else if (server.GetUser(user.Id).Roles.ToList().FindAll(r => r.Name.StartsWith("Attempted-")).Count == 1)
						scheduleName = server.GetUser(user.Id).Roles.ToList().Find(r => r.Name.StartsWith("Attempted-")).Name.Replace("Attempted-", "");
				}

				//found schedule name
				if (!string.IsNullOrWhiteSpace(scheduleName))
				{
					if (scheduleName.Contains('-'))
						scheduleName = scheduleName.Contains("Biphasic-X") ? "Biphasic-X" : scheduleName.Replace(scheduleName[scheduleName.IndexOf("-")..], "");

					//valid schedule
					if ((schedule = Config._napGodSchedules.Find(s => s._nameClean == scheduleName || s._nameClean.ToLower() == scheduleName.ToLower())) != null || (schedule = Config._napGodSchedules.Find(s => s._name.Contains(scheduleName) || s._nameClean.Contains(scheduleName.ToLower()))) != null)
					{
						IRole attemptedScheduleRole = server.GetRole(schedule._role._id);
						attemptedScheduleRole ??= Bot.getRole($"Attempted-{schedule._role._name.Replace("-", "")}", server.Id);
						IRole adaptedScheduleRole = Bot.getRole($"Adapted-{schedule._nameClean.Replace("-", "")}", server.Id);
						IRole currentlyAdaptedRole = Bot.getRole("Currently Adapted", server.Id);

						//remove attemped role
						if (attemptedScheduleRole != null)
						{
							if (user.RoleIds.ToList().Exists(r => r == attemptedScheduleRole.Id))
							{
								try
								{
									await user.RemoveRoleAsync(attemptedScheduleRole);
								}
								catch
								{
									errors += $"{_alert} <@&{attemptedScheduleRole.Id}> role unable to be removed.\n";
								}
							}
						}
						else errors += $"{_alert} Could not find Attempted-{scheduleName} role.\n";

						//add adapted role
						if (adaptedScheduleRole != null)
						{
							try
							{
								await user.AddRoleAsync(adaptedScheduleRole);
							}
							catch
							{
								errors += $"{_alert} <@&{adaptedScheduleRole}> role unable to be set.\n";
							}
						}
						else errors += $"{_alert} Could not find Adapted-{scheduleName} role.\n";

						//add currently adapted role
						if (currentlyAdaptedRole != null)
						{
							try
							{
								await user.AddRoleAsync(currentlyAdaptedRole);
							}
							catch
							{
								errors += $"{_alert} <@&{currentlyAdaptedRole}> role unable to be set.\n";
							}
						}
						else errors += $"{_alert} Could not find Currently Adapted role.\n";
					}
					else errors = $"{_alert} Could not find schedule: {scheduleName}.";
				}
				else errors = $"{_alert} Could not determine <@{user.Id}>'s schedule. Try using the command `/adapted`.";
			}
			else errors = $"{_alert} Could not find user.";

			if (errors.Length > 0)
				AdminCommand.sendEmbed(Discord.Color.Red, $"Adapted Command Error", errors, interaction: c);
			else
				AdminCommand.sendEmbed(Discord.Color.Green, $"{user.Nickname} is now Adapted to {scheduleName}!", $"Congratulations, <@{user.Id}>! :partying_face:", interaction: c, ephemeral: false);
		}
		#endregion

		#region tg/assignroles
		public static async void tg(SocketMessage message)
        {
            bool consoleWrite = false;

            if (!Bot.confirmAdmin(message, Bot.getGuildChannel(message.Channel.Id))) return;
            if (message.Content.Contains(' '))
            {
                string msg = message.Content[message.Content.IndexOf(' ')..].ToLower().Trim();
                string roleText = "";
                IGuild server = (message.Author as IGuildUser).Guild;
                IGuildUser target = null;
                List<IRole> roles = new();
                string errors = "", successes = "";
                if (message.MentionedUsers.Count == 1)
                {
                    IGuildUser user = server.GetUserAsync(message.MentionedUsers.First().Id).Result;
                    if (user != null) target = user;
                }

                if (message.MentionedRoles.Count > 0)
                {
                    foreach (SocketRole r in message.MentionedRoles)
                    {
                        if (r != null)
                        {
                            IRole role = server.GetRole(r.Id);
                            if (role != null) roles.Add(role);
                        }
                    }
                }


                if (roles.Count < (msg.Length - msg.Replace(",", "").Length))
                {
                    List<string> arguments = msg.Split(',').ToList();

                    foreach (string argument in arguments)
                    {
                        if (argument != null)
						{
                            if (arguments.IndexOf(argument) == arguments.Count - 1)
                            {
                                target ??= Bot.getUser(argument.Trim(), server.Id)[0];
                            }
                            else if (!roles.Exists(r => r.Name.ToLower() == argument.Trim().Replace("@", "")) && !argument.Contains("<@&"))
                            {
                                IRole role = Bot.getRole(argument.Trim(), server.Id);
                                if (role != null) roles.Add(role);
                                else errors += $"Unable to find role [{argument.Trim()}].\n";
                            }
                        }
                    }
                }

                if (roles.Count == 0 && message.MentionedRoles.Count == 0)
                    roles.Add(Bot.getRole(roleText, server.Id));

                if (roles.Count > 0 && target != null)
                {
                    foreach (IRole r in roles.FindAll(rl => rl != null))
                    {
                        if (target != null && r != null)
                        {
                            if (!target.RoleIds.ToList().Contains(r.Id))
                            {
                                try
                                {
                                    await target.AddRoleAsync(r);
                                    MM.Print(consoleWrite, $"Gave the role @{r.Name} to {target.Username}");
                                    successes += $"Gave the role <@&{r.Id}> to <@{target.Id}>.\n";
                                }
                                catch
                                {
                                    errors += $"Unable to give the role {r} to <@{target.Id}>.\n";
                                }
                            }
                            else
                            {
                                try
                                {
                                    await target.RemoveRoleAsync(r);
                                    MM.Print(consoleWrite, $"{target.Username} the role: @{r.Name}");
                                    successes += $"Removed the role <@&{r.Id}> from <@{target.Id}>.\n";
                                }
                                catch
                                {
                                    errors += $"Unable to remove the role <@&{r.Id}> from <@{target.Id}>.\n";
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (roles.Count == 0) errors += "Unable to find mentioned role(s).";
                    if (target == null) errors += "Unable to find mentioned target.";
                }

                string description = $"{(successes.Length > 0 ? $"**Successes:**\n{successes}" : "")}{(successes.Length > 0 && errors.Length > 0 ? "\n" : "")}{(errors.Length > 0 ? $"**Errors:**\n{errors}" : "")}";

                Bot.sendEmbed(message.Channel,
                    successes.Length > 0 && errors.Length > 0 ? Discord.Color.Orange : successes.Length == 0 && errors.Length > 0 ? Discord.Color.Red : Discord.Color.Green,
                    title: "TG Results", description: description, author: message.Author as IGuildUser);
            }
            else Bot.sendEmbed(message.Channel, Discord.Color.Red, title: $"{_alert} TG Error {_alert}", 
                description: "*Adds a listed role to a user if they don't have it already, and removes it if they do.\nMultiple roles can be listed. \nIf only a role is listed, the role will be added/removed from the user who used the command.*",
                subtitle: "Proper Format:", text: $"`{(message.Content.Trim().StartsWith("|") ? "|" : "+" )}tg [List of roles followed by a user, separated by commas  OR  A single role]`");
        }

        public static void assignRoles(IDiscordInteraction di)
        {
            List<IRole> roles = new();
            IGuildUser user = null;
            IGuild server = Bot.getGuild(di.ChannelId.Value);

            switch (di)
            {
                case SocketUserCommand uc:
                {
                    user = uc.Data.Member as IGuildUser;

                    ModalBuilder modal = new()
                    {
                        Title = $"Roles for {user.DisplayName}",
                        CustomId = $"AssignRoles[{user.Id}]",
                        Components = new()
                    };

                    modal.Components.WithTextInput(new()
                    {
                        Style = TextInputStyle.Paragraph,
                        CustomId = "roles",
                        Required = true,
                        Label = $"Enter the roles to give (separated by comas).",
                        MinLength = 1,
                        Placeholder = "role1, role2, role3, ..."
                    });

                    uc.RespondWithModalAsync(modal.Build()).Wait();
                    return;
                }

                case SocketSlashCommand sc:
                {
                    user = sc.Data.Options.First().Value as IGuildUser;

                    foreach (SocketSlashCommandDataOption role in sc.Data.Options.ToArray()[1..])
                        roles.Add(role.Value as IRole);

                    break;
                }

                case IModalInteraction mi:
                {
                    user = Bot.getGuildUser(mi.ChannelId.Value, ulong.Parse(mi.Data.CustomId.Split('[')[1][0..^1]));

                    string[] parameters = mi.Data.Components.First().Value.ToString().Split(',');

                    foreach (string role in parameters)
                    {
                        string roleName = role.Trim();
                        if (roleName.StartsWith('@')) roleName = roleName[1..];

                        IRole foundRole = null;
                        if ((foundRole = server.Roles.ToList().Find(match: r => r.Name == roleName || r.Name.Replace(" ", "").Replace("_", "") == roleName.Replace(" ", "").Replace("_", "") ||
                                r.Name.ToLower() == roleName.ToLower() || r.Name.ToLower().Replace(" ", "").Replace("_", "") == roleName.ToLower().Replace(" ", "").Replace("_", ""))) != null)
                            roles.Add(foundRole);
                    }

                    break;
                }
            }

            string errors = "";

            if (user == null) errors += "⚠️ Could not find user.\n";
            if (roles.Count == 0) errors += "⚠️ No roles identified. *Please verify spelling of roles.*";

            if (errors.Length > 0)
            {
                AdminCommand.sendEmbed(Discord.Color.Red, $"{_alert} AssignRoles Error {_alert}", errors, interaction: di, ephemeral: true);
                return;
            }

            string successes = "", alreadyHad = "";

            foreach (IRole role in roles)
            {
                if (user.RoleIds != null && user.RoleIds.Count > 0 && user.RoleIds.Contains(role.Id))
                    alreadyHad += $"{role.Mention}\n";
                else
                {
                    user.AddRoleAsync(role).Wait();
                    successes += $"{role.Mention}\n";
                }
            }

            List<EmbedFieldBuilder> fields = new();

            if (successes.Length > 0) fields.Add(new()
            {
                Name = $"{Bot.getEmote(Config._emojiNames["Yes"])} Successfully Added:",
                Value = successes,
                IsInline = true
            });

            if (alreadyHad.Length > 0) fields.Add(new()
            {
                Name = $"{Bot.getEmote(Config._emojiNames["Empty"])} Already Had:",
                Value = alreadyHad,
                IsInline = true
            });

            di.RespondAsync(embed: new EmbedBuilder()
            {
                Color = Discord.Color.Green,
                Fields = fields,
                Author = new()
                {
                    Name = $"Roles for {user.DisplayName}",
                    IconUrl = user.GetDisplayAvatarUrl()
                }
            }.Build(), ephemeral: successes.Length == 0);
		}
		#endregion

        internal static void advise(SocketMessageCommand mc)
        {
            if (mc.Data.Message.Content?.Length == 0 || !mc.Data.Message.Content.Contains('!'))
            {
                AdminCommand.sendEmbed(Color.Red, $"{_alert} Advising Error {_alert}", "No commands found in selected message.", interaction: mc);
                return;
            }

            (string _, string _, List<string> commands) = processHelpCommand(mc.Channel, "advise", mc.Data.Message.Content);
            ComponentBuilder components = new();
            IMessageChannel botspam = Bot.getIMessageChannel(Bot.getChannel("botspam", mc.GuildId.Value).Id);

            foreach(string command in commands)
            {
                if (command.StartsWith('!'))
                {
                    (string commandName, NapGodText.Category _, string text) = NapGodText._help.Find(c => c.commandName == command[1..].ToLower().Replace(" ", "").Replace("-", "").Replace("_", ""));

					IUserMessage message = Bot.sendNGHelpCommand(commandName, text, Bot.getGuildUser(mc.Channel.Id, mc.User.Id), botspam);

                    if (message != null) components.WithButton(new ButtonBuilder()
                    {
                        Label = command,
                        Style = ButtonStyle.Link,
                        Url = Bot.getMessageLink(message)
                    });
                }
            }

            mc.RespondAsync(components: components.Build()).Wait();
        }

		internal bool NG_InteractionHandler(IDiscordInteraction i)
		{
            switch (i)
            {
                case SocketMessageComponent mi:
                {
					switch (mi.Data.CustomId.Split(':')[0])
					{
						case "Help":
						{
							if (mi.Data.CustomId.Contains('!'))
							{
								(string commandName, NapGodText.Category _, string text) = NapGodText._help.Find(c => c.commandName == mi.Data.CustomId[(mi.Data.CustomId.IndexOf('!') + 1)..].ToLower().Replace(" ", "").Replace("-", ""));
								if (!string.IsNullOrWhiteSpace(text)) Bot.sendNGHelpCommand(commandName, text.Replace("\r", ""), Bot.getGuildUser(mi.Channel.Id, mi.User.Id), interaction: mi);
							}
							else if (mi.Data.CustomId.Contains("{}"))
							{
								(string commandName, NapGodText.Category _, string text) = NapGodText._help.Find(c => c.commandName == mi.Data.Values.First().ToString().Replace("!", "").ToLower().Replace(" ", "").Replace("-", ""));
								if (!string.IsNullOrWhiteSpace(text)) Bot.sendNGHelpCommand(commandName, text.Replace("\r", ""), Bot.getGuildUser(mi.Channel.Id, mi.User.Id), interaction: mi);
							}
							else
							{
								string customID = mi.Data.CustomId[(mi.Data.CustomId.IndexOf('+') + 1)..].ToLower().Replace(" ", "").Replace("-", "");
								(string commandName, NapGodText.NapGodCommandType type, string text) = NapGodText._commands.Find(c => c.commandName == customID);
								if (!string.IsNullOrWhiteSpace(text) && commandName != "schedules") mi.RespondAsync(text);
								else if (!string.IsNullOrWhiteSpace(text) && commandName == "schedules")
									Bot.sendSchedulesNGCommand(Bot.getGuildUser(mi.Channel.Id, mi.User.Id), interaction: mi);
								else
								{
									(string nameLookup, Category scheduleCategory, string description, string napchartLink, bool show) schedule = NapGodText._schedules.Find(s => s.nameLookup.Contains(customID));
									if (!string.IsNullOrWhiteSpace(text)) Bot.sendScheduleInfoNGCommand(schedule, Bot.getGuildUser(mi.Channel.Id, mi.User.Id), interaction: mi);
								}
							}

							return true;
						}

						case "Delete":
						case "NextMessage":
						{
							if (!Bot.confirmOwner(mi.User.Id)) return false;

							string data = mi.Data.CustomId[(mi.Data.CustomId.IndexOf(":") + 1)..];
							List<ButtonComponent> btns = mi.Message.Components.First().Components.OfType<ButtonComponent>().ToList();

							if (mi.Data.CustomId.StartsWith("Delete:"))
							{
								string[] IDs = data.Split('/');
								if (ulong.TryParse(IDs[0], out ulong channelID) && ulong.TryParse(IDs[1], out ulong messageID))
								{
									string content = mi.Message.Content;
									(Bot._client.GetChannelAsync(channelID).Result as IMessageChannel).GetMessageAsync(messageID).Result.DeleteAsync().Wait();

									mi.Message.ModifyAsync(msg =>
									{
										msg.Components = btns.Count == 2
											? (Optional<MessageComponent>)new ComponentBuilder()
											.WithButton(btns[0].Label, btns[0].CustomId, btns[0].Style, disabled: true)
											.WithButton(btns[1].Label, btns[1].CustomId, btns[1].Style, disabled: btns[1].IsDisabled).Build()
											: (Optional<MessageComponent>)new ComponentBuilder().WithButton(btns[0].Label, btns[0].CustomId, btns[0].Style, disabled: true).Build();

										msg.Content = $"❌ `DELETED` ❌\n{content}";
									}).Wait();
								}
							}
							else
							{
								if (int.TryParse(data, out int currentIndex))
								{
									Bot._ac.sendDeleteMessagesEmbed(mi.Channel, Bot._ac._deleteMessagesResults.allMatches[currentIndex + 1]);

									mi.Message.ModifyAsync(msg => msg.Components = new ComponentBuilder()
											.WithButton(btns[0].Label, btns[0].CustomId, btns[0].Style, disabled: btns[0].IsDisabled)
											.WithButton(btns[1].Label, btns[1].CustomId, btns[1].Style, disabled: true).Build()).Wait();
								}
							}

							return true;
						}

						case "Get":
						{
							IUser user = null;
							if (ulong.TryParse(mi.Data.Values.First()[4..], out ulong userID) && (user = Bot._client.GetUserAsync(userID).Result) != null)
								Get(user, mi);

							return true;
						}
					}

                    break;
				}

                case SocketSlashCommand sc:
                {
                    switch (sc.CommandName)
                    {
						case "get": Get(sc.Data.Options.First().Value as IUser, sc); break;

                        case "set":
                        {
                            List<SocketSlashCommandDataOption> options = sc.Data.Options.First().Options.ToList();

                            string schedule = options.Find(o => o.Name == "schedule").Value.ToString();
							IGuildUser user = Bot.getGuildUser(sc.Channel.Id, sc.User.Id);

							SocketSlashCommandDataOption modifier = null;
                            Modifier mod = (modifier = options.Find(o => o.Name == "modifier" && o.Value != null)) != null ? Enum.Parse<Modifier>(modifier.Value.ToString()) : Modifier.none;
							
                            SocketSlashCommandDataOption napchart = null;
                            string napchartLink = (napchart = options.Find(o => o.Name == "napchart" && o.Value != null)) != null ? napchart.Value.ToString() : "";

							Set(schedule, sc.Channel, user, mod, napchartLink, sc: sc);

							break;
                        }

                        case "mset":
						{
							List<SocketSlashCommandDataOption> options = sc.Data.Options.First().Options.ToList();

							string schedule = options.Find(o => o.Name == "schedule").Value.ToString();
							IGuildUser user = Bot.getGuildUser(sc.Channel.Id, (options.Find(o => o.Name == "user").Value as IUser).Id);

							SocketSlashCommandDataOption modifier = null;
							Modifier mod = (modifier = options.Find(o => o.Name == "modifier" && o.Value != null)) != null ? Enum.Parse<Modifier>(modifier.Value.ToString()) : Modifier.none;

							SocketSlashCommandDataOption napchart = null;
							string napchartLink = (napchart = options.Find(o => o.Name == "napchart" && o.Value != null)) != null ? napchart.Value.ToString() : "";

							Set(schedule, sc.Channel, user, mod, napchartLink, sc: sc);

							break;
						}

                        case "info":
						{
							(string commandName, NapGodText.Category _, string text) = 
                                NapGodText._help.Find(h => h.commandName == (sc.Data.Options.Count > 0 ? sc.Data.Options.First().Value.ToString() : "help"));

                            Bot.sendNGHelpCommand(commandName, text, Bot.getGuildUser(sc.Channel.Id, sc.User.Id), interaction: sc);

                            break;
                        }

                        case "schedules":
                        {
                            if (sc.Data.Options.Count == 0) 
                                Bot.sendSchedulesNGCommand(Bot.getGuildUser(sc.Channel.Id, sc.User.Id), interaction: sc);
                            else
							{
                                var x = sc.Data;
								var schedule = NapGodText._schedules.Find(s => s.nameLookup.Contains(
								Config._napGodSchedules.Find(s => s._nameClean == sc.Data.Options.First().Value.ToString())._name));

								Bot.sendScheduleInfoNGCommand(schedule, Bot.getGuildUser(sc.Channel.Id, sc.User.Id), interaction: sc);
							}

                            break;
                        }
					}

                    break;
		        }

                case SocketUserCommand uc:
				{
					switch (uc.CommandName)
                    {
                        case "get": Get(uc.Data.Member, uc); break;
                    }

                    break;
                }
			}
			

            return false;
		}
		#endregion

		#region utilities
		internal (string, string) setScheduleHelperSchedule(IGuild server, ulong serverID = 0, string dbName = "")
		{
			bool consoleWrite = true;

			if (serverID == 0 || !string.IsNullOrEmpty(dbName)) (serverID, dbName) = Config._dbSwap.Find(d => d.serverID == server.Id);
			if (string.IsNullOrWhiteSpace(dbName)) dbName = "scheduledata";

			createScheduleDataTables(serverID > 0, serverID > 0 ? serverID : server.Id);

			IUser sh = Bot._client.CurrentUser;
			bool userInDb = false;

			DataSet result = Bot._msm.executeQuery($"SELECT `userID` FROM `{dbName}`.`users` WHERE `userID` = {sh.Id};");

			//If not in db, add user
			if (result.Tables == null || result.Tables.Count == 0 || result.Tables?[0].Rows == null || result.Tables[0].Rows.Count == 0)
			{
				if (!Bot._msm.executeNonQuery($"INSERT INTO `{dbName}`.`users` (`userID`, `dateAdded`) VALUES ({sh.Id}, current_timestamp());"))
				{
					MM.Print(consoleWrite, $"Unable to add user [{sh.Username}][{sh.Id}] to DB.");
				}
				else userInDb = true;
			}
			else userInDb = true;


			if (userInDb)
			{
				//Get current schedule, if one is set
				result = Bot._msm.executeQuery($"SELECT `scheduleName`, `napchartLink`, `setDate` FROM `{dbName}`.`schedules` WHERE `userID` = '{sh.Id}' ORDER BY `setDate` DESC LIMIT 1;");
				DataRow record;
				string oldNapchart = "", napchart = "", scheduleName = "";

				bool makeNewSchedule = false;

				if (result.Tables != null && result.Tables.Count > 0 && result.Tables[0].Rows != null && result.Tables[0].Rows.Count > 0 && (record = result.Tables[0].Rows[0]) != null)
				{
					oldNapchart = record["napchartLink"].ToString();
					scheduleName = record["scheduleName"].ToString();

					//if schedule is set and it wasn't created today, make a new one
					if (DateTime.TryParse(record["setDate"]?.ToString(), out DateTime date) && (DateTime.Today - date.Date).Days > 0)
						makeNewSchedule = true;
				}
				else makeNewSchedule = true; //if there is no schedule set, make a new one


				if (makeNewSchedule)
				{
					string errors;
					while (string.IsNullOrEmpty(napchart) || !Uri.IsWellFormedUriString(napchart, UriKind.Absolute))
					{
						(napchart, scheduleName, errors) = Bot._uc.makeRandomSchedule();

						if (!string.IsNullOrWhiteSpace(errors)) MM.Print(consoleWrite, errors);
					}

					if (!Bot._msm.executeNonQuery($"INSERT INTO `{dbName}`.`schedules` (`userID`, `userNickname`, `scheduleName`, `napchartLink`, `setDate`) VALUES ({sh.Id}, '{sh.Username}', '{scheduleName}', '{napchart}', current_timestamp());"))
					{
						MM.Print(consoleWrite, $"nable to add schedule [{sh.Username}][{scheduleName}] to DB.");
					}

					return (napchart, scheduleName);
				}
				else return (oldNapchart, scheduleName);
			}
			else return ("", "");
		}

		internal static NapGodSchedule getNapGodSchedule(string name) => Config._napGodSchedules.Find(s => s._name.Split(';').ToList().Exists(ss => ss == name));

        private void createScheduleDataTables(bool altDB = false, ulong serverID = 0)
		{
            if (!_scheduledataCreated || (altDB && !_altScheduledataCreated))
            {
                Bot._msm.createSchema(createScheduledataTables: true, serverIDForScheduleDataTables: serverID);

                if (!altDB) _scheduledataCreated = true;
                else _altScheduledataCreated = true;
            }
        }

        internal static (string title, string body, List<string> components) processHelpCommand(IMessageChannel channel, string command, string text)
		{

			string title = text.Contains('\n') ? text[..text.IndexOf("\n")] : "";
			string body = title.Length > 0 ? text.Replace(title, "").Remove(0, 2) : text;
            List<string> components = new();
            ulong serverID = Bot.getGuild(channel.Id).Id;

			if (text.StartsWith('|'))
			{
				title = title[1..];
				string commands = text;
                components.Add("selectmenu");

				while (commands.Contains('!'))
				{
					commands = commands[commands.IndexOf('!')..];
					string refCmd = commands[..commands.IndexOf(" ")];
                    components.Add(refCmd);
					commands = commands.Replace(refCmd, "");
				}
			}
			else if (command == "help")
			{
                components.Add("selectmenu-help");
                components.AddRange(NapGodText._help.FindAll(c => c.category == NapGodText.Category.Category).Select(c => $"!{c.commandName}"));
			}
			else
			{
                components.Add("");

				while (text.Contains("`+"))
				{
					text = text[(text.IndexOf("`+") + 1)..];
					components.Add(text[..text.IndexOf("`")]);
				}

				text = body;
				while (text.Contains("`!"))
				{
					text = text[(text.IndexOf("`!") + 1)..];
					components.Add(text[..text.IndexOf("`")]);
				}

				while (body.Contains("|#"))
				{
					string channelPlainText = body.Substring(body.IndexOf("|#"), body.IndexOf("#|") - body.IndexOf("|#") + 2).Replace("|", "").Replace("#", "");
					ulong? channelID = Bot.getGuild(channel.Id).GetChannelsAsync().Result.ToList().Find(c => c.Name == channelPlainText || c.Name.ToLower() == channelPlainText.ToLower())?.Id;
					body = body.Replace($"|#{channelPlainText}#|", channelID != null ? $"<#{channelID}>" : $"#{channelPlainText}");
				}

				while (body.Contains("|@"))
				{
					string rolePlainText = body.Substring(body.IndexOf("|@"), body.IndexOf("@|") - body.IndexOf("|@") + 2).Replace("|", "").Replace("@", "");
					ulong? roleID = Bot.getRole(rolePlainText, serverID)?.Id;
					body = body.Replace($"|@{rolePlainText}@|", roleID != null ? $"<@&{roleID}>" : $"`@{rolePlainText}`");
				}
			}

            return (title, body, components);
		}
		#endregion
	}

	class NapGodSchedule
    {
        internal NapGodSchedule(string name, string nameClean, NapGodRole role, NapGodRole catRole)
        {
            _name = name;
            _nameClean = nameClean;
            _role = role;
            _categoryRole = catRole;
        }

        internal enum Category { Biphasic, Everyman, Dual_Core, Tri_Core, Monophasic, Nap_Only, Experimental, Random };
        internal enum Modifier { none, shortened, _short, extended, _ext, flipped, modified, _mod, recovery };
        internal string _name;
        internal string _nameClean;
        internal NapGodRole _role;
        internal NapGodRole _categoryRole;
    }

    class NapGodRole
    {
        internal NapGodRole(Category category, string name, ulong roleID)
        {
            _category = category;
            _name = name;
            _id = roleID;
        }

        internal Category _category;
        internal string _name;
        internal ulong _id;
    }
}