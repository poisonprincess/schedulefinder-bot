﻿using Discord;
using Discord.WebSocket;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace PolyphasicScheduleFinder_Bot
{
    class MySQLManager
    {
		#region attributes
		internal enum StoredProcedure { addPoll, getActivePolls, deactivatePoll };

		/// <summary> Connection to MySQL interface </summary>
		internal MySqlConnection _connection;
        /// <summary> References to interactable embeds </summary>
        internal List<IMessage> _embedReferences = new();

		internal bool _createdData = false;

		/// <summary> MySQLManager object to connect via the MySQLConnection </summary>
		public MySQLManager(bool logger)
        {
            bool consoleWrite = true;
            try
            {
                _connection = new MySqlConnection($"datasource=localhost;port=3306;" +
                $"username={(logger ? "Logger" : "root")};" +
                $"password={File.ReadAllText($"sqlup{(logger ? "Logger" : "")}.txt")}");
                openIfPossible();
                closeIfPossible();
            }
            catch (Exception e) { MM.Print(consoleWrite, $"\nstartConnection:\n{e}\n"); }
        }
		#endregion

		#region general commands
		/// <summary> Executes a command query and returns the DataSet of results </summary>
		public DataSet executeQuery(string query)
        {
            bool consoleWrite = true, retry, attemptedDisposal;
            int tries = 0;
            DataSet ds;

            do
            {
                closeIfPossible(false);
                openIfPossible(false);
                MySqlDataAdapter adapter = new(query, _connection);
                ds = new DataSet();
                retry = attemptedDisposal = false;

                try
                {
                    int waitingTries = 0;
                    while (_connection.State == System.Data.ConnectionState.Executing ||
                        _connection.State == System.Data.ConnectionState.Fetching
                        && waitingTries < 100) 
                    { waitingTries++; }

                    adapter.Fill(ds);
                    attemptedDisposal = true;
                    adapter.Dispose();
                    closeIfPossible(false);
                }
                catch (Exception e)
                {
                    if(!attemptedDisposal) adapter.Dispose();
                    closeIfPossible(false);
                    if (e.Message.Contains("associated with this Connection which must be closed first") ||
                        e.Message.Contains("Connection must be valid and open") ||
                        e.Message.Contains("Object reference not set to an instance of an object"))
                    {
                        retry = true;
                        tries++;
                    }
                    else if (e.Message.Contains("method cannot be called when another") && e.Message.Contains("is pending"))
                    {
                        int waitingTries = 0;
                        while (_connection.State != System.Data.ConnectionState.Closed && waitingTries < 100)
                        {
                            closeIfPossible(false);
                            waitingTries++;
                        }

                        retry = true;
                        tries++;
                    }
                    else MM.Print(consoleWrite, $"[{query.Replace("\n", " ")}]:\n{e}\n");
                }
            }
            while (retry && tries < 100);

            return ds;
        }

		/// <summary> Executes a command </summary>
		public bool executeNonQuery(string query)
        {
            bool consoleWrite = true, executed = false, retry;
            int tries = 0;

            do
            {
                openIfPossible(false);
                retry = false;

                try
                {
                    executed = new MySqlCommand(query, _connection).ExecuteNonQuery() > 0;
                    int waitingTries = 0;
                    while (_connection.State == System.Data.ConnectionState.Executing || 
                        _connection.State == System.Data.ConnectionState.Fetching
                        && waitingTries < 100) 
                    { waitingTries++; }

                    closeIfPossible(false);
                }
                catch (Exception e)
                {
                    closeIfPossible(false);
                    if (e.Message.Contains("associated with this Connection which must be closed first") ||
                        e.Message.Contains("Connection must be valid and open"))
                    {
                        retry = true;
                        tries++;
                    }
                    else if (e.Message.Contains("method cannot be called when another") && e.Message.Contains("is pending"))
                    {
                        int waitingTries = 0;
                        while (_connection.State != System.Data.ConnectionState.Closed && waitingTries < 100)
                        {
                            closeIfPossible(false);
                            waitingTries++;
                        }

                        retry = true;
                        tries++;
                    }
                    else MM.Print(consoleWrite, $"[{query.Replace("\n", " ")}]:\n{e}\n");
                }
            }
            while (retry && tries < 100);

            return executed;
        }

        /// <summary> Returns a DataSet of results from a simple selection via Schema, Column list, and Table parameters </summary>
        public DataSet simpleSelect(string schema, List<string> columns, string table) => executeQuery($"SELECT {combineColumns(columns)} FROM `{schema}`.{table};");

        /// <summary> Updates a table using parameters </summary>
        public bool updateTable(string schema, string table, List<string> columns, List<string> entries, string where = "")
        {
            if (columns.Count != entries.Count) return false;

            string set = "";

            for (int i = 0; i < columns.Count; i++)
                set += $"{columns[i]} = {entries[i]}{(i + 1 < columns.Count ? ", " : "")}";

            return executeNonQuery($"UPDATE `{schema}`.`{table}`\nSET {set}{(where != "" ? $"\nWHERE {where}" : "")};");
        }

        /// <summary> Creates a new Schema </summary>
        public bool createSchema(string name = "", bool createServerTables = false, bool createScheduledataTables = false, ulong serverIDForScheduleDataTables = 0)
        {
            bool consoleWrite = true, madeTables = false, created = false;

            try
            {
                if (createScheduledataTables)
				{
                    DataSet result;
                    bool tableExists = false;
                    string commands = File.ReadAllText("./../../../Misc/Create scheduledata Tables.sql");

                    if (serverIDForScheduleDataTables > 0)
					{
                        result = executeQuery("SHOW DATABASES LIKE 'scheduledata';");

                        if (result.Tables == null || result.Tables.Count == 0 || result.Tables[0] == null || result.Tables[0].Rows == null || result.Tables[0].Rows.Count == 0)
                        {
                            commands = commands.Replace("`scheduledata`", $"`{Config._dbSwap.Find(d => d.serverID == serverIDForScheduleDataTables).newDBName}`");
                        }
                        else tableExists = true;
                    }
                    else
                    {
                        result = executeQuery($"SHOW DATABASES LIKE '{name}';");

                        if (result.Tables != null && result.Tables.Count > 0 && result.Tables[0] != null && result.Tables[0].Rows != null && result.Tables[0].Rows.Count > 0)
                            tableExists = true;
                    }

					return tableExists || executeNonQuery(commands);
				}
                else
				{
                    created = executeNonQuery($"CREATE SCHEMA `{name}`;");
                    if (createServerTables)
                        madeTables = executeNonQuery(File.ReadAllText("./../../../Misc/Create Server Tables.sql").Replace("`SERVERNAME`", $"`{name}`"));
                    return created && (!createServerTables || createServerTables && madeTables);
                }                
            }
            catch (Exception e)
            {
                closeIfPossible();
                MM.Print(consoleWrite, $"Schema{(created ? " " : " not ")}created.\nTables{(created ? " " : " not ")}created.\n{e}");
                return created && (!createServerTables || createServerTables && madeTables);
            }
		}

		/// <summary> Creates Schema </summary>
		public bool createPollData(string schemaName)
		{
			if (_createdData) return true;

			bool madeTables = false, madeProcedures = false;

			try
			{
				bool tableExists = false, procedureExists = false;

				//Schema and Tables
				string commands = File.ReadAllText("./../../../Misc/Create Poll Tables.sql").Replace("[schemaName]", schemaName);
				DataSet result = executeQuery($"USE `{schemaName.Replace("`", "")}`; SHOW Tables LIKE 'polls';");

				if (result.Tables != null && result.Tables.Count > 0 && result.Tables[0] != null && result.Tables[0].Rows != null && result.Tables[0].Rows.Count > 0)
					tableExists = true;

				if (!tableExists) madeTables = executeNonQuery(commands);

				//Stored Procedures
				commands = File.ReadAllText("./../../../Misc/Create Stored Procedures.sql").Replace("[schemaName]", schemaName);
				result = executeQuery($"USE {schemaName}; SHOW PROCEDURE STATUS LIKE 'addPoll';");

				if (result.Tables != null && result.Tables.Count > 0 && result.Tables[0] != null && result.Tables[0].Rows != null && result.Tables[0].Rows.Count > 0)
				{
					foreach (DataRow row in result.Tables[0].Rows)
					{
						if (row["Db"].ToString() == schemaName.Replace("`", ""))
						{
							procedureExists = true;
							break;
						}
					}
				}

				if (!procedureExists) madeProcedures = executeNonQuery(commands);

				return _createdData = (madeTables || tableExists) && (madeProcedures || procedureExists);
			}
			catch (Exception e)
			{
				closeIfPossible();
				Console.WriteLine($"\ncreateSchema:\n{e}\n");
				return madeTables && madeProcedures;
			}
		}
		#endregion

		#region logging
		/// <summary> Logs a message to the DB related to the Server the message was sent on </summary>
		public bool logMessage(LogEntry message)
        {
            bool consoleWrite = false;

            DateTime dateTime = DateTime.Parse($"{message._date} {message._time}");
            ulong serverID = message._server._id;
            string messageFID = $"{message._inputType}:{message._messageId}:{Bot._rng.Next(100000000, 999999999)}"; ;
            bool executedMessages = false,
                executedAttachments = false,
                executedEmbeds = false,
                executedEmbedFields = false;

            try
            {
                while (executeQuery($"SELECT `FID` FROM `{serverID}`.`messages` WHERE `FID` = '{messageFID}';").Tables?[0].Rows.Count > 0) 
                    messageFID = $"{message._inputType}:{message._messageId}:{Bot._rng.Next(100000000, 999999999)}"; 
            }
            catch { }

            #region query
            string content = message._content.Replace("'", "''");

            string query =
            $"INSERT INTO `{serverID}`.`messages` " +
            $"(`datetime`, " +
            $"`FID`, " +
            $"`type`, " +
            $"`authorID`, " +
            $"`channelID`, " +
            $"`messageID`, " +
            $"`channelName`, " +
            $"`authorName`, " +
            $"`content`, " +
            $"`embedCount`," +
            $"`attachmentCount`) VALUES " +
            $"\n('{dateTime:yyyy-MM-dd HH:mm:ss}', " +            //datetime
            $"\n'{messageFID}', " +                               //FID
            $"\n'{message._inputType}', " +                       //type
            $"\n'{(message._authorId ?? "0")}', " +               //authorID
            $"\n'{message._channelId}', " +                       //channelID
            $"\n'{message._messageId}', " +                       //messageID
            $"\n'{message._channelName.Replace("'", "''")}', " +  //channelName
            $"\n'{message._authorName.Replace("'", "''")}', " +   //authorName
            $"\n'{(content.EndsWith("\\") ? content + "\\" : content)}', " +        //content
            $"\n'{(message._embedData != null ? message._embedData.Count.ToString() : "0")}', " +              //embedCount
            $"\n'{(message._attachmentData != null ? message._attachmentData.Count.ToString() : "0")}');";    //attachmentCount
            #endregion

            try
            {
                MM.Print(consoleWrite, query);
                executedMessages = executeNonQuery(query);

                if (message._attachmentData != null && message._attachmentData.Count > 0)
                {
                    foreach (LogAttachment la in message._attachmentData)
                    {
                        string attachmentID = $"{message._inputType}:{la._id}:{Bot._rng.Next(100000000, 999999999)}";

                        try
                        {
                            while (executeQuery($"SELECT `attachmentID` FROM `{serverID}`.`attachments` WHERE `attachmentID` = '{attachmentID}';").Tables?[0].Rows.Count > 0)
                                attachmentID = $"{message._inputType}:{la._id}:{Bot._rng.Next(100000000, 999999999)}";
                        }
                        catch { }
                                                
                        #region query
                        string laQuery =
                            $"INSERT INTO `{serverID}`.attachments " +
                            $"(`datetime`, " +
                            $"`messageFID`, " +
                            $"`messageID`, " +
                            $"`attachmentID`, " +
                            $"`filename`, " +
                            $"`url`, " +
                            $"`proxyUrl`) VALUES " +
                            $"('{dateTime:yyyy-MM-dd HH:mm:ss}', " +    //datetime
                            $"'{messageFID}', " +                       //messageFID
                            $"'{message._messageId}', " +               //messageID
                            $"'{attachmentID}', " +        //attachmentID
                            $"'{la._filename.Replace("'", "''")}', " +  //filename
                            $"'{la._url}', " +                          //url
                            $"'{la._proxyUrl}');";                      //proxyurl
                        #endregion
                        MM.Print(consoleWrite, laQuery);
                        executedAttachments = executeNonQuery(laQuery);
                    }
                }
                else executedAttachments = true;

                if (message._embedData != null && message._embedData.Count > 0)
                {
                    foreach (LogEmbed le in message._embedData)
                    {
                        string embedHash = le._embedHash.ToString();

                        try
                        {
                            while (executeQuery($"SELECT `embedHash` FROM `{serverID}`.`embeds` WHERE `embedHash` = '{embedHash}';").Tables?[0].Rows.Count > 0)
                                embedHash = Bot._rng.Next(10000000, 99999999).ToString();
                        }
                        catch { }

                        #region query
                        string leQuery =
                            $"INSERT INTO `{serverID}`.embeds " +
                            $"(`datetime`, " +
                            $"`embedHash`, " +
                            $"`messageFID`, " +
                            $"`messageID`, " +
                            $"`color`, " +
                            $"`author_name`, " +
                            $"`author_url`, " +
                            $"`author_iconUrl`, " +
                            $"`author_proxyIconUrl`, " +
                            $"`title`, " +
                            $"`description`, " +
                            $"`field_count`, " +
                            $"`url`, " +
                            $"`thumbnailUrl`, " +
                            $"`thumbnailProxyUrl`, " +
                            $"`videoUrl`, " +
                            $"`footer_text`, " +
                            $"`footer_iconUrl`, " +
                            $"`footer_proxyUrl`, " +
                            $"`provider_name`, " +
                            $"`provider_url`, " +
                            $"`timestamp`) VALUES " +
                            $"('{dateTime:yyyy-MM-dd HH:mm:ss}', " +        //datetime
                            $"'{embedHash}', " +                            //embedHash
                            $"'{messageFID}', " +                           //messageFID
                            $"'{message._messageId}', " +                   //messageID
                            $"'{le._color}', " +                            //color
                            $"'{le._authorName.Replace("'", "''")}', " +    //author_name
                            $"'{le._authorUrl}', " +                        //author_url
                            $"'{le._authorIconUrl}', " +                    //author_iconUrl
                            $"'{le._authorProxyIconUrl}', " +               //author_proxyIconUrl
                            $"'{le._title.Replace("'", "''")}', " +         //title
                            $"'{le._description.Replace("'", "''")}', " +   //description
                            $"'{le._fields.Count}', " +                     //field_count
                            $"'{le._url}', " +                              //url
                            $"'{le._thumbnailUrl}', " +                     //thumbnailUrl
                            $"'{le._thumbnailProxyUrl}', " +                //thumbnailProxyUrl
                            $"'{le._videoUrl}', " +                         //videoUrl
                            $"'{le._footerText.Replace("'", "''")}', " +    //footer_text
                            $"'{le._footerIconUrl}', " +                    //footer_iconUrl
                            $"'{le._footerProxyUrl}', " +                   //footer_proxyUrl
                            $"'{le._providerName.Replace("'", "''")}', " +  //provider_name
                            $"'{le._providerUrl}', " +                      //provider_url
                            $"'{le._timestamp}');";                         //timestamp
                        #endregion
                        MM.Print(consoleWrite, leQuery);
                        executedEmbeds = executeNonQuery(leQuery);

                        if (le._fields != null && le._fields.Count > 0)
                        {
                            foreach (LogEmbedField lef in le._fields)
                            {
                                #region query
                                string lefQuery =
                                    $"INSERT INTO `{serverID}`.embed_fields " +
                                    $"(`datetime`, " +
                                    $"`embedHash`, " +
                                    $"`name`, " +
                                    $"`value`, " +
                                    $"`index`) VALUES " +
                                    $"('{dateTime:yyyy-MM-dd HH:mm:ss}', " +    //datetime
                                    $"'{le._embedHash}', " +                    //embedHash
                                    $"'{lef._name.Replace("'", "''")}', " +     //name
                                    $"'{lef._value.Replace("'", "''")}', " +    //value
                                    $"'{le._fields.IndexOf(lef)}');";           //index
                                #endregion
                                MM.Print(consoleWrite, lefQuery);
                                executedEmbedFields = executeNonQuery(lefQuery);
                            }
                        }
                        else executedEmbedFields = true;
                    }
                }
                else executedEmbeds = executedEmbedFields = true;

                if (executedMessages && executedAttachments && executedEmbeds && executedEmbedFields) return true;
                else
                {
                    MM.Print(consoleWrite, $"Messages: {executedMessages}{(message._attachmentData != null ? $"\nAttachments: {executedAttachments}" : "")}{(message._embedData != null ? $"\nEmbeds: {executedEmbeds}\nEmbed Fields: {executedEmbedFields}" : "")}");
                    return false;
                }
            }
            catch (Exception e)
            {
                closeIfPossible();
                MM.Print(consoleWrite, $"ERROR\nMessages: {executedMessages}{(message._attachmentData != null ? $"\nAttachments: {executedAttachments}" : "")}{(message._embedData != null ? $"\nEmbeds: {executedEmbeds}\nEmbed Fields: {executedEmbedFields}" : "")}\n{e}\n\n[{query}]");
                return false;
            }
        }

        /// <summary> Logs an interaction to the DB related to the server that the interaction occurred on </summary>
        public bool logInteraction(ulong serverID, string interactionType)
        {
            bool consoleWrite = true,
				 executedInsert = false,
                 executedUpdate = false;
            try
            {                
                DataSet result = executeQuery($"SELECT `type`, `count` from `{serverID}`.interactions WHERE `type` = '{interactionType}';");
                int count = 0;

                if (result.Tables.Count > 0 && result.Tables[0] != null && result.Tables[0].Rows.Count == 0)
                {
                    executedInsert = 
                        executeNonQuery($"INSERT INTO `{serverID}`.`interactions` (`type`, `count`) VALUES ('{interactionType}', '0');");

                    string contents = File.ReadAllText("./../../Misc/Create Server Tables.sql");
                    if (!contents.Contains(interactionType))
                    {
                        contents = contents.Replace("INSERT INTO `interactions` (`type`, `count`) VALUES ('ping', '0');",
                            $"INSERT INTO `interactions` (`type`, `count`) VALUES ('{interactionType}', '0');" +
                            "\nINSERT INTO `interactions` (`type`, `count`) VALUES ('ping', '0');");
                        File.WriteAllText("./../../Misc/Create Server Tables.sql", contents);
                    }

                    count = 0;
                }
                else if (int.TryParse(result.Tables[0].Rows[0]["count"].ToString(), out int ct)) count = ct;

                executedUpdate = updateTable(serverID.ToString(),
                    "interactions",
                    new List<string>() { "`count`" },
                    new List<string>() { $"'{count + 1}'" },
                    $"`type` = '{interactionType}'");

                return executedInsert && executedUpdate;
            }
            catch (Exception e)
            {
                closeIfPossible();
                MM.Print(consoleWrite, e.ToString());
                return executedInsert && executedUpdate;
            }
        }
		#endregion

		#region polls
		public bool addPoll(string ID, string pollTitle, string schemaName)
		{
			try
			{
				openIfPossible();
				MySqlCommand cmd = new($"{schemaName}.{StoredProcedure.addPoll}", _connection)
				{ CommandType = CommandType.StoredProcedure };
				cmd.Parameters.AddWithValue("inID", ID);
				cmd.Parameters.AddWithValue("poll_title", pollTitle);
				cmd.ExecuteNonQuery();
				return true;
			}
			catch (Exception e)
			{ Console.WriteLine($"addPoll: {e.Message}"); }

			return false;
		}

		public List<string> getActivePolls(string schemaName)
		{
			try
			{
				openIfPossible();
				MySqlCommand cmd = new($"{schemaName}.{StoredProcedure.getActivePolls}", _connection)
				{ CommandType = CommandType.StoredProcedure };

				using MySqlDataAdapter sda = new(cmd);

				DataTable data = new();
				sda.Fill(data);

				if (data != null && data.Rows.Count > 0)
				{
					List<string> ids = new();

					foreach (object row in data.Rows)
					{
						if (row.ToString().Length > 20 && row.ToString().Contains('/'))
							ids.Add(row.ToString());
					}

					return ids;
				}
			}
			catch (Exception e)
			{ Console.WriteLine($"getActivePolls: {e.Message}"); }

			return new List<string>();
		}

		public bool deactivatePoll(string ID, string schemaName)
		{
			try
			{
				openIfPossible();
				MySqlCommand cmd = new($"{schemaName}.{StoredProcedure.deactivatePoll}", _connection)
				{ CommandType = CommandType.StoredProcedure };
				cmd.Parameters.AddWithValue("inID", ID);
				cmd.ExecuteNonQuery();
				return true;
			}
			catch (Exception e)
			{ Console.WriteLine($"deactivatePoll: {e.Message}"); }

			return false;
		}
		#endregion

		#region utilities
		/// <summary> Combines a list of columns into a comma-separated list </summary>
		internal static string combineColumns(List<string> columnList)
        {
            string columns = "";
            foreach (string s in columnList) columns += s.Trim() + (columnList.IndexOf(s) + 1 == columnList.Count ? "" : ", ");
            return columns;
        }

		#region data formatting
		/// <summary> Prints results from query to a results embed </summary>
		public bool printDataEmbeds(IMessageChannel channel, List<string> columns, DataSet result, int maxResults = 0, string resultTitle = "Row", bool formatAsMessage = false, int indexFromEnd = -1, ulong serverID = 0)
        {
            if (columns.Count == 1 && columns[0].Trim() == "*")
            {
                columns = new List<string>();

                foreach (DataColumn column in result.Tables[0].Columns)
                    columns.Add(column.ColumnName);
            }

            if (!formatAsMessage)
            {
                List<List<EmbedFieldBuilder>> efList = new();

                foreach (DataRow row in result.Tables[0].Rows)
                {
                    if (maxResults > 0 && efList.Count >= maxResults) break;

                    if (indexFromEnd == -1 || (indexFromEnd != -1 && result.Tables[0].Rows.Count - result.Tables[0].Rows.IndexOf(row) <= indexFromEnd))
                    {
                        List<EmbedFieldBuilder> efs = new();

                        foreach (string column in columns)
                        {
                            string content = row[column.Trim()].ToString();
                            List<string> contentSplit = new();

                            while (content.Length > 0)
                            {
                                int toRemove = content.Length;
                                if (content.Length > 1023) toRemove = 1023;

                                contentSplit.Add(content[..toRemove]);
                                content = content.Remove(0, toRemove);
                            }

                            foreach (string c in contentSplit)
                            {
                                efs.Add(new EmbedFieldBuilder()
                                {
                                    Name = contentSplit.IndexOf(c) == 0 ? column : $"{column} continued",
                                    Value = c,
                                    IsInline = true
                                });
                            }
                        }

                        efList.Add(efs);
                    }
                }

                bool successfulSendForAllEmbeds = true;

                foreach (List<EmbedFieldBuilder> efs in efList)
                {
                    List<List<EmbedFieldBuilder>> efsShortened = new();

                    while (efs.Count > 0)
                    {
                        int toRemove = 25;
                        if (efs.Count < 25) toRemove = efs.Count;

                        List<EmbedFieldBuilder> temp = new();

                        for (int i = 0; i < toRemove; i++)
                        {
                            temp.Add(efs[0]);
                            efs.RemoveAt(0);
                        }

                        efsShortened.Add(temp);
                    }

                    foreach (List<EmbedFieldBuilder> efs2 in efsShortened)
                    {
                        if (null == Bot.sendEmbed(channel, Color.Blue, fields: efs2, title: $"{resultTitle} {efList.IndexOf(efs) + 1}/{efList.Count}{(efsShortened.IndexOf(efs2) == 0 ? "" : " continued")}"))
                            successfulSendForAllEmbeds = false;
                    }
                }

                return successfulSendForAllEmbeds;
            }
            else
            {
                List<List<EmbedFieldBuilder>> efList = new();
                List<EmbedFieldBuilder> efs = new();
                List<ComponentBuilder> cbList = new();
                ComponentBuilder cb = new();
				SelectMenuBuilder menu = new()
				{
					Placeholder = "Select to view a message's Embed or Attachment",
					CustomId = serverID.ToString()
				};
				int messageCount = 0;

                foreach (DataRow row in result.Tables[0].Rows)
                {
                    if (maxResults > 0 && messageCount >= maxResults) break;

                    if (indexFromEnd == -1 || (indexFromEnd != -1 && result.Tables[0].Rows.Count - result.Tables[0].Rows.IndexOf(row) <= indexFromEnd))
                    {
                        bool showDetails = columns.Exists(col => col == "showDetails");
                        string numSize = new('x', maxResults > 0 ? maxResults.ToString().Length : result.Tables[0].Rows.Count);
                        int embedSize = $"{resultTitle} [`{numSize}`-`{numSize}`/`{numSize}`]".Length,
                            embedCount = 0, attachmentCount = 0,
                            rowIndex = result.Tables[0].Rows.IndexOf(row);
                        foreach (EmbedFieldBuilder e in efs) embedSize += e.Name.Length + e.Value.ToString().Length;
						if (int.TryParse(row["embedCount"].ToString(), out int eCt)) embedCount = eCt;
						if (int.TryParse(row["attachmentCount"].ToString(), out int aCt)) attachmentCount = aCt;
                        string newFieldName = $"``` ```";
                        string newFieldContent = $"**#{messageCount + 1}: {(resultTitle.StartsWith("Deleted") ? $"{row["type"]} " : "")}[[{row["messageID"]}]({Bot.getChannelLink(ulong.Parse(row["channelID"].ToString()))}/{row["messageID"]})] ({row["datetime"]})\n" +
                            $"__<@{row["authorID"]}> ({row["authorName"]}{(showDetails ? $" : `{row["authorID"]}`)\n" : ")")} " +
                            $"in [{row["channelName"]}]({Bot.getChannelLink(ulong.Parse(row["channelID"].ToString()))}){(showDetails ? $" : (`{row["channelID"]}`)" : "")}{(embedCount > 0 || attachmentCount > 0 ? "\n" : ":__**\n")}" +
                            $"{(embedCount > 0 ? $"[Embeds: `{embedCount}`]{(attachmentCount > 0 ? "\n" : ":__**\n")}" : "")}" +
                            $"{(attachmentCount > 0 ? $"[Attachments: `{attachmentCount}`]:__**\n" : "")}";
                        int numOfFields = (int)Math.Ceiling((double)row["content"].ToString().Length / 1023);
                        int newFieldSize = newFieldName.Length + newFieldContent.Length + row["content"].ToString().Length + ("previous message continued".Length * 3);

                        if (efs.Count + numOfFields >= 24 || embedSize + newFieldSize > 6000)
                        {
                            efList.Add(efs);
                            efs = new List<EmbedFieldBuilder>();
                            cb.WithSelectMenu(menu);
                            menu = new SelectMenuBuilder();
                            cbList.Add(cb);
                            cb = new ComponentBuilder();
                            menu.Placeholder = "Select to view a message's Embed or Attachment";
                            menu.CustomId = $"{serverID}";
                        }

                        string content = $"{newFieldContent}\n{(string.IsNullOrEmpty(row["content"].ToString()) ? "" : $"{row["content"]}\n ")}";
                        List<string> contentSplit = new();

                        while (content.Length > 0)
                        {
                            int toRemove = content.Length;
                            if (content.Length > 1023) toRemove = 1023;

                            contentSplit.Add(content[..toRemove]);
                            content = content.Remove(0, toRemove);
                        }

                        if (contentSplit.Count == 0) contentSplit.Add("`[NULL MSG CONTENT]`");
                        if (embedCount > 0 || attachmentCount > 0)
                        {
                            string FID = row["FID"].ToString();

                            for (int i = 0; i < embedCount; i++)
                            {
                                menu.AddOption(new SelectMenuOptionBuilder()
                                {
                                    Label = $"Message {messageCount + 1}: Embed {(embedCount > 1 ? $"{i + 1}" : "")}",
                                    Value = $"e{FID};{i}"
                                });
                            }

                            for (int i = 0; i < attachmentCount; i++)
                            {
                                menu.AddOption(new SelectMenuOptionBuilder()
                                {
                                    Label = $"Message {messageCount + 1}: Attachment {(embedCount > 1 ? $"{i + 1}" : "")}",
                                    Value = $"a{FID};{i}"
                                });
                            }
                        }                        

                        foreach (string c in contentSplit)
                        {
                            efs.Add(new EmbedFieldBuilder()
                            {
                                Name = contentSplit.IndexOf(c) == 0 ? newFieldName : "previous message continued",
                                Value = c,
                                IsInline = false
                            });
                        }

                        messageCount++;
                    }
                }

                if (!efList.Exists(e => e == efs)) efList.Add(efs);
                if (!cbList.Exists(c => c == cb))
                {
                    cb.WithSelectMenu(menu);
                    cbList.Add(cb);
                }
                
                bool successfulSendForAllEmbeds = true;
                int index = 0, indexEnd = 0;

                for (int i = 0; i < efList.Count; i++)
                {
                    foreach (EmbedFieldBuilder e in efList[i])
                    {
                        if (e.Name != "previous message continued") indexEnd++;
                    }

                    IMessage message = Bot.sendEmbed(channel, Color.Blue,
                        title: $"{resultTitle} [`{index + 1}`-`{indexEnd}`/`{messageCount}`]",
                        author: resultTitle.Contains("Messages from ") ?
                            Bot._client.GetGuild(serverID).GetUser
                            (
                                getUser(Bot._client.GetGuild(serverID),
                                resultTitle[..resultTitle.IndexOf(resultTitle.Contains(" in ") ? " in " : " on ")].Replace(resultTitle[..(resultTitle.IndexOf("from ") + 5)], "").Trim().Replace("`", ""))
                            ) :
                            null,
                        fields: efList[i], 
                        components: 
                            cbList[i] != null && (cbList[i].ActionRows[0].Components[0] as SelectMenuComponent).Options.Count >= 1 ? 
                            cbList[i] : null);
                    if (message == null) successfulSendForAllEmbeds = false;
                    else _embedReferences.Add(message);

                    index = indexEnd + 1;
                }

                return successfulSendForAllEmbeds;
            }
        }

        /// <summary> Send Attachments or Embeds related to messages shown in results Embed that the user selects </summary>
        public bool sendAttachmentsOrEmbeds(IMessageChannel channel, string serverID, string customID)
        {
            bool attachment = customID[0] == 'a'; //true for attachment, false for embed
            string FID = customID.Remove(customID.IndexOf(";"))[1..];
            int index = 0;
            if (int.TryParse(customID[(customID.IndexOf(";") + 1)..], out int indx)) index = indx;
            DataSet messageInfo = executeQuery($"SELECT `authorID`, `channelID`, `messageID`, `authorName`, `channelName`, `type` FROM `{serverID}`.messages WHERE `FID` = '{FID}';");

            if (messageInfo.Tables.Count > 0 && messageInfo.Tables[0] != null && messageInfo.Tables[0].Rows.Count > 0)
            {
                DataRow mi = messageInfo.Tables[0].Rows[0];
                string type = mi["type"].ToString(),
                    userName = mi["authorName"].ToString(),
                    channelName = mi["channelName"].ToString();
                ulong userID = 0, channelID = 0, messageID = 0;
                if (ulong.TryParse(mi["authorID"].ToString(), out ulong uID)) userID = uID;
				if (ulong.TryParse(mi["channelID"].ToString(), out ulong cID)) channelID = cID;
				if (ulong.TryParse(mi["messageID"].ToString(), out ulong mID)) messageID = mID;

                if (attachment)
                {
                    DataSet data = executeQuery($"SELECT * from `{serverID}`.attachments \n WHERE `messageFID` = '{FID}';");
                    if (data.Tables.Count > 0 && data.Tables[0] != null && data.Tables[0].Rows.Count > 0)
                    {
                        DataRow result = data.Tables[0].Rows[index];
                        Bot.sendEmbed(channel, Color.Green, title: $"__**GetRecent:**__ Attachment {(type == "D" ? $"from deleted message (`{messageID}`) in `{channelName}`" : $"sent by {userName} in {channelName}. (`{messageID}`)")}", 
                            description: $"Sent by: <@{userID}>\n{(type == "D" ? $"[Channel Link]({Bot.getChannelLink(channelID)})" : $"[Message Link]({Bot.getChannelLink(channelID)}/{messageID})")}\n[Proxy Link]({result["proxyUrl"]})", imageURL: result["url"].ToString());
                    }
                    else return false;
                }
                else
                {
                    DataSet data = executeQuery($"SELECT * from `{serverID}`.embeds \n WHERE `messageFID` = '{FID}';");
                    if (data.Tables.Count > 0 && data.Tables[0] != null && data.Tables[0].Rows.Count > 0)
                    {
                        DataRow result = data.Tables[0].Rows[index];
                        string embedHash = result["embedHash"].ToString();
                        if (!string.IsNullOrEmpty(embedHash))
                        {
                            DataSet dataFields = executeQuery($"SELECT * from `{serverID}`.embed_fields \n WHERE `embedHash` = '{embedHash}';");
                            if (dataFields.Tables.Count > 0 && dataFields.Tables[0] != null)
                            {

                                DataRowCollection fieldsResults = dataFields.Tables[0].Rows;
                                EmbedBuilder eb = new()
								{
                                    Color = !string.IsNullOrEmpty(result["color"].ToString()) ? Bot.getColorByHash(result["color"].ToString()) : Color.Default
                                };

                                if (!string.IsNullOrEmpty(result["author_name"].ToString()))
                                {
									EmbedAuthorBuilder eba = new()
									{
										Name = result["author_name"].ToString()
									};
									if (!string.IsNullOrEmpty(result["author_url"].ToString()))
                                        eba.Url = result["author_url"].ToString();
                                    if (!string.IsNullOrEmpty(result["author_iconUrl"].ToString())) 
                                        eba.IconUrl = result["author_iconUrl"].ToString();

                                    eb.Author = eba;
                                }

                                if (!string.IsNullOrEmpty(result["title"].ToString())) 
                                    eb.Title = result["title"].ToString();

                                if (!string.IsNullOrEmpty(result["description"].ToString())) 
                                    eb.Description = result["description"].ToString();

                                if (!string.IsNullOrEmpty(result["url"].ToString()))
                                    eb.Url = result["url"].ToString();

                                if (!string.IsNullOrEmpty(result["thumbnailUrl"].ToString()))
                                    eb.ThumbnailUrl = result["thumbnailUrl"].ToString();

                                if (!string.IsNullOrEmpty(result["footer_text"].ToString()) ||
                                    !string.IsNullOrEmpty(result["footer_iconUrl"].ToString()))
                                {
                                    EmbedFooterBuilder ebf = new();
                                    if (!string.IsNullOrEmpty(result["footer_text"].ToString()))
                                        ebf.Text = result["footer_text"].ToString();
                                    if (!string.IsNullOrEmpty(result["footer_iconUrl"].ToString()))
                                        ebf.IconUrl = result["footer_iconUrl"].ToString();

                                    eb.Footer = ebf;
                                }

                                if (result["timestamp"].ToString() != "00:00:00")
                                    eb.Timestamp = DateTime.Parse(result["timestamp"].ToString());

                                if (fieldsResults.Count > 0)
                                {
                                    List<EmbedFieldBuilder> fields = new();

                                    foreach (DataRow field in fieldsResults)
                                    {
                                        fields.Add(new EmbedFieldBuilder()
                                        {
                                            Name = field["name"].ToString(),
                                            Value = field["value"].ToString()
                                        });
                                    }

                                    eb.Fields = fields;
                                }

                                channel.SendMessageAsync(
                                    $"__**GetRecent:**__ Embed {(type == "D" ? $"from deleted message (`{messageID}`) in `{channelName}`" : $"sent by ({userName}) in {channelName}. (`{messageID}`)")}\n" +
                                    $"Sent by: <@{userID}>\n{(type == "D" ? $"**Channel:** {Bot.getChannelLink(channelID)}" : $"**Message:** {Bot.getChannelLink(channelID)}/{messageID}")}", 
                                    embed: eb.Build());
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
            }
            else return false;            

            return true;
        }
		#endregion

		#region get
		/// <summary> Returns user ID based on a Server and username </summary>
		public ulong getUser(SocketGuild server, string userName)
        {
            int count = 0;
            ulong userID = 0;

            //Checks the server for a user with the same username or nickname as the userName parameter
            foreach (IGuildUser user in server.Users)
            {
                if (user.Username == userName || user.Nickname == userName)
                {
                    count++;
                    userID = user.Id;
                }
            }

            //If there are no results from the server, search the database for the user
            if (count == 0)
            {
                DataSet userData = executeQuery("SELECT `authorID`, `authorName`\n" +
                $"FROM `{server.Id}`.`messages` \nGROUP BY(`authorID`)");

                if (userData.Tables.Count > 0 && userData.Tables[0] != null)
                {
                    DataRowCollection userDataRows = userData.Tables[0].Rows;
                    foreach (DataRow userDataRow in userDataRows)
                    {
                        if (userDataRow["authorName"].ToString() == userName)
                        {
                            count++;
                            userID = ulong.Parse(userDataRow["authorID"].ToString());
                        }
                    }
                }
            }

            if (count > 1) userID = 1;

            //userID = 0 means no results found; userID = 1 means too many results found
            return userID;
        }

        /// <summary> Returns channel ID based on a Server and channel name </summary>
        public ulong getChannel(SocketGuild server, string channelName)
        {
            int count = 0;
            ulong channelID = 0;

            //Checks the server for a channel with the same name as the channeName parameter
            foreach (IChannel channel in server.Channels)
            {
                if (channel.Name == channelName)
                {
                    count++;
                    channelID = channel.Id;
                }
            }

            //If there are no results from the server, search the database for the channel
            if (count == 0)
            {
                DataSet channelData = executeQuery("SELECT `channelID`, `channelName`\n" +
                $"FROM `{server.Id}`.`messages` \nGROUP BY(`channelID`)");

                if (channelData.Tables.Count > 0 && channelData.Tables[0] != null)
                {
                    DataRowCollection channelDataRows = channelData.Tables[0].Rows;
                    foreach (DataRow channelDataRow in channelDataRows)
                    {
                        if (channelDataRow["channelName"].ToString() == channelName)
                        {
                            count++;
                            channelID = ulong.Parse(channelDataRow["channelID"].ToString());
                        }
                    }
                }
            }

            if (count > 1) channelID = 1;

            //channelID = 0 means no results found; channelID = 1 means too many results found
            return channelID;
        }

        /// <summary> Returns server ID based on a server name </summary>
        public ulong getServer(string serverName)
        {
            //Search the database for the channel
            DataSet serverResults = executeQuery($"SELECT DISTINCT `serverID` FROM `servers`.`servernames` WHERE `serverName` = '{serverName.Replace("'", "''")}';");
            ulong serverID = 0;

            if (serverResults.Tables.Count > 0 && serverResults.Tables[0] != null)
            {
                DataRowCollection serverOptions = serverResults.Tables[0].Rows;

                //serverID = 0 means no results found; serverID = 1 means too many results found
                if (serverOptions.Count > 1) return 1;
                else if (serverOptions.Count == 0) return 0;
                else if (ulong.TryParse(serverOptions[0]["serverID"].ToString(), out ulong sID)) serverID = sID;
            }
                
            return serverID;
        }
		#endregion

		#region connections
		/// <summary> Connect to the MySQL DB if possible </summary>
		public bool openIfPossible(bool consoleWrite = true)
        {
            //Create a new connection if the connection is null
            if (_connection == null) (_connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=" + File.ReadAllText("sqlup.txt"))).Open();

            try
            {
                //If it's closed, open
                if (_connection.State == System.Data.ConnectionState.Closed)
                {
                    _connection.Open();
                    //Wait until the connection has been established
                    while (_connection.State == System.Data.ConnectionState.Connecting) { }
                }
			}
			catch (Exception e) { MM.Print(consoleWrite, e.ToString()); }

			return _connection.State is not System.Data.ConnectionState.Closed and not System.Data.ConnectionState.Broken;
        }

        /// <summary> Close connection to the MySQL DB if possible </summary>
        public bool closeIfPossible(bool consoleWrite = true) 
        {
            if (_connection == null) (_connection = new MySqlConnection("datasource=localhost;port=3306;username=root;password=" + File.ReadAllText("sqlup.txt"))).Open();
            try
            {
                //If it's not already closed, fetching results, or executing a query, close
                if (_connection.State is not System.Data.ConnectionState.Closed and
					not System.Data.ConnectionState.Fetching and
					not System.Data.ConnectionState.Executing)
                    _connection.Close();
            }
            catch (Exception e) { MM.Print(consoleWrite, e.ToString()); }

            return _connection.State == System.Data.ConnectionState.Closed;
        }
        #endregion
        #endregion
    }
}