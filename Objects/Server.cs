﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PolyphasicScheduleFinder_Bot
{
    class Server
    {
        #region attributes
        /// <summary> Server object to track the server parameters and variables </summary>
        public Server(ulong id, string name, bool inDMs)
        {
            _id = id;
            _name = name;
            _inDMs = inDMs;
			if (Bot._testBot || Config._doNotLog.Contains(id)) _logging = false;
			else if (Config._LoggingWithGSheets) _logger = new Logger();
			_SH = new ScheduleFinder(this, new NapchartInterface());
        }
        
        /// <summary> Server ID </summary>
        internal ulong _id;
        /// <summary> Server Name </summary>
        internal string _name;
        internal bool _inDMs;
        /// <summary> Tracks whether or not server is logging messages sent to it </summary>
        internal bool _logging = true;
        /// <summary> Logger object for handling logging </summary>
        internal Logger _logger;
        /// <summary> ScheduleHelper object for handling Schedule Finding </summary>
        internal ScheduleFinder _SH;

        /// <summary> If it's been validated in the logger then don't check again </summary>
        internal bool _validated = false;

        /// <summary> Used to react with thumbsup if user thanks on server </summary>
        internal bool _awaitingThanks;
        /// <summary> The channel that thanks may be occurring in </summary>
        internal ulong _responseChannel = 0;
        /// <summary> The person that is using the user potentially replying with "Thanks" </summary>
        internal SocketUser _responder;

        private const string _memePath = @"..\..\..\Misc\Memes\";
        /// <summary> The time at which SH begins sending memes </summary>
        internal DateTime memeStart = new();
        /// <summary> Current meme in the List being sent </summary>
        internal int memeIndex = 0;
        /// <summary> List of image file links to be sent by SH </summary>
        internal List<string> memes = new();
        /// <summary> List of captions to be added to memes </summary>
        internal List<string> memeCaptions = new();
        /// <summary> Tracks when SH has become exhausted by sending memes and is taking a break </summary>
        internal bool exhausted = false;
        /// <summary> Count of how many memes can be sent before SH is exhausted </summary>
        internal int memeExhaustion;
        /// <summary> List of replies SH sends when asked for a meme while exhausted </summary>
        internal List<string> exhaustedReplies = new();
        /// <summary> Time at which SH became exhausted </summary>
        internal DateTime exhaustionTime = new();

        /// <summary> Server Random Number Generator </summary>
        private static readonly Random rng = new();
        #endregion

        #region utilities
        /// <summary> Create list of memes </summary>
        /// <param name="napgodID">ID of current server to see if it's a NapGod server</param>
        internal void createMemeList(SocketMessage message)
        {
            ulong napgodID = !_inDMs ? (message.Channel as SocketGuildChannel).Guild.Id : 0;

            exhausted = false;
            exhaustionTime = new DateTime();
            memeStart = DateTime.Now;
            memeIndex = 0;

            string memePath = Directory.Exists($@".\{_memePath}") ? _memePath : ".\\..\\" + _memePath;
            
            if (napgodID == 249219704655183876)
            {
                foreach (string link in Directory.GetFiles($@".\{memePath}NapGod\", "*.jpg", SearchOption.AllDirectories)) memes.Add(link);
                memeCaptions.Add("Check out #polymemes for more :wink:");
                exhaustedReplies.Add("... hold_your_horses");
            }
            else exhaustedReplies.Add("Hold your horses my guy, I'll send more in a bit!");
            foreach (string link in Directory.GetFiles($@".\{memePath}Polymemes\", "*.jpg", SearchOption.AllDirectories)) memes.Add(link);
            memes.Add("<https://tinyurl.com/6ec4p4fj>");

            memes = memes.OrderBy(a => rng.Next()).ToList();

            memeExhaustion = rng.Next(2, 10);

            #region captions & replies
            memeCaptions.Add("Here you are, a delectable meme straight from the press.");
            memeCaptions.Add("Wisdom of the NapGods");
            memeCaptions.Add("Come one, come all! Get your memes here!");
            memeCaptions.Add("I think you'll like this one");
            memeCaptions.Add("Boom.");
            memeCaptions.Add("Here you go!");
            memeCaptions.Add("Hope this does the trick");
            memeCaptions.Add("How's this one?");
            memeCaptions.Add("Only for you uwu");
            memeCaptions.Add("Only the finest stolen memes here :triumph:");
            memeCaptions.Add(":raised_hands:");
            memeCaptions.Add("There's more where this came from!");
            memeCaptions.Add(":joy:");
            memeCaptions.Add(":rofl:");
            memeCaptions.Add(":laughing:");
            memeCaptions.Add("rip");
            memeCaptions.Add("This is a good one");
            memeCaptions.Add("I've got more memes.");
            memeCaptions.Add("Don't *sleep* on this one :zany_face:");
            memeCaptions.Add("ok here");

            exhaustedReplies.Add("Bruh. :rolling_eyes:");
            exhaustedReplies.Add("Be patient, dude.");
            exhaustedReplies.Add("Impatient much?");
            exhaustedReplies.Add("Geez, will you chill?");
            exhaustedReplies.Add("Why don't *you* send memes for once?");
            exhaustedReplies.Add("Don't you have a nap to take or something? Shoo");
            exhaustedReplies.Add(":neutral_face:");
            exhaustedReplies.Add(":unamused::watch:");
            exhaustedReplies.Add("...");
            #endregion
        }

        /// <summary> Reset server variables </summary>
        internal void clear()
        {
            if(_SH._monoBaseline != 8) SFAlgorithm.modifyMono(8); //reset mono
            _awaitingThanks = false;
            _responseChannel = 0;
            _responder = null;

            _SH._advancedResults = false;
            _SH._scheduleFinding = ScheduleFinder.ScheduleFindingStage.NOT_SCHEDULEFINDING;
            _SH._responder = null;
            _SH._responderName = null;
            _SH._responseChannel = null;
            _SH._embedMsg = null;
            _SH._initialMessage = null;
            _SH._napchartSchedules = null;
            _SH._results = new List<Schedule>();
            _SH._sleepTimeAvailability = null;
            _SH._resultsMessage = null;
            _SH._resultsInitialEmbed = null;
            _SH._componentInteraction = null;
            _SH._age = 0;
            _SH._experience = Bot.Level.unset;
            _SH._monoBaseline = -1;
            _SH._activityLevel = Bot.Level.unset;
            _SH._sleepTimes = "";
            _SH.updateErrorCount(0);
        }
        #endregion
    }
}