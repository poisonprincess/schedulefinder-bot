﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace PolyphasicScheduleFinder_Bot
{
    class UserCommand : Command
    {
        /// <summary> updates any local stored config variables </summary>
        internal void updateConfigVars()
        {
            _alert = Bot.getEmote(Config._emojiNames["Alert"]);
            _prefix = new string(Config._commandPrefix[0], Bot._testBot ? 2 : 1);

            _commands = new List<(string commandName, string category, string description, string properFormat)>
            {
                ("Help", "Information",
                    "*Shows commands.*\n",
                    $"```{_prefix}Help [Optional: Command category (show categories if blank)]```"),
                ("FindSchedules", "ScheduleFinding",
                    "*Begin ScheduleFinding.*\n",
                    $"```(Command: FindSchedules or FS) {_prefix}FindSchedules [Optional: Activity/Age/Polyphasic Sleeping Experience/Monophasic sleep length baseline/Sleep time restrictions (if blank, prompt for each entry)]```"),
                ("Fss", "ScheduleFinding",
                    "*Begin ScheduleFinding.*\n",
                    $"```(Command: FindSchedules or FS) {_prefix}FindSchedules [Optional: Activity/Age/Polyphasic Sleeping Experience/Monophasic sleep length baseline/Sleep time restrictions (if blank, prompt for each entry)]```"),
                ("Exit", "ScheduleFinding",
                    "*Exit ScheduleFinding process.*\n",
                    $"```{_prefix}Exit```"),
                ("RandomSchedule", "Napcharts",
                    "*Sends a randomly generated schedule.*\n",
                    $"`(Command: RandomSchedule or RS)`\n```{_prefix}RandomSchedule```\n" +
					$"```{_prefix}RandomSchedule [Number of sleeps]```\n" +
                    $"```{_prefix}RandomSchedule [Name of recognized polyphasic schedule]```\n" +
                    $"```{_prefix}RandomSchedule [Any of the following: [Count: (number of sleeps)], [LenAlg: (random, equal, minmax, spiral)], [GapAlg: (random, equal, spiral)], [Start: (start time)]]```\n" +
					$"```{_prefix}RandomSchedule help```"),
                ("CreateNapchart", "Napcharts",
                    "*Create a custom [napchart](https://Napchart.com/app) using the Napchart Api (creating napcharts this way allows for usage of hidden features such as [custom colors](https://napchart.com/snapshot/6X7MD228D), time increments [less than the normal minimum](https://napchart.com/snapshot/JxBGkdQ7r) of 5m, and [24h blocks](https://napchart.com/snapshot/ip5YuFHDs)).*\n",
                    $"`(Command: CreateNapchart or CN)`\n```{_prefix}CreateNapchart objective: sleep times (ex: 00:00-07:00 ([Optional: block text]), 12:30-14:50, 21:00-21:35); Lane Number; Color (up to 10 objectives) \ncolorTag: Color = Color Label [Optional: up to one per color]```"),
                ("CNExample", "Napcharts",
                    "*Sends an example for the `CreateNapchart` command.*\n",
                    $"```{_prefix}CNExample```"),
                ("Ban", "Misc",
                    "*Bans users ||not really lmao||.*\n",
                    $"```{_prefix}Ban @user [Optional:, @user2, @user3, etc.]```")
            };
        }

		#region schedulefinding
		#region findschedules
		/// <summary> begin schedulefinding | -findSchedules </summary>
		public static void FindSchedules(SocketMessage message, string commandName = "FindSchedules")
		{
			if (message.Content.Split(' ')[0].ToLower() == "-findschedule") commandName = "—FindSchedule";
			Server server = Bot.getServer(message.Channel);
			string prefix = $"{Config._commandPrefix}{ (Bot._testBot ? Config._commandPrefix : "")}";
			string msg = message.Content.ToString().Trim().Remove(0, $"{prefix}{commandName}".Length);
			GuildEmote alert = Bot.getEmote(Config._emojiNames["Alert"]);

			string msgTemp = msg;
			if (msgTemp.Contains("http") && Uri.IsWellFormedUriString(msgTemp[msgTemp.IndexOf("http")..], UriKind.Absolute)) msgTemp = msgTemp.Remove(msgTemp.IndexOf("http"));

			if ((msgTemp.Length - msgTemp.Replace("\\", "").Length) == 4 || (msgTemp.Length - msgTemp.Replace("/", "").Length) == 4)
			{
				string[] entries = msgTemp.Contains('\\') ? msgTemp.Split('\\') : msgTemp.Split('/');

				Bot.Level activity = Bot.Level.unset;
				if (!string.IsNullOrWhiteSpace(entries[0]) && Enum.TryParse(entries[0].Trim().ToLower() == "med" ? "Medium" : char.ToUpper(entries[0].Trim()[0]) + entries[0].Trim()[1..], out Bot.Level act)) activity = act;

				int age = 0;
				if (int.TryParse(entries[1].Trim(), out int agetemp)) age = agetemp;

				Bot.Level experience = Bot.Level.unset;
				if (!string.IsNullOrWhiteSpace(entries[2]) && Enum.TryParse(entries[2].Trim().ToLower() == "med" ? "Medium" : char.ToUpper(entries[2].Trim()[0]) + entries[2].Trim()[1..], out Bot.Level e)) experience = e;

				double monoBaseline = 0;
				if (double.TryParse(entries[3].Trim(), out double mB)) monoBaseline = mB;
				bool unsure = entries[3].Trim().ToLower().Contains("unsure");

				string[] sleepTimes = msgTemp.Length != msg.Length ? msg.Replace(msgTemp, "").Trim().Split(',') : entries[4].Split(',');
				bool noRestrict = sleepTimes[0].ToLower().Contains("no restrictions") || sleepTimes[0].ToLower().Contains("no rs") || sleepTimes[0].ToLower().Contains("nr");
				bool napchart = NapchartInterface.isLinkValidNapchart(sleepTimes[0].Replace("<", "").Replace(">", ""));
				Schedule a = null;

				if (napchart)
				{
					List<Schedule> schedulesFromChart = NapchartInterface.getSchedulesFromLink(sleepTimes[0].Replace("<", "").Replace(">", ""), "Availability");
					if (schedulesFromChart.Count == 1) schedulesFromChart[0].name = "Availability";
					a = schedulesFromChart.Find(s => s.name == "Availability");
				}

				bool validated = ScheduleFinder.validateSleepTimes(sleepTimes);

				if (activity == Bot.Level.unset || age < ScheduleFinder._minAge || age > ScheduleFinder._maxAge || experience == Bot.Level.unset || ((monoBaseline < ScheduleFinder._minMono || monoBaseline > ScheduleFinder._maxMono) && !unsure) || (!noRestrict && (!napchart || (napchart && a == null)) && !validated))
				{
					List<EmbedFieldBuilder> errors = new();

					if (activity == Bot.Level.unset)
					{
						errors.Add(new EmbedFieldBuilder
						{
							Name = "**Activity Level**:",
							Value = "Please enter `None`, `Med`, or `High`."
						});
					}

					if (age < ScheduleFinder._minAge || age > ScheduleFinder._maxAge)
					{
						errors.Add(new EmbedFieldBuilder
						{
							Name = "**Age**:",
							Value = $"Please enter a number between `{ScheduleFinder._minAge}` and `{ScheduleFinder._maxAge}`."
						});
					}

					if (experience == Bot.Level.unset)
					{
						errors.Add(new EmbedFieldBuilder
						{
							Name = "**Experience Level**:",
							Value = "Please enter `None`, `Med`, or `High`."
						});
					}

					if ((monoBaseline < ScheduleFinder._minMono || monoBaseline > ScheduleFinder._maxMono) && !unsure)
					{
						errors.Add(new EmbedFieldBuilder
						{
							Name = "**Mono Sleep Length**:",
							Value = $"Please enter a number between `{ScheduleFinder._minMono}` and {ScheduleFinder._maxMono}`\n" +
							"*Enter `Unsure` if you don't know.*"
						});
					}

					if (!noRestrict && (!napchart || (napchart && a == null)) && !validated)
					{
						errors.Add(new EmbedFieldBuilder
						{
							Name = "**Sleep Times**:",
							Value = "Enter blocks of times you'd be able to sleep during, separated by comas like this with no overlapping sleep times:\n" +
							"`00:00-07:00, 12:30-14:50, 21:00-21:35`\n" +
							"\nOr send a [Napchart Link](https://napchart.com/app) of the times you're available to sleep with either:\n" +
							"1. Only one lane and one color.\n2. A single color labeled \"Availability\" on only one of the Napchart's lanes.\n" +
							"\n*Enter `No Restrictions` if you don't have any scheduling restrictions.*"
						});
					}

					Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **FindSchedules (condensed entry) Error** {_alert}",
						description: $"**Proper Format:** `{prefix}{commandName} [Activity Level]/[Age]/[Experience Level]/[Mono Sleep Length]/[Sleep Times]`", fields: errors);
				}
				else
				{
					ScheduleFinder sh = server._SH;
					sh._responder = message.Author;
					sh._responderName = Bot.getUsersName(message.Author, server);
					sh._responseChannel = message.Channel;
					sh._age = age;
					sh._experience = experience;
					sh._monoBaseline = unsure ? ScheduleFinder._defaultMono : monoBaseline;
					sh._activityLevel = activity;
					sh._sleepTimes = entries[4];
					if (!noRestrict)
					{
						if (napchart && a != null)
						{
							sh._sleepTimes = ScheduleFinder.getSleepTimesFromSchedule(a);
							sleepTimes = sh._sleepTimes.Split(',');
						}

						sh._sleepTimeAvailability = ScheduleFinder.createScheduleFromSleepTimes(sleepTimes, true, $"Availability for {sh._responderName}", $"Generated by ScheduleHelper.", new ColorTag("Availability", Napchart.Color.green), Napchart.Color.green);
					}
					else sh._sleepTimes = "No Restrictions";

					server._SH._scheduleFinding = ScheduleFinder.ScheduleFindingStage.SLEEPTIMES;
					sh.updateEmbed(message.Channel);

					sh.startSchedulefinding(message.Channel, message.Author, age, experience, monoBaseline, activity, sleepTimes, noRestrict);
				}
			}
			else server._SH.scheduleFinding(message, null);

			if (!Bot.isDMs(message.Channel)) Bot.changeNickname(Bot._client.GetGuild(server._id).GetUser(Bot._client.CurrentUser.Id), Bot._client.CurrentUser.Username).Wait();
		}

		#region modal ScheduleFinding
        /// <summary> Returns modal for ScheduleFinding </summary>
        internal static ModalBuilder getFSModal(ulong userID, 
            bool activityError = false, Bot.Level activity = Bot.Level.unset,
            bool ageError = false, int age = 0,
            bool experienceError = false, Bot.Level experience = Bot.Level.unset,
            bool monoError = false, int mono = 0,
            bool sleepError = false, string sleep = "", bool usedNapchart = false, bool noAvailiabilityFound = false, bool overlap = false)
        {
            string ageRange = $"{ScheduleFinder._minAge}-{ScheduleFinder._maxAge}", 
                  monoRange = $"{ScheduleFinder._minMono}-{ScheduleFinder._maxMono} *(8 if unsure)*";

			ModalBuilder modal = new()
			{
				Title = "ScheduleFinding Form",
				CustomId = $"fs[{userID}]",
				Components = new ModalComponentBuilder()

				.WithTextInput(new TextInputBuilder()
                {
					CustomId = $"activity",
					Label = $"{(activityError ? "[ERROR] " : "")}Your physical activity level:",
					Placeholder = "None, Medium, or High",
					MaxLength = 6,
					MinLength = 3,
					Required = true,
					Style = TextInputStyle.Short,
                    Value = activity != Bot.Level.unset ? activity.ToString() : null
				}) //Activity

                .WithTextInput(new TextInputBuilder()
                {
					CustomId = $"age",
					Label = $"{(ageError ? "[ERROR] " : "")}Your Age:",
					Placeholder = ageRange,
					MaxLength = 3,
					MinLength = 2,
					Required = true,
					Style = TextInputStyle.Short,
                    Value = age > 0 ? age.ToString() : null
				}) //Age

                .WithTextInput(new TextInputBuilder()
                {
					CustomId = $"experience",
					Label = $"{(experienceError ? "[ERROR] " : "")}Experience sleeping polyphasically:",
					Placeholder = "None, Medium, or High",
					MaxLength = 6,
					MinLength = 3,
					Required = true,
					Style = TextInputStyle.Short,
                    Value = experience != Bot.Level.unset ? experience.ToString() : null
				}) //experience

                .WithTextInput(new TextInputBuilder()
				{
					CustomId = $"mono",
					Label = !monoError ? "Sleep req each night to feel rested on mono?" : $"[ERROR] Sleep req each night to feel rested?",
					Placeholder = monoRange,
					MaxLength = 5,
					MinLength = 1,
					Required = true,
					Style = TextInputStyle.Short,
					Value = mono > 0 ? mono.ToString() : null
				}) //Mono Baseline

                .WithTextInput(new TextInputBuilder()
				{
					CustomId = $"sleep",
					Label =  !sleepError ? "Time ranges daily sleep is possible or 'Any'." : "[ERROR] Sleep Availability ranges:",
					Placeholder = !sleepError ? $"Ex: '23:00-8:00, 12:00-14:00' or Napchart" : usedNapchart ?
						(noAvailiabilityFound ? "Napchart needs color labeled \"Availability\"" : "Invalid Napchart Link") :
						(overlap ? "Fix overlapping sleep ranges" : "Invalid Ranges (Ex: '23:00-3:00, 9:00-14:00')"),
					MaxLength = 1500,
					MinLength = 2,
					Required = false,
					Style = TextInputStyle.Paragraph,
                    Value = !sleepError && !string.IsNullOrWhiteSpace(sleep) ? sleep : null
				}) //Sleep Times
            };

            return modal;
		}

        internal static List<(ModalBuilder modal, ulong userID)> _failedModals = new();
		public static async void FSModal(IModalInteraction interaction)
		{
			List<(string name, string value)> values = interaction.Data.Components.ToList().Select(c => (c.CustomId, c.Value)).ToList<(string name, string value)>();

			Bot.Level activity = Bot.Level.unset, experience = Bot.Level.unset;
			int age = 0, mono = 0;
			string sleep = "";
			List<string> sleepTimesList = new();
			bool noRestrict = false, napchart = false, noAvailiabilityFound = false, overlap = false;

			foreach ((string name, string value) in values)
			{
				switch (name)
				{
					case "activity":
						if (Enum.TryParse(value, true, out Bot.Level act) && act != Bot.Level.unset) activity = act;
						break;

					case "age":
						if (int.TryParse(value, out int a) && a > ScheduleFinder._minAge && a < ScheduleFinder._maxAge) age = a;
						break;

					case "experience":
						if (Enum.TryParse(value, true, out Bot.Level exp) && exp != Bot.Level.unset) experience = exp;
						break;

					case "mono":
						if (int.TryParse(value, out int m) && m > ScheduleFinder._minMono && m < ScheduleFinder._maxMono) mono = m;
						break;

					case "sleep":
						sleep = value;
						if (sleep.ToLower() == "any") noRestrict = true;
						break;
				}
			}

			//determine viability of sleep times provided
			if (sleep.Contains(',') || SFAlgorithm.checkLongSleepTime(sleep))
            {
                foreach (string time in sleep.Split(','))
                {
                    if (SFAlgorithm.checkLongSleepTime(time.Trim()))
                        sleepTimesList.Add(time);
                }
            }
            else if (NapchartInterface.isLinkValidNapchart(sleep.Trim()))
            {
                napchart = true;
                List<Schedule> schedulesFromChart = NapchartInterface.getSchedulesFromLink(sleep, "Availability");
                if (schedulesFromChart != null)
				{
					if (schedulesFromChart.Count == 1) schedulesFromChart[0].name = "Availability";
					Schedule availability = schedulesFromChart.Find(s => s.name.ToLower() == "availability");
					if (availability != null) sleepTimesList = availability.sleeps?.Select(sb => $"{sb.startTime}-{sb.endTime}").ToList();
					else noAvailiabilityFound = true;
				}                
            }

            if (SFAlgorithm.checkSleepOverlaps(sleepTimesList.ToArray()))
            {
                sleepTimesList = new();
                overlap = true;
            }

            string[] sleepTimes = sleepTimesList.Count > 0 ? sleepTimesList.ToArray() : null;

            bool activityError = activity == Bot.Level.unset,
                 ageError = age == 0,
                 experienceError = experience == Bot.Level.unset,
                 monoError = mono == 0,
                 sleepError = !noRestrict && sleepTimes == null;

			if (activityError || ageError || experienceError || monoError || sleepError)
			{
				ModalBuilder modal = getFSModal(
				interaction.User.Id,
				activityError, activity,
				ageError, age,
				experienceError, experience,
				monoError, mono,
				sleepError, sleep, napchart, noAvailiabilityFound, overlap);

				_failedModals.Add((modal, interaction.User.Id));

				ComponentBuilder retryButton = new ComponentBuilder().WithButton(new ButtonBuilder()
				{
					CustomId = $"FSModal:{interaction.User.Id}",
					Label = "Edit Entries",
					Style = ButtonStyle.Primary
				});

				await interaction.RespondAsync("Invalid entries, please try again!", ephemeral: true, components: retryButton.Build());
			}
			else
			{
				string napchartLink = napchart ? NapchartInterface.getChartImg(sleep.Trim()) : !noRestrict ? 
					NapchartInterface.getChartImg(NapchartInterface.createChart(NapchartInterface.createNapchartFromSleepArray(sleepTimes, Napchart.Color.green, label: "Availability"))) : "";

				Server server = Bot.getServer(interaction.ChannelId.Value);
				ScheduleFinder sh = server._SH;
				sh.startSchedulefinding(Bot._client.GetChannelAsync(interaction.ChannelId.Value).Result as IMessageChannel,
					interaction.User, age, experience, mono, activity, sleepTimes, noRestrict, interaction, napchartLink);
			}			
		}
		#endregion

		#region redirects
		public static void fs(SocketMessage message) => FindSchedules(message, "FS");
		public static void findschedule(SocketMessage message) => FindSchedules(message);
		public static void findschedules(SocketMessage message) => FindSchedules(message);
		#endregion
		#endregion

		internal static bool SH_ComponentInteractionHandler(SocketMessageComponent ci, ScheduleFinder sh)
        {
            //handle interaction
			if (sh._scheduleFinding != ScheduleFinder.ScheduleFindingStage.NOT_SCHEDULEFINDING && //schedulefinding
			   sh._embedMsg != null && ci.Message.Id == sh._embedMsg.Id) //has already begun
			{
                //continue schedulefinding
				if (ci.User.Id == sh._responder.Id)
					sh.scheduleFinding(null, ci).Wait();

                //not schedulefinding user
				else
					ci.RespondAsync($"Please wait your turn, {sh._responder.Mention} is ScheduleFinding right now.", ephemeral: true).Wait();

                return true;
			}

            //continue interactions
			else if ((sh._scheduleFinding == ScheduleFinder.ScheduleFindingStage.VIEWING_RESULTS || (sh._scheduleFinding == ScheduleFinder.ScheduleFindingStage.SLEEPTIMES && sh._napchartSchedules != null)) &&
				sh._resultsMessage != null && ci.Message.Id == sh._resultsMessage.Id &&
				sh._responder.Id != 0 && ci.User.Id == sh._responder.Id)
			{
                //Update results
				if (sh._scheduleFinding == ScheduleFinder.ScheduleFindingStage.VIEWING_RESULTS)
					sh.updateResultsEmbed(ci.Data.Values.First()).Wait();

				//continue interactions
				else if (sh._scheduleFinding == ScheduleFinder.ScheduleFindingStage.SLEEPTIMES && sh._napchartSchedules != null)
					sh.scheduleFinding(null, ci).Wait();

                return true;
			}

            return false;
		}

		/// <summary> exit schedulefinding | -exit </summary>
		public static void exit(SocketMessage message) => Bot.getServer(message.Channel)._SH.exit(message.Author, message.Channel);
		#endregion

		#region napcharts
		#region randomschedule
		public void randomschedule(SocketMessage message)
		{
			Schedule schedule = null;
			string msg = message.Content.Contains(' ') ? message.Content[message.Content.IndexOf(" ")..].Trim().ToLower() : "";
			int sleepCount = 0, lengthAlgorithm = -1, gapAlgorithm = -1, start = -1;

			if (msg.Length > 0)
			{
				string[] options = msg.Split(',');

				if (options.Length >= 1 && options[0].Contains(':'))
				{
					foreach (string option in options)
					{
						if (!option.Contains(':')) break;

						string parameter = option.Split(':')[0].Trim(),
							value = option.Split(':')[1].Trim().Replace("\n", "");

						switch (parameter)
						{
							case "count":
								if (int.TryParse(value, out int slpCt))
									sleepCount = slpCt;
								break;

							case "lenalg":
								if (!int.TryParse(value, out lengthAlgorithm))
								{
									switch (value)
									{
										case "random":
											lengthAlgorithm = (int)SlpLenAlg.Random;
											break;

										case "equal":
											lengthAlgorithm = (int)SlpLenAlg.Equal;
											break;

										case "minmax":
											lengthAlgorithm = (int)SlpLenAlg.MinMax;
											break;

										case "spiral":
											lengthAlgorithm = (int)SlpLenAlg.Spiral;
											break;
									};
								}

								break;

							case "gapalg":
								if (!int.TryParse(value, out gapAlgorithm))
								{
									switch (value)
									{
										case "random":
											gapAlgorithm = (int)GapLenAlg.Random;
											break;

										case "equal":
											gapAlgorithm = (int)GapLenAlg.Equal;
											break;

										case "spiral":
											gapAlgorithm = (int)GapLenAlg.Spiral;
											break;
									};
								}

								break;

							case "start":
								if (int.TryParse(value, out int s))
									start = s;
								break;
						}
					}
				}
				else if (options[0] == "help")
				{
					Bot.sendEmbed(message.Channel, Color.Green, title: "RandomSchedule Help",
					description: _commands.Find(c => c.commandName == "RandomSchedule").properFormat + "\n\n**Example:**\n`-RandomSchedule Count: 10, LenAlg: spiral, GapAlg: equal, start: 720`");

					return;
				}
				else if (!int.TryParse(options[0], out sleepCount)) schedule = getExistingSchedule(options[0].ToLower().Trim());
			}

			randomSchedule(message.Channel, sleepCount, lengthAlgorithm, gapAlgorithm, start, null, schedule);
		}

		public void randomschedule(SocketSlashCommand interaction)
		{
			int sleepCount = 0, lengthAlgorithm = -1, gapAlgorithm = -1, start = -1;

			foreach (SocketSlashCommandDataOption parameter in interaction.Data.Options)
			{
				string name = parameter.Name;
				string value = parameter.Value.ToString();

				switch (name)
				{
					case "count":
						if (int.TryParse(value, out int sc)) sleepCount = sc;
						break;

					case "sleepalgorithm":
						if (Enum.TryParse(value, true, out SlpLenAlg slpAlg)) lengthAlgorithm = (int)slpAlg;
						break;

					case "gapalgorithm":
						if (Enum.TryParse(value, true, out GapLenAlg gapAlg)) gapAlgorithm = (int)gapAlg;
						break;

					case "start":
						if (int.TryParse(value, out int st)) start = st;
						break;
				}
			}

			randomSchedule(interaction.Channel, sleepCount, lengthAlgorithm, gapAlgorithm, start, interaction, null);
		}
		/// <summary> sends a randomly created schedule | -randomSchedule </summary>
		public void randomSchedule(IMessageChannel channel, int sleepCount = 0, int lengthAlgorithm = -1, int gapAlgorithm = -1, int start = -1, SocketSlashCommand interaction = null, Schedule schedule = null)
		{
            (string napchartLink, string scheduleName, string errors) = makeRandomSchedule(sleepCount, lengthAlgorithm, gapAlgorithm, start, schedule);

            if (string.IsNullOrEmpty(errors))
            {
                while (string.IsNullOrEmpty(napchartLink) || !Uri.IsWellFormedUriString(napchartLink, UriKind.Absolute))
                    (napchartLink, scheduleName, errors) = Bot._uc.makeRandomSchedule();
            }

            if (!string.IsNullOrEmpty(napchartLink) && Uri.IsWellFormedUriString(napchartLink, UriKind.Absolute))
            {
                EmbedBuilder embed = new()
                {
                    Color = string.IsNullOrEmpty(errors) ? Color.Green : Color.Orange,
                    Title = scheduleName,
                    ImageUrl = NapchartInterface.getChartImg(napchartLink),
                    Description = $"[Link]({napchartLink}){(errors.Length > 0 ? $"\n\n{_alert} There were errors with your parameters, so invalid parameters were ignored:\n\n{errors}" : "")}"
                };

                if (interaction == null)
                    channel.SendMessageAsync(embed: embed.Build());
                else
                    interaction.RespondAsync(embed: embed.Build());
            }
            else
			{
				EmbedBuilder embed = new()
				{
					Color = Color.Red,
					Title = $"{_alert} RandomSchedule Errors {_alert}",
					Description = errors
				};

				if (interaction == null)
					channel.SendMessageAsync(embed: embed.Build());
				else
					interaction.RespondAsync(embed: embed.Build());
			}
        }

		#region algorithms
		private enum SlpLenAlg { Random1, Random, Random2, Equal, Equal2, MinMax, Spiral };
        private enum GapLenAlg { Random1, Random, Equal, Spiral };
        internal readonly static int 
            _resolutionByMin = 1, _incrementsPerDay = 24 * (60 / _resolutionByMin),
            _minSlpCt = 1, _minGapLen = _resolutionByMin, _minSlpLen = _resolutionByMin,
            _maxSlpCt = _incrementsPerDay / (_minSlpLen + _minGapLen),
            _maxCountForIncremental = (int)Math.Floor((Math.Sqrt((4 * _minGapLen * _minGapLen) + (4 * _minGapLen) + (8 * _incrementsPerDay) + 1) - (2 * _minGapLen) - 1) / 2);

        private static Schedule getExistingSchedule(string scheduleName)
        {
			return SFAlgorithm._scheduleDB.Find(S =>
						S.name.Replace("Everyman ", "E").Replace("Dual-Core ", "DC").Replace("Tri-Core ", "").Replace("Quad-Core", "QC0") ==
						Config._napGodSchedules.Find(
							s => s._name.Contains(scheduleName.ToLower().Replace("-", "")
							.Replace("extended", "").Replace("ext", "").Replace("shortened", "").Replace("short", "")
							.Replace("flipped", "").Replace("flip", "").Replace("modified", "").Replace("mod", "")))._nameClean);
		}

		public (string, string, string) makeRandomSchedule(int sleepCount = 0, int lengthAlgorithm = -1, int gapAlgorithm = -1, int start = -1, Schedule scheduleOption = null)
        {
            string algorithms = "", errors = "", napchart = "", scheduleName, description;
            int sleepCt = 0, startTime = -1, maxGapLen, maxSlpLen, scheduleWithMostSleeps = SFAlgorithm._scheduleDB.OrderByDescending(s => s.sleeps.Count).First().sleeps.Count;

            (double lengthRangeMin, double lengthRangeMax) = ((double)1 / Bot._rng.Next(4, 8) * _incrementsPerDay, 1 / (2.2 + Bot._rng.NextDouble()) * _incrementsPerDay);

            (int min, int max) individualLengthsRange = (20 / _resolutionByMin, 600 / _resolutionByMin);

            List<int> multipliers = new() { 7 },
                sleepLengths = new(),
                gapLengths = new();

            Schedule schedule;

            //Time for first sleep to start
            if (start >= 0)
            {
                if (start % _resolutionByMin != 0) errors += $"-Start time must be a multiple of {_resolutionByMin}\n";
                else if (start < 0 || start > _incrementsPerDay) errors += $"-Start time outside of range: 0-{_incrementsPerDay}\n";
                else startTime = start;
            }

            if (startTime == -1) startTime = Bot._rng.Next(0, _incrementsPerDay);
            algorithms += $"Start Time: {SFAlgorithm.convertDoubleToTime((double)startTime / (60 / _resolutionByMin))}\n";


            //Number of sleeps and Length of each
            if (((sleepCount == 0 || sleepCount <= scheduleWithMostSleeps) && lengthAlgorithm == -1 && Bot._rng.Next(2) == 1) || scheduleOption != null)
			{
                schedule = scheduleOption ?? (sleepCount > 0 ? SFAlgorithm._scheduleDB.Find(s => s.sleeps.Count == sleepCount) : SFAlgorithm._scheduleDB.OrderBy(s => Bot._rng.Next()).ToList().First());

				scheduleName = schedule.name;
                description = "Not a particularly viable schedule...";

                foreach (SleepBlock sleep in schedule.sleeps)
                    sleepLengths.Add(SFAlgorithm.convertTimeToInt(sleep.length));
                sleepCt = sleepLengths.Count;
			}
            else
            {
                //Number of sleeps
                if (sleepCount > 0)
                {
                    if (sleepCount <= _maxSlpCt) sleepCt = sleepCount;
                    else errors += $"-Sleep count outside of range: {_minSlpCt}-{_maxSlpCt}\n";
                }

                if (sleepCt == 0) sleepCt = getRandomSkewed(lengthAlgorithm == 4 ? 3 : _minSlpCt, lengthAlgorithm == 4 ? _maxCountForIncremental : _maxSlpCt, multipliers);

                scheduleName = $"RS{sleepCt}";
                description = "A very viable schedule.";


                //Length of each sleep
                maxSlpLen = _incrementsPerDay - (((sleepCt - 1) * _minSlpLen) + ((sleepCt - 1) * _minGapLen));

                if (lengthAlgorithm == (int)SlpLenAlg.Spiral && (_maxCountForIncremental < sleepCt))
                {
                    errors += $"-Sleep count {sleepCt} exceeds maximum for incremental algorithm. Maximum: {_maxCountForIncremental}\n";
                    lengthAlgorithm = 0;
                }
                else if (lengthAlgorithm is not (-1) and (< 1 or > ((int)SlpLenAlg.Spiral + 1)))
                {
                    errors += $"Algorithm value doesn't match any of the following options: [Random, Equal, MinMax, Spiral].\n";
                    lengthAlgorithm = 0;
                }

                if (sleepCount == _maxSlpCt && (lengthAlgorithm != (int)SlpLenAlg.Random || gapAlgorithm != 2))
                {
                    if (lengthAlgorithm != -1 || gapAlgorithm != -1)
                        errors += $"-{(lengthAlgorithm != -1 ? $"Length Algorithm{(gapAlgorithm != -1 ? " and " : "")}" : "")}{(gapAlgorithm != -1 ? "Gap Algorithm" : "")} must be 'Equal' if Sleep Count is {_maxSlpCt}.\n";
                    lengthAlgorithm = gapAlgorithm = 2;
                }
                else if (sleepCount == 0 && sleepCt == _maxSlpCt && lengthAlgorithm != (int)SlpLenAlg.Random && gapAlgorithm != 2)
                    sleepCt = getRandomSkewed(lengthAlgorithm == (int)SlpLenAlg.Spiral ? 3 : _minSlpCt, _maxSlpCt, multipliers);

                switch (lengthAlgorithm > 0 ? lengthAlgorithm : Bot._rng.Next(_maxCountForIncremental >= sleepCt ? (int)SlpLenAlg.Spiral + 1 : (int)SlpLenAlg.Spiral))
                {
					#region random
					case (int)SlpLenAlg.Random: case (int)SlpLenAlg.Random1: case (int)SlpLenAlg.Random2:
                        for (int i = 0; i < sleepCt; i++)
                            sleepLengths.Add(getRandomSkewed(_minSlpLen,
                            _incrementsPerDay - ((sleepCt - sleepLengths.Count - 1) * _minSlpLen) - ((sleepCt - 1) * _minGapLen) - sleepLengths.Sum(),
                            new List<int> { 3, 12, 14, 22 }));

                        sleepLengths = sleepLengths.OrderBy(l => Bot._rng.Next()).ToList();

                        algorithms += "Sleep Lengths: Completely random\n";
                        break;
					#endregion

					#region equal
					case (int)SlpLenAlg.Equal: case (int)SlpLenAlg.Equal2:
                        int length = Bot._rng.Next(1, ((_incrementsPerDay - (sleepCt * _minGapLen)) / sleepCt) + 1);

                        for (int i = 0; i < sleepCt; i++) sleepLengths.Add(length);

                        algorithms += "Sleep Lengths: Equal\n";
                        break;
					#endregion

					#region minmax
					case (int)SlpLenAlg.MinMax:
                        int len;
                        len = Bot._rng.Next(2) == 1 ? _minSlpLen : (maxSlpLen / sleepCt) - ((sleepCt - 1) * _minGapLen);

						for (int i = 0; i < sleepCt; i++) sleepLengths.Add(len);

                        algorithms += "Sleep Lengths: Max or min\n";
                        break;
					#endregion

					#region spiral
					case (int)SlpLenAlg.Spiral: //Increasing or decreasing in length by equal increments
                        int incrementLen = Bot._rng.Next(1, (int)Math.Floor((double)-((2 * _minGapLen * sleepCt) - (2 * _incrementsPerDay)) / ((sleepCt * sleepCt) + sleepCt)) + 1);

                        for (int i = 1; i <= sleepCt; i++) sleepLengths.Add(incrementLen * i);
                        if (Bot._rng.Next(2) == 0) sleepLengths.Reverse();

                        algorithms += "Sleep Lengths: Increasing or decreasing in length by equal increments (Spiral)\n";
                        break;
					#endregion
				}

                if ((sleepCt < (100.0 / 1440) * _incrementsPerDay || sleepLengths.Exists(s => s > individualLengthsRange.max)) && Bot._rng.Next(10) != 1)
                {
                    List<int> newLengths = new(sleepLengths);
                    bool altered = false;

                    if (sleepLengths.Sum() < lengthRangeMin)
                    {
                        int difference = (int)Math.Floor(lengthRangeMin) - sleepLengths.Sum();

                        while (difference > 0 && newLengths.FindAll(n => n < individualLengthsRange.max).Count > 0)
                        {
                            int numberToModify;
                            if (Bot._rng.Next(4) == 1)
                            {
                                List<int> modifiable = newLengths.FindAll(n => n < individualLengthsRange.max);
                                numberToModify = modifiable[Bot._rng.Next(modifiable.Count)];
                            }
                            else numberToModify = newLengths.Min();

                            numberToModify = newLengths.IndexOf(numberToModify);

                            newLengths[numberToModify]++;
                            difference--;
                            altered = true;
                        }
                    }
                    else if (sleepLengths.Sum() > lengthRangeMax)
                    {
                        int difference = sleepLengths.Sum() - (int)Math.Floor(lengthRangeMax);

                        while (difference > 0 && newLengths.FindAll(n => n > individualLengthsRange.min).Count > 0)
                        {
                            int numberToModify;
                            if (Bot._rng.Next(4) == 1)
                            {
                                List<int> modifiable = newLengths.FindAll(n => n > individualLengthsRange.min);
                                numberToModify = modifiable[Bot._rng.Next(modifiable.Count)];
                            }
                            else numberToModify = newLengths.Max();

                            numberToModify = newLengths.IndexOf(numberToModify);

                            newLengths[numberToModify]--;
                            difference--;
                            altered = true;
                        }
                    }

                    if (altered)
                    {
                        string min = (lengthRangeMin / 60 * _resolutionByMin).ToString(), max = (lengthRangeMax / 60 * _resolutionByMin).ToString();
                        algorithms += $"Modified sleep lengths to be more reasonable [{(min.Contains('.') ? min[..(min.IndexOf(".") + 2)] : min)}, {(max.Contains('.') ? max[..(max.IndexOf(".") + 2)] : max)}] \n";
                    }
                }
            }


            //Length of each gap
            if (sleepCt > 1)
            {
                maxGapLen = _incrementsPerDay - (sleepLengths.Sum() + ((sleepLengths.Count - 1) * _minGapLen));

                if (gapAlgorithm is not (-1) and (< 1 or > ((int)GapLenAlg.Spiral + 1)))
                {
                    errors += $"-Algorithm value outside of range: 1-4\n";
                    gapAlgorithm = 0;
                }

                switch (gapAlgorithm > 0 ? gapAlgorithm : Bot._rng.Next(_maxCountForIncremental >= sleepCt ? (int)GapLenAlg.Spiral + 1 : (int)GapLenAlg.Spiral))
                {
                    #region random
                    case (int)GapLenAlg.Random: case (int)GapLenAlg.Random1:
                        for (int i = 0; i < sleepCt - 1; i++)
                            gapLengths.Add(getRandomSkewed(_minGapLen,
                                _incrementsPerDay - sleepLengths.Sum() - ((sleepCt - gapLengths.Count - 1) * _minGapLen) - gapLengths.Sum(),
                                new List<int> { 3, 12, 14, 2 }));

                        gapLengths.Add(_incrementsPerDay - sleepLengths.Sum() - gapLengths.Sum());

                        lengthRangeMax = 1.0 / Bot._rng.Next((int)Math.Floor(24 / gapLengths.Count / 4.0), (int)Math.Floor(24 / gapLengths.Count / 2.0) + 1) * _incrementsPerDay;
                        lengthRangeMin = 1.0 / Bot._rng.Next((int)Math.Floor(24 / gapLengths.Count * 1.25), (int)Math.Floor(24 / gapLengths.Count * 1.75) + 1) * _incrementsPerDay;

                        if (gapLengths.Max() > maxGapLen || gapLengths.Min() < _minGapLen || (Bot._rng.Next(10) != 1 && (gapLengths.Max() > lengthRangeMax || gapLengths.Min() < lengthRangeMin)))
                        {
                            (List<int> gaps, List<bool> cantChange) = (gapLengths, new List<bool>());

                            foreach (int gap in gaps) cantChange.Add(false);

                            int lrm = (int)Math.Floor(lengthRangeMin), lrM = (int)Math.Floor(lengthRangeMax);

                            while (((gaps.Min() < lrm || gaps.Max() > lrM) && cantChange.FindAll(b => b).Count < gaps.FindAll(g => lrm > g || lrM < g).Count) ||
                                (gaps.Min() < _minGapLen || gaps.Max() > maxGapLen))
                            {
                                for (int i = 0; i < gapLengths.Count; i++)
                                {
                                    if (gaps[i] < _minGapLen && !cantChange[i])
                                    {
                                        List<int> othersToMod = gaps.FindAll(t => t > _minGapLen);

                                        if (othersToMod.Count > 0)
                                        {
                                            int difference = lrm - gaps[i];
                                            gaps[i] += difference;

                                            difference /= othersToMod.Count;

                                            gaps.RemoveAll(t => t > _minGapLen);

                                            for (int j = 0; j < othersToMod.Count; j++)
                                                othersToMod[j] -= difference;

                                            gaps.AddRange(othersToMod);
                                        }
                                        else return makeRandomSchedule(sleepCount, lengthAlgorithm, gapAlgorithm, start, scheduleOption);
                                    }
                                    else if (gaps[i] > maxGapLen && !cantChange[i])
                                    {
                                        List<int> othersToMod = gaps.FindAll(t => t < maxGapLen);
                                        if (othersToMod.Count > 0)
                                        {
                                            int difference = gaps[i] - lrM;
                                            gaps[i] -= difference;

                                            difference /= othersToMod.Count;

                                            gaps.RemoveAll(t => t < maxGapLen);

                                            for (int j = 0; j < othersToMod.Count; j++)
                                                othersToMod[j] += difference;

                                            gaps.AddRange(othersToMod);
                                        }
                                        else return makeRandomSchedule(sleepCount, lengthAlgorithm, gapAlgorithm, start, scheduleOption);
                                    }
                                    else if (gaps[i] < lrm && !cantChange[i])
                                    {
                                        List<int> othersToMod = gaps.FindAll(t => t > lrm);

                                        if (othersToMod.Count > 0)
                                        {
                                            int difference = lrm - gaps[i];
                                            gaps[i] += difference;

                                            difference /= othersToMod.Count;

                                            gaps.RemoveAll(t => t > lrm);

                                            for (int j = 0; j < othersToMod.Count; j++)
                                                othersToMod[j] -= difference;

                                            gaps.AddRange(othersToMod);
                                        }
                                        else cantChange[i] = true;
                                    }
                                    else if (gaps[i] > lrM && !cantChange[i])
                                    {
                                        List<int> othersToMod = gaps.FindAll(t => t < lrM);
                                        if (othersToMod.Count > 0)
                                        {
                                            int difference = gaps[i] - lrM;
                                            gaps[i] -= difference;

                                            difference /= othersToMod.Count;

                                            gaps.RemoveAll(t => t < lrM);

                                            for (int j = 0; j < othersToMod.Count; j++)
                                                othersToMod[j] += difference;

                                            gaps.AddRange(othersToMod);
                                        }
                                        else cantChange[i] = true;
                                    }
                                }
                            }

                            if (gaps.Max() < gapLengths.Max() || gaps.Min() > gapLengths.Min())
                            {
                                gapLengths = gaps;
                                string min = (lengthRangeMin / 60 * _resolutionByMin).ToString(), max = (lengthRangeMax / 60 * _resolutionByMin).ToString();
                                algorithms += $"Modified gap lengths to be more reasonable [{(min.Contains('.') ? min[..(min.IndexOf(".") + 2)] : min)}, {(max.Contains('.') ? max[..(max.IndexOf(".") + 2)] : max)}] \n";
                            }
                        }

                        gapLengths = gapLengths.OrderBy(l => Bot._rng.Next()).ToList();

                        algorithms += "Gap Lengths: Completely random\n";
                        break;
                    #endregion

                    #region equal
                    case (int)GapLenAlg.Equal:
                        int length = (_incrementsPerDay - sleepLengths.Sum()) / sleepCt;

                        for (int i = 0; i < sleepCt - 1; i++) gapLengths.Add(length);

                        algorithms += "Gap Lengths: Equal\n";
                        break;
                    #endregion

                    #region spiral
                    case (int)GapLenAlg.Spiral: //Increasing or decreasing in length by equal increments
                        int incrementLen = (int)Math.Floor((2 * _incrementsPerDay - 2 * sleepLengths.Sum()) / (Math.Pow(sleepCt, 2) + sleepCt)) + 1;

                        for (int i = 1; i < sleepCt; i++) gapLengths.Add(incrementLen * i);
                        if (Bot._rng.Next(2) == 0) gapLengths.Reverse();

                        algorithms += "Gap Lengths: Increasing or decreasing by equal increments (Spiral)\n";
                        break;
                        #endregion
                }
            }


            //Build schedule
            List<SleepBlock> sleeps = new();

            for (int i = 0; i < sleepCt; i++)
            {
                int endTime = startTime + sleepLengths[i];

                sleeps.Add(new SleepBlock(
                    SFAlgorithm.convertDoubleToTime((double)sleepLengths[i] / (60 / _resolutionByMin)),
                    SFAlgorithm.convertDoubleToTime((double)startTime / (60 / _resolutionByMin)),
                    SFAlgorithm.convertDoubleToTime((double)endTime / (60 / _resolutionByMin)),
                    inType: sleepLengths[i] >= 50 ? SFAlgorithm.SleepBlockType.Core : SFAlgorithm.SleepBlockType.Nap));

                if (i < sleepCt - 1) startTime = endTime + gapLengths[i];
            }

            //sleepLengths = new List<int> { 429, 16 };
            //sleeps = new List<SleepBlock> { new SleepBlock("07:09", "00:00", "07:09", inType: SFAlgorithm.SleepBlockType.Core), new SleepBlock("00:16", "16:00", "16:16", inType: SFAlgorithm.SleepBlockType.Core) };

            if (sleepCt <= scheduleWithMostSleeps)
            {
				#region attributes
				int ipdm = 60 / _resolutionByMin;
                double minNap = (double)10 / 60 * ipdm, napToCore = (double)50 / 60 * ipdm, maxCore = (24 * ipdm) - _resolutionByMin;

                (double min, double max) nap = (minNap, napToCore),
                    core1Cycle = (1.5 * ipdm, 3 * ipdm),
                    core2Cycle = (3 * ipdm, 4.5 * ipdm),
                    core3Cycle = (4.5 * ipdm, 6 * ipdm),
                    core4Cycle = (6 * ipdm, 7.5 * ipdm);
                string newName;
				#endregion

				switch (sleepCt)
                {
                    case 1:
						#region mono
                        if (scheduleOption == null || scheduleOption.name == "Monophasic")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name.Contains("Mono")), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { (6 * ipdm, 9 * ipdm) },
                                new List<(double min, double max)> { (napToCore, 6 * ipdm) },
                                new List<(double min, double max)> { (9 * ipdm, maxCore) },
                            });
                            if (!string.IsNullOrWhiteSpace(newName)) scheduleName = newName;
                        }
						#endregion
						break;

                    case 2:
                        #region siesta
                        if (scheduleOption == null || scheduleOption.name == "Siesta")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Siesta"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, (5 * ipdm, 7.5 * ipdm) },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (3 * ipdm, 5 * ipdm) },
                                new List<(double min, double max)> { core2Cycle, (7.5 * ipdm, maxCore) }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region segmented
                        if (scheduleOption == null || scheduleOption.name == "Segmented")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Segmented"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core2Cycle, core2Cycle },
                                new List<(double min, double max)> { (napToCore, 3 * ipdm), (napToCore, 3 * ipdm) },
                                new List<(double min, double max)> { (4.5 * ipdm, maxCore), (4.5 * ipdm, maxCore) }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region e1
                        if (scheduleOption == null || scheduleOption.name == "Everyman 1")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Everyman 1"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { nap, core4Cycle },
                                new List<(double min, double max)> { nap, (napToCore, 6 * ipdm) },
                                new List<(double min, double max)> { nap, (7.5 * ipdm, maxCore) }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        break;

                    case 3:
                        #region e2
                        if (scheduleOption == null || scheduleOption.name == "Everyman 2")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Everyman 2"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core3Cycle, nap, nap },
                                new List<(double min, double max)> { (napToCore, 4.5 * ipdm), nap, nap },
                                new List<(double min, double max)> { (6 * ipdm, maxCore), nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region dc1
                        if (scheduleOption == null || scheduleOption.name == "Dual-Core 1")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Dual-Core 1"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { ((3 + (20 / 60)) * ipdm, 4.5 * ipdm), (1.5 * ipdm, 3 * ipdm), nap },
                                new List<(double min, double max)> { (napToCore, (3 + (20 / 60)) * ipdm), (napToCore, 1.5 * ipdm), nap },
                                new List<(double min, double max)> { (4.5 * ipdm, maxCore), (3 * ipdm, maxCore), nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region tc
                        if (scheduleOption == null || scheduleOption.name == "Triphasic")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Triphasic"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, core1Cycle, core1Cycle },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm) },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), (3 * ipdm, maxCore), (3 * ipdm, maxCore) }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        break;

                    case 4:
                        #region e3
                        if (scheduleOption == null || scheduleOption.name == "Everyman 3")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Everyman 3"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core2Cycle, nap, nap, nap },
                                new List<(double min, double max)> { (napToCore, 3 * ipdm), nap, nap, nap },
                                new List<(double min, double max)> { (4.5 * ipdm, maxCore), nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region dc2
                        if (scheduleOption == null || scheduleOption.name == "Dual-Core 2")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Dual-Core 2"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core2Cycle, core1Cycle, nap, nap },
                                new List<(double min, double max)> { (napToCore, 3 * ipdm), (napToCore, 1.5 * ipdm), nap, nap },
                                new List<(double min, double max)> { (4.5 * ipdm, maxCore), (3 * ipdm, maxCore), nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region tc1
                        if (scheduleOption == null || scheduleOption.name == "Tri-Core 1")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Tri-Core 1"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, core1Cycle, core1Cycle, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), (3 * ipdm, maxCore), (3 * ipdm, maxCore), nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region qc
                        if (scheduleOption == null || scheduleOption.name == "Quad-Core")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Quad-Core"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, core1Cycle, core1Cycle, core1Cycle },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm) },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), (3 * ipdm, maxCore), (3 * ipdm, maxCore), (3 * ipdm, maxCore) }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region bimaxion
                        if (scheduleOption == null || scheduleOption.name == "Bimaxion")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Bimaxion"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, core1Cycle, nap, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), nap, nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), (3 * ipdm, maxCore), nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region trimaxion
                        if (scheduleOption == null || scheduleOption.name == "Trimaxion")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Trimaxion"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, nap, nap, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), nap, nap, nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region dymaxion
                        if (scheduleOption == null || scheduleOption.name == "Dymaxion")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Dymaxion"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { nap, nap, nap, nap },
                                new List<(double min, double max)> { nap, nap, nap, nap },
                                new List<(double min, double max)> { nap, nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        break;

                    case 5:
                        #region e4
                        if (scheduleOption == null || scheduleOption.name == "Everyman 4")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Everyman 4"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, nap, nap, nap, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), nap, nap, nap, nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), nap, nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region tc2
                        if (scheduleOption == null || scheduleOption.name == "Tri-Core 2")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Tri-Core 2"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, core1Cycle, core1Cycle, nap, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), nap, nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), (3 * ipdm, maxCore), (3 * ipdm, maxCore), nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region dc3
                        if (scheduleOption == null || scheduleOption.name == "Dual-Core 3")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Dual-Core 3"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, core1Cycle, nap, nap, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), nap, nap, nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), (3 * ipdm, maxCore), nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        break;

                    case 6:
                        #region e5
                        if (scheduleOption == null || scheduleOption.name == "Everyman 5")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Everyman 5"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, nap, nap, nap, nap, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), nap, nap, nap, nap, nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), nap, nap, nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region dc4
                        if (scheduleOption == null || scheduleOption.name == "Dual-Core 4")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Dual-Core 4"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { core1Cycle, core1Cycle, nap, nap, nap, nap },
                                new List<(double min, double max)> { (napToCore, 1.5 * ipdm), (napToCore, 1.5 * ipdm), nap, nap, nap, nap },
                                new List<(double min, double max)> { (3 * ipdm, maxCore), (3 * ipdm, maxCore), nap, nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        #region uberman
                        if (scheduleOption == null || scheduleOption.name == "Uberman")
                        {
                            newName = getScheduleName(sleeps, SFAlgorithm._scheduleDB.Find(s => s.name == "Uberman"), sleepLengths, gapLengths, new List<List<(double min, double max)>>
                            {
                                new List<(double min, double max)> { nap, nap, nap, nap, nap, nap },
                                new List<(double min, double max)> { nap, nap, nap, nap, nap, nap },
                                new List<(double min, double max)> { nap, nap, nap, nap, nap, nap }
                            });

                            if (!string.IsNullOrWhiteSpace(newName))
                            {
                                scheduleName = newName;
                                break;
                            }
                        }
                        #endregion
                        break;
                }
            }

            schedule = new Schedule(scheduleName, sleeps);

            if (schedule.sleeps.Count == 0 || schedule.sleeps.Exists(s => s.length == "00:00")) 
                return makeRandomSchedule(sleepCount, lengthAlgorithm, gapAlgorithm, start, scheduleOption);

            try
			{
                napchart = NapchartInterface.createChart(new Napchart(
                new List<Objective> { new Objective(schedule, 0, Napchart.Color.red) },
                new List<ColorTag> { new ColorTag("Sleep", Napchart.Color.red) },
                scheduleName, $"{description} | {algorithms.Replace("\n", " | ")}"));
            }
			catch 
            {
                return makeRandomSchedule(sleepCount, lengthAlgorithm, gapAlgorithm, start, scheduleOption);
            }

            //return schedule, napchart, and any errors
            return (napchart, scheduleName, errors);
        }

        private static int getRandomSkewed(double min, double max, List<int> multipliers)
		{
            int total = 0;
            
            foreach (int multiplier in multipliers)
                total += (int)Math.Floor(min + (max - min) * Math.Pow(Bot._rng.NextDouble(), multiplier));
            
            total /= multipliers.Count;
            
            return total;
        }

        private static string getScheduleName(List<SleepBlock> sleeps, Schedule schedule, List<int> sleepLengths, List<int> gapLengths, List<List<(double min, double max)>> ranges)
		{
			return getMatchesTimes(sleepLengths, ranges[0])
				? getMatchesSchedule(sleeps, schedule, gapLengths)
				: getMatchesTimes(sleepLengths, ranges[1])
				? $"{schedule.name}-Shortened"
				: getMatchesTimes(sleepLengths, ranges[2]) ? $"{schedule.name}-Extended" : "";
		}

        private static bool getMatchesTimes(List<int> sleepLengths, List<(double min, double max)> lengths)
		{
            for (int i = 0; i < sleepLengths.Count; i++)
			{
                List<bool> matches = new();

                for (int j = 0; j < lengths.Count; j++)
				{
                    int index = i + j < sleepLengths.Count ? i + j : i + j - sleepLengths.Count;
                    if (sleepLengths[index] >= lengths[j].min && sleepLengths[index] < lengths[j].max) matches.Add(true);
                    else matches.Add(false);

                    if (matches.Count == sleepLengths.Count && matches.FindAll(m => m).Count == sleepLengths.Count)
                        return true;
				}
			}

            return false;
		}

        private static string getMatchesSchedule(List<SleepBlock> sleeps, Schedule schedule, List<int> gapLengths)
		{
            List<int> matched = new();

            //Match Gaps
            for (int gap = 0; gap < gapLengths.Count; gap++)
            {
                if (gapLengths[gap] < 2)
                    gap++;
                else
                {
                    for (int slp = 0; slp < schedule.sleeps.Count; slp++)
                    {
                        if (!matched.Exists(s => s == slp) && SFAlgorithm.stringToMinutesInt(schedule.sleeps[slp].maxDistanceFromPreviousSleepBlock, 0) >= gapLengths[gap])
                        {
                            matched.Add(slp);
                            gap++; slp = 0;
                            if (gap >= gapLengths.Count) slp = schedule.sleeps.Count;
                        }
                    }
                }
            }

            if (matched.Count == gapLengths.Count)
            {
                List<(int sleepIndex, List<int> scheduleIndexes)> sleepMatches = new();

                //Match sleeps
                for (int slp = 0; slp < sleeps.Count; slp++)
                {
                    (int sleepIndex, List<int> scheduleIndexes) currentSM = (slp, new List<int>());

                    for (int sch = 0; sch < schedule.sleeps.Count; sch++)
                    {
                        if (sleeps[slp].type == schedule.sleeps[sch].type &&
                            SFAlgorithm.stringToMinutesInt(SFAlgorithm.getDifferenceTime(schedule.sleeps[sch].earliestStartTime, sleeps[slp].startTime), 0) >= 0 &&
                            SFAlgorithm.stringToMinutesInt(SFAlgorithm.getDifferenceTime(schedule.sleeps[sch].latestEndTime, sleeps[slp].endTime), 0) >= 0)
                            currentSM.scheduleIndexes.Add(sch);
                    }

                    sleepMatches.Add(currentSM);
                }

                if (sleepMatches.Count == sleeps.Count) return schedule.name;
            }

            return $"{schedule.name}-Modified";
        }
		#endregion

		/// <summary> shortcut for randomSchedule </summary>
		public void rs(SocketMessage message) => randomschedule(message);
		#endregion

		#region createnapchart
		internal static void createnapchart_simple(SocketSlashCommand interaction)
        {
            Schedule schedule = null;
			Napchart.Color color = Napchart.Color.red;
			string usersName = Bot.getUsersName(interaction.User, Bot.getServer(interaction.Channel)),
				label = "", 
                scheduleName = "Custom Napchart", 
                description = $"Created by ScheduleHelper for {usersName}.",
                timesError = "";
            bool selectedColor = false;

			foreach (SocketSlashCommandDataOption parameter in interaction.Data.Options.First().Options)
			{
				string name = parameter.Name;
				string value = parameter.Value.ToString();

                switch (name)
                {
                    case "times":
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            string[] blocks = value.Split(',');

                            if (!SFAlgorithm.checkSleepOverlaps(blocks))
                                schedule = ScheduleFinder.createScheduleFromSleepTimes(blocks);
                            else timesError = value;
                        }

                        break;
                    }

					case "color":
                    {
                        if (Enum.TryParse(value, true, out Napchart.Color c))
                        {
                            color = c;
                            selectedColor = true;
                        }

                        break;
                    }

					case "label": if (!string.IsNullOrEmpty(value)) label = value; break;

					case "name": if (!string.IsNullOrEmpty(value)) scheduleName = value; break;

					case "description": if (!string.IsNullOrEmpty(value)) description = value; break;
				}
			}

            if (schedule != null)
			{
                if (string.IsNullOrWhiteSpace(label))
                {
                    if (selectedColor)
                    {
                        switch (color)
                        {
                            case Napchart.Color.red:    label = "Sleep";        break;
                            case Napchart.Color.purple: label = "Dark Period";  break;
                            case Napchart.Color.yellow: label = "Can Eat";      break;
                            case Napchart.Color.blue:   label = "Sleep Range";  break;
                            case Napchart.Color.green:  label = "Availability"; break;
                            case Napchart.Color.pink:   label = "Exercise";     break;
                            case Napchart.Color.gray:   label = "Work";         break;
                        }
                    }				
				}

                string hex = (int)color > 7 ? Napchart.extraColors.Find(c => c.colorName == color.ToString()).hex : "";

				Napchart napchart = new
                (
                    new List<Objective> { new Objective(schedule, 0, color) },
                    new List<ColorTag> { new ColorTag(label, color, hex) },
					scheduleName, description
				);

				if (!string.IsNullOrEmpty(hex)) napchart.customColors = new() { (color.ToString(), hex) };
				napchart.publicLink = NapchartInterface.createChart(napchart);

                EmbedBuilder embed = new()
                {
                    Color = Napchart.getDominantColor(napchart),
                    ImageUrl = NapchartInterface.getChartImg(napchart.publicLink),
                    Description = napchart.publicLink,
                    Author = new EmbedAuthorBuilder()
                    {
                        Name = $"Requested By: {usersName}",
                        IconUrl = Bot.getGuildUser(interaction.Channel.Id, interaction.User.Id).GetAvatarUrl()
                    }
                };

                interaction.RespondAsync(embed: embed.Build());
			}
            else
            {
                //handle error
                interaction.RespondAsync($"Error with entry: [{timesError}]", ephemeral: true);
            }
		}

        #region detailed
        internal static Dictionary<string, (Napchart napchart, IUserMessage embedMessage, ColorTag tempColor)> _inProgressNapcharts = new();
        internal enum Step { Naming, AddElements, EditElement, DeleteElement, SelectColor, Done };

		internal async static Task<Task> createnapchart_detailed(IDiscordInteraction interaction)
		{
			IUser user = interaction.User;
			IChannel channel = Bot._client.GetChannelAsync(interaction.ChannelId.Value).Result;
			string ID = $"{user.Id}/{channel.Id}";

			switch (interaction)
            {
                case SocketSlashCommand sInt:
				{
                    //end existing creation process
                    if (_inProgressNapcharts.TryGetValue(ID, out var current))
                    {
                        (EmbedBuilder embed, ComponentBuilder _) = getCNEmbeds(ID, Step.Done, user, channel);

                        await current.embedMessage.ModifyAsync(m =>
                        {
                            m.Embed = embed.Build();
                            m.Components = null;
                        });

                        _inProgressNapcharts.Remove(ID);
                    }

					await sInt.RespondWithModalAsync(getCNModal(sInt.User.Id, sInt.Channel.Id, Step.Naming).Build());
					_inProgressNapcharts.TryAdd(ID, (null, null, null));
					break;
                }

                case SocketMessageComponent cInt:
                {
					if (_inProgressNapcharts.TryGetValue(ID, out var inProgress) && Enum.TryParse(cInt.Data.CustomId.Split(':')[1], out Step step))
					{
						switch (step)
						{
							case Step.AddElements:
                            {
                                //disable buttons
								(EmbedBuilder oEmbed, ComponentBuilder oComp) = getCNEmbeds(ID, Step.SelectColor, user, channel, true);
								await inProgress.embedMessage.ModifyAsync(m =>
								{
									m.Embed = oEmbed.Build();
									m.Components = oComp.Build();
                                });

                                //send color selection embed
								(EmbedBuilder embed, ComponentBuilder comp) = getCNEmbeds(ID, Step.SelectColor, user, channel, false);
								await cInt.RespondAsync(embed: embed.Build(), components: comp.Build());
								break;
							}

							case Step.EditElement:
							{
                                //select element
                                if (cInt.Data.CustomId == "CreateNapchart:EditElement")
                                {
                                    //disable buttons
									(EmbedBuilder oEmbed, ComponentBuilder oComp) = getCNEmbeds(ID, step, user, channel, true);
                                    await _inProgressNapcharts[ID].embedMessage.ModifyAsync(m =>
                                    {
										m.Embed = oEmbed.Build();
										m.Components = oComp.Build();
									});

									//send element selection embed
									(EmbedBuilder embed, ComponentBuilder comp) = getCNEmbeds(ID, step, user, channel);
									await cInt.RespondAsync(embed: embed.Build(), components: comp.Build());

									return Task.CompletedTask;
								}
								//select color
								else if (int.TryParse(cInt.Data.Values?.First(), out int elementIndex))
								{
									//add selected element to colortag's tag to be retrieved later
									_inProgressNapcharts[ID] = (inProgress.napchart, inProgress.embedMessage, new(elementIndex.ToString(), Napchart.Color.custom));

									//replace element selection embed with color selection embed
									(EmbedBuilder embed, ComponentBuilder comp) = getCNEmbeds(ID, Step.SelectColor, user, channel, addKeepColor: true);
									await cInt.Message.DeleteAsync();
									await cInt.RespondAsync(embed: embed.Build(), components: comp.Build());

									return Task.CompletedTask;
								}
								//send modal
								else
								{
                                    elementIndex = int.Parse(inProgress.tempColor.tag);
									//Get new color or previous color if they decided to keep it the same
									if (!Enum.TryParse(cInt.Data.Values?.First(), out Napchart.Color color) && int.TryParse(inProgress.tempColor.tag, out int elmementIndex))
                                        color = inProgress.napchart.chartData.elements[elementIndex].color;

									//update colortag with new color
									_inProgressNapcharts[ID] = (inProgress.napchart, inProgress.embedMessage, new(inProgress.tempColor.tag, color));

									await cInt.RespondWithModalAsync(getCNModal(user.Id, channel.Id, step, color: color, elementIndex: elementIndex).Build());
									await cInt.Message.DeleteAsync();

									//reenable buttons in case they cancel the modal
									(EmbedBuilder embed, ComponentBuilder comp) = getCNEmbeds(ID, Step.Naming, user, channel);
									await _inProgressNapcharts[ID].embedMessage.ModifyAsync(m =>
                                    {
                                        m.Embed = embed.Build();
										m.Components = comp.Build();
									});
								}

								break;
							}

                            case Step.DeleteElement:
							{
								//select element
								if (cInt.Data.CustomId == "CreateNapchart:DeleteElement")
								{
									//disable buttons
									(EmbedBuilder oEmbed, ComponentBuilder oComp) = getCNEmbeds(ID, step, user, channel, true);
									await _inProgressNapcharts[ID].embedMessage.ModifyAsync(m =>
									{
										m.Embed = oEmbed.Build();
										m.Components = oComp.Build();
									});

									//send element selection embed
									(EmbedBuilder embed, ComponentBuilder comp) = getCNEmbeds(ID, step, user, channel);
									await cInt.RespondAsync(embed: embed.Build(), components: comp.Build());

									return Task.CompletedTask;
								}
								//select color
								else if (int.TryParse(cInt.Data.Values?.First(), out int elementIndex))
								{
									Napchart tempChart = new(inProgress.napchart);
                                    Element e = tempChart.chartData.elements[elementIndex];
                                    List<Objective> objectives = tempChart.objectives;

									for (int i = 0; i < objectives.Count; i++)
									{
										Objective o = objectives[i];
										for (int j = 0; j < o.schedule.sleeps.Count; j++)
										{
											SleepBlock sb = o.schedule.sleeps[j];
											if (sb.startTime == SFAlgorithm.intToString(e.start) && sb.endTime == SFAlgorithm.intToString(e.end)
												&& o.color == e.color && (o.color != Napchart.Color.custom || o.customColor == e.customColor))
                                            {
												o.schedule.sleeps.Remove(sb);

												if (o.schedule.sleeps.Count == 0)
													objectives.Remove(o);

												i = objectives.Count;
												break;
											}
										}
									}

                                    List<Element> elements = tempChart.chartData.elements.ToList();
                                    elements.Remove(e);
                                    tempChart.chartData.elements = elements.Count == 0 ? (Array.Empty<Element>()) : elements.ToArray();


                                    string link = NapchartInterface.createChart(tempChart);

                                    //no error
                                    if (!string.IsNullOrWhiteSpace(link) && Uri.IsWellFormedUriString(link, UriKind.Absolute))
                                    {
                                        tempChart.publicLink = link;
										_inProgressNapcharts[ID] = (tempChart, inProgress.embedMessage, null);
									}

									//remove element selection embed
									await cInt.Message.DeleteAsync();

                                    //rebuild initial embed
                                    (EmbedBuilder embed, ComponentBuilder comp) = getCNEmbeds(ID, Step.Naming, user, channel);
                                    await _inProgressNapcharts[ID].embedMessage.ModifyAsync(m =>
                                    {
                                        m.Embed = embed.Build();
                                        m.Components = comp.Build();
                                    });

                                    await cInt.DeferAsync();

									return Task.CompletedTask;
								}

								break;
                            }

							case Step.SelectColor:
							{
								if (Enum.TryParse(cInt.Data.Values.First(), true, out Napchart.Color color))
                                {
									_inProgressNapcharts[ID] = (inProgress.napchart, inProgress.embedMessage, new ColorTag("", color, ""));

                                    await interaction.RespondWithModalAsync(getCNModal(user.Id, channel.Id, Step.AddElements, color: color).Build());
									await cInt.Message.DeleteAsync();

									//reenable buttons in case they cancel the modal
									(EmbedBuilder embed, ComponentBuilder comp) = getCNEmbeds(ID, Step.Naming, user, channel);
									await _inProgressNapcharts[ID].embedMessage.ModifyAsync(m =>
									{
										m.Embed = embed.Build();
										m.Components = comp.Build();
									});
								}

								break;
							}

							case Step.Done:
							{
								(EmbedBuilder embed, var _) = getCNEmbeds(ID, step, user, channel);
								await inProgress.embedMessage.ModifyAsync(m =>
								{
									m.Embed = embed.Build();
									m.Components = null;
								});
								_inProgressNapcharts.Remove(ID);
								await cInt.DeferAsync();
								break;
							}
						}
					}

					break;
                }

                case IModalInteraction mInt:
                {
					string[] data = mInt.Data.CustomId[..^1].Split('[')[1].Split('|');

					if (_inProgressNapcharts.TryGetValue(data[1], out var inProgress) && Enum.TryParse(data[0], out Step step))
					{
						switch (step)
						{
							case Step.Naming:
                            {
								Dictionary<string, string> values = mInt.Data.Components.ToList().Select(c => (c.CustomId, c.Value)).ToDictionary(c => c.CustomId, c => c.Value);
								inProgress.napchart ??= new(new(), new(), values["name"], values["description"]);

								_inProgressNapcharts[ID] = (inProgress.napchart, inProgress.embedMessage, null);

								(EmbedBuilder embed, ComponentBuilder components) = getCNEmbeds(ID, Step.Naming, interaction.User, Bot._client.GetChannelAsync(interaction.ChannelId.Value).Result);
								await interaction.RespondAsync(embed: embed.Build(), components: components.Build());

								_inProgressNapcharts[ID] = (inProgress.napchart, inProgress.embedMessage ?? interaction.GetOriginalResponseAsync().Result, null);

								break;
							}

							case Step.AddElements:
                            {
								Dictionary<string, string> values = mInt.Data.Components.ToList().Select(c => (c.CustomId, c.Value)).ToDictionary(c => c.CustomId, c => c.Value);
								string[] elements = values["elements"].Split(',');
                                int      lane = processCNElementValues(mInt, inProgress.napchart, elements, values["lane"]);

                                //add element
								if (lane >= 0)
								{
									List<ColorTag> tags = inProgress.napchart.chartData.colorTags.ToList();
									List<(string colorName, string hex)> customColors = inProgress.napchart.customColors ??= new();
									Napchart.Color color = inProgress.tempColor.color;
									if (values.TryGetValue("hex", out string hex))
										inProgress.tempColor.customColor = hex;
									bool colorTagDuplicate = false;
									string label = "";

									if (values.TryGetValue("label", out string l)) label = l;

									if (inProgress.napchart.chartData.colorTags.ToList().Exists(c => (c.color == Napchart.Color.custom && c.customColor == hex) || c.color == color))
									{
										ColorTag dupe = tags.Find(c => (c.color == Napchart.Color.custom && c.customColor == hex) || c.color == color);

										if (string.IsNullOrWhiteSpace(label) && !string.IsNullOrWhiteSpace(dupe.tag)) label = dupe.tag;
										else if (!string.IsNullOrWhiteSpace(label) && string.IsNullOrWhiteSpace(dupe.tag)) tags.Find(t => t == dupe).tag = label;

										tags.Find(t => t == dupe).tag = label;
										colorTagDuplicate = true;
									}

									if (inProgress.tempColor != null && !colorTagDuplicate)
									{
										inProgress.tempColor.tag = label;
										tags.Add(inProgress.tempColor);
										color = inProgress.tempColor.color;
									}

                                    inProgress.napchart.chartData.colorTags = tags.ToArray();

									inProgress.napchart.objectives.Add(new(ScheduleFinder.createScheduleFromSleepTimes(elements), lane, color, !string.IsNullOrWhiteSpace(hex) ? hex : ""));
									inProgress.napchart = new(inProgress.napchart) { customColors = customColors };

									if ((int)color > 7)
									{
										if (string.IsNullOrEmpty(hex)) hex = Napchart.extraColors.Find(c => c.colorName == color.ToString()).hex;

										inProgress.napchart.customColors.Add(($"{color}{(color == Napchart.Color.custom ? $"({hex})" : "")}", hex));
									}

									string link = NapchartInterface.createChart(inProgress.napchart);

                                    if (string.IsNullOrWhiteSpace(link) || !Uri.IsWellFormedUriString(link, UriKind.Absolute))
                                    {
                                        _inProgressNapcharts[ID] = (_inProgressNapcharts[ID].napchart, _inProgressNapcharts[ID].embedMessage, null);
                                        await mInt.RespondAsync("There was an issue with your entry and Napchart returned an error. Please try again.", ephemeral: true);
                                    }
                                    else
                                    {
                                        inProgress.napchart.publicLink = link;
                                        _inProgressNapcharts[ID] = (inProgress.napchart, inProgress.embedMessage, null);

                                        (EmbedBuilder embed, ComponentBuilder components) = getCNEmbeds(ID, Step.AddElements, user, channel);
										await _inProgressNapcharts[ID].embedMessage.ModifyAsync(m =>
										{
											m.Embed = embed.Build();
											m.Components = components.Build();
										});
										await mInt.DeferAsync();
									}
								}

								break;
                            }

							case Step.EditElement:
							{
								if (int.TryParse(inProgress.tempColor?.tag, out int elementID))
								{
									Dictionary<string, string> values = mInt.Data.Components.ToList().Select(c => (c.CustomId, c.Value)).ToDictionary(c => c.CustomId, c => c.Value);
									string[] element = values["element"].Split(',');
									string text      = values["text"],
									       label     = values["label"];
									int    lane = processCNElementValues(mInt, inProgress.napchart, element, values["lane"], elementID);

									//update element
									if (lane >= 0)
									{
										string[] startEnd = element[0].Split('-');

										Napchart tempChart = new(inProgress.napchart);
										ColorTag tempColor = inProgress.tempColor;
										Element e = tempChart.chartData.elements[elementID];
										Objective newObj = null;
										List<(string colorName, string hex)> customColors = tempChart.customColors;

										for (int i = 0; i < tempChart.objectives.Count; i++)
										{
											Objective o = tempChart.objectives[i];
											for (int j = 0; j < o.schedule.sleeps.Count; j++)
											{
												SleepBlock sb = o.schedule.sleeps[j];
												if (sb.startTime.Trim() == SFAlgorithm.intToString(e.start) && sb.endTime.Trim() == SFAlgorithm.intToString(e.end)
													&& o.color == e.color && (o.color != Napchart.Color.custom || o.customColor == e.customColor))
												{
													if (o.color != tempColor.color || (tempColor.color == Napchart.Color.custom && tempColor.customColor != o.customColor) ||
														o.laneNum != lane || sb.blockText != text)
													{
														o.schedule.sleeps.Remove(sb);

														newObj = new
														(
															new("", new() { new(SFAlgorithm.getDifferenceTime(startEnd[0], startEnd[1]), startEnd[0], startEnd[1], inBlockText: text) }),
															lane, tempColor.color, tempColor.customColor
														);

														if (o.schedule.sleeps.Count == 0)
															tempChart.objectives.Remove(o);

														tempChart.objectives.Add(newObj);
													}

													i = tempChart.objectives.Count;
													break;
												}
											}
										}

										e.start = SFAlgorithm.convertTimeToInt(startEnd[0]);
										e.end = SFAlgorithm.convertTimeToInt(startEnd[1]);
										e.lane = lane;
										e.text = text;

										//get hex and add customcolor value
										if (values.TryGetValue("hex", out string hex) && tempColor.color == Napchart.Color.custom)
										{
											tempChart.customColors ??= new();
											tempChart.customColors.Add(new(tempColor.color.ToString(), tempColor.customColor));
										}

										List<ColorTag> colorTags = tempChart.chartData.colorTags.ToList();
										//if color, tag, or hex will be different from colortag's current value, need to update current or add new
										if (e.color != tempColor.color || colorTags.Find(c => c.color == e.color && c.customColor == e.customColor)?.tag != label || e.customColor != tempColor.customColor)
										{
											int index = -1;
											//find colortag for element's old color and label and if there are no other elements using them, update them
											if ((index = colorTags.IndexOf(colorTags.Find(c => c.color == e.color && (c.color != Napchart.Color.custom || c.customColor == e.customColor))))
												>= 0 && !tempChart.chartData.elements.ToList().Exists(x => x.color == e.color && x.customColor == e.customColor))
											{
												tempChart.chartData.colorTags[index] = new ColorTag(label, tempColor.color, tempColor.customColor);

												//same with customcolor if applicable
												if ((index = customColors.IndexOf(customColors.Find(c => c.colorName == $"{e.color}{(e.color == Napchart.Color.custom ? $"({e.customColor})" : "")}"
														&& (c.colorName != Napchart.Color.custom.ToString() || c.hex == e.customColor)))) >= 0)
													tempChart.customColors[index] = (tempColor.color.ToString(), tempColor.customColor);
											}
											else
											{
												//insert new color info
												e.color = tempColor.color;
												e.customColor = !string.IsNullOrEmpty(hex) ? hex : "";

												if ((int)e.color > 7)
												{
													e.customColor = Napchart.extraColors.Find(c => c.colorName == e.color.ToString()).hex;
													customColors ??= new();
													customColors.Add(($"{e.color}{(e.color == Napchart.Color.custom ? $"({e.customColor})" : "")}", e.customColor));
												}

												colorTags.Add(new ColorTag(label, e.color, e.customColor));
												tempChart.chartData.colorTags = colorTags.ToArray();
											}
										}

										tempChart = new(tempChart.objectives, colorTags, tempChart.title, tempChart.description);
										tempChart.customColors ??= new();
										tempChart.customColors.Add(($"{e.color}{(e.color == Napchart.Color.custom ? $"({e.customColor})" : "")}", e.customColor));

										string link = NapchartInterface.createChart(tempChart);

										//error
										if (string.IsNullOrWhiteSpace(link) || !Uri.IsWellFormedUriString(link, UriKind.Absolute))
											await mInt.RespondAsync("There was an issue with your entry and Napchart returned an error. Please try again.", ephemeral: true);
										//Success
										else
										{
											tempChart.publicLink = link;
											_inProgressNapcharts[ID] = (tempChart, inProgress.embedMessage, null);

											(EmbedBuilder embed, ComponentBuilder components) = getCNEmbeds(ID, Step.AddElements, user, channel);
											await _inProgressNapcharts[ID].embedMessage.ModifyAsync(m =>
											{
												m.Embed = embed.Build();
												m.Components = components.Build();
											});
											await mInt.DeferAsync();
										}

										_inProgressNapcharts[ID] = (_inProgressNapcharts[ID].napchart, _inProgressNapcharts[ID].embedMessage, null);
									}
								}								

								break;
                            }
						}
					}

					break;
                }
            }

            return Task.CompletedTask;
        }

		internal static ModalBuilder getCNModal(ulong userID, ulong channelID, Step type, Napchart.Color? color = null, int elementIndex = -1)
		{
            ModalBuilder modal = new() { Components = new() };
            string ID = $"{userID}/{channelID}";
			string customID = $"createnapchart[type|{ID}]", 
                   title = $"CreateNapchart: ";
            Element element = null;
            if (elementIndex >= 0)
            {
                element = _inProgressNapcharts[ID].napchart.chartData.elements[elementIndex];
                color ??= element.color;
            }

            switch (type)
            {
                case Step.Naming:
                {
                    modal
                        .WithCustomId(customID.Replace("type", type.ToString()))
                        .WithTitle(title + "Add a Name and Description");
                    modal.Components
                        .WithTextInput(new()
                        {
                            CustomId = "name",
                            Label = "Name",
                            Placeholder = "Napchart Name",
                            MinLength = 1,
                            MaxLength = 50,
                            Required = false,
                            Style = TextInputStyle.Short
                        }) //name
					    .WithTextInput(new()
                        {
                            CustomId = "description",
                            Label = "Description",
                            Placeholder = "Napchart Discription",
                            MinLength = 0,
                            Required = false,
                            Style = TextInputStyle.Paragraph
                        }); //description
					break;
                }

				case Step.AddElements:
				{
                    modal
                        .WithCustomId(customID.Replace("type", type.ToString()))
                        .WithTitle(title + "Add Elements");
                    modal.Components
                        .WithTextInput(new()
                        {
                            CustomId = "elements",
                            Label = "Elements",
                            Placeholder = "[ex: 00:00-5:29, 17:15-23:00]",
                            MinLength = 9,
                            Required = true,
                            Style = TextInputStyle.Short
                        })  //element
                        .WithTextInput(new()
                        {
                            CustomId = "lane",
                            Label = "Lane",
                            Placeholder = "Lane Elements will appear on.",
                            MinLength = 1,
                            MaxLength = 1,
                            Required = true,
                            Style = TextInputStyle.Short
                        }); //lane

                    if (color == Napchart.Color.custom || !_inProgressNapcharts[ID].napchart.chartData.colorTags.ToList().Exists(c => c.color == color))
                        modal.Components
                            .WithTextInput(new()
						    {
							    CustomId = "label",
							    Label = $"Color Label for {(color != null ? $"{color}" : "selected color")}",
							    Placeholder = "Sleep",
							    MaxLength = 15,
							    Required = false,
							    Style = TextInputStyle.Short
						    }); //label
					    
                    if (color is not null and Napchart.Color.custom)
                        modal.Components.WithTextInput(new()
						{
							CustomId = "hex",
							Label = "Hex value for custom color:",
							Placeholder = "[ex: #000000]",
							MinLength = 7,
							MaxLength = 7,
							Style = TextInputStyle.Short,
							Required = true
						}); //hex
					
					break;
				}

				case Step.EditElement:
				{
                    ColorTag cTag = _inProgressNapcharts[ID].napchart.chartData.colorTags.ToList()
                        .Find(c => color == c.color && (color != Napchart.Color.custom || element.customColor == c.customColor));

					modal.WithCustomId(customID.Replace("type", type.ToString()));
					modal.WithTitle(title + "Edit Element");
					modal.Components
                        .WithTextInput(new()
					    {
						    CustomId = "element",
						    Label = "Edit Element",
						    Placeholder = "[ex: 00:00-5:29]",
						    MinLength = 0,
                            MaxLength = 11,
						    Style = TextInputStyle.Short,
                            Required = false,
                            Value = SFAlgorithm.intsToRange(element.start, element.end)
						})  //element
						.WithTextInput(new()
						{
							CustomId = "text",
							Label = "Text",
							Placeholder = "Text to be displayed above element",
							MinLength = 0,
							MaxLength = 30,
							Style = TextInputStyle.Short,
							Required = false,
                            Value = !string.IsNullOrEmpty(element.text) ? element.text : null
						})  //text
					    .WithTextInput(new()
					    {
						    CustomId = "lane",
						    Label = "Lane",
						    Placeholder = "Lane Element will appear on.",
						    MinLength = 1,
						    MaxLength = 1,
						    Required = true,
						    Style = TextInputStyle.Short,
                            Value = element.lane >= 0 ? (element.lane + 1).ToString() : "1"
					    })  //lane
                        .WithTextInput(new()
						{
							CustomId = "label",
							Label = $"Color Label for {(color != null ? $"{color}{(color == Napchart.Color.custom ? $"({element.customColor})" : "")}" : "selected color")}",
							Placeholder = "Sleep",
							MaxLength = 15,
							Required = false,
							Style = TextInputStyle.Short,
                            Value = !string.IsNullOrEmpty(cTag?.tag) ? cTag.tag : null
						}); //label

					if (color is not null and Napchart.Color.custom)
						modal.Components
                            .WithTextInput(new()
						    {
							    CustomId = "hex",
							    Label = "Hex value for custom color:",
							    Placeholder = "[ex: #000000]",
							    MinLength = 7,
							    MaxLength = 7,
							    Style = TextInputStyle.Short,
							    Required = true,
                                Value = !string.IsNullOrEmpty(element.customColor) ? element.customColor : null
						    }); //hex
					break;
				}
			}

			return modal;
		}
        
        internal static (EmbedBuilder, ComponentBuilder) getCNEmbeds(string inProgressID, Step step, IUser user, IChannel channel, bool oEmbed = false, string dominantColorHex = "", bool addKeepColor = false)
        {
			(Napchart napchart, IMessage embedMessage, var _) = _inProgressNapcharts[inProgressID];

			EmbedBuilder embed = new();
			ComponentBuilder components = new();

			Discord.Color embedColor = dominantColorHex == "" && napchart?.objectives?.Count > 0 && !(!oEmbed && step == Step.SelectColor) ? 
                Napchart.getDominantColor(napchart) : Color.Default;

			string napchartLink = "";
			try
			{
				if (!string.IsNullOrWhiteSpace(napchart.publicLink) && step != Step.Naming)
					napchartLink = NapchartInterface.getChartImg(napchart.publicLink);
			}
			catch { }

			switch (step)
            {
                case Step.Naming:
                {
					embed = new()
					{
						Title = $"Custom Napchart{(!string.IsNullOrWhiteSpace(napchart.title) ? $": {napchart.title}" : "")}",
						Description =
				            $"`1.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Name and Description.\n\n" +
				            $"`2.` {Bot.getEmote(Config._emojiNames["Empty"])} Add Elements\n" +
							"`   a.` Use `Add Elements` button to show dropdown to select color of elements.\n" +
							"`   b.` Specify element times, lane, and label for color selected in **a**, then submit.\n" +
				            "`   c.` Repeat **a** and **b** as much as you'd like!\n\n" +
				            $"`3.` {Bot.getEmote(Config._emojiNames["Empty"])} Add Element Text\n" +
				            "`   a.` Select `Edit Element` button.\n" +
				            "`   b.` Select element to add text to.\n" +
				            "`   c.` Enter text to display over element and submit.\n\n" +
				            $"`4.` {Bot.getEmote(Config._emojiNames["Empty"])} Select `Done` to finish!",

						Author = new EmbedAuthorBuilder()
						{
							Name = Bot.getUsersName(user, Bot.getServer(Bot._client.GetChannelAsync(channel.Id).Result as IMessageChannel)),
							IconUrl = Bot.getGuildUser(channel.Id, user.Id).GetAvatarUrl()
						},
						ImageUrl = !string.IsNullOrWhiteSpace(napchart.publicLink) ? NapchartInterface.getChartImg(napchart.publicLink) : null,
						Color = embedColor
					};

					components
			            .WithButton(new ButtonBuilder()
			            {
				            CustomId = "CreateNapchart:AddElements",
				            Label = "Add Elements",
				            Style = ButtonStyle.Success
			            })  //AddElements
			            .WithButton(new ButtonBuilder()
			            {
				            CustomId = "CreateNapchart:EditElement",
				            Label = "Edit Element",
				            Style = ButtonStyle.Secondary,
				            IsDisabled = napchart.objectives.Count == 0
					    })  //EditElement
						.WithButton(new ButtonBuilder()
						{
							CustomId = "CreateNapchart:DeleteElement",
							Label = "Delete Element",
							Style = ButtonStyle.Danger,
							IsDisabled = napchart.objectives.Count == 0
						})  //DeleteElement
						.WithButton(new ButtonBuilder()
			            {
				            CustomId = "CreateNapchart:Done",
				            Label = "Done",
				            Style = ButtonStyle.Primary
			            }); //Done
                    break;
				}

                case Step.AddElements:
				{
					embed = new()
					{
						Title = $"Custom Napchart{(!string.IsNullOrWhiteSpace(napchart.title) ? $": {napchart.title}" : "")}",
						Description =
							$"`1.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Name and Description.\n\n" +
							$"`2.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Elements\n" +
							$"`   a.` Use `Add Elements` button to show dropdown to select color of elements.\n" +
							"`   b.` Specify element times, lane, and label for color selected in **a**, then submit.\n" +
							"`   c.` Repeat **a** and **b** as much as you'd like!\n\n" +
							$"`3.` {Bot.getEmote(Config._emojiNames["Empty"])} Add Element Text\n" +
							"`   a.` Select `Edit Element` button.\n" +
							"`   b.` Select element to add text to.\n" +
							"`   c.` Enter text to display over element and submit.\n\n" +
							$"`4.` {Bot.getEmote(Config._emojiNames["Empty"])} Select `Done` to finish!",

						Author = new EmbedAuthorBuilder()
						{
							Name = Bot.getUsersName(user, Bot.getServer(Bot._client.GetChannelAsync(channel.Id).Result as IMessageChannel)),
							IconUrl = Bot.getGuildUser(channel.Id, user.Id).GetAvatarUrl()
						},
						ImageUrl = !string.IsNullOrWhiteSpace(napchartLink) ? napchartLink : "",
						Color = embedColor
					};

					components
						.WithButton(new ButtonBuilder()
						{
							CustomId = "CreateNapchart:AddElements",
							Label = "Add Elements",
							Style = ButtonStyle.Success
						})  //AddElements
						.WithButton(new ButtonBuilder()
						{
							CustomId = "CreateNapchart:EditElement",
							Label = "Edit Element",
							Style = ButtonStyle.Secondary,
							IsDisabled = napchart.objectives.Count == 0
						})  //EditElement
						.WithButton(new ButtonBuilder()
						{
							CustomId = "CreateNapchart:DeleteElement",
							Label = "Delete Element",
							Style = ButtonStyle.Danger
						})  //DeleteElement
						.WithButton(new ButtonBuilder()
						{
							CustomId = "CreateNapchart:Done",
							Label = "Done",
							Style = ButtonStyle.Primary
						}); //Done
					break;
                }

                case Step.EditElement:
				{
					Napchart n = new(napchart);
					List<Element> elements = new(n.chartData.elements.ToList());

					List<Element> elements1;
					foreach (Element element in elements1 = elements.FindAll(e => !string.IsNullOrWhiteSpace(e.text)))
					{
						List<Element> elements2 = elements1.FindAll(e => e.text == element.text);
						if (elements2.Count > 1)
						{
							for (int i = 0; i < elements2.Count; i++)
								elements[elements.IndexOf(elements2[i])].text = $"{elements2[i].text} {i + 1}";
						}
					}

					if (!oEmbed)
					{
						embed = new()
						{
							Description = "Select an Element",
							Color = embedColor
						};

						components
							.WithSelectMenu(new SelectMenuBuilder()
							{
								CustomId = "CreateNapchart:EditElement:Select",
								Options = elements.ToList()
									.Select(e => new SelectMenuOptionBuilder()
									{
										Label = $"{e.color}{(e.color == Napchart.Color.custom ? $"({e.customColor})" : "")} on {e.lane + 1}: " +
                                        (!string.IsNullOrWhiteSpace(e.text) ? e.text : $"#{elements.IndexOf(e) + 1}"),
										Value = elements.IndexOf(e).ToString()
									}).ToList()
							});
					}
					else
					{
						foreach (Element e in elements.FindAll(e => string.IsNullOrWhiteSpace(e.text)))
                            e.text = (elements.IndexOf(e) + 1).ToString();

						n.chartData.elements = elements.ToArray();

                        string link = NapchartInterface.createChart(n);
                        if (!string.IsNullOrWhiteSpace(link) && Uri.IsWellFormedUriString(link, UriKind.Absolute))
                            n.publicLink = link;

						embed = new()
						{
							Title = $"Custom Napchart{(!string.IsNullOrWhiteSpace(n.title) ? $": {n.title}" : "")}",
							Description =
							$"`1.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Name and Description.\n\n" +
							$"`2.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Elements\n" +
							"`   a.` Specify element times, lane, and label for color selected in **a**, then submit.\n" +
							"`   b.` Use `Add Elements` button to specify element times, lane, and label for color selected in **a**, then submit.\n" +
							"`   c.` Repeat **a** and **b** as much as you'd like!\n\n" +
							$"`3.` {Bot.getEmote(Config._emojiNames["Empty"])} Add Element Text\n" +
							$"`   a.` {Bot.getEmote(Config._emojiNames["Yes"])} Select `Edit Element` button.\n" +
							"`   b.` ➡️ Select element to add text to.\n" +
							"`   c.` Enter text to display over element and submit.\n\n" +
							$"`4.` {Bot.getEmote(Config._emojiNames["Empty"])} Select `Done` to finish!",

							Author = new EmbedAuthorBuilder()
							{
								Name = Bot.getUsersName(user, Bot.getServer(Bot._client.GetChannelAsync(channel.Id).Result as IMessageChannel)),
								IconUrl = Bot.getGuildUser(channel.Id, user.Id).GetAvatarUrl()
							},
							ImageUrl = !string.IsNullOrWhiteSpace(n.publicLink) ? NapchartInterface.getChartImg(n.publicLink) : "",
							Color = embedColor
						};

						components
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:AddElements",
								Label = "Add Elements",
								Style = ButtonStyle.Success,
								IsDisabled = true
							})  //AddElements
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:EditElement",
								Label = "Edit Element",
								Style = ButtonStyle.Secondary,
								IsDisabled = true
							})  //EditElement
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:DeleteElement",
								Label = "Delete Element",
								Style = ButtonStyle.Danger,
								IsDisabled = true
							})  //DeleteElement
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:Done",
								Label = "Done",
								Style = ButtonStyle.Primary,
								IsDisabled = true
							}); //Done
					}

					break;
				}

				case Step.DeleteElement:
				{
					Napchart n = new(napchart);
					List<Element> elements = new(n.chartData.elements.ToList());

					List<Element> elements1;
					foreach (Element element in elements1 = elements.FindAll(e => !string.IsNullOrWhiteSpace(e.text)))
					{
						List<Element> elements2 = elements1.FindAll(e => e.text == element.text);
						if (elements2.Count > 1)
						{
							for (int i = 0; i < elements2.Count; i++)
								elements[elements.IndexOf(elements2[i])].text = $"{elements2[i].text} {i + 1}";
						}
					}

					if (!oEmbed)
					{
						embed = new()
						{
							Description = "Select an Element",
							Color = embedColor
						};

						components
							.WithSelectMenu(new SelectMenuBuilder()
							{
								CustomId = "CreateNapchart:DeleteElement:Select",
								Options = elements.ToList()
									.Select(e => new SelectMenuOptionBuilder()
									{
										Label = $"{e.color}{(e.color == Napchart.Color.custom ? $"({e.customColor})" : "")} on {e.lane + 1}: " +
										(!string.IsNullOrWhiteSpace(e.text) ? e.text : $"#{elements.IndexOf(e) + 1}"),
										Value = elements.IndexOf(e).ToString()
									}).ToList()
							});
					}
					else
					{
						foreach (Element e in elements.FindAll(e => string.IsNullOrWhiteSpace(e.text)))
							e.text = (elements.IndexOf(e) + 1).ToString();

						n.chartData.elements = elements.ToArray();

						string link = NapchartInterface.createChart(n);
						if (!string.IsNullOrWhiteSpace(link) && Uri.IsWellFormedUriString(link, UriKind.Absolute))
							n.publicLink = link;

						embed = new()
						{
							Title = $"Custom Napchart{(!string.IsNullOrWhiteSpace(n.title) ? $": {n.title}" : "")}",
							Description =
							$"`1.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Name and Description.\n\n" +
							$"`2.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Elements\n" +
							"`   a.` Specify element times, lane, and label for color selected in **a**, then submit.\n" +
							"`   b.` Use `Add Elements` button to specify element times, lane, and label for color selected in **a**, then submit.\n" +
							"`   c.` Repeat **a** and **b** as much as you'd like!\n\n" +
							$"`3.` {Bot.getEmote(Config._emojiNames["Empty"])} Add Element Text\n" +
							$"`   a.` {Bot.getEmote(Config._emojiNames["Yes"])} Select `Edit Element` button.\n" +
							"`   b.` ➡️ Select element to add text to.\n" +
							"`   c.` Enter text to display over element and submit.\n\n" +
							$"`4.` {Bot.getEmote(Config._emojiNames["Empty"])} Select `Done` to finish!",

							Author = new EmbedAuthorBuilder()
							{
								Name = Bot.getUsersName(user, Bot.getServer(Bot._client.GetChannelAsync(channel.Id).Result as IMessageChannel)),
								IconUrl = Bot.getGuildUser(channel.Id, user.Id).GetAvatarUrl()
							},
							ImageUrl = !string.IsNullOrWhiteSpace(n.publicLink) ? NapchartInterface.getChartImg(n.publicLink) : "",
							Color = embedColor
						};

						components
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:AddElements",
								Label = "Add Elements",
								Style = ButtonStyle.Success,
								IsDisabled = true
							})  //AddElements
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:EditElement",
								Label = "Edit Element",
								Style = ButtonStyle.Secondary,
								IsDisabled = true
							})  //EditElement
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:DeleteElement",
								Label = "Delete Element",
								Style = ButtonStyle.Danger,
								IsDisabled = true
							})  //DeleteElement
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:Done",
								Label = "Done",
								Style = ButtonStyle.Primary,
								IsDisabled = true
							}); //Done
					}

					break;
				}

				case Step.SelectColor:
				{
					if (!oEmbed)
					{
						embed = new()
						{
							Description = "Select a Color",
							Color = embedColor != Color.Default ? embedColor : Color.LightGrey
						};

						components
							.WithSelectMenu(new SelectMenuBuilder()
							{
								CustomId = $"CreateNapchart:{(addKeepColor ? "EditElement:" : "")}SelectColor",
								Options = MM.getEnums<Napchart.Color>().Select(s => new SelectMenuOptionBuilder()
								{
									Label = s.ToString(),
									Value = s.ToString(),
									Description = s == Napchart.Color.custom ? "Custom color (by hex value)" : null
								}).ToList()
							});

						if (addKeepColor)
							components
								.WithButton(new ButtonBuilder()
								{
									CustomId = "CreateNapchart:EditElement:Skip",
									Label = "Keep Current",
									Style = ButtonStyle.Secondary
								});
					}
					else
					{
						embed = new()
						{
							Title = $"Custom Napchart{(!string.IsNullOrWhiteSpace(napchart.title) ? $": {napchart.title}" : "")}",
							Description =
							$"`1.` {Bot.getEmote(Config._emojiNames["Yes"])} Add Name and Description.\n\n" +
							$"`2.` {Bot.getEmote(Config._emojiNames["Empty"])} Add Elements\n" +
							"`   a.` ➡️ Specify element times, lane, and label for color selected in **a**, then submit.\n" +
							"`   b.` Use `Add Elements` button to specify element times, lane, and label for color selected in **a**, then submit.\n" +
							"`   c.` Repeat **a** and **b** as much as you'd like!\n\n" +
							$"`3.` {Bot.getEmote(Config._emojiNames["Empty"])} Add Element Text\n" +
							"`   a.` Select `Edit Element` button.\n" +
							"`   b.` Select element to add text to.\n" +
							"`   c.` Enter text to display over element and submit.\n\n" +
							$"`4.` {Bot.getEmote(Config._emojiNames["Empty"])} Select `Done` to finish!",

							Author = new EmbedAuthorBuilder()
							{
								Name = Bot.getUsersName(user, Bot.getServer(Bot._client.GetChannelAsync(channel.Id).Result as IMessageChannel)),
								IconUrl = Bot.getGuildUser(channel.Id, user.Id).GetAvatarUrl()
							},
							ImageUrl = !string.IsNullOrWhiteSpace(napchartLink) ? NapchartInterface.getChartImg(napchart.publicLink) : null,
							Color = embedColor
						};

						components
						    .WithButton(new ButtonBuilder()
						    {
							    CustomId = "CreateNapchart:AddElements",
							    Label = "Add Elements",
							    Style = ButtonStyle.Success,
							    IsDisabled = true
						    })  //AddElements
						    .WithButton(new ButtonBuilder()
						    {
							    CustomId = "CreateNapchart:EditElement",
							    Label = "Edit Element",
							    Style = ButtonStyle.Secondary,
							    IsDisabled = true
						    })  //EditElement
							.WithButton(new ButtonBuilder()
							{
								CustomId = "CreateNapchart:DeleteElement",
								Label = "Delete Element",
								Style = ButtonStyle.Danger,
								IsDisabled = true
							})  //DeleteElement
							.WithButton(new ButtonBuilder()
						    {
							    CustomId = "CreateNapchart:Done",
							    Label = "Done",
							    Style = ButtonStyle.Primary,
							    IsDisabled = true
						    }); //Done
					}

					break;
                }

                case Step.Done:
				{
					embed = new()
                    {
                        Title = $"Custom Napchart{(!string.IsNullOrWhiteSpace(napchart.title) ? $": {napchart.title}" : "")}",
                        Description = napchart.publicLink,
                        Author = new EmbedAuthorBuilder()
                        {
                            Name = Bot.getUsersName(user, Bot.getServer(Bot._client.GetChannelAsync(channel.Id).Result as IMessageChannel)),
                            IconUrl = Bot.getGuildUser(channel.Id, user.Id).GetAvatarUrl()
                        },
                        ImageUrl = !string.IsNullOrWhiteSpace(napchartLink) ? napchartLink : "",
                        Color = embedColor
                    };

                    break;
                }
            }

            return (embed, components);
		}

        internal static int processCNElementValues(IModalInteraction mInt, Napchart napchart, string[] elements, string laneVal, int elementID = -1)
        {
			string errors = "";

            //check value of lane
            if (!int.TryParse(laneVal, out int lane))
                errors += $"Lane value [`{laneVal}` is not a valid lane number (1-9).\n";
            else lane--;

            //make sure element(s) are valid
			for (int i = 0; i < elements.Length; i++)
			{
                string range = elements[i];

				if (!SFAlgorithm.checkLongSleepTime(range.Trim()))
					errors += $"Element [`{range.Trim()}`] is in an invalid format.\n";
                else
                {
                    string[] startEnd = range.Split('-');
                    if (startEnd[0].Trim().Length == 4) startEnd[0] = "0" + startEnd[0].Trim();
					if (startEnd[1].Trim().Length == 4) startEnd[1] = "0" + startEnd[1].Trim();

                    elements[i] = (startEnd[0] + "-" + startEnd[1]).Trim();
				}
			}

            //check for overlaps
			List<string> elementsTemp = elements.ToList();
			if (napchart.chartData.elements.Length > 0)
				elementsTemp.AddRange(napchart.chartData.elements.ToList().Where(e => elementID < 0 || e != napchart.chartData.elements[elementID]).ToList().FindAll(el => el.lane == lane).Select(t => SFAlgorithm.intsToRange(t.start, t.end)));

			if (errors.Length == 0 && SFAlgorithm.checkSleepOverlaps(elementsTemp.ToArray()))
				errors += "Some of your elements overlap. Please try again without overlapping elements, or enter conflicting elements separately on different lanes.\n";

            if (errors.Length > 0)
            {
                mInt.RespondAsync($"Your input had errors, please try again.\n\n{errors}", ephemeral: true);
                return -1;
            }

			return lane;
		}
		#endregion

		/// <summary> Enter parameters to get a custom Napchart | -createNapchart objective: sleep times (ex: `00:00-07:00 (block text [optional]), 12:30-14:50, 21:00-21:35`); Lane Number; Color (up to 10 objectives) \n colorTag: Color Label; Associated color (optional; up to one per color) </summary> 
		public static void CreateNapchart(SocketMessage message, string commandName = "CreateNapchart", bool delete = false)
        {
            NapchartInterface ni = new();
            string description = "*Enter parameters to get a custom Napchart*\n";
            string objectiveFormat = "`Block Times (ex: 00:00-07:00 (block text [optional]), 12:30-14:50, 21:00-21:35); Lane Number; Color` *(up to 10 each on separate lines)*";
            string colorTagFormat = "`Color = Color Label` *(optional; up to one colorTag per color each on separate lines)*";
            string errorMsg = $"**Proper Format:** `-{commandName} Block Times (ex: 00:00-07:00, 12:30-14:50, 21:00-21:35)`\n" +
                $"*Or*\n`-{commandName} [optional: Chart Name]`\n{objectiveFormat}\n{colorTagFormat}";
            string prefix = commandName == "Create" ? Config._napGodCommandPrefixes["NapGod Help Command Prefix"] : commandName == "CND" ? Config._adminCommandPrefix : Config._commandPrefix;
            string msg = Bot.updateMsg(message.ToString().Trim().Remove(0, $"{prefix}{(Bot._testBot && message.Content.StartsWith(prefix) ? prefix : "")}{commandName}".Length), !delete, delete).Trim().Replace("Grey", "Gray").Replace("grey", "gray");

            if (commandName == "CND")
			{
                string[] lines = msg.Split('\n');

                if (lines.Length == 1)
                {
                    if (msg.Contains(';') && msg.ToLower().Contains("red"))
                        msg += "\nRed = Sleep";
                    else msg += "; 1; Red\nRed = Sleep";
                }
                else
                {
                    foreach (string line in lines)
                    {
                        if (line.ToLower().Contains("red") && !msg.Contains("Red = ")) msg += "\nRed = Sleep";
                        else if (line.ToLower().Contains("purple") && !msg.Contains("Purple = ")) msg += "\nPurple = Dark Period";
                        else if (line.ToLower().Contains("yellow") && !msg.Contains("Yellow = ")) msg += "\nYellow = Can Eat";
                        else if (line.ToLower().Contains("blue") && !msg.Contains("Blue = ")) msg += "\nBlue = Sleep Range";
                        else if (line.ToLower().Contains("green") && !msg.Contains("Green = ")) msg += "\nGreen = Availability";
                        else if (line.ToLower().Contains("pink") && !msg.Contains("Pink = ")) msg += "\nPink = Exercise";
                        else if (line.ToLower().Contains("gray") && !msg.Contains("Gray = ")) msg += "\nGray = Work";
                    }
                }
			}

            string[] entries = msg.Trim().Split('\n');
            string name = $"Custom Napchart";
            List<Objective> objectives = new();
            List<ColorTag> colorTags = new();
            ScheduleFinder sf = new(Bot.getServer(message.Channel), ni);
            List<EmbedBuilder> errorEmbedBuilders = new();
            List<(string line, string errorType)> objectiveErrors = new();
            List<(string line, string errorType)> colorTagErrors = new();
            string lineErrors = "";
            List<(string colorName, string hex)> customColors = new();

            try
            {
                if (entries.Length > 1 || (entries.Length == 1 && !string.IsNullOrWhiteSpace(entries[0])))
                {
                    int objectiveIndex = 0, colorTagIndex = 0;
                    foreach (string entry in entries)
                    {
                        if (entries[0] == entry && entry.Trim().Length > 0 && entry.Trim().Length < 100 && !entry.Contains(';') && !entry.Contains('=') && entries.Length > 1)
                        {
                            name = entries[0].Trim();
                        }
                        else if (entry.Length - entry.Replace(";", "").Length == 2)
                        {
                            string[] parameters = entry.Split(';');
                            if (parameters.Length == 3)
                            {
                                objectiveIndex++;
                                string[] sleepTimes = parameters[0].Trim().Split(','),
                                    sleepTimesTemp = sleepTimes,
                                    sleepText = new string[sleepTimes.Length];
                                foreach (string sleepTime in sleepTimes)
                                {
                                    if (sleepTime.Contains('(') && sleepTime.Contains(')'))
                                    {
                                        sleepText[Array.IndexOf(sleepTimes, sleepTime)] = 
                                            sleepTime.Substring(sleepTime.IndexOf('(') + 1, sleepTime.Length - sleepTime.IndexOf('(') - 2);
                                        sleepTimesTemp[Array.IndexOf(sleepTimes, sleepTime)] = sleepTime[..sleepTime.IndexOf('(')].Trim();
                                    }
                                }

                                sleepTimes = sleepTimesTemp;
                                bool parameterCountCorrect = parameters.Length == 3;
                                bool sleepTimesValid = ScheduleFinder.validateSleepTimes(sleepTimes, true, true);
                                bool validLaneNum = int.TryParse(parameters[1].Trim(), out int laneNum) && laneNum <= 10 && laneNum >= 1;
                                bool validColor = Enum.TryParse(
                                    parameters[2].ToLower().Contains("custom") ? parameters[2].ToLower().Remove(parameters[2].IndexOf("(")).Trim() :
                                    parameters[2].ToLower().Trim(), out Napchart.Color color);
                                string colorHex = color == Napchart.Color.custom ? parameters[2].Replace("custom(", "").Replace(")", "").Trim() : "";

                                if (parameterCountCorrect && sleepTimesValid && validLaneNum && validColor)
                                {
                                    objectives.Add(new Objective(ScheduleFinder.createScheduleFromSleepTimes(sleepTimes, false, sleepBlockText: sleepText), laneNum - 1, color, colorHex));

                                    if ((int)color > 7)
                                    {
                                        string cName = color + (color == Napchart.Color.custom ? $"({colorHex})" : "");
                                        if (!customColors.Exists(c => c.colorName == cName))
                                            customColors.Add((cName, colorHex));
                                    }
                                }
                                else
                                {
                                    List<EmbedFieldBuilder> errors = new();
                                    (string line, string errorType) errorPair = (entries.ToList().IndexOf(entry).ToString(), "");

                                    if (!parameterCountCorrect)
                                    {
                                        string type = "Incorrect Parameters";
                                        errors.Add(new EmbedFieldBuilder
                                        {
                                            Name = $"**{type}:**",
                                            Value = objectiveFormat
                                        });
                                        errorPair = (errorPair.line, errorPair.errorType + type + "\n");
                                    }

                                    if (!sleepTimesValid)
                                    {
                                        string type = "Invalid Sleep Times";
                                        errors.Add(new EmbedFieldBuilder
                                        {
                                            Name = $"**{type}:**",
                                            Value = "Enter blocks of time separated by comas like this with no overlapping sleep times:\n`00:00-07:00, 12:30-14:50, 21:00-21:35`"
                                        });
                                        errorPair = (errorPair.line, errorPair.errorType + type + "\n");
                                    }

                                    if (!validLaneNum)
                                    {
                                        string type = "Invalid Lane Number";
                                        errors.Add(new EmbedFieldBuilder
                                        {
                                            Name = $"**{type}:**",
                                            Value = "Enter the lane that you want this objective to appear on (1-10)."
                                        });
                                        errorPair = (errorPair.line, errorPair.errorType + type + "\n");
                                    }

                                    if (!validColor)
                                    {
                                        string type = "Invalid Color";
                                        errors.Add(new EmbedFieldBuilder
                                        {
                                            Name = $"**{type}:**",
                                            Value = $"Valid color options are:\n[`{Napchart.getColorOptions()}`]."
                                        });
                                        errorPair = (errorPair.line, errorPair.errorType + type + "\n");
                                    }

                                    objectiveErrors.Add(errorPair);
                                    errorEmbedBuilders.Add(new EmbedBuilder
                                    {
                                        Color = Color.LightOrange,
                                        Title = $"{_alert} **{commandName} Error on Line {errorPair.line}** {_alert}",
                                        Description = $"**Error with sleep blocks {objectiveIndex}:** [{entry}]\n\n",
                                        Fields = errors
                                    });
                                }
                            }
                            else if (parameters.Length == 1)
                            {
                                string[] sleepTimes = parameters[0].Split(',');

                                if (ScheduleFinder.validateSleepTimes(sleepTimes))
                                    objectives.Add(new Objective(ScheduleFinder.createScheduleFromSleepTimes(sleepTimes, false), 0, Napchart.Color.red));
                            }
                        }
                        else if (entry.Length - entry.Replace("=", "").Length == 1)
                        {
                            string[] parameters = entry.Split('=');
                            colorTagIndex++;
                            bool parameterCountCorrect = parameters.Length == 2;
                            bool validColor = Enum.TryParse(
                                parameters[0].ToLower().Contains("custom") ? parameters[0].ToLower().Remove(parameters[0].IndexOf("(")).Trim() :
                                parameters[0].ToLower().Trim(), out Napchart.Color color);
                            string colorHex = color == Napchart.Color.custom ? parameters[0].Replace("custom(", "").Replace(")", "").Trim() : "";
                            bool validTagName = parameters[1].Length > 30;

                            if (parameterCountCorrect && validColor)
                            {
                                colorTags.Add(new ColorTag(parameters[1].Trim(), color, colorHex));

                                if ((int)color > 7)
                                {
                                    string cName = color + (color == Napchart.Color.custom ? $"({colorHex})" : "");
                                    if (!customColors.Exists(c => c.colorName == cName))
                                        customColors.Add((cName, colorHex));
                                }
                            }
                            else
                            {
                                List<EmbedFieldBuilder> errors = new();
                                (string line, string errorType) errorPair = (entries.ToList().IndexOf(entry).ToString(), "");

                                if (!parameterCountCorrect)
                                {
                                    string type = "Incorrect Parameters";
                                    errors.Add(new EmbedFieldBuilder
                                    {
                                        Name = $"**{type}:**",
                                        Value = colorTagFormat
                                    });
                                    errorPair = (errorPair.line, errorPair.errorType + type + "\n");
                                }

                                if (!validColor)
                                {
                                    string type = "Invalid Color";
                                    errors.Add(new EmbedFieldBuilder
                                    {
                                        Name = $"**{type}:**",
                                        Value = $"Valid color options are:\n[`{Napchart.getColorOptions()}`]."
                                    });
                                    errorPair = (errorPair.line, errorPair.errorType + type + "\n");
                                }

                                colorTagErrors.Add(errorPair);
                                errorEmbedBuilders.Add(new EmbedBuilder
                                {
                                    Color = Color.LightOrange,
                                    Title = $"{_alert} **{commandName} Error on Line {errorPair.line}** {_alert}",
                                    Description = $"**Error with ColorTag {colorTagIndex}:** [{entry}]\n\n",
                                    Fields = errors
                                });
                            }
                        }
                        else if (entry.Length > 0 && ScheduleFinder.validateSleepTimes(entry.Split(','), true, true))
                        {
                            objectives.Add(new Objective(ScheduleFinder.createScheduleFromSleepTimes(entry.Split(','), false), 0, Napchart.Color.red));
                        }
                        else if (entry.Length > 0)
                        {
                            int line = entries.ToList().IndexOf(entry);
                            lineErrors += $"{line}: [{entry}]\n";

                            errorEmbedBuilders.Add(new EmbedBuilder
                            {
                                Color = Color.LightOrange,
                                Title = $"{_alert} **{commandName} Parameters Error on Line {line}** {_alert}",
                                Description = $"**Issue with:** [{entry}]\n\n{errorMsg}\n\nEnter `{Config._commandPrefix}cnExample` to see an example of how to use this command."
                            });
                        }
                    }

                    if (customColors.Count > 4)
                    {
                        string colorOptions = Napchart.getColorOptions();
                        string customColorList = "";
                        foreach((string colorName, string hex) in customColors) customColorList += colorName + "\n";
                        
                        errorEmbedBuilders.Add(new EmbedBuilder
                        {
                            Color = Color.LightOrange,
                            Title = $"**{commandName} Error**",
                            Description = "Unfortunately the Napchart site only supports 4 non-standard colors per napchart.",
                            Fields = new List<EmbedFieldBuilder>
                            {
                                new EmbedFieldBuilder()
                                {
                                    Name = $"You entered {customColors.Count}:",
                                    Value = customColorList
                                }
                            },
                            Footer = new EmbedFooterBuilder
                            {
                                Text = $"Standard colors: [{colorOptions.Remove(colorOptions.IndexOf(", Black"))}]"
                            }
                        });
                    }
                }
                else
                {
                    errorEmbedBuilders.Add(new EmbedBuilder
                    {
                        Color = Color.LightGrey,
                        Title = $"**{commandName}**",
                        Description = $"{description}\n{errorMsg}\n\n**Color Options:** [`{Napchart.getColorOptions()}`]\n\nEnter `{Config._commandPrefix}cnExample` to see an example of how to use this command."
                    });
                }
            }
            catch
            {
                errorEmbedBuilders.Add(new EmbedBuilder
                {
                    Color = Color.LightOrange,
                    Title = $"{_alert} **{commandName} Error** {_alert}",
                    Description = $"{description}\n{errorMsg}\n\n**Color Options:** [`{Napchart.getColorOptions()}`]\n\nEnter `{Config._commandPrefix}cnExample` to see an example of how to use this command."
                });
            }
            
            if(objectives.Count > 0  && errorEmbedBuilders.Count == 0)
            {
                if (customColors.Count > colorTags.Count)
                {
                    foreach((string colorName, string hex) in customColors)
                    {
                        if (!colorTags.Exists(ct => Napchart.getCustomColorName(colorTag: ct) == colorName))
                        {
                            bool custom = colorName.Contains("custom");
                            Napchart.Color color = Napchart.Color.purple;
                            if (Enum.TryParse(colorName, out Napchart.Color c)) color = c;
                            colorTags.Add(new ColorTag("",
                                custom ? Napchart.Color.custom : color,
                                custom ? hex : ""));
                        }
                    }
                }

				Napchart napchart = new(objectives, colorTags, name, $"Created by ScheduleHelper for {Bot.getUsersName(message.Author, Bot.getServer(message.Channel))}.")
				{
					customColors = customColors
				};
				string result = NapchartInterface.createChart(napchart);
                if (Uri.IsWellFormedUriString(napchart.publicLink, UriKind.Absolute))
                {
                    string imgLink = NapchartInterface.getChartImg(napchart.publicLink);

                    if (!Bot.isDMs(message.Channel)) Bot.sendEmbed(message.Channel, Color.Green, imageURL: imgLink, description: napchart.publicLink, author: Bot.getGuildUser(message.Channel.Id, message.Author.Id), requestedByAuthor: true);
                    else Bot.sendEmbed(message.Author, Color.Green, imageURL: napchart.publicLink, description: napchart.publicLink);

                    return;
                }
                else
                {
                    errorEmbedBuilders.Add(new EmbedBuilder
                    {
                        Color = Color.Red,
                        Title = $"**CreateNapchart NapchartAPI Error**",
                        Description = $"`{result}`"
                    });
                }
            }
                
            if (objectiveErrors.Count > 1)
            {
                errorEmbedBuilders.RemoveAll(x => x.Title.Contains("Error on Line") && x.Description.Contains("sleep blocks"));
                List<EmbedFieldBuilder> fields = new();

                foreach((string line, string errorType) in objectiveErrors) 
                {
                    fields.Add(new EmbedFieldBuilder
                    {
                        Name = $"Errors with line {line}",
                        Value = errorType
                    });
                }
                
                errorEmbedBuilders.Add(new EmbedBuilder
                {
                    Color = Color.LightOrange,
                    Title = $"{commandName} Error",
                    Description = $"Error with {objectiveErrors.Count} sleep block lines, containing the following errors:",
                    Fields = fields
                });
            }

            if (colorTagErrors.Count > 1)
            {
                errorEmbedBuilders.RemoveAll(x => x.Title.Contains("Error on Line") && x.Description.Contains("ColorTag"));
                List<EmbedFieldBuilder> fields = new();

                foreach ((string line, string errorType) in colorTagErrors)
                {
                    fields.Add(new EmbedFieldBuilder
                    {
                        Name = $"Errors with line {line}",
                        Value = errorType
                    });
                }

                errorEmbedBuilders.Add(new EmbedBuilder
                {
                    Color = Color.LightOrange,
                    Title = $"{commandName} Error",
                    Description = $"Error with {colorTagErrors.Count} sleep block lines, containing the following errors:",
                    Fields = fields
                });
            }

            if(lineErrors.Length - lineErrors.Replace("\n", "").Length > 1)
            {
                errorEmbedBuilders.RemoveAll(x => x.Title.Contains("Error on Line") && x.Description.Contains("Issue with:"));
                List<EmbedFieldBuilder> fields = new();

                if(lineErrors.Length / 1024 >= 1)
                {
                    string[] lines = lineErrors.Split('\n');
                    string done = "";
                    for(int i = 0; i < (int)Math.Ceiling((double)lineErrors.Length / 1024); i++)
                    {
                        string e = lineErrors;
                        int previousLine = lines.Length - 1;

                        if (done.Length > 0) e = e.Replace(done, "");

                        while (e.Length >= 1024)
                        {
                            if (lines[previousLine].Length > 0)
                            {
                                string toRemove = lines[previousLine] + "\n";
                                e = e.Replace(toRemove, "");
                            }

                            previousLine--;
                        }

                        done += e;

                        fields.Add(new EmbedFieldBuilder
                        {
                            Name = "Errors with the following lines:",
                            Value = e
                        });
                    }
                }
                else
                {
                    fields.Add(new EmbedFieldBuilder
                    {
                        Name = "Errors with the following lines:",
                        Value = lineErrors
                    });
                }

                fields.Add(new EmbedFieldBuilder
                {
                    Name = Config._blank,
                    Value = $"{errorMsg}\n\nEnter `{Config._commandPrefix}cnExample` to see an example of how to use this command."
                });

                errorEmbedBuilders.Add(new EmbedBuilder
                {
                    Color = Color.LightOrange,
                    Title = $"{commandName} Error",
                    Fields = fields
                });
            }

            foreach (EmbedBuilder e in errorEmbedBuilders)
                        message.Channel.SendMessageAsync(embed: e.Build()).Wait();
        }

		/// <summary> Sends an example -createNapchart command </summary>
		public static void cnexample(SocketMessage message) =>
            Bot.send(message, $"{(Bot._testBot ? Config._commandPrefix : "")}-createNapchart 'CreateNapchart' Example\n" +
                "23:00-00:40 (C1), 5:20-7:00 (C2), 12:00-12:30 (N1), 17:30-18:00 (N2); 1; Red\n" +
                "21:00-5:19 (Dark Period); 2; Black\n" +
                "7:00-9:00, 12:30-14:30, 18:00-20:00; 2; Yellow\n" +
                "00:00-24:00; 3; custom(#698238)\n\n" +
                "Red = Sleep\n" +
                "Yellow = Can Eat\n" +
                "custom(#698238) = Think about Pesto");

		#region redirects
		/// <summary> shortened redirect to CreateNapchart </summary>
		public static void cn(SocketMessage message) => CreateNapchart(message, "CN");

		/// <summary> redirect to CreateNapchart that defaults colors to certain descriptions if not provided </summary>
		public static void cnd(SocketMessage message, bool _0, bool delete) => CreateNapchart(message, "CND", delete);

		/// <summary> 1-parameter redirect to createnapchart </summary>
		public static void createnapchart(SocketMessage message) => CreateNapchart(message);
		#endregion
		#endregion
		#endregion

		#region information
		#region help
		/// <summary> send help message | -help [Optional: Command category (show categories if blank)] </summary>
		public void help(SocketMessage message)
        {
            bool consoleWrite = false;
            string commandName = "Help";
            (string commandName, string category, string description, string properFormat) command = _commands.Find(c => c.commandName == commandName);
            string description = command.description;
            string errorMsg = $"**Proper Format:** {command.properFormat}";
            string msg = message.ToString().Trim().Remove(0, $"{_prefix}{commandName}".Length);

            try
            {
                string[] entries = msg.Split('\\');

                if (entries.Length == 1 && !string.IsNullOrWhiteSpace(entries[0].Trim()))
                {
                    bool useCategory = entries[0].ToLower().Trim() != "all";
                    string category = !useCategory ? "" : _commands.Find(c => c.category.ToLower().Contains(entries[0].ToLower().Trim())).category;
					List<(string commandName, string category, string description, string properFormat)> commands = _commands.FindAll(c => !useCategory || (c.category == category));

					if (!useCategory || (useCategory && !string.IsNullOrWhiteSpace(category)))
                        sendHelp(commands, useCategory, category, message);
                    else
                        Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: "Invalid Category.", subtitle: "Please use one of the following categories:", text: getCategoriesStringFormat(this, true));
                }
                else if (entries.Length == 0 || string.IsNullOrWhiteSpace(entries[0].Trim()))
                    Bot.sendEmbed(message.Channel, Color.Blue, title: $"{commandName} Categories", description: getCategoriesStringFormat(this, true));
                else
                    Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description, subtitle: "Incorrect parameters:", text: errorMsg);
            }
            catch
            {
                MM.Print(consoleWrite, $"ERROR: {errorMsg}");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{_alert} **{commandName} Error** {_alert}", description: description + errorMsg);
            }
        }

		public void Help(SocketSlashCommand si)
		{
			bool consoleWrite = false;
			string commandName = "Help";
			(string commandName, string category, string description, string properFormat) command = _commands.Find(c => c.commandName == commandName);
			string description = command.description;
			string errorMsg = $"**Proper Format:** {command.properFormat}";

			try
			{
				string category = si.Data.Options.Count > 0 ? si.Data.Options.First().Value.ToString().Trim() : "";

				if (!string.IsNullOrWhiteSpace(category) && category != "categories")
				{
					bool useCategory = category != "all";
					category = !useCategory ? "" : _commands.Find(c => c.category.ToLower().Contains(category.ToLower().Trim())).category;
					List<(string commandName, string category, string description, string properFormat)> commands = _commands.FindAll(c => !useCategory || (c.category == category));

					if ((!useCategory || (useCategory && !string.IsNullOrWhiteSpace(category))) && commands.Count > 0)
						sendHelp(commands, useCategory, category, interaction: si);
					else si.RespondAsync(embed: new EmbedBuilder()
					{
						Color = Color.LightOrange,
						Title = $"{_alert} **{commandName} Error** {_alert}",
						Description = "Invalid Category.",
						Fields = new()
						{
							new()
							{
								Name = "Please use one of the following categories:",
								Value = getCategoriesStringFormat(this, includeAll: true)
							}
						}
					}.Build(), ephemeral: true);
				}
				else si.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.Blue,
					Title = $"{commandName} Categories",
					Description = getCategoriesStringFormat(this, includeAll: true)
				}.Build());
			}
			catch
			{
				MM.Print(consoleWrite, $"ERROR: {errorMsg}");
				si.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Color.LightOrange,
					Title = $"{_alert} **{commandName} Error** {_alert}",
					Description = description + errorMsg
				}.Build(), ephemeral: true);
			}
		}

		private static void sendHelp(List<(string commandName, string category, string description, string properFormat)> commands, bool useCategory, string category, IMessage message = null, IDiscordInteraction interaction = null)
		{
			List<(List<EmbedFieldBuilder> embeds, string count)> fieldsList = new();
			(List<EmbedFieldBuilder> embeds, string count) fields = (null, "");

			foreach ((string commandName, string category, string description, string properFormat) cmd in commands)
			{
				if ((useCategory && (fields.embeds == null || fields.embeds.Count >= 24) && cmd.category == category) ||
				(!useCategory && (fields.embeds == null || fields.embeds.Count >= 24 || commands.IndexOf(cmd) == 0 || (commands.IndexOf(cmd) > 0 && commands[commands.IndexOf(cmd) - 1].category != cmd.category))))
				{
					if (commands.IndexOf(cmd) > 0 && fields.embeds != null) fieldsList.Add(fields);

					fields = (new List<EmbedFieldBuilder>(), cmd.category);
				}

				if (!useCategory || (useCategory && cmd.category == category))
				{
					fields.embeds.Add(new EmbedFieldBuilder()
					{
						Name = cmd.commandName,
						Value = cmd.description + cmd.properFormat
					});
				}
			}

			fieldsList.Add(fields);
			List<Embed> embeds = new();

			foreach ((List<EmbedFieldBuilder> embeds, string count) entry in fieldsList)
			{
				if (message != null) Bot.sendEmbed(message.Channel, color: Color.Green, fields: entry.embeds,
					title: $"{entry.count} Commands{(fieldsList.Count > 1 ? $" [{(fieldsList.IndexOf(entry) + 1)}/{fieldsList.Count}]" : "")}:");

				else embeds.Add(new EmbedBuilder()
				{
					Color = Color.Green,
					Title = $"{entry.count} Commands{(fieldsList.Count > 1 ? $" [{(fieldsList.IndexOf(entry) + 1)}/{fieldsList.Count}]" : "")}:",
					Fields = entry.embeds
				}.Build());
			}

			if (message == null) interaction.RespondAsync(embeds: embeds.ToArray()).Wait();
		}
		#endregion

		#region userinfo
		public static void userInfo(IDiscordInteraction di)
		{
			IGuild server = Bot.getGuild(di.ChannelId.Value);
			#region user
			(IGuildUser user, bool ephemeral) = di switch
			{
				SocketSlashCommand sc => ((IGuildUser)sc.Data.Options.First().Value, false),
				SocketUserCommand uc => (Bot.getGuildUser(uc.Channel.Id, uc.Data.Member.Id), true),
				_ => (null, true)
			};
			#endregion

			#region icon
			GuildEmote icon = user.IsBot ?
				Bot.getEmote(Config._emojiNames["Bot"])   : user.Hierarchy == int.MaxValue ?
				Bot.getEmote(Config._emojiNames["Owner"]) : user.GuildPermissions.ManageGuild ?
				Bot.getEmote(Config._emojiNames["Mod"])   :
				Bot.getEmote(Config._emojiNames["Member"]);
			#endregion

			#region status
			(string status, string statusName) = user.Status switch
			{
				UserStatus.Offline or UserStatus.Invisible => ($"{Bot.getEmote(Config._emojiNames["Offline"])}", "Offline"),
				UserStatus.Idle or UserStatus.AFK => ($"{Bot.getEmote(Config._emojiNames["Away"])}", "Away"),
				UserStatus.DoNotDisturb => ($"{Bot.getEmote(Config._emojiNames["DND"])}", "Do Not Disturb"),
				UserStatus.Online => ($"{Bot.getEmote(Config._emojiNames["Online"])}", "Online"),
				_ => ($"{Bot.getEmote(Config._emojiNames["Loading"])}", "Status Unknown")
			};
			#endregion

			#region roles
			string roles = "", roles2 = "";
			int index = 0;
			IOrderedEnumerable<SocketRole> roleList = Bot._client.GetGuild(server.Id).GetUser(user.Id).Roles.Where(r => !r.IsEveryone).OrderByDescending(r => r.Position);

			foreach (SocketRole role in roleList)
			{
				string roleText = $"{role.Mention}\n";
				if (index <= ((roleList.Count() - 1) / 2)) roles += roleText;
				else roles2 += roleText;

				index++;
			}
			#endregion

			#region fields
			List<EmbedFieldBuilder> fields = new();

			fields.AddRange(new List<EmbedFieldBuilder>
			{
				new()
				{
					Name = $"{Bot.getEmote(Config._emojiNames["Discord"])} Member Since:",
					Value = $"{user.CreatedAt.ToUniversalTime().UtcDateTime}"
				}, //Discord join date

			    new()
				{
					Name = $"{((Config._napGodServerID == server.Id || (Bot._testBot && Config._testingServers.Exists(id => id == server.Id))) ? Bot.getEmote(Config._emojiNames["NapGod"]) : "Server")} Member Since:",
					Value = $"{user.JoinedAt.Value.ToUniversalTime().UtcDateTime}"
				}//Server join date
            }); //Discord/Server join date

			if (user.PremiumSince.HasValue) fields.Add(new()
			{
				Name = $"{Bot.getEmote(Config._emojiNames["Nitro"])} Booster Since:",
				Value = $"{user.PremiumSince.Value.ToUniversalTime().UtcDateTime}"
			}); //Nitro booster since

			if (roles.Length > 0) fields.Add(new()
			{
				Name = $"Roles ({user.RoleIds.Count - 1})",
				Value = roles,
				IsInline = true
			}); //Roles 1

			if (roles2.Length > 0) fields.Add(new()
			{
				Name = "Roles (continued)",
				Value = roles2,
				IsInline = true
			}); //Roles 2
			#endregion

			#region components
			ComponentBuilder comp = new();
			comp.WithButton("Show Permissions", $"UserInfo:{user.Id}|{server.Id}{(ephemeral ? "|0" : "")}", ButtonStyle.Primary);
			#endregion

			di.RespondAsync(embed: new EmbedBuilder()
			{
				Color = Bot.getUsersColor(Bot._client.GetGuild(server.Id).GetUser(user.Id)),
				Author = new()
				{
					Name = $"{user.DisplayName}#{user.Discriminator}",
					IconUrl = icon.Url
				},
				Description = $"{user.Mention} {status} ({statusName})",
				Fields = fields,
				ThumbnailUrl = user.GetDisplayAvatarUrl(),
				Footer = new() { Text = $"ID: {user.Id}" }
			}.Build(),
			components: user.GuildPermissions.ToList().Count > 0 ? comp.Build() : null, ephemeral: ephemeral);
		}

		public static void userInfo_sendPermissions(SocketMessageComponent sc)
		{
			string[] ids = sc.Data.CustomId[(sc.Data.CustomId.IndexOf(':') + 1)..].Split('|');
			IGuild server; IGuildUser user;
			bool ephemeral = ids.Length > 2;

			if (ulong.TryParse(ids[0], out ulong userID) && ulong.TryParse(ids[1], out ulong serverID) &&
				(server = Bot._client.GetGuild(serverID)) != null && (user = server.GetUserAsync(userID).Result) != null)
			{
				string perms = "", perms2 = "";
				int index = 0;
				List<GuildPermission> permissions = user.GuildPermissions.ToList().Where(p => !ulong.TryParse(p.ToString(), out ulong _)).ToList();

				foreach (GuildPermission perm in permissions)
				{
					string p = $"{perm}\n";
					if (index <= ((permissions.Count - 1) / 2)) perms += p;
					else perms2 += p;

					index++;
				}

				sc.RespondAsync(embed: new EmbedBuilder()
				{
					Color = Bot.getUsersColor(Bot._client.GetGuild(serverID).GetUser(userID)),
					Title = $"Permissions for {user.DisplayName}#{user.Discriminator}",
					ThumbnailUrl = user.GetDisplayAvatarUrl(),
					Author = ephemeral ? null : new()
					{
						IconUrl = sc.User.GetAvatarUrl(),
						Name = $"Requested by: {Bot.getUsersName(sc.User, Bot.getServer(sc.Channel.Id))}"
					},
					Fields = new()
					{
						new()
						{
							Name = "Permissions",
							Value = perms,
							IsInline = true
						},

						new()
						{
							Name = "Permissions (continued)",
							Value = perms2,
							IsInline = true
						}
					}
				}.Build(), ephemeral: ephemeral);
			}
			else AdminCommand.sendEmbed(Color.Red, "Couldn't find requested user.", interaction: sc, ephemeral: ephemeral);
		}
		#endregion
		#endregion

		#region misc
		#region poll
		internal static void retrievePolls(string schemaName)
		{
			if (Bot._polls == null)
			{
				Bot._msm.createPollData(schemaName);

				Bot._polls = new List<IMessage>();

				foreach (string id in Bot._msm.getActivePolls(schemaName))
				{
					string[] ids = id.Split('/');

					if (ulong.TryParse(ids[0], out ulong channelID) && ulong.TryParse(ids[1], out ulong messageID))
					{
						IMessage m = Bot.getMessage(channelID, messageID);

						if (m != null) Bot._polls.Add(m);
					}
				}
			}
		}

		private static bool _alreadySubscribed = false;
		internal async static void Poll(SocketSlashCommand interact)
		{
			IReadOnlyCollection<SocketSlashCommandDataOption> parameters = interact.Data.Options;
            string schemaName = $"`{interact.GuildId.Value}`";

			string title = "poll";
			List<(object emoji, string name)> options = new();

			foreach (SocketSlashCommandDataOption p in parameters)
			{
				string name = p.Name;
				string value = p.Value.ToString().Trim();

				if (name == "title") title = value;
				else if (name.Contains("option"))
				{
					object e = null;
					string option;

					if ((value.StartsWith(":") || new Emoji(value[0].ToString()) != null) && value.Contains(' '))
					{
						string emoji;

						emoji = value[..value.IndexOf(" ")];
						emoji = emoji.Replace(",", "").Trim();

						if ((e = new Emoji(emoji)) == null)
							e = Bot.getEmote(emoji.Replace(":", ""));

						option = value[value.IndexOf(" ")..].Trim();
					}
					else option = value.Trim();

					if (e == null)
					{
						string emojiName = "";
						List<SocketSlashCommandDataOption> optionList = parameters.ToList().FindAll(_ => _.Name.Contains("option"));

						bool yes = false, no = false, maybe = false;
						if (optionList.Count is 2 or 3)
						{
							foreach (SocketSlashCommandDataOption o in optionList)
							{
								string val = o.Value.ToString();

								if (val.Contains(' ')) val = val.Split(' ').Last().Trim();

								if (val.Length > 0)
								{
									if (val.ToLower() == "yes") yes = true;
									else if (val.ToLower() == "no") no = true;
									else if (val.ToLower() is "maybe" or "unsure" or "idk") maybe = true;
								}
							}
						}

						if (yes || no || maybe)
						{
							switch (option.ToLower())
							{
								case "yes": emojiName = $"{Bot.getEmote(Config._emojiNames["Yes"])}"; break;
								case "no": emojiName = $"{Bot.getEmote(Config._emojiNames["No"])}"; break;
								case "unsure": emojiName = $"{Bot.getEmote(Config._emojiNames["Empty"])}"; break;
								case "maybe": emojiName = "❔"; break; //grey
								case "idk": emojiName = "❓"; break; //red
							}
						}
						else
						{
							string number = name.Replace("option", "");

							switch (number)
							{
								case ("1"): emojiName = "1️⃣"; break;
								case ("2"): emojiName = "2️⃣"; break;
								case ("3"): emojiName = "3️⃣"; break;
								case ("4"): emojiName = "4️⃣"; break;
								case ("5"): emojiName = "5️⃣"; break;
								case ("6"): emojiName = "6️⃣"; break;
								case ("7"): emojiName = "7️⃣"; break;
								case ("8"): emojiName = "8️⃣"; break;
								case ("9"): emojiName = "9️⃣"; break;
							}
						}

						e = new Emoji(emojiName);
					}

					options.Add((e, option));
				}
			}

			List<EmbedFieldBuilder> fields = new();

			foreach ((object emoji, string name) in options)
			{
				fields.Add(new EmbedFieldBuilder()
				{
					Name = $"{emoji}: {name}",
					Value = "No votes."
				});
			}

			await interact.RespondAsync(embed: new EmbedBuilder().WithTitle(title).WithFields(fields).WithColor(Color.Green).Build());

			IMessage message = interact.Channel.GetMessageAsync(interact.GetOriginalResponseAsync().Result.Id).Result;

			retrievePolls(schemaName);
			Bot._polls.Add(message);
			Bot._msm.addPoll($"{message.Channel.Id}/{message.Id}", title, schemaName);

			foreach (object emote in options.Select(o => o.emoji).ToList())
			{
				try { await message.AddReactionAsync(emote as Emoji); }
				catch
				{
					try
					{
						string emoteName = (emote as IEmote).Name[2..];
						if (emoteName[emoteName.IndexOf(':')..].Length > 1)
							await message.AddReactionAsync(Bot.getEmote(emoteName[..emoteName.IndexOf(':')]));
					}
					catch { Console.WriteLine($"Unknown emote {(emote as IEmote).Name}"); }
				}
			}

			if (!_alreadySubscribed)
			{
				Bot._client.ReactionAdded += Bot.reactionAdded;
				Bot._client.ReactionRemoved += Bot.reactionRemoved;
				_alreadySubscribed = true;
			}
		}

		internal static void addressReaction(IMessage message, SocketReaction reaction, bool reactionAdded)
		{
			retrievePolls($"`{Bot.getGuild(message.Channel.Id).Id}`");
			IEmbed embed = message.Embeds.First();
			List<EmbedField> fields = new();

			if (reactionAdded)
			{
				KeyValuePair<IEmote, ReactionMetadata> react = message.Reactions.ToList().Find(r => r.Value.IsMe && r.Key.Name == reaction.Emote.Name && r.Value.ReactionCount > 1);

				if (react.Key != null)
				{
					foreach (EmbedField field in embed.Fields)
					{
						string value = field.Value;

						if (field.Name.Contains(react.Key.Name))
							value = $"{react.Value.ReactionCount - 1}: {new string('✅', react.Value.ReactionCount - 1)}";

						fields.Add(new EmbedFieldBuilder()
						{
							Name = field.Name,
							Value = value
						}.Build());
					}
				}
			}
			else
			{
				KeyValuePair<IEmote, ReactionMetadata> react = message.Reactions.ToList().Find(r => r.Value.IsMe && r.Key.Name == reaction.Emote.Name && r.Value.ReactionCount >= 1);

				if (react.Key != null)
				{
					foreach (EmbedField field in embed.Fields)
					{
						string value = field.Value;

						if (field.Name.Contains(react.Key.Name))
						{
							value = react.Value.ReactionCount - 1 == 0
								? "No votes."
								: $"{react.Value.ReactionCount - 1}: {new string('✅', react.Value.ReactionCount - 1)}";
						}

						fields.Add(new EmbedFieldBuilder()
						{
							Name = field.Name,
							Value = value
						}.Build());
					}
				}
			}

			if (fields.Count > 0) Bot.editEmbed(message, embed, embed.Color, title: embed.Title, fields: fields);
		}

		internal async static void endPoll(SocketMessageCommand interact)
		{
			SocketMessage message = interact.Data.Message;
            string schemaName = $"`{interact.GuildId.Value}`";
			retrievePolls(schemaName);

			if (Bot._polls.Exists(m => m.Id == message.Id))
			{
				List<(string option, int count)> results = new();

				IEmbed embed = message.Embeds.First();

				foreach (EmbedField field in embed.Fields.ToList())
					results.Add((field.Name.Split(':').Last(), field.Value == "No votes." ? 0 : Int32.Parse(field.Value[..field.Value.IndexOf(":")])));

				results = results.OrderByDescending(r => r.count).ToList();
				string description = "";

				foreach ((string option, int count) in results)
					description += $"`{option}`: {count}\n{string.Concat(Enumerable.Repeat("🟩", count))}\n";

				await interact.RespondAsync(embed: new EmbedBuilder().WithColor(Color.Green).WithTitle($"Results for poll: \"{embed.Title}\"").WithDescription(description).Build());

				if (interact.GetOriginalResponseAsync().Result.Embeds.Count > 0)
				{
					IMessage msg = Bot._polls.Find(m => m.Id == message.Id);
					IEmbed embd = Bot.getMessage(msg.Channel.Id, msg.Id).Embeds.First();

					Bot.editEmbed(msg, embd, Color.Default, title: embd.Title + " | [CLOSED]", fields: embd.Fields.ToList());

					Bot._polls.Remove(msg);
					Bot._msm.deactivatePoll($"{message.Channel.Id}/{message.Id}", schemaName);

					if (Bot._polls.Count == 0)
					{
						Bot._client.ReactionAdded -= Bot.reactionAdded;
						Bot._client.ReactionRemoved -= Bot.reactionRemoved;
						_alreadySubscribed = false;
					}
				}
			}
			else await interact.RespondAsync("*This message either does not contain a recognized poll, or the poll has already been ended.*", ephemeral: true);
		}
		#endregion

		#region ban
		/// <summary> -ban @user [optional:, @user2, @user3, etc.] </summary>
		public static void ban(SocketMessage message)
        {
            bool consoleWrite = false;

            List<SocketUser> mentioned = message.MentionedUsers.ToList();
            List<EmbedFieldBuilder> fields = new();

            foreach(SocketUser u in mentioned)
            {
                EmbedFieldBuilder field = new()
				{
                    Name = "Banning:",
                    Value = $"<@{u.Id}> {Bot.getEmote(Config._emojiNames["Loading"])}"
                };
                fields.Add(field);
            }

            GuildEmote alert = Bot.getEmote(Config._emojiNames["Alert"]);
            try
            {
                if(fields.Count > 0) Bot.sendEmbed(message.Channel, Color.Green, fields: fields);
                else Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{alert} **Error** {alert}", subtitle: "**ban:**", text: "*Bans users ||not really lmao||*\nProper format: `-ban @user [optional: @user2, @user3, etc.]`");
            }
            catch
            {
                MM.Print(consoleWrite, "ERROR: Proper format: `-ban @user [optional: @user2, @user3, etc.]`");
                Bot.sendEmbed(message.Channel, Color.LightOrange, title: $"{alert} **Error** {alert}", subtitle: "**ban:**", text: "*Bans users ||not really lmao||*\nProper format: `-ban @user [optional: @user2, @user3, etc.]`");
            }
        }

		/// <summary> -ban @user [optional:, @user2, @user3, etc.] </summary>
		public static void ban(SocketSlashCommand sc)
		{
			bool consoleWrite = false;
            List<SocketUser> mentioned = new();

            foreach (SocketSlashCommandDataOption user in sc.Data.Options)
                mentioned.Add(Bot._client.GetUser((user.Value as IUser).Id));

			List<EmbedFieldBuilder> fields = new();

			foreach (SocketUser u in mentioned)
			{
				EmbedFieldBuilder field = new()
				{
					Name = "Banning:",
					Value = $"<@{u.Id}> {Bot.getEmote(Config._emojiNames["Loading"])}"
				};
				fields.Add(field);
			}

			GuildEmote alert = Bot.getEmote(Config._emojiNames["Alert"]);
			try
			{
				if (fields.Count > 0) AdminCommand.sendEmbed(Color.Green, fields: fields, interaction: sc, ephemeral: false);
                else AdminCommand.sendEmbed(Color.LightOrange, $"{alert} **Error** {alert}", subtitle: "**ban:**", text: "Something went wrong. Please try again!", interaction: sc);
			}
			catch
			{
				MM.Print(consoleWrite, "ERROR: Proper format: `-ban @user [optional: @user2, @user3, etc.]`");
				AdminCommand.sendEmbed(Color.LightOrange, $"{alert} **Error** {alert}", subtitle: "**ban:**", text: "Something went wrong. Please try again!", interaction: sc);
			}
		}

		#endregion
		internal static void sendMeme(SocketMessage message, Server server, bool inDMs)
		{
			if (server.memeIndex == 0 || (server.memeIndex >= server.memes.Count - 1 && !server.exhausted) ||
				server.memeStart.AddMinutes(10) < DateTime.Now || (server.exhaustionTime.AddMinutes(2) < DateTime.Now && server.exhausted)) 
					server.createMemeList(message);

			//Jerk - don't send
			if (Bot._jerks.Exists(u => u == message.Author.Id))
			{
				Bot.send(message, "I only send memes to people who are nice to me :triumph:");
				return;
			}

			//Prepare settings
			if (!server.exhausted)
			{
				server._awaitingThanks = true;
				server._responder = message.Author;
				server._responseChannel = message.Channel.Id;
			}

			//Vars
			bool rickRoll = false;
			string memeLink		= server.memes[server.memeIndex],
				   memeCaption	= server.memeCaptions[Bot._rng.Next(0, server.memeCaptions.Count)],
				   rickRollLink = "<https://tinyurl.com/6ec4p4fj>";

			//Alter based on rr and exhaustion level
			if (server.memeIndex < server.memeExhaustion && rickRoll)
			{
				memeCaption += $"\nEnjoy: {rickRollLink}";
				memeLink = "";
			}
			else if (server.memeIndex == server.memeExhaustion)
			{
				memeCaption = "Okay last meme for now, I need a break :sweat_smile:. Ask for more in a bit." + (rickRoll ? $" For now, enjoy:\n{rickRollLink}" : "");
				server.exhausted = true;
				server.exhaustionTime = DateTime.Now;
			}
			else if (server.memeIndex > server.memeExhaustion)
			{
				server.exhaustedReplies.Add($"You've asked for {server.memeIndex - server.memeExhaustion} memes in {(DateTime.Now - server.exhaustionTime).Minutes} minutes and {(DateTime.Now - server.exhaustionTime).Seconds} seconds since I asked you to give me a break :tired_face:");
				server.exhaustedReplies.Add($"It's only been {(DateTime.Now - server.exhaustionTime).Minutes} minutes and {(DateTime.Now - server.exhaustionTime).Seconds} seconds. Chill.");
				
				memeCaption = server.exhaustedReplies[Bot._rng.Next(0, server.exhaustedReplies.Count)];
				memeLink = "";
				
				server.exhaustedReplies.RemoveAt(server.exhaustedReplies.Count - 1);
				server.exhaustedReplies.RemoveAt(server.exhaustedReplies.Count - 1);

				if (server.memeIndex > server.memeExhaustion + 2 && !Bot._jerks.Exists(u => u == message.Author.Id)) Bot._jerks.Add(message.Author.Id);
			}

			Bot.sendFile(message, memeCaption, memeLink, true, message);

			server.memeIndex++;
		}
		#endregion
	}
}