﻿using System.Collections.Generic;

namespace PolyphasicScheduleFinder_Bot
{
	/// <summary> Representation of blocks of time on a 24h cycle </summary>
	class Schedule
    {
        #region attributes
        public Schedule(string inName, List<SleepBlock> inSleeps, bool rec = false, string inDPS = "", string inDPE = "", string inLink = "", bool consoleWrite = false)
        {
            name = inName;
            sleeps = inSleeps;
            link = inLink == "" ? "https://www.polyphasic.net/schedules/" + name.Replace(" ", "-") + "/" : inLink;

			recommended = rec;
            DPEarliestStart = inDPS;
            DPLatestEnd = inDPE;

            double totalSleepTime = 0;
            foreach (SleepBlock block in sleeps) totalSleepTime += SFAlgorithm.convertDifferenceToDouble("00:00", block.length);
            tst = SFAlgorithm.convertDoubleToTime(totalSleepTime);
            //MM.Print(consoleWrite, $"  {name}{new string(' ', 20 - name.Length)}  |  {tst}  |  {link}");
        }
        
        /// <summary> Name of schedule </summary>
        internal string name;
        /// <summary> Length of schedule (00:00) </summary>
        internal string tst;
        /// <summary> Hyperlink to Polyphasic.net link for this schedule </summary>
        internal string link;
        /// <summary> List of sleepblocks that the user should sleep in for this schedule </summary>
        internal List<SleepBlock> sleeps;
        /// <summary> Is this schedule recommended for average sleepers? (TST >= 5h?) </summary>
        internal bool recommended;
        /// <summary> Earliest time the dark period could start for this schedule </summary>
        internal string DPEarliestStart;
        /// <summary> Latest time the dark period could end for this schedule </summary>
        internal string DPLatestEnd;

        internal static int maxScheduleNameLength = 19;
		#endregion

		#region utilities
		internal static void updateScheduleVars() { if (int.TryParse(Config._scheduleFinderVars["Max Schedule Name Length"], out int maxLength)) maxScheduleNameLength = maxLength; }

		/// <summary> Find TST based on sleep block lengths </summary>
		internal string getTST()
        {
            int sleepTime = 0;

            foreach(SleepBlock s in sleeps)
            {
                sleepTime += SFAlgorithm.convertTimeToInt(SFAlgorithm.getDifferenceTime("00:00", s.length.Trim()));
            }

            return SFAlgorithm.convertDoubleToTime((double)sleepTime / 60);
        }

        internal static Schedule scheduleFromSleepArray(string[] sleepTimes)
        {
            List<SleepBlock> sleepBlocks = new();
            foreach(string sleepTime in sleepTimes)
            {
                string[] startEnd = sleepTime.Split('-');
                sleepBlocks.Add(new(SFAlgorithm.getDifferenceTime(startEnd[0], startEnd[1]), startEnd[0], startEnd[1]));
            }

            return new("", sleepBlocks);
        }
        #endregion
    }

    /// <summary> A block of time in a 24h cycle </summary>
    class SleepBlock
    {
        #region attributes
        public SleepBlock(string inLength, string ST, string ET, string EST = "", string LET = "", string MD = "", SFAlgorithm.SleepBlockType inType = SFAlgorithm.SleepBlockType.Core, string inBlockText = "")
        {
            length = inLength;
            startTime = ST;
            endTime = ET;
            earliestStartTime = EST;
            latestEndTime = LET;
            maxDistanceFromPreviousSleepBlock = MD;
            type = inType;
            blockText = inBlockText;
        }

        /// <summary> Length of sleep block </summary>
        internal string length;
        /// <summary> Start time of sleep block (00:00) </summary>
        internal string startTime;
        /// <summary> End time of sleep block (00:00) </summary>
        internal string endTime;
        /// <summary> Earliest that the sleep block can begin (00:00) </summary>
        internal string earliestStartTime;
        /// <summary> Latest that the sleep block can end (00:00) </summary>
        internal string latestEndTime;
        /// <summary> The amount of time that this sleep block can have between its start time and the end time of the previous sleep block (00:00) </summary>
        internal string maxDistanceFromPreviousSleepBlock;
        /// <summary> Type of sleepblock (Nap/Core) </summary>
        internal SFAlgorithm.SleepBlockType type;
        /// <summary> Text associated with this sleep block </summary>
        internal string blockText;
        #endregion
    }
}
