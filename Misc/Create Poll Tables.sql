USE [schemaName];

CREATE TABLE `polls` (
  `ID` varchar(40) NOT NULL,
  `pollTitle` varchar(100) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;